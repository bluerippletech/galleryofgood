$(function () {

    var dropbox = $('#dropbox'),
        message = $('.message', dropbox);

    dropbox.filedrop({
        // The name of the $_FILES entry:
        paramname:'pic',

        maxfiles:5,
        maxfilesize:5,
        url:'../ajaxUpload/upload2',

        uploadFinished:function (i, file, response) {
            $.data(file)
                .addClass('done')
                .attr("tempid", response.tempid)
                .bind('click', function () {
                    var tempid = $(this).attr("tempid");
                    currentArtworkSelected = tempid;

                    $("#initialmessage").hide();
                    $("ul[name='tags']").tagit("removeAll");
                    $("small").text("");
                    $("#metadatainput").show();

                    $(".selected").removeClass("selected").find('.close').hide();
                    $(this).find(".imageHolder").addClass("selected");
                    $(this).find(".close").show();
                    $('#metaSaved').hide();
                    $("#tempartworkid").val(response.tempid);
                    var artworkMetadata = $.ajax({
                        type:'GET',
                        url:projectWebRoot + "/" + tempid,
                        success:function (data, textStatus) {
                            $("option").removeAttr("selected")
                            var selects=$("#metadata").find("select").next();
                            _.each(selects,function(select){
                                $(select).find(".current").html($(select).find("li").first().html());

                            });


                            _.each(data, function (num, key) {
                                var el = $("[name='" + key + "']");
                                if (el.is("select") && !_.isNull(num)) {
                                    var opt = el.find("option[value='" + num + "']");
                                    opt.attr("selected", true);
                                    el.next().find(".current").text(opt.text());
                                }
                                else
                                    el.val(num);

                                if (key == 'id' && !_.isEmpty(num)) {
                                    $.ajax({type:'GET',
                                        url:getArtworkTags + "/" + num,
                                        success:function (data, textStatus) {
                                            _.each(data, function (num) {
                                                $("ul[name='tags']").tagit("createTag",num);

                                            });
                                        }
                                    });
                                }
                            });
                        },
                        error:function (XMLHttpRequest, textStatus, errorThrown) {
//                            alert(textStatus);
                        }
                    });


                });

            $.data(file).find(".close").bind("click", function () {
//                $(this).parent().parent().remove();

                $('#modalConfirm').reveal({
                    animation:'fadeAndPop',
                    animationspeed:300,
                    closeOnBackgroundClick:false,
                    dismissModalClass:'close-reveal'
                });


            });
//                .bind("click", retrieveMetadata(this));
            // response is the JSON object that post_file.php returns
        },

        error:function (err, file) {
            switch (err) {
                case 'BrowserNotSupported':
                    showMessage('Your browser does not support HTML5 file uploads!');
                    break;
                case 'TooManyFiles':
                    alert('Too many files! Please select 5 at most! (configurable)');
                    break;
                case 'FileTooLarge':
                    alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
                    break;
                default:
                    break;
            }
        },

        // Called before each upload is started
        beforeEach:function (file) {
            if (!file.type.match(/^image\//)) {
                alert('Only images are allowed!');

                // Returning false will cause the
                // file to be rejected
                return false;
            }
        },

        uploadStarted:function (i, file, len) {
            createImage(file);
            if (!$("#initialmessage").is(":visible")&& _.isNull(currentArtworkSelected)) {
                $("#initialmessage").show();
            }
        },

        progressUpdated:function (i, file, progress) {
            $.data(file).find('.progress').width(progress);
        }

    });

    var template = '<div class="preview">' +
        '<span class="imageHolder">' +
        '<img />' +
        '<div class="close">x</div>' +
        '<span class="uploaded"></span>' +
        '</span>' +
        '<div class="progressHolder">' +
        '<div class="progress"></div>' +
        '</div>' +
        '</div>';


    function createImage(file) {

        var preview = $(template),
            image = $('img', preview);

        var reader = new FileReader();

        image.width = 100;
        image.height = 100;

        reader.onload = function (e) {

            // e.target.result holds the DataURL which
            // can be used as a source of the image:

            image.attr('src', e.target.result);
        };

        // Reading the file as a DataURL. When finished,
        // this will trigger the onload function above:
        reader.readAsDataURL(file);

        message.hide();
        preview.appendTo($('.mCSB_container'));


        // Associating a preview container
        // with the file, using jQuery's $.data():

        $.data(file, preview);
    }

    function showMessage(msg) {
        message.html(msg);
    }

    function retrieveMetadata(anId) {
        var tempid = $(anId).attr("tempid");
        var artworkMetadata = $.ajax({
            type:'GET',
            url:projectWebRoot + "/" + tempid,
            success:function (data, textStatus) {
                alert(textStatus);
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
//
//
//        jQuery.ajax({type:'GET',
//            url:'${createLink(controller: 'artwork', action: 'renderNewlyUploaded')}?tempUrl='+url+"&fileTitle="+fileTitle+"&title="+title+"&count="+newImageCount,
//            success:function(data, textStatus) {
//                $(au_yourUploaderId._getItemByFileId(newImageCount-1)).html(data);
//                $(".buttons").show();
//            },
//            error:function(XMLHttpRequest, textStatus, errorThrown) {
////		                	$.jGrowl(XMLHttpRequest.responseText);
//            }
//        });


    }

});