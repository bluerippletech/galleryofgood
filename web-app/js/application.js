if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}


function flashWarning(warningMessage){
    $("#warning").children().first().html(warningMessage);
    $("#warning").show().fadeOut(5000);
}