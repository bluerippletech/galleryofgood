package com.galleryofgood.amazon;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: onats
 * Date: 5/28/12
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AwsPropertiesCredentials implements AWSCredentials {
    private String accessKey;
    private String secretAccessKey;

    public AwsPropertiesCredentials(String anAccessKey, String aSecretKey){
        accessKey = anAccessKey;
        secretAccessKey = aSecretKey;
    }


    @Override
    public String getAWSAccessKeyId() {
        return accessKey;
    }

    @Override
    public String getAWSSecretKey() {
        return secretAccessKey;
    }
}
