package com.galleryofgood.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;
import javax.servlet.http.HttpServletRequest;

public class AdminWebAuthenticationDetails extends WebAuthenticationDetails {

    public AdminWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        this.isAdminLogin = "admin".equals(request.getParameter("login_page"));
    }

    public boolean isAdminLogin() {
        return isAdminLogin;
    }

    private boolean isAdminLogin;
}