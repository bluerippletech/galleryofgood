package com.galleryofgood.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.TextEscapeUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: onats
 * Date: 5/16/12
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminAuthenticationFilter extends UsernamePasswordAuthenticationFilter {



    protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
        String adminLogin = request.getParameter("login_page");
        request.getSession().setAttribute("ADMIN_LOGIN", adminLogin);
        WebAuthenticationDetailsSource authenticationDetailsSource = (WebAuthenticationDetailsSource) getAuthenticationDetailsSource();
        authenticationDetailsSource.setClazz(AdminWebAuthenticationDetails.class);
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));

        setAuthenticationSuccessHandler(new AdminSavedRequestAwareAuthenticationSuccessHandler());
    }
}
