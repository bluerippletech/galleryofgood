package com.galleryofgood.security

import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUser
import org.springframework.security.core.GrantedAuthority
import com.galleryofgood.user.UserDetails

/**
 * Created by IntelliJ IDEA.
 * User: onats
 * Date: 1/31/12
 * Time: 10:09 PM
 * To change this template use File | Settings | File Templates.
 */
class MyUserDetails extends GrailsUser{
    final String ageGroup
    final String optin
    final String hasLoggedIn
    final String website
    final Date lastLoginDate

    final UserDetails userDetails


    MyUserDetails (String username, String password, boolean enabled,
    boolean accountNonExpired, boolean credentialsNonExpired,
    boolean accountNonLocked,
            Collection<GrantedAuthority> authorities,
    String id, String ageGroup,String optin,String hasLoggedIn,UserDetails userDetails,String website,Date lastLoginDate){
        super(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities, id)
        this.ageGroup=ageGroup
        this.optin=optin
        this.hasLoggedIn=hasLoggedIn
        this.userDetails=userDetails
        this.website=website
        this.lastLoginDate=lastLoginDate

    }
}
