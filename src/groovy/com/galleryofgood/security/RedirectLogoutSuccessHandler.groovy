package com.galleryofgood.security

import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.core.Authentication
import javax.servlet.ServletException

/**
 * Created by IntelliJ IDEA.
 * User: onats
 * Date: 2/1/12
 * Time: 1:08 AM
 * To change this template use File | Settings | File Templates.
 */
class RedirectLogoutSuccessHandler extends AbstractAuthenticationTargetUrlRequestHandler
implements LogoutSuccessHandler {


    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
    throws IOException, ServletException {
        super.handle(request, response, authentication);
    }

}
