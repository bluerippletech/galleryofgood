package com.galleryofgood.artwork

/**
 * Created with IntelliJ IDEA.
 * User: onats
 * Date: 5/25/12
 * Time: 2:45 PM
 * To change this template use File | Settings | File Templates.
 */
class ArtworkUtils {

    static Map ageGroups = ['0-3':0..3,
                            '4-7':4..7,
                            '8-10':8..10,
                            '11-13':11..13,
                            '14-18':14..18]

    static Map yearGroups = ['0-3':(new Date().getAt(Calendar.YEAR).toInteger()-3)..(new Date().getAt(Calendar.YEAR).toInteger()),
                             '4-7':(new Date().getAt(Calendar.YEAR).toInteger()-7)..(new Date().getAt(Calendar.YEAR).toInteger()-4),
                             '8-10':(new Date().getAt(Calendar.YEAR).toInteger()-10)..(new Date().getAt(Calendar.YEAR).toInteger()-8),
                             '11-13':(new Date().getAt(Calendar.YEAR).toInteger()-13)..(new Date().getAt(Calendar.YEAR).toInteger()-11),
                             '14-18':(new Date().getAt(Calendar.YEAR).toInteger()-18)..(new Date().getAt(Calendar.YEAR).toInteger()-14),
                            ]
}
