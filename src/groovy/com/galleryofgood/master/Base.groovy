package com.galleryofgood.master


abstract class Base {

	String id


    Date dateCreated
    Date lastUpdated
//    Date createdDate
//    Date editedDate
//    String editedBy
//    String createdBy

//    static auditable = [ignore:['version','createdDate','editedDate','editedBy','createdBy']]

	static mapping = {
		id generator:'uuid'
		cache true
	}
	
    static constraints = {
//        createdDate(nullable:true)
//        editedDate(nullable:true)
//        editedBy(nullable:true)
//        createdBy(nullable:true)
        dateCreated(nullable:true)
        lastUpdated(nullable:true)

    }
}
