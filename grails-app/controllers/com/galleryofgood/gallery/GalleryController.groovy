package com.galleryofgood.gallery

import org.springframework.dao.DataIntegrityViolationException

import grails.converters.JSON
import com.galleryofgood.common.UserFollow
import com.galleryofgood.security.AppUser

class GalleryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", saveNewGallery: "POST"]

    def springSecurityService, artworkService, galleryService, notificationService

    def index() {
        def user = springSecurityService.getCurrentUser()?.username

        def gallery = Gallery.findByOwner(user)
        if (gallery) {
            redirect(action: 'edit', id: gallery?.id)
            //TODO: show edit gallery
        } else {
            //TODO:create new gallery
            render view: 'create', model: [ageGroup: springSecurityService.getPrincipal().ageGroup, activeLink: "Artworks"]
        }


    }

    def saveNewGallery() {
        def galleryInstance = new Gallery(params)
        def isYoungArtistGallery = "YoungArtist".equals(galleryInstance?.galleryOwner)

        galleryInstance.owner = springSecurityService.getPrincipal().username
        if (Gallery?.findAllByOwner(galleryInstance.owner)?.size() < 1) {
            galleryInstance.galleryName = galleryInstance.galleryName.replaceAll("^\\s+", "").replaceAll("\\s+\$", "");
            galleryInstance?.galleryNameUrl = galleryInstance?.galleryName.replaceAll(" ", "").replaceAll("[^A-Za-z0-9_]", "").toLowerCase()

            def youngArtists = galleryInstance?.youngArtists.findAll {it.nickname.trim() == ""}
            if (youngArtists)
                youngArtists.each {galleryInstance.removeFromYoungArtists(it)};

            if (isYoungArtistGallery) {
                galleryInstance?.youngArtists.each {
                    galleryInstance?.addToTimelines(new Timeline(artistName: it.nickname))
                }
            } else {
                galleryInstance.addToTimelines(new Timeline(artistName: galleryInstance?.owner))
            }
            galleryInstance.validate()
            if (!galleryInstance?.hasErrors() && galleryInstance.save(flush: true)) {
                //flash.message = message(code: 'gallery.created.message', args: [message(code: 'gallery.label', default: 'Gallery'), galleryInstance.id])
                flash.successMessage = message(code: 'gallery.created.message', args: [message(code: 'gallery.label', default: 'Gallery'), galleryInstance.id])
                redirect(controller: 'artwork', action: "index", id: galleryInstance.id)
            } else {
                println galleryInstance.errors
                render(view: "create", model: [galleryInstance: galleryInstance, ageGroup: springSecurityService.getPrincipal().ageGroup, activeLink: "Artworks"])
            }
        }
        else {
            flash.message = "You already have a gallery."
            redirect(controller: 'artwork', action: "index")
        }
    }

    def uploadArtwork() {
        render(view: "uploadArtwork", model: [activeLink: "Artworks"])
    }


    def save() {
        def galleryInstance = new Gallery(params)
        if (!galleryInstance.save(flush: true)) {
            render(view: "create", model: [galleryInstance: galleryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'gallery.label', default: 'Gallery'), galleryInstance.id])
        redirect(action: "show", id: galleryInstance.id)
    }

    def show() {
        def galleryInstance = Gallery.get(params.id)
        if (!galleryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "list")
            return
        }

        [galleryInstance: galleryInstance]
    }

    def edit() {
        def galleryInstance = Gallery.get(params.id)
        if (!galleryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "create")
            return
        }

        [galleryInstance: galleryInstance, ageGroup: springSecurityService.getPrincipal().ageGroup, activeLink: "Artworks"]
    }

    def update() {
        def galleryInstance = Gallery.get(params.id)
        if (!galleryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (galleryInstance.version > version) {
                galleryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'gallery.label', default: 'Gallery')] as Object[],
                        "Another user has updated this Gallery while you were editing")
                render(view: "edit", model: [galleryInstance: galleryInstance, activeLink: "Artworks"])
                return
            }
        }
        galleryInstance.properties = params
        List aList = galleryInstance?.youngArtists
        def indexesToRemove = []
        for (int i = 0; i < aList.size(); i++) {
            def anArtist = aList.get(i);
            if (anArtist == null)
                indexesToRemove<<i;
            else {
                if ("".equals(anArtist.id)&&"".equals(anArtist.nickname.trim()))
                    aList.putAt(i,null);
            }
        }
        aList.removeAll(Collections.singletonList(null));
        galleryInstance?.youngArtists = aList


        def youngArtists = galleryInstance?.youngArtists.findAll {"".equals(it?.nickname?.trim())}
        if (youngArtists) {
            youngArtists.each {
                it.refresh();
                galleryInstance?.removeFromYoungArtists(it)
            }
        }

        if (!galleryInstance.save()) {

            galleryInstance.errors.allErrors.each {
                println it
            }
            //flash.message="You've entered an invalid information.<br/>Please check and try again"
            render(view: "edit", model: [galleryInstance: galleryInstance,activeLink:"Artworks"])
            return
        }else{
            aList = galleryInstance?.youngArtists
            indexesToRemove = []
            for (int i = 0; i < aList.size(); i++) {
                def anArtist = aList.get(i);
                if (anArtist == null)
                    indexesToRemove<<i;
                else {
                    if ("".equals(anArtist.id)&&"".equals(anArtist.nickname.trim()))
                        aList.putAt(i,null);
                }
            }
            aList.removeAll(Collections.singletonList(null));
            galleryInstance?.youngArtists = aList
            galleryInstance?.save()
        }

        flash.message = "${galleryInstance?.galleryName} settings have been updated."
        redirect(action: "edit", id: galleryInstance.id)
    }


    def delete() {
        def galleryInstance = Gallery.get(params.id)
        if (!galleryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "list")
            return
        }

        try {
            galleryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def guide() {
        def gallery = Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)
        render view: 'galleryGuide', model: [activeLink: 'guide', gallery: gallery]
    }

    def timeline() {
        def user = springSecurityService.getCurrentUser()?.username

        def gallery = Gallery.findByOwner(user)
        render view: 'timeline', model: [activeLink: 'timeline', gallery: gallery]
    }

    def updateArtisticJourney() {
        def galleryInstance = Gallery.get(params.id)
        def artisticJourneys = params.artisticJourney

        if (galleryInstance?.galleryOwner == "YoungArtist") {
            artisticJourneys.each {
                def timeline = Timeline.findByArtistName(it.key)
                timeline.artisticJourney = it.value
                timeline.save()
            }
        } else {
            def timeline = Timeline.findByArtistName(galleryInstance.owner)
            timeline.artisticJourney = artisticJourneys
            timeline.save()
        }
        if (!galleryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'gallery.label', default: 'Gallery'), params.id])
            redirect(action: "index")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (galleryInstance.version > version) {
                galleryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'gallery.label', default: 'Gallery')] as Object[],
                        "Another user has updated this Gallery while you were editing")
                render(view: "edit", model: [galleryInstance: galleryInstance, activeLink: "Artworks"])
                return
            }
        }
        galleryInstance.properties = params
        if (!galleryInstance.save(flush: true)) {
            render(view: "timeline", model: [gallery: galleryInstance, activeLink: "timeline"])
            return
        }

        flash.message = "Timelines updated"
        redirect(action: "timeline", id: galleryInstance.id)
    }

    def showGallery() {
        //todo: use filtering, all, by artist, and by artwork type, and by pagination.
        def gallery = Gallery.findByGalleryNameUrl(params.galleryName)
        if (gallery) {
            def ageGroups = galleryService.getArtistsAgeGroupsInGallery(gallery) //galleryService.getAgeGroups(gallery)
            def artworks = gallery?.artworks
            def artist = params.artist
            def artworkType = params.artworkType
            def tag = params.tag

            if (gallery?.galleryOwner?.equals("YoungArtist")) {

            } else {
                artist = springSecurityService.getCurrentUser()?.username
            }



            def tags = artworkService.getArtworksTagCount(artworks)
            artworks = artworkService.findApprovedArtworks(gallery, artist, artworkType, tag)

            render(view: 'galleryview',
                    model: [gallery: gallery,
                            artworks: artworks,
                            tags: tags,
                            website: springSecurityService.getCurrentUser()?.website,
                            loggedUser: springSecurityService.getCurrentUser(),
                            activeArtist: artist,
                            activeArtworkType: artworkType,
                            ageGroups: ageGroups
                    ])
        } else {
            redirect(controller: 'homepage', action: 'index')
        }
    }
    //initial loading
    def itemsList() {
        def artworksInstance;
        def sd = params;
        def galleryInstance = Gallery.get(params.id);
        def criteria
        if (params?.criteria instanceof String) {
            criteria = params?.criteria;
        } else {
            criteria = params?.list('criteria');
        }
        def status = "Approved"
        def artist = params?.artist
        if (criteria instanceof String) {
            if (criteria == "All Types") {
                if (artist == "All")
                    artworksInstance = Artwork.findAllByArtworkTypeInListAndGalleryAndStatus(Artwork.constraints.artworkType.inList, galleryInstance, status)
                else
                    artworksInstance = Artwork.findAllByArtworkTypeInListAndArtistAndStatus(Artwork.constraints.artworkType.inList, artist, status)
            } else {
                if (artist == "All")
                    artworksInstance = Artwork.findAllByArtworkTypeAndGalleryAndStatus(criteria, galleryInstance, status);
                else
                    artworksInstance = Artwork.findAllByArtworkTypeAndArtistAndStatus(criteria, artist, status);
            }
        } else if (criteria) {
            if (artist == "All")
                artworksInstance = Artwork.findAllByArtworkTypeInListAndGalleryAndStatus(criteria?.toList(), galleryInstance, status);
            else
                artworksInstance = Artwork.findAllByArtworkTypeInListAndArtistAndStatus(criteria?.toList(), artist, status);
        }
        render(template: "artworksLoad", model: [modelInstance: artworksInstance, criteria: criteria]);
    }
    //paginate items
    def itemsPagination = {
        def artworksInstance;
        def galleryInstance = Gallery.findByGalleryName(params.galleryname);
        def criteria
        if (params?.criteria instanceof String) {
            criteria = params?.criteria;
        } else {
            criteria = params?.list('criteria');
        }
        def status = "Approved"
        def artist = params?.artist
        if (criteria instanceof String) {
            if (criteria == "All Types") {
                if (artist == "All")
                    artworksInstance = Artwork.findAllByArtworkTypeInListAndGalleryAndStatus(Artwork.constraints.artworkType.inList, galleryInstance, status, [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order])
                else
                    artworksInstance = Artwork.findAllByArtworkTypeInListAndArtistAndStatus(Artwork.constraints.artworkType.inList, artist, status, [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order])
            } else {
                if (artist == "All")
                    artworksInstance = Artwork.findAllByArtworkTypeAndGalleryAndStatus(criteria, galleryInstance, status, [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order]);
                else
                    artworksInstance = Artwork.findAllByArtworkTypeAndArtistAndStatus(criteria, artist, status, [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order]);
            }
        } else if (criteria) {
            if (artist == "All")
                artworksInstance = Artwork.findAllByArtworkTypeInListAndGalleryAndStatus(criteria?.toList(), galleryInstance, status, [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order]);
            else
                artworksInstance = Artwork.findAllByArtworkTypeInListAndArtistAndStatus(criteria?.toList(), artist, status, [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order]);
        }
        render(template: "artworksLoadPerPage", model: [modelInstance: artworksInstance]);
    }

    def viewComments = {
        def galleryInstance = Gallery.get(params.id);
        [galleryInstance: galleryInstance];
    }

    def addComment = {

        String message = "Saving Comment....";

        def galleryInstance = Gallery.get(params.id);
        def commentInstance = new com.galleryofgood.common.Comment(username: springSecurityService.getCurrentUser()?.username, content: params.content, location: "Gallery", locationId: galleryInstance.id);
        notificationService.addNotification(AppUser.findByUsername(galleryInstance.owner), "Hi, <br><br> ${springSecurityService.getCurrentUser().username} has commented on your gallery. Thank you. <br><br>Gallery Of Good", new Date(), params.id, "Gallery", "comment");
        //galleryInstance.addToComments(commentInstance);

        if (commentInstance.save())
            message = "Comment Saved.";
        else
            println(commentInstance.errors)
        message = "An error has occured while saving your comment.";

        def response = [username: springSecurityService.getCurrentUser()?.username, message: message];

        render response as JSON;
    }

    def follow(){

        def galleryInstance
        if (params?.galleryName){
            galleryInstance = Gallery.findByGalleryName(params?.galleryName);
        }else{
            galleryInstance = Gallery.get(params?.id);
        }

        def response = [status: "failed", message: "No gallery"]
        if (galleryInstance) {
            def galleryFollower = Gallery.findByOwner(springSecurityService.getCurrentUser()?.username);
            if (!UserFollow.findByFollowerIdAndFollowingId(springSecurityService.getCurrentUser()?.id, galleryInstance?.id)) {
                def message
                if (UserFollow.findByFollowerIdAndFollowingId(AppUser.findByUsername(galleryInstance?.owner)?.id, Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.id))
                    message = "${springSecurityService.getCurrentUser()?.username} of <a href=\'${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}\'>${galleryFollower?.galleryName}</a> gallery is following your gallery. Copy and paste this URL if the link does not work: <a href='${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}\'>${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}</a>"
                else
                    message = "${springSecurityService.getCurrentUser()?.username} of <a href=\'${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}\'>${galleryFollower?.galleryName}</a> gallery is following your gallery. Visit <a href=\'${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}\'>${galleryFollower?.galleryName}</a> and follow back. Copy and paste this URL if the link does not work: <a href='${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}\'>${createLink(uri: '/', absolute: true) + galleryFollower?.galleryNameUrl}</a>"
                notificationService.addNotification(AppUser.findByUsername(galleryInstance?.owner), g.render(template: '/common/followEmail', model: [notiOwner: galleryInstance.owner, message: message]).toString(), new Date(), "${galleryInstance?.id}", "Gallery", "follow");
                if (new UserFollow(followerId: springSecurityService.getCurrentUser()?.id, followingId: galleryInstance?.id).save(flush: true))
                    response = [status: "followed", message: "Unfollow"];
                else
                    response = [status: "failed", message: "An error has occurred"];
            } else
                response = [status: "failed", message: "You are already following this gallery. No need to follow."];
        }

        if (params?.galleryName){
            if (params?.artworkId)
                redirect(controller: 'artwork', action: 'artworkview', id: params?.artworkId)
            else
                redirect(action: 'showGallery', params: ['galleryName':params?.galleryName])
        }else
            render response as JSON;

    }

    def unfollow(){
        def response = [status: "failed", message: "No gallery"]
        if (params.id) {
            def followInstance = UserFollow.findByFollowerIdAndFollowingId(springSecurityService.getCurrentUser()?.id, params.id)
            if (followInstance) {
                try {
                    followInstance.delete(flush: true);
                    response = [status: "unfollowed", message: "Follow Gallery"];
                } catch (Exception e) {
                    response = [status: "failed", message: "An error has occurred"];
                }
            } else
                response = [status: "failed", message: "You are not following this gallery. No need to unfollow."];
        }
        render response as JSON;
    }

    def fbLiking = {

    }
}
