package com.galleryofgood.gallery

import grails.converters.JSON
import org.grails.taggable.Tag
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.auth.PropertiesCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.galleryofgood.amazon.AwsPropertiesCredentials
import com.galleryofgood.common.Homepage
import com.galleryofgood.security.AppUser
import com.galleryofgood.user.UserDetails

class ArtworkController {

    def springSecurityService, artworkService, galleryService, amazonImageService, notificationService

    private static AmazonS3 s3
    private static AwsPropertiesCredentials awsCreds

    def beforeInterceptor = {
        if (!s3) {
            def secretKey = grailsApplication.config.aws.accessKey
            def accessKey = grailsApplication.config.aws.secretKey
            awsCreds = new AwsPropertiesCredentials(secretKey, accessKey)
            s3 = new AmazonS3Client(awsCreds);
        }
    }

    def index() {
        def gallery = Gallery.findByOwner(springSecurityService.getPrincipal().username)
        if (gallery)
            if (gallery.getArtworks().size() > 0)
                render view: 'artworkListing', model: [activeLink: "artworks", gallery: gallery]
            else{
                render view: 'upload2', model: [activeLink: "Artworks", gallery: gallery]
            }
        else
            redirect(controller: 'gallery')
    }

    def uploadArtwork() {
        def gallery = Gallery.findByOwner(springSecurityService.getPrincipal().username)
        render view: 'upload2', model: [activeLink: "Artworks", gallery: gallery]
    }

    def uploadArtwork2() {
        def gallery = Gallery.findByOwner(springSecurityService.getPrincipal().username)
        render view: 'upload2', model: [activeLink: "Artworks", gallery: gallery]
    }

    def editArtwork() {
        def artwork = Artwork.get(params.id)
        if (!artwork)
            redirect action: 'index'
        else
            render view: 'editArtwork', model: [activeLink: "Artworks", artwork: artwork, relevantTags: artworkService.getRelevantTags(artwork?.title)]
    }

    def updateArtwork() {
        def username = springSecurityService.getPrincipal().username
        def artworkInstance = Artwork.read(params.id)
        if (!artworkInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'artwork.label', default: 'Artwork'), params.id])
            redirect(action: "index")
            return
        }

//        if (params.version) {
//            def version = params.version.toLong()
//            if (artworkInstance.version > version) {
//                artworkInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
//                        [message(code: 'artwork.label', default: 'Artwork')] as Object[],
//                        "Another user has updated this Artwork while you were editing")
//                render(view: "editArtwork", model: [artwork: artworkInstance, activeLink: "Artworks"])
//                return
//            }
//        }


        artworkInstance.properties = params
        if (artworkInstance.showInTimeline == 'on') {
            def off = Artwork.findAllByShowInTimelineAndArtworkAgeAndArtist("on", artworkInstance?.artworkAge, artworkInstance?.artist).find {it != artworkInstance}
            off*.setShowInTimeline(null)
            off*.save()
        }
        artworkInstance.tags = params.list('item[tags][]')
//        if (params.w && params.h){
//            if ((params.w as Double) >0 && (params.h as Double) >0) {
//                artworkService.resizeImage(artworkInstance,params.x,params.y,params.x2,params.y2,params.w,params.h,username)
//    //            artworkService.rotateImage(artworkInstance, true);
//
//            }
//        }
//
//        else
//            println "no resizing needed."

        //TODO: remove other artworks in that timeline if there exist same artwork on same age, if checkbox is toggled.
        //TODO: add image cropping and rotation here.
        if (!artworkInstance.save(flush: true)) {
            render(view: "editArtwork", model: [artwork: artworkInstance, activeLink: "Artworks"])
            return
        }
//        flash.message = message(code: 'artwork.updated.message', args: [artworkInstance.title])
        flash.message = "${artworkInstance.title} has been updated."
        redirect(action: "editArtwork", id: artworkInstance.id)

    }

    def cropImage() {
        def paa = params;
        def username = springSecurityService.getPrincipal().username
        def artworkInstance = Artwork.read(params.id)
        if (params.w && params.h) {
            if ((params.w as Double) > 0 && (params.h as Double) > 0) {
                try {
                    artworkService.resizeImage(artworkInstance, params.x, params.y, params.x2, params.y2, params.w, params.h, username)
                    //            artworkService.rotateImage(artworkInstance, true);
                } catch (Exception e) {
                    throw e;
                    response.status = response.SC_INTERNAL_SERVER_ERROR
                    render "Resize failed."
                }

            }
        }
        response.status = response.SC_OK
        render template: 'imageSection', model: [artwork: artworkInstance]
    }

    def rotateImage() {
        def username = springSecurityService.getPrincipal().username
        def artwork = Artwork.get(params.id)
        try {
            if (params.right == 'Y')
                artworkService.rotateImage(artwork, true, username)
            else
                artworkService.rotateImage(artwork, false, username)
        } catch (Exception e) {
            throw e;
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "Rotate failed."

        }
        response.status = response.SC_OK
        render template: 'imageSection', model: [artwork: artwork]
//        render "Rotate Successful."
    }

    def crop() {
        def username = springSecurityService.getPrincipal().username
        def artwork = Artwork.get(params.id)
        try {
            artworkService.resizeImage(artwork, params.x, params.y, params.x2, params.y2, params.w, params.h, username)
        } catch (Exception e) {
            throw e;
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "Rotate failed."

        }
        response.status = response.SC_OK
        render template: 'imageSection', model: [artwork: artwork]
//

    }

    def getArtworkMetadata() {
        println params.id;
        def temporaryArtwork = TemporaryArtwork.get(params.id);
        def artwork = temporaryArtwork.artwork
        if (!artwork) {
            artwork = new Artwork(title: temporaryArtwork.originalFilename
            )
        }
        response.status = response.SC_OK
        render artwork as JSON
    }

    def remoteDeleteTemporary(){
        def tempArtData = TemporaryArtwork.get(params.id)

        if (tempArtData){
            def artwork = tempArtData.artwork
            if (artwork)
                artwork.delete()
            tempArtData.delete()
        }
        response.status = response.SC_OK
        render "deleted"
    }

    def getArtworkTags(){
        def artwork = Artwork.get(params.id)
        response.status = response.SC_OK
        render artwork.tags as JSON
    }

    def saveNewArtworkMetadata() {
        def username = springSecurityService.getPrincipal().username
        def gallery = Gallery.findByOwner(username)
        def tempArtwork = TemporaryArtwork.get(params.tempartworkid)
        def artwork = tempArtwork.artwork

        if (!artwork){
            artwork = new Artwork()
//            artwork = amazonImageService.createImagesFromTemporaryUrl(tempArtwork.artworkUrl,username,artwork)
        }
        artwork.properties = params

        artwork.url = tempArtwork.artworkUrl
        artwork.fileTitle = tempArtwork.originalFilename
        def off = Artwork.findAllByShowInTimelineAndArtworkAgeAndArtist("on", artwork?.artworkAge, artwork?.artist).find {it != artwork}
        off*.setShowInTimeline(null)
        off*.save()
        if ("Myself".equals(gallery?.galleryOwner)){
            artwork.artist=springSecurityService.getPrincipal().username
            artwork.showInTimeline="on";
        }
        gallery.addToArtworks(artwork)

        if (artwork.save()){
            runAsync {
                artwork = amazonImageService.createImagesFromTemporaryUrl(tempArtwork.artworkUrl,username,artwork)
                artwork.save()
            }

            artwork.tags = params.list('item[tags][]')
            tempArtwork.artwork = artwork
            tempArtwork.save()
            response.status = response.SC_OK
            render "success"
        }else{
            response.status = response.SC_INTERNAL_SERVER_ERROR
            def errors = artwork.errors
            render artwork.errors.allErrors.collect {
                [field:it.field,
                message:message(error:it,encodeAs:'HTML')]
            } as JSON
        }

    }



    def renderNewlyUploaded() {
        def gallery = Gallery.findByOwner(springSecurityService.getPrincipal().username)
        def artwork = new Artwork()
        artwork.artworkType = gallery?.artworkType
        artwork.url = params.tempUrl
        artwork.title = params.title
        artwork.fileTitle = params.fileTitle
        render template: 'newlyUploaded', model: [artwork: artwork, newImageCount: params.count, gallery: gallery]
    }

    def renderDetails() {
        def gallery = Gallery.findByOwner(springSecurityService.getPrincipal().username)
        def artwork
        if (params.id) {
            artwork = Artwork.get(params.id)
        } else {
            artwork = new Artwork()
            artwork.artworkType = gallery?.artworkType
            artwork.url = params.tempUrl
            artwork.title = params.title
            artwork.messFactor = "No muss,no fuss"
        }
        render template: 'editArtworkDetails', model: [artwork: artwork, gallery: gallery, formName: params.formName, relevantTags: artworkService.getRelevantTags(artwork?.title)]

    }

    def saveNewArtworkDetails() {
        def username = springSecurityService.getPrincipal().username
        def bucketName = grailsApplication.config.aws.bucketName
        def fullDomain = "https://" + grailsApplication.config.aws.domain + "/"
        def gallery = Gallery.findByOwner(username)

        def artwork
        if (params.id) {
            artwork = Artwork.get(params.id)
        }



        if (artwork) {
            artwork.properties = params
            if ("Myself".equals(gallery?.galleryOwner)) {
                artwork.artist = username
            }

            artwork.tags = params.list('item[tags][]')
            artwork.validate()
            gallery.addToArtworks(artwork)
            if (!artwork.hasErrors() && artwork.save()) {
                response.status = response.SC_OK
                render artwork as JSON
            } else {
                response.status = response.SC_INTERNAL_SERVER_ERROR
                renderErrors(bean: artwork)
            }
        } else {
            artwork = new Artwork(params)
            if ("Myself".equals(gallery?.galleryOwner)) {
                artwork.artist = username
            }
            def oldImageUrl = artwork.url
            def newImageUrl = fullDomain + bucketName + "/userImages/" + username + "/" + artwork.fileTitle


            artwork.url = newImageUrl
            /*
            if (!Artwork.findAllByShowInTimelineAndArtworkAgeAndArtist("on",artwork?.artworkAge,artwork?.artist))
            {
                artwork?.showInTimeline='on'
            }
            */

            artwork.validate()
            gallery.addToArtworks(artwork)
            if (!artwork.hasErrors() && artwork.save(flush: true)) {
                artwork.refresh()
                gallery.refresh()
                def taglist = params.list('item[tags][]')
                if (taglist)
                    artwork.tags = taglist



                def oldImageKey = oldImageUrl?.replaceFirst(fullDomain + bucketName + "/", "")

                def (originalUrl, detailUrl, thumbnailUrl) = amazonImageService.createImagesFromArtwork(oldImageUrl, username, artwork)

                //todo: delete other artworks before saving it to artwork.
                amazonImageService.deleteImage(artwork.url)
                amazonImageService.deleteImage(artwork.artworkDetailImageUrl)
                amazonImageService.deleteImage(artwork.thumbnailUrl)

                artwork.url = originalUrl
                artwork.artworkDetailImageUrl = detailUrl
                artwork.thumbnailUrl = thumbnailUrl
                artwork.save(flush: true)

                response.status = response.SC_OK
                render artwork as JSON
            } else {
                response.status = response.SC_INTERNAL_SERVER_ERROR
                render artwork.errors as JSON
            }
        }
    }

    def updateTimelineImages() {
        def response

        def newMap = params;

        def user = springSecurityService?.getCurrentUser()
        def userDetails = UserDetails?.findByAppUser(user);
        def uploadReminder = userDetails?.uploadReminder
        response = [status: 'ok', reminder: uploadReminder];

        newMap.findAll {it?.key != 'action' && it?.key != 'controller'}?.each {
            def artworkInstance = Artwork.get(it?.key);
            if (!Artwork.findAllByShowInTimelineAndArtworkAgeAndArtist("on", it?.value as Integer, artworkInstance.artist)) {
                artworkInstance.showInTimeline = 'on';
                if (!artworkInstance.save(flush: true)) {
                    artworkInstance.errors.println();
                    response = [status: 'An error has occured while updating your timeline Images.']
                }
            }
        }
        render response as JSON;
    }

    def tags = {
        def taglist = Tag.findAllByApprovedAndNameIlike(true, "${params.term}%")*.name
        render taglist as JSON
    }

    def remoteRemoveArtwork = {
        def artwork = Artwork.get(params.id);
        if (!artwork)
            artwork = new Artwork(url: params.url)
        if (artworkService.deleteArtworkFile(artwork)) {
            response.status = response.SC_OK
            render "Artwork deleted successfully."
        } else {
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "An error was encountered while deleting this artwork."
        }
    }

    def delete() {
        if (params.bulkAction == 'delete') {
            def artworksToDelete = params.list('artwork_id');
            def artworks = Artwork.getAll(artworksToDelete)
            def tempArtworks = TemporaryArtwork.findAllByArtworkInList(artworks)
            if (tempArtworks)
                tempArtworks*.delete()
            if (artworks)
                artworks*.delete()
//            if (artworksToDelete instanceof String[]) {
//                artworksToDelete.each() {
//                    artworkService.deleteArtworkFile(Artwork.get(it));
//                }
//            } else {
//                artworkService.deleteArtworkFile(Artwork.get(artworksToDelete))
//            }
            flash.message = 'Artworks have been deleted.'
            redirect controller: 'artwork', action: 'index', params: ['selectedTab': params?.selectedTab]
        } else {
            redirect controller: 'artwork', action: 'index'

        }

    }

    def artworkview() {
        def artworkInstance = Artwork.get(params.id);
        if (!artworkInstance || artworkInstance?.status != "Approved")
            redirect controller: 'homepage', action: 'index'
        def ageGroup = galleryService.getArtworksAgeGroups(artworkInstance)
        def artworkTags = artworkInstance?.tags
        def tagCloud = artworkService.getCloudForTags(artworkTags)
        [artworkInstance: artworkInstance, tags: tagCloud, activeArtist: springSecurityService.getCurrentUser()?.username];
    }

    def emergingThemes = {
        def tagsInstance = artworkService.getThemesTop(99);
        [modelInstance: tagsInstance];
    }

    def artworkAutocomplete = {

        def artworkList = Artwork.createCriteria().list(max: 10) {
            ilike('title', "%${params.term}%")
        }

        def totalRows = artworkList.totalCount
        def numberOfPages = Math.ceil(totalRows / 10)

        def results = artworkList?.collect {
            [
                    id: it?.id,
                    value: it?.title,
                    label: it?.title,
                    description: it?.description,
                    image: it?.thumbnailUrl
            ]
        }


        render results as JSON

    }


    def landing = {
        def homepageInstance = Homepage?.findAll()?.first();
        def sliderArtworks = []

        for (int i = 1; i < 11; i++) {
            if (homepageInstance?."featuredArtwork${i}") {
                def artworkInstance = Artwork.get(homepageInstance?."featuredArtwork${i}")
                if (artworkInstance != null) {
                    sliderArtworks << artworkInstance
                }
            }
        }
        //construct emerging themes section
        def emergingThemes = artworkService.getEmergingThemesArtworks(3)


        render view: 'artworkLanding', model: ['homepageInstance': homepageInstance, emergingThemes: emergingThemes, sliderArtworks: sliderArtworks]

    }

    def viewNext = {
        def artwork = Artwork.get(params.artwork.id)
        def gallery = artwork?.gallery
        def artworks = gallery?.artworks.findAll {it.status == "Approved"}

        def iterator
        List artworkList = artworks as List
        def currentArtworkIndex = artworkList.indexOf(artwork)
        if (currentArtworkIndex + 1 < artworks.size()) {
            iterator = artworkList.listIterator(currentArtworkIndex)
        } else {
            iterator = artworkList.listIterator()
        }

        while (iterator.hasNext()) {
            def tempArtwork = iterator.next();
            if (tempArtwork && !(tempArtwork.equals(artwork))) {
                artwork = tempArtwork;
                break;
            }
        }

        redirect(controller: 'artwork', action: 'artworkview', id: artwork?.id)

    }

    def viewPrevious = {
        def artwork = Artwork.get(params.artwork.id)
        def gallery = artwork?.gallery
        def artworks = gallery?.artworks.findAll {it.status == "Approved"}
        def iterator

        List artworkList = artworks as List
        def currentArtworkIndex = artworkList.indexOf(artwork)
        if (currentArtworkIndex == 0) {
            iterator = artworkList.listIterator(artworks.size())
        } else {
            iterator = artworkList.listIterator(currentArtworkIndex)
        }


        while (iterator.hasPrevious()) {
            def tempArtwork = iterator.previous();
            if (tempArtwork && !(tempArtwork.equals(artwork))) {
                artwork = tempArtwork;
                break;
            }
        }

        redirect(controller: 'artwork', action: 'artworkview', id: artwork?.id)

    }
    def uploadReminder = {
        def user = springSecurityService.getCurrentUser()
        def userDetails = UserDetails.findByAppUser(user);
        if (params.uploadReminder == 'no') {
            userDetails.uploadReminder = 'no';

        } else {
            userDetails.uploadReminder = 'yes';
        }
        userDetails.save(flush: true);
        def reply = ['status': 'ok'];
        render reply as JSON;
    }


}
