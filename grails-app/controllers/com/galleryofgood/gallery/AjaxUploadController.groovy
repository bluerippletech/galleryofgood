package com.galleryofgood.gallery

import grails.converters.JSON
import javax.servlet.http.HttpServletRequest
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.MultipartFile


class AjaxUploadController {

    def randomService,amazonImageService

    def cancelAll = {
        println params
    }

    def upload = {
        def fileName = ((randomService.nextInteger(999999999))+params.qqfile)
        def artwork = Artwork.findOrCreateWhere(title: params.qqfile, fileTitle: fileName)
        try{
            artwork.url = amazonImageService.uploadTemporaryFile(fileName,selectInputStream(request))
            return render(text: [success: true,
                    imageThumbnail: artwork.url,
                    'artwork_id': artwork.id,
                    'fileTitle':artwork.fileTitle,
                    'title': artwork.title,
                    'artworkTempUrl': artwork.url] as JSON, contentType: 'text/json')
        }catch (Exception e){
            log.error("Failed to upload file.", e)
            return render(text: [success: false] as JSON,error:true, contentType: 'text/json')
        }
    }

    def upload2 = {
        def originalFilename = params?.pic?.fileItem?.name
        def temporaryFile = File.createTempFile("temp","upload")
        try{
            def fileName = ((randomService.nextInteger(999999999))+originalFilename)
            params.pic.transferTo(temporaryFile)
            String url = ""
            url = amazonImageService.uploadNewFile(fileName,temporaryFile)

            def tempArtwork = TemporaryArtwork.findOrSaveWhere(artworkUrl: url,originalFilename: originalFilename)
            response.status=response.SC_OK;
            return render(text: [success: true,
                    'tempid': tempArtwork.id] as JSON, contentType: 'text/json')
        }catch (Exception e){
            log.error("Failed to upload file.", e)
            return render(text: [success: false] as JSON,error:true, contentType: 'text/json')
        }finally {
            temporaryFile.delete()
        }
    }

    private InputStream selectInputStream(HttpServletRequest request) {
        if (request instanceof MultipartHttpServletRequest) {
            MultipartFile uploadedFile = ((MultipartHttpServletRequest) request).getFile('qqfile')
            return uploadedFile.inputStream
        }
        return request.inputStream
    }
}
