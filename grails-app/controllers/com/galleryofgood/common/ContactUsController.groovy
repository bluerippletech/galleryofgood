package com.galleryofgood.common

import com.galleryofgood.security.RegisterController

class ContactUsController {

	def mailService;
	
    def index={ ContactUsCommand command ->
		render(view:"/general/contactUs", model: [command: new ContactUsCommand()] );
    }
	
	def sendEmail={ ContactUsCommand command ->
        command.setSessionValue(session["captcha"] as Integer);
        command.validate();
        if (command.hasErrors()) {
            render(view:"/general/contactUs", model: [command:command]);
        }else{
            String contactMessage = "${message(code: 'contactUs.name', default: 'Name')}: "+ params.name +"<br/> ${message(code: 'contactUs.email', default: 'Email')}: " + params.email + "<br/> ${message(code: 'contactUs.subject', default: 'Subject')}: " + params.subject + "<br/> ${message(code: 'contactUs.message', default: 'Message')}: " + params.message;

            try{
                runAsync{
                    mailService.sendMail {
                        to SystemParameter.findByName("ContactUsEmail").content
                        from grailsApplication.config.security.forgotPassword.emailFrom
                        subject "Contact Us"
                        html contactMessage
                    }
                }
                flash.message=message(code:"contactUs.success");
            }catch (Exception e){
                println(e);
                flash.errors=[message(code: "contactUs.failed")];
            }
            render(view:"/general/contactUs", model: [command:command]);
        }
	}

    def captcha = {
        def random =new Random()
        def randomNumber=random.nextInt(4)
        session["captcha"]=randomNumber
        render session["captcha"]
    }

    static final captchaValidator = {value, command->
        if (command.captcha != command.sessionValue){
            return 'command.captcha.error.invalid'
        }
    }


}


class ContactUsCommand{
	String name;
	String email;
	String subject;
	String message;
    Integer captcha;
    Integer sessionValue;
	
	static constraints={
		name (blank:false);
		email (email:true, blank:false);
		message (blank:false);
        subject (inList: ["General Inquiry", "Tech Support", "Partnerships & Sponsorships", "Volunteer & Job Opportunities"])
        captcha (blank:false, validator: ContactUsController.captchaValidator)
        sessionValue (blank:true, nullable: true)
	}
}