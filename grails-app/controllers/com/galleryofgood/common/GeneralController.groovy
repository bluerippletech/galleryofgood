package com.galleryofgood.common

class GeneralController {



    def galleryGuide(){
        render view:'/gallery/galleryGuide'
    }

    def gettingStarted() {
        render view:'/general/gettingStarted'
    }

    def parentGuide() {
        render view:'/general/parentGuide'
    }

    def privacyPolicy(){
        render view:"/general/privacyPolicy"
    }

    def termsOfService(){
        render view:'/general/termsOfService'
    }

    def twoCents(){
        render view:"/general/twocentscircle"
    }

    def communityGuidelines(){
        render view:'/general/communityGuidelines'
    }

    def aboutUs(){
        def homepageInstance = Homepage.list().first();
        render view: '/general/aboutUs', model:[homepageInstance: homepageInstance], params:[id:homepageInstance.id]
    }

    def galleryFlag(){
        redirect(controller: 'gallery', action: 'showGallery', params: ['galleryName':params.galleryName, 'myModal':'true']);
    }

    def galleryFollow(){
        redirect(controller: 'gallery', action: 'follow', params: ['galleryName':params.galleryName, 'artworkId':params?.artworkId]);
    }

    def artworkFlag(){
        redirect(controller: 'artwork', action: 'artworkview', params: ['id':params.id, 'myModal':'true']);
    }

    def commentRedirect(){
        redirect(controller: params.formerController, action: params.formerAction, params: ['id':params.id], fragment: 'commentsSection');
    }
}
