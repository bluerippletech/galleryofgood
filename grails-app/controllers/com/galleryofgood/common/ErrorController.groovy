package com.galleryofgood.common

import grails.util.GrailsUtil

class ErrorController {

    def errorService
    def springSecurityService

    def index() { }

    def handle() {
        def exception = request.exception
//        if (('development'.equals(GrailsUtil.getEnvironment())||'test'.equals(GrailsUtil.getEnvironment())))
        if (!'development'.equals(GrailsUtil.getEnvironment()))
            errorService.sendMail(exception, springSecurityService.getCurrentUser()?.username?:'Anonymous', request.'javax.servlet.error.request_uri', params)

        render(view: 'error', model: [exception:exception]);
    }
}
