package com.galleryofgood.common

import com.galleryofgood.gallery.Gallery
import com.galleryofgood.gallery.Artwork
import com.galleryofgood.security.AppUser

class FlagController {

    def springSecurityService,flagService,notificationService

    def notifyAdminOfFlag(anEntity,aFlag){
        if (anEntity instanceof Gallery){
            flagService.notifyOfFlaggedGallery(anEntity,aFlag)
        }else if (anEntity instanceof Artwork){
            flagService.notifyOfFlaggedArtwork(anEntity,aFlag)
        }else if (anEntity instanceof Comment){
            flagService.notifyOfFlaggedComment(anEntity,aFlag)
        }
    }

    def flagGallery(){
        def gallery=Gallery.get(params.id)
        def flag=new Flag(comment: params.comment,
                          flaggingUser:springSecurityService.getCurrentUser()?.username,
                          flaggedObject: Gallery.class.getCanonicalName(),
                          flaggedObjectId: gallery?.id,
                          flaggedUser:gallery?.owner,
                          sectionFlagged:gallery?.galleryName)
        runAsync{
            notificationService.addNotification(AppUser.findByUsername(gallery.owner),
                    g.render(template: '/common/flagEmail', model: [notiOwner:gallery.owner,
                            location:'Gallery', locationId:params.id]).toString(), new Date(), params.id ,"Gallery", "flag");
            notificationService.addNotification(AppUser.findByUsername(flag.flaggingUser),
                    g.render(template: '/common/flagFromEmail', model: [notiOwner:flag.flaggingUser,
                            location:'Gallery', locationId:params.id]).toString(), new Date(), params.id ,"Gallery", "flagFrom");

        }

        try{
            gallery.addToFlags(flag)
            response.status=response.SC_OK
            render message(code:'flag.gallery.success')
        }catch (Exception e){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render message(code:'flag.gallery.failure')
        }
        notifyAdminOfFlag(gallery,flag);

    }


    def flagArtwork(){
        def artworkInstance=Artwork.get(params.id)
        def flag=new Flag(comment: params.comment,flaggingUser:springSecurityService.getCurrentUser()?.username,
                flaggedObject: Artwork.class.getCanonicalName(),
                flaggedObjectId: artworkInstance?.id,
                flaggedUser: artworkInstance?.gallery?.owner,
                sectionFlagged: artworkInstance?.title)
        runAsync{
            notificationService.addNotification(AppUser.findByUsername(artworkInstance.gallery.owner), g.render(template: '/common/flagEmail', model: [notiOwner:artworkInstance.gallery.owner, location:'Artwork', locationId:params.id]).toString(), new Date(), params.id ,"Artwork", "flag");
            notificationService.addNotification(AppUser.findByUsername(flag.flaggingUser), g.render(template: '/common/flagFromEmail', model: [notiOwner:flag.flaggingUser, location:'Artwork', locationId:params.id]).toString(), new Date(), params.id ,"Artwork", "flagFrom");
        }

        try{
            artworkInstance.addToFlags(flag)
            response.status=response.SC_OK
            render message(code:'flag.artwork.success')
        }catch (Exception e){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render message(code:'flag.artwork.failure')
        }
        notifyAdminOfFlag(artworkInstance,flag);

    }

    def flagComment(){
        def commentInstance=Comment.get(params.id)
        def flag=new Flag(comment: params.comment,flaggingUser:springSecurityService.getCurrentUser()?.username,
                flaggedObject: Comment.class.getCanonicalName(),
                flaggedObjectId: commentInstance?.id,
                flaggedUser: commentInstance?.username,
                sectionFlagged: commentInstance?.content)
        try{
            commentInstance.addToFlags(flag)
            commentInstance.status="Reported";
            commentInstance?.validate()
            if (commentInstance.save(flush: true)){
                response.status=response.SC_OK
            }else{
                response.status=response.SC_INTERNAL_SERVER_ERROR
            }
            render message(code:'flag.comment.success')
        }catch (Exception e){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render message(code:'flag.comment.failure')
        }
        notifyAdminOfFlag(commentInstance,flag);




    }
}
