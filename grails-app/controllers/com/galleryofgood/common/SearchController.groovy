package com.galleryofgood.common

import com.galleryofgood.gallery.Artwork
import com.galleryofgood.artwork.ArtworkUtils
import com.galleryofgood.security.AppUser
import com.galleryofgood.gallery.YoungArtist

class SearchController {

    def index() { }

    def searchItems ={
        [params: params]
    }

    def findAnArtist = {
        render view:'searchItems',model: ['searchType':'Artists']
    }

    def searchItemsGet ={
        def modelInstance = []
        if (params?.searchType == "Artworks"){
            if (params.searchKey){
                if (params?.criteria instanceof String){
                    if (params?.criteria == "All")
                        modelInstance += Artwork.findAllByTitleLikeAndArtworkTypeInListAndStatus("%${params.searchKey}%", Artwork.constraints.artworkType.inList, 'Approved');
                    else
                        modelInstance += Artwork.findAllByTitleLikeAndArtworkTypeAndStatus("%${params.searchKey}%", params?.criteria, 'Approved');
                }else
                    modelInstance += Artwork.findAllByTitleLikeAndArtworkTypeInListAndStatus("%${params.searchKey}%", params?.list('criteria'), 'Approved');
            }
        }else if (params.searchType == "Artists"){
            def ageGroups=[]
            if (params?.criteriaAge instanceof String){
                ArtworkUtils.yearGroups.get(params.criteriaAge)?.each {
                    ageGroups.add(it?.value.toString());
                }
            }else if (params?.criteriaAge){
                params?.list('criteriaAge').each{
                    ArtworkUtils.yearGroups.get(it)?.each {
                        ageGroups.add(it?.value.toString());
                    }
                }
            }
            if (params.searchKey){
                if (params?.criteriaAge instanceof String)
                    if (params?.criteriaAge == "All"){
                        modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%");
                        modelInstance += YoungArtist.findAllByNicknameLike("%${params.searchKey}%");
                    }
                    else{
                        if (params?.criteriaAge == "Above 13")
                            modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%");
                        else
                            modelInstance += YoungArtist.findAllByNicknameLikeAndYearOfBirthInList("%${params.searchKey}%", ageGroups);
                    }
                else{
                    modelInstance += YoungArtist.findAllByNicknameLikeAndYearOfBirthInList("%${params.searchKey}%", ageGroups);
                    if ("Above 13" in params?.criteriaAge)
                        modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%");
                }
            }
        }else if (params.searchType == "All"){ //if searchType ="All"
            //search for the artworks
            modelInstance += Artwork.findAllByTitleLikeAndStatus("%${params.searchKey}%", 'Approved');
            //search for the artists
            modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%");
            modelInstance += YoungArtist.findAllByNicknameLike("%${params.searchKey}%");
        }
        render(template: "resultsSearchItems", model:[modelInstance: modelInstance, params: params, artworkType:params?.criteria]);
    }

    def searchItemsPerPage ={
        def modelInstance = []
        if (params?.searchType == "Artworks"){
            if (params.searchKey){
                if (params?.criteria instanceof String){
                    if (params?.criteria == "All")
                        modelInstance += Artwork.findAllByTitleLikeAndArtworkTypeInListAndStatus("%${params.searchKey}%", Artwork.constraints.artworkType.inList, 'Approved', [sort:"title"]);
                    else
                        modelInstance += Artwork.findAllByTitleLikeAndArtworkTypeAndStatus("%${params.searchKey}%", params?.criteria, 'Approved', [sort:"title"]);
                }else
                    modelInstance += Artwork.findAllByTitleLikeAndArtworkTypeInListAndStatus("%${params.searchKey}%", params?.list('criteria'), 'Approved', [sort:"title"]);
            }
        }else if (params.searchType == "Artists"){
            def ageGroups=[]
            if (params?.criteriaAge instanceof String){
                ArtworkUtils.yearGroups.get(params.criteriaAge)?.each {
                    ageGroups.add(it?.value.toString());
                }
            }else if (params?.criteriaAge){
                params.list('criteriaAge')?.each{
                    ArtworkUtils.yearGroups.get(it)?.each {
                        ageGroups.add(it?.value.toString());
                    }
                }
            }
            if (params.searchKey){
                if (params?.criteriaAge instanceof String)
                    if (params?.criteriaAge == "All"){
                        modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%", [sort:"username"]);
                        modelInstance += YoungArtist.findAllByNicknameLike("%${params.searchKey}%", [sort:"nickname"]);
                    }
                    else{
                        if (params?.criteriaAge == "Above 13")
                            modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%", [sort:"username"]);
                        else
                            modelInstance += YoungArtist.findAllByNicknameLikeAndYearOfBirthInList("%${params.searchKey}%", ageGroups, [sort:"nickname"]);
                    }
                else{
                    modelInstance += YoungArtist.findAllByNicknameLikeAndYearOfBirthInList("%${params.searchKey}%", ageGroups, [sort:"nickname"]);
                    if ("Above 13" in params?.criteriaAge)
                        modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%", [sort: "username"]);
                }
            }
        }else if (params.searchType == "All"){ //if searchType ="All"
            //search for the artworks
            modelInstance += Artwork.findAllByTitleLikeAndStatus("%${params.searchKey}%",[sort: "title"], 'Approved');
            //search for the artists
            modelInstance += AppUser.findAllByUsernameLike("%${params.searchKey}%", [sort: "username"]);
            modelInstance += YoungArtist.findAllByNicknameLike("%${params.searchKey}%", [sort:"nickname"]);
        }
        // to determine the offset index
        def offset = params.offset.toInteger()
        def offsetTo
            if ((modelInstance.size()-(params.offset.toInteger() + params.max.toInteger() -1))<=0)
                offsetTo = modelInstance.size().toInteger()-1;
            else
                offsetTo = params.offset.toInteger() + params.max.toInteger() -1;
        // offset paging
        render(template: "resultsSearchPerPage", model:[modelInstance: modelInstance[offset..offsetTo], totalCount: modelInstance.size(), searchType:params.searchType, artworkType:params?.criteria]);
    }

    def typeAndAge = {}

    //Render Items for artwork type and age
    def itemsList ={
        println params;
        def artworksInstance

        def ageGroups=[]
        if (params?.criteriaAge instanceof String){
            if (params?.criteriaAge == "All"){
                ArtworkUtils.ageGroups.values().each {
                    it.each {ageGroups.add(it)};
                }
            }else{
                ArtworkUtils.ageGroups.get(params.criteriaAge)?.each {
                    ageGroups.add(it?.value);
                }
            }
        }else if (params?.criteriaAge){
            params?.list('criteriaAge')?.each{
               ArtworkUtils.ageGroups.get(it)?.each {
                   ageGroups.add(it?.value);
               }
            }
        }

        if (params.criteria instanceof String){
            if (params?.criteria == "All"){
                artworksInstance = Artwork.findAllByArtworkTypeInListAndArtworkAgeInListAndStatus(Artwork.constraints.artworkType.inList, ageGroups, 'Approved')
            }else{
                artworksInstance = Artwork.findAllByArtworkTypeAndArtworkAgeInListAndStatus(params?.criteria, ageGroups, 'Approved');
            }
        }else if (params.criteria){
            artworksInstance = Artwork.findAllByArtworkTypeInListAndArtworkAgeInListAndStatus(params?.list('criteria'), ageGroups, 'Approved');
        }

        render(template: "resultsTypeAndAge", model:[artworksInstance:artworksInstance]);
    }

    def itemsPerPage ={
        def artworksInstance

        def ageGroups=[]
        if (params?.criteriaAge instanceof String){
            if (params?.criteriaAge == "All"){
                ArtworkUtils.ageGroups.values().each {
                    it.each {ageGroups.add(it)};
                }
            }else{
                ArtworkUtils.ageGroups.get(params.criteriaAge)?.each {
                    ageGroups.add(it?.value);
                }
            }
        }else if (params?.criteriaAge){
            params?.list('criteriaAge')?.each{
                ArtworkUtils.ageGroups.get(it)?.each {
                    ageGroups.add(it?.value);
                }
            }
        }

        if (params.criteria instanceof String){
            if (params?.criteria == "All"){
                artworksInstance = Artwork.findAllByArtworkTypeInListAndArtworkAgeInListAndStatus(Artwork.constraints.artworkType.inList, ageGroups, 'Approved', [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order])
            }else{
                artworksInstance = Artwork.findAllByArtworkTypeAndArtworkAgeInListAndStatus(params?.criteria, ageGroups, 'Approved', [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order]);
            }
        }else if (params.criteria){
            println params?.list('criteria')
            println ageGroups;
            artworksInstance = Artwork.findAllByArtworkTypeInListAndArtworkAgeInListAndStatus(params?.list('criteria'), ageGroups, 'Approved', [max: params?.max, offset: params?.offset, sort: params?.sort, order: params?.order]);
        }

        render(template: "resultsPerPage", model:[artworksInstance:artworksInstance]);
    }

    def freshPaint ={
        def artworksInstance =  Artwork.findAllByDateCreatedBetweenAndStatus(new Date()-90, new Date(), 'Approved')
        [artworksInstance: artworksInstance];
    }

    def freshPaintPerPage ={
        def artworksInstance =  Artwork.findAllByDateCreatedBetweenAndStatus(new Date()-90, new Date(), 'Approved', params)
        render(template: "resultsPerPage", model:[artworksInstance:artworksInstance]);
    }

    def themeTag ={
        def artworksInstance = Artwork.findAllByTag(params.theme);
        [artworksInstance:artworksInstance.findAll{it.status ==  'Approved'}]
    }

    def themeTagPerPage ={
        def artworksInstance = Artwork.findAllByTag(params.tag, params);
        render(template: "resultsPerPage", model:[artworksInstance:artworksInstance.findAll{it.status ==  'Approved'}]);
    }
}
