package com.galleryofgood.common

import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib
import com.galleryofgood.gallery.Gallery
import com.galleryofgood.gallery.Artwork
import grails.converters.JSON
import com.galleryofgood.security.AppUser
import com.galleryofgood.gallery.Timeline
import com.galleryofgood.user.UserDetails

class CommentController {

    def notificationService,springSecurityService;

    def index() { }


    def viewComment() {
        def commentInstance= Comment.get(params.id)
        def taglib = new ApplicationTagLib()
        def url
        if (commentInstance?.inGallery()){
            def aGallery=Gallery.get(commentInstance?.locationId)
               url= taglib.createLink(controller:aGallery?.galleryNameUrl,absolute: true,fragment: commentInstance?.id)
        }else if (commentInstance?.inArtwork()){
            url= taglib.createLink(controller:'artwork',action:'artworkview',id:commentInstance?.locationId,absolute: true,fragment:commentInstance?.id)

        }else if (commentInstance?.inTimeline()){
            url= taglib.createLink(controller:'timeline',action:'timelineview',id:commentInstance?.locationId,absolute: true,fragment:commentInstance?.id)

        }
        redirect(url: url)
    }

    def addCommentGallery={

        String message = "Saving Comment....";

        def galleryInstance =  Gallery.get(params.id );
        def commentInstance = new com.galleryofgood.common.Comment(username:springSecurityService.getCurrentUser()?.username , content:params.content, location: "Gallery", locationId: galleryInstance.id);

        def messageTemplate = g.render(template: '/common/commentEmail', model: [notiOwner:galleryInstance.owner, content:commentInstance.content, user:commentInstance.username, galleryName:galleryInstance.galleryName, location: 'gallery']);

        notificationService.addNotification(AppUser.findByUsername(galleryInstance.owner), messageTemplate.toString(), new Date(), params.id ,"Gallery", "comment");
        //galleryInstance.addToComments(commentInstance);

        if (commentInstance.save())
            message="Comment Saved.";
        else
            println(commentInstance.errors)
        message = "An error has occured while saving your comment.";

        def galleryCommenterLink = "<a href='${createLink( uri: '/')+Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.galleryNameUrl}'>${Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.galleryName}</a>"
        def imgUrl = UserDetails.findByAppUser(springSecurityService.getCurrentUser())?.avatarLocationUrl?:createLink(uri: '/')+'static/images/eb-3.jpg';

        def response = [username:springSecurityService.getCurrentUser()?.username, message:message, link:galleryCommenterLink, imgUrl:imgUrl];

        render response as JSON;
    }


    def addCommentTimeline={

        String message = "Saving Comment....";

        def timelineInstance =  Timeline.get(params.id );
        def commentInstance = new com.galleryofgood.common.Comment(username:springSecurityService?.getCurrentUser()?.username, content:params.content, location: "Artist Timeline", locationId: timelineInstance.id );

        def messageTemplate = g.render(template: '/common/commentEmail', model: [notiOwner:timelineInstance.gallery.owner, content:commentInstance.content, user:commentInstance.username, location: 'timeline']);

        notificationService.addNotification(AppUser.findByUsername(timelineInstance.gallery.owner), messageTemplate.toString(), new Date(), params.id ,"Timeline", "comment");
        //timelineInstance.addToComments(commentInstance);

        if (commentInstance.save())
            message="Comment Saved.";
        else
            message = "An error has occured while saving your comment.";

        def galleryCommenterLink = "<a href='${createLink( uri: '/')+Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.galleryNameUrl}'>${Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.galleryName}</a>"
        def imgUrl = UserDetails.findByAppUser(springSecurityService.getCurrentUser())?.avatarLocationUrl?:createLink(uri: '/')+'static/images/eb-3.jpg';

        def response = [username:springSecurityService.getCurrentUser()?.username, message:message, link:galleryCommenterLink, imgUrl:imgUrl];

        render response as JSON;
    }

    def addCommentArtwork={

        String message = "Saving Comment....";

        def artworkInstance = Artwork.get(params.id );
        def commentInstance = new com.galleryofgood.common.Comment(username:springSecurityService.getCurrentUser()?.username, content:params.content, location: "Artwork", locationId: artworkInstance.id);

        def messageTemplate = g.render(template: '/common/commentEmail', model: [notiOwner:artworkInstance.gallery.owner, content:commentInstance.content, user:commentInstance.username, location: 'artwork']);

        notificationService.addNotification(AppUser.findByUsername(artworkInstance.gallery.owner), messageTemplate.toString(), new Date(), params.id ,"Artwork", "comment");

        //artworkInstance.addToComments(commentInstance);

        if (commentInstance.save())
            message="Comment Saved.";
        else
            message = "An error has occured while saving your comment.";

        def galleryCommenterLink = "<a href='${createLink( uri: '/')+Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.galleryNameUrl}'>${Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.galleryName}</a>"
        def imgUrl = UserDetails.findByAppUser(springSecurityService.getCurrentUser())?.avatarLocationUrl?:createLink(uri: '/')+'static/images/eb-3.jpg';

        def response = [username:springSecurityService.getCurrentUser()?.username, message:message, link:galleryCommenterLink, imgUrl:imgUrl];

        render response as JSON;
    }

    def addCommentHomepage={

        String message = "Saving Comment....";
        def homepageInstance = Homepage.get(params.id);
        def commentInstance = new com.galleryofgood.common.Comment(username:springSecurityService.getCurrentUser()?.username, content:params.content, location: "Homepage", locationId: homepageInstance.id);

        def messageTemplate = g.render(template: '/common/commentEmail', model: [notiOwner:'admin', content:commentInstance.content, user:commentInstance.username, location: 'homepage']);

        notificationService.addNotification(AppUser.findByUsername('admin'), messageTemplate.toString(), new Date(), params.id ,"Homepage", "comment");

        //artworkInstance.addToComments(commentInstance);

        if (commentInstance.save())
            message="Comment Saved.";
        else
            message = "An error has occured while saving your comment.";

        def galleryCommenterLink = "<a href='${createLink( uri: '/')+Gallery.findByOwner(springSecurityService.getCurrentUser()?.username).galleryNameUrl}'>${Gallery.findByOwner(springSecurityService.getCurrentUser()?.username).galleryName}</a>"
        def imgUrl = UserDetails.findByAppUser(springSecurityService.getCurrentUser()).avatarLocationUrl?:createLink(uri: '/')+'static/images/eb-3.jpg';

        def response = [username:springSecurityService.getCurrentUser()?.username, message:message, link:galleryCommenterLink, imgUrl:imgUrl];

        render response as JSON;
    }

}
