package com.galleryofgood.common

import org.springframework.dao.DataIntegrityViolationException

class HomepageController {

    def amazonImageService,springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {

        def homepageInstance = Homepage.findAll()?.first();
        render(view: '/index', model: [homepage: homepageInstance])
    }

    def redirectHome(){
        def loggedInUser=springSecurityService.getPrincipal()
        if (loggedInUser&&!"anonymousUser"?.equals(loggedInUser)){
            def roles=loggedInUser?.getAuthorities()*.role
            if ("ROLE_ADMIN" in roles){
                redirect controller:'admin',action: 'homepage'
            }else{
                redirect controller: 'homepage',action: 'index'
            }
        }else{
            redirect controller: 'homepage',action: 'index'
        }


    }


    def deleteSliderImage(){
        def imageNumber=params.imageNumber
        def homepageInstance = Homepage.findAll().first()
        def amazonImageUrl=homepageInstance."featureImageUrl${imageNumber}"
        homepageInstance?."featureImageUrl${imageNumber}"=""
        homepageInstance?."featureImageLink${imageNumber}"=""

        //Todo: delete from amazon
        if (!amazonImageService.deleteImage(amazonImageUrl)){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render "An error was encountered while deleting image."
        }else{
            if (homepageInstance?.save()){
                response.status=response.SC_OK
                render "Image deleted successfully."
            }else{
                response.status=response.SC_INTERNAL_SERVER_ERROR
                render "An error was encountered while deleting image."
            }
        }
    }

    def deleteThumbnailImage(){
        def imageNumber=params.imageNumber
        def homepageInstance = Homepage.findAll().first()
        def amazonImageUrl=homepageInstance."featureImageThumbnail${imageNumber}"
        homepageInstance?."featureImageThumbnail${imageNumber}"=""

        //Todo: delete from amazon
        if (!amazonImageService.deleteImage(amazonImageUrl)){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render "An error was encountered while deleting image."
        }else{
            if (homepageInstance?.save()){
                response.status=response.SC_OK
                render "Image deleted successfully."
            }else{
                response.status=response.SC_INTERNAL_SERVER_ERROR
                render "An error was encountered while deleting image."
            }
        }
    }


    def deleteAcclaimImage(){
        def imageNumber=params.imageNumber
        def homepageInstance = Homepage.findAll().first()
        def amazonImageUrl=homepageInstance."imageurl${imageNumber}"
        homepageInstance?."imageurl${imageNumber}"=""

        //Todo: delete from amazon
        if (!amazonImageService.deleteImage(amazonImageUrl)){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render "An error was encountered while deleting image."
        }else{
            if (homepageInstance?.save()){
                response.status=response.SC_OK
                render "Image deleted successfully."
            }else{
                response.status=response.SC_INTERNAL_SERVER_ERROR
                render "An error was encountered while deleting image."
            }
        }
    }



    def deleteRetrospectiveImage(){
        def homepageInstance = Homepage.findAll().first()
        def amazonImageUrl=homepageInstance.retrospectiveImage
        homepageInstance.retrospectiveImage=""
        homepageInstance.retrospectiveUrl=""

        //Todo: delete from amazon
        if (!amazonImageService.deleteImage(amazonImageUrl)){
            response.status=response.SC_INTERNAL_SERVER_ERROR
            render "An error was encountered while deleting image."
        }else{
            if (homepageInstance?.save()){
                response.status=response.SC_OK
                render "Image deleted successfully."
            }else{
                response.status=response.SC_INTERNAL_SERVER_ERROR
                render "An error was encountered while deleting image."
            }
        }
    }

}
