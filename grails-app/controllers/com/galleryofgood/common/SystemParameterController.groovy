package com.galleryofgood.common

class SystemParameterController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def editParameters(){
        render view:'/systemParameters/systemParameters'
    }

    def saveParameters(){
        def parameters = params.name

        parameters.each{
            def aParam = SystemParameter.findOrCreateByName(it.key)
            aParam.setContent(it.value);
            aParam.save()
        }

        flash.message="System Parameters have been saved."

        redirect action:'editParameters'
    }
}
