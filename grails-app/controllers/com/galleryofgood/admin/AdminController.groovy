package com.galleryofgood.admin

import com.galleryofgood.common.Homepage
import grails.converters.JSON
import com.galleryofgood.security.AppUser
import com.galleryofgood.security.Role
import com.galleryofgood.security.AppUserRole

import com.galleryofgood.user.UserDetails

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import com.galleryofgood.gallery.Artwork
import com.galleryofgood.gallery.Gallery
import com.galleryofgood.gallery.Timeline
import com.galleryofgood.common.Comment
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import com.galleryofgood.common.Flag
import org.codehaus.groovy.grails.plugins.springsecurity.ui.RegistrationCode
import groovy.text.SimpleTemplateEngine
import org.grails.taggable.Tag
import com.galleryofgood.gallery.TemporaryArtwork

class AdminController {
    def userCache,amazonImageService,springSecurityService,notificationService,appUserService,exportService,mailService,commentService;


    def index() {
        render view: 'artwork'
    }

    def homepage() {
        def homepageInstance = Homepage.findAll().first();
        render view: 'homepage', model: [homepageInstance: homepageInstance, params: params]
    }

    def artwork() {
        def homepageInstance = Homepage.findAll().first();
        render view: 'artwork', model: [homepageInstance: homepageInstance]
    }

    def twoCC() {
        def homepageInstance = Homepage.findAll().first();
        render view: 'twocc', model: [homepageInstance: homepageInstance]
    }

    def comments() {
        def homepageInstance = Homepage.findAll().first();
        def commentsList = commentService.getAdminComments(springSecurityService.getCurrentUser().username);
        render view: 'aboutUsComments', model: [homepageInstance: homepageInstance, commentsList: commentsList]
    }
    def commentsList(){
        def commentsInstance=  commentService.getAdminComments(springSecurityService.getCurrentUser().username);
        def commentsMapInstance = [];
        commentsMapInstance = commentsInstance.collect {
            [
                    "<input type=\"checkbox\" value=\"${it?.id}\" name=\"myComments\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                    "<div style=\"width: 50px; height: 50px; overflow: hidden;display:inline;\"><img class=\"lazy\" src=\"${resource(dir:'images', file: 'progressImg-test.gif')}\" alt=\"\" data-original=\"${UserDetails.findByAppUser(AppUser.findByUsername(it?.username)).avatarLocationUrl?:resource(dir: 'images', file:'profile03.jpg')}\" width=\"50px\"></div>${it?.username}",
                    "${it?.location=="Homepage"?"About Us":it?.location} ${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId )?.id], fragment: 'commentsSection', Gallery.get(it?.locationId )?.galleryName):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId )?.id], fragment: 'commentsSection', Artwork.get(it?.locationId ).title):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId )?.id], fragment: 'commentsSection', Timeline.get(it?.locationId )?.artistName):''}",
                    "${it?.content}<br />"+
                    (it?.status!='Approved'?"<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"updateStatusLink('${it?.id}', commentList, 'Approve')\">Approve</a>":'<span style="font-size:11px;">Approved</span>')+"&nbsp;\n" +
                    "<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"deleteLink(this, commentList)\">Delete</a>&nbsp;\n",
                    formatDate(format: "dd/MM/yy HH:mm:ss", date: it?.date),
                    it?.status
            ]
        }
        def data = [aaData:commentsMapInstance];
        render data as JSON;
    }

    def deleteComment={
        def response = [message:"Nothing to delete."];
        def commentInstance = Comment.get(params.id );
        try{
            commentInstance.delete(flush: true);
            response = [message:"Deleted"];
        }catch(Exception e){
            println(e);
            response = [message:"An error has occurred."];
        }
        render response as JSON;
    }

    def updateCommentStatus={
        def response = [message:"Nothing to update."];

        def commentInstance = Comment.get(params.id );
        commentInstance.status=params.newStatus=="Approve"?"Approved":params.newStatus=="Publish"?"Published":params.newStatus=="Report"?"Reported":'New!';

        if (commentInstance.save()){
            response = [message:"Updated"];
        }else{
            response = [message:"An error has occurred."];
        }

        render response as JSON;
    }

    def updateArtworkSettings() {
        def homepageInstance = Homepage.get(params.id)
        if (!homepageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'homepage.label', default: 'Homepage'), params.id])
            redirect(action: "index")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (homepageInstance.version > version) {
                homepageInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'homepage.label', default: 'Homepage')] as Object[],
                        "Another user has updated this Homepage while you were editing")
                render(view: "/admin/artwork", model: [homepageInstance: homepageInstance])
                return
            }
        }

        homepageInstance.properties = params
        if (params.retrospectiveFile.getSize()>0) {
            homepageInstance?.retrospectiveImage = amazonImageService.uploadImage(params.retrospectiveFile, "retrospective")
        }
        homepageInstance?.retrospectiveUrl = params.retrospectiveUrl

        if (!homepageInstance.save(flush: true)) {
            render(view: "/admin/artwork", model: [homepageInstance: homepageInstance])
        }

        flash.message = "Artwork settings updated."
        redirect(controller: 'admin', action: "artwork", params: ['selectedTab': params?.selectedTab])

    }

    def updateHomepage() {
        def homepageInstance = Homepage.get(params.id)
        if (!homepageInstance) {
            //TODO: should we create a homepage record?
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'homepage.label', default: 'Homepage'), params.id])
            redirect(action: "index")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (homepageInstance.version > version) {
                homepageInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'homepage.label', default: 'Homepage')] as Object[],
                        "Another user has updated this Homepage while you were editing")
                render(view: "/admin/homepage", model: [homepageInstance: homepageInstance])
                return
            }
        }

        homepageInstance.properties = params
        //TODO: handle file uploads to amazon s3. Use bucketname: galleryofgood, with key starting with 'common'
        if (params.sliderImage1?.getSize() > 0) {
            homepageInstance?.featureImageUrl1 = amazonImageService.uploadImage(params.sliderImage1, "slider1")
        }
        if (params.sliderImage2?.getSize() > 0) {
            homepageInstance?.featureImageUrl2 = amazonImageService.uploadImage(params.sliderImage2, "slider2")
        }
        if (params.sliderImage3?.getSize() > 0) {
            homepageInstance?.featureImageUrl3 = amazonImageService.uploadImage(params.sliderImage3, "slider3")
        }
        if (params.sliderImage4?.getSize() > 0) {
            homepageInstance?.featureImageUrl4 = amazonImageService.uploadImage(params.sliderImage4, "slider4")
        }
        if (params.sliderImage5?.getSize() > 0) {
            homepageInstance?.featureImageUrl5 = amazonImageService.uploadImage(params.sliderImage5, "slider5")
        }


        if (params.acclaimImage1?.getSize() > 0) {
            homepageInstance?.setImageurl1(amazonImageService.uploadImage(params.acclaimImage1, "acclaim1"))
        }

        if (params.acclaimImage2?.getSize() > 0) {
            homepageInstance?.imageurl2 = amazonImageService.uploadImage(params.acclaimImage2, "acclaim2")

        }
        if (params.acclaimImage3?.getSize() > 0) {
            homepageInstance?.imageurl3 = amazonImageService.uploadImage(params.acclaimImage3, "acclaim3")
        }

        if (params.thumbnailImage1?.getSize() > 0) {
            homepageInstance?.featureImageThumbnail1 = amazonImageService.uploadImage(params?.thumbnailImage1, "thumbnail1", false)
        }
        if (params.thumbnailImage2?.getSize() > 0) {
            homepageInstance?.featureImageThumbnail2 = amazonImageService.uploadImage(params?.thumbnailImage2, "thumbnail2", false)
        }
        if (params.thumbnailImage3?.getSize() > 0) {
            homepageInstance?.featureImageThumbnail3 = amazonImageService.uploadImage(params?.thumbnailImage3, "thumbnail3", false)
        }
        if (params.thumbnailImage4?.getSize() > 0) {
            homepageInstance?.featureImageThumbnail4 = amazonImageService.uploadImage(params?.thumbnailImage4, "thumbnail4", false)
        }
        if (params.thumbnailImage5?.getSize() > 0) {
            homepageInstance?.featureImageThumbnail5 = amazonImageService.uploadImage(params?.thumbnailImage5, "thumbnail5", false)
        }


        def noErrors = homepageInstance?.validate()
        if (!homepageInstance?.hasErrors() && homepageInstance.save(flush: true)) {
            flash.message = message(code: 'default.updated.message', args: [message(code: 'homepage.label', default: 'Homepage'), homepageInstance.id])
            redirect(controller: 'admin', action: "homepage", params: params)
        } else {
            render(view: "/admin/homepage", model: [homepageInstance: homepageInstance, params: params])
        }
    }


    def listAdministrators = {
        render(view: 'adminList')
    }

    def listUsers = {
        def newMap = [:]

        def appNewUsers = AppUser.findByDateCreatedBetween(new Date() - 30, new Date())
        newMap.put('allNew', appNewUsers?.count()?:0);
        newMap.put('standardNew', appNewUsers?.list()?.count {it?.getAuthorities()?.contains(Role.findByAuthority('ROLE_USER'))}?:0);
        newMap.put('allAllTime', AppUser?.count());
        newMap.put('standardAllTime', AppUser?.list()?.count {it?.getAuthorities()?.contains(Role.findByAuthority('ROLE_USER'))}?:0);

        def newArtworksList = Artwork.findAllByDateCreatedGreaterThanEquals((new Date()).minus(7), [sort: "dateCreated", order: "desc"])
        def nowCal = Calendar.instance
        nowCal.set(date: 1)
        def newThisMonth = Artwork.countByDateCreatedGreaterThanEquals(nowCal.getTime())
        def totalArtworks = Artwork.count()

        def flaggedThisMonth = Flag.countByDateCreatedGreaterThanEquals(nowCal.getTime())
        def totalFlags = Flag.count()

        render(view: 'userList', model: ['page': params.page, counter: newMap,newArtworksList: newArtworksList,
                newArtworksThisMonth: newThisMonth,
                totalArtworks: totalArtworks,flaggedThisMonth:flaggedThisMonth,totalFlags:totalFlags, fragment: params?.renderPageFragment]);
    }

    def administratorListJSON() {
        render userListJSON(true)
    }


    def regularListJSON() {
        render userListJSON(false)
    }

    def userListJSON(isAdmin) {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        //def adminRole = Role.findByAuthority("ROLE_ADMIN")
        def userlist = AppUser.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike('username', "%${sSearch}%")
                    ilike('email', "%${sSearch}%")
                }
            }
        }

        def totalRecords = AppUser.count();
        def totalDisplayRecords = AppUser.createCriteria().count {
            if (sSearch) {
                or {
                    ilike('username', "%${sSearch}%")
                    ilike('email', "%${sSearch}%")
                }
            }

        }

        def jsonList = [];

        if (params.sSearch_1 == 'Standard') {
            userlist = userlist.findAll {it.getAuthorities().contains(Role.findByAuthority('ROLE_USER'))}
            totalDisplayRecords = totalDisplayRecords - userlist.count {!it.getAuthorities().contains(Role.findByAuthority('ROLE_USER'))};
        }

        if (params.sSearch_1 == 'Administrator') {
            def superAdminRole = Role.findByAuthority('ROLE_SUPER_ADMIN')
            def adminRole = Role.findByAuthority('ROLE_ADMIN')
            userlist = userlist.findAll {it.getAuthorities().contains(adminRole) || it.getAuthorities().contains(superAdminRole)}
            totalDisplayRecords = totalDisplayRecords - userlist.count {!(it.getAuthorities().contains(adminRole) || it.getAuthorities().contains(superAdminRole))};
        }

        jsonList = userlist.collect {
            def galleryInstance = Gallery.findByOwner(it.username);
            [
                    0: "<input type=\"checkbox\" value=\"${it?.id}\" name=\"users\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                    1: it?.role?:'',
                    2: formatDate(date: it.dateCreated, format: 'dd/MM/yy'),
                    3: params.sSearch_1=='Administrator'?"<a href='${createLink(controller: 'admin', action: 'userEdit', id: it.id)}'>"+it.username+'</a>':"<a href='${createLink(controller:'admin',action:'userDetails',id:it.id)}'>${it.username}</a>",
                    4: it.email,
                    5: it.ageGroup == 'above13' ? 'yes' : 'no',
                    6: galleryInstance?.galleryName ?: 'no gallery',
                    7: "<a href='${createLink(controller: 'admin', action:'commentsAll', id: galleryInstance?.id)}'>"+Comment.countByUsername(it.username)+"</a>",
                    8: galleryInstance?.artworks?.flags?.size() ?: 0,
                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;

    }

    def actionUsers(){
        if(params?.selectedAction == 'Delete' || params?.selectedAction == 'DeleteAdmins'){
            def users
            def userParams = params.users
            if (userParams instanceof String) {
                users = AppUser.get(userParams)
            } else {
                users = AppUser.findAllByIdInList(params.list('users'));
            }
            if (users) {
                try {
                    users.each {
                        appUserService.deleteAppUser(it);
                    }
                    flash.message = message(code: 'admin.delete.success');
                } catch (Exception e) {
                    e.printStackTrace();
                    flash.error = message(code: 'admin.delete.failed');
                }
            }
        }
        if (params?.selectedAction == 'DeleteAdmins')
            redirect(action: listAdministrators,);
        else
            redirect(action: listUsers, params: ['pageFragment':params.renderPageFragment]);
    }

    def exportToCSV(){
        if(!params.max) params.max = 10

        if(params?.format && params.format != "html"){
            response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
            response.setHeader("Content-disposition", "attachment; filename=all-users.${params.extension}")

            List fields = ['role', "username", "email", 'ageGroup', 'galleryName', 'activity', 'flagged']
            Map labels = ["role":"Account", "username": "Username", "email": "Email", "ageGroup":"Age Group", "galleryName":"Gallery", "activity":"Activity", "flagged":"Flagged"]

            /* Formatter closure in previous releases def upperCase = { value -> return value.toUpperCase() } */

            // Formatter closure
            //def upperCase = { domain, value -> return value.toUpperCase() }

            //Map formatters = [username: upperCase]
            //Map parameters = [title: "Cool books", "column.widths": [0.2, 0.3, 0.5]]
            if(params?.userRole == 'standard')
                exportService.export(params.format, response.outputStream, AppUser.list(params).findAll{it.getAuthorities().contains(Role.findByAuthority('ROLE_USER'))}, fields, labels, [:], [:])
            else
                exportService.export(params.format, response.outputStream, AppUser.list(params), fields, labels, [:], [:])
        }
    }

    def createAdmin(){
        AppUser user = new AppUser()
        //render view: 'adminEdit', model: ['appUserInstance': user]
        ['appUserInstance': user]
    }

    def save() {
        def appUserInstance = new AppUser(userDetails: new UserDetails())
        appUserInstance?.properties = params
        if (!appUserInstance.save(flush: true)) {
            render(view: "createAdmin", model: [appUserInstance: appUserInstance, activeLink: "You", authority:params?.authority])
            return
        }

        addAdminRoles(appUserInstance, params?.authority?:"ROLE_ADMIN")
        flash.message = message(code: 'default.created.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUserInstance.id])
        redirect(action: "userEdit", id: appUserInstance.id)
    }

    def userEdit() {
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "listAdministrators")
            return
        }
        render view: 'adminEdit', model: [appUserInstance: appUserInstance, roleMap: buildUserModel(appUserInstance)]
    }

    def yourProfile() {
        def appUserInstance = AppUser.get(springSecurityService.getPrincipal()?.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "listAdministrators")
            return
        }
        render view: 'adminEdit', model: [appUserInstance: appUserInstance, roleMap: buildUserModel(appUserInstance), loggedInUser: springSecurityService.getPrincipal().username, authority:params?.authority]
    }

    def updateAdmin() {
        String passwordFieldName = SpringSecurityUtils.securityConfig.userLookup.passwordPropertyName

        def user = findById()
        if (!user) return
        if (!versionCheck('user.label', 'User', user, [user: user])) {
            return
        }

        def oldPassword = user.password
        bindData(user, params, [exclude: 'password'])
//        user.properties = params
        if (params.password && !params.password.equals(oldPassword) && !"".equals(params.password)) {
            user.password = params.password
        }

        if (!user.save(flush: true)) {
            render view: 'edit', model: buildUserModel(user)
            return
        }

        AppUserRole.removeAll user
        addAdminRoles(user, params?.authority?:"ROLE_ADMIN")
//        addRoles(user)
        userCache.removeUserFromCache user['username']
        flash.message = "${message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])}"
        redirect action: 'userEdit', id: user.id
    }


    def newArtworks() {
        def newArtworksList = Artwork.findAllByDateCreatedGreaterThanEquals((new Date()).minus(7), [sort: "dateCreated", order: "desc"])
        def nowCal = Calendar.instance
        nowCal.set(date: 1)
        def newThisMonth = Artwork.countByDateCreatedGreaterThanEquals(nowCal.getTime())
        def totalArtworks = Artwork.count()
        render view: 'newArtwork', model: [newArtworksList: newArtworksList,
                newArtworksThisMonth: newThisMonth,
                totalArtworks: totalArtworks,
                activeTab: params?.fragment]
    }

    def flaggedArtworks() {
        def newArtworksList = Artwork.findAllByDateCreatedGreaterThanEquals((new Date()).minus(7), [sort: "dateCreated", order: "desc"])
        def nowCal = Calendar.instance
        nowCal.set(date: 1)
        def newThisMonth = Artwork.countByDateCreatedGreaterThanEquals(nowCal.getTime())
        def totalArtworks = Artwork.count()
        render view: 'newArtwork', model: [newArtworksList: newArtworksList,
                newArtworksThisMonth: newThisMonth,
                totalArtworks: totalArtworks,
                activeTab: params?.fragment]
    }

    def changeArtworkStatus(){
        def artworkIds = params.list('artwork_id')
        def artworks
        if (artworkIds.size() > 0)
            artworks = Artwork.findAllByIdInList(artworkIds);
        if ("Delete".equals(params?.bulkAction)&&artworks){

            artworks.each{artwork->
                def tempArtwork = TemporaryArtwork.findByArtwork(artwork)
                if (tempArtwork){
                    tempArtwork.delete();
                }
                //TODO: remove tag links
                artwork.setTags(null);
                //TODO: remove from gallery
                artwork.delete();

                //TODO: delete artworks
                //TODO: notify success.


            }




        }else if (!"".equals(params?.bulkAction) && artworks) {
            artworks*.status = params?.bulkAction
            artworks?.each {
                Tag.findAllByNameInListAndApproved(it.tags, false)?.each {
                    it?.approved = true
                    it?.save(flush: true);
                };
            }
            if (artworks*.gallery*.save(flush: true)) {
                if (params?.bulkAction == 'Approved') {
                    def grouped = artworks.groupBy({it.gallery})
                    grouped.each {
                        def gallery = it.key;
                        notificationService.addNotification(AppUser.findByUsername(gallery.owner), g.render(template: '/common/artworkApprovalEmail', model: [notiOwner: gallery.owner, gallery: gallery, artworks: it.value]).toString(), new Date(), "0", "Admin", "artworkApproval");
                    }
                } else if (params?.bulkAction == 'Disapproved') {
                    def grouped = artworks.findAll {it.status == 'Disapproved'}.groupBy({it.gallery})
                    grouped.each {
                        def gallery = it.key;
                        notificationService.addNotification(AppUser.findByUsername(gallery.owner), g.render(template: '/common/artworkDisapprovalEmail', model: [notiOwner: gallery.owner, gallery: gallery, artworks: it.value]).toString(), new Date(), "0", "Admin", "artworkDisapproval");
                    }
                }
            } else {
                artworks.each {println(it.errors)}
            }

        }
//        println artworks
        redirect action: 'listUsers', params: [page: 'newArtwork'], fragment: 'newArtwork'
    }


    def newArtworksJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        //def adminRole = Role.findByAuthority("ROLE_ADMIN")
        def artworksList = Artwork.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            createAlias('gallery', 'g')
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike('g.owner', "%${sSearch}%")
                    ilike('g.galleryName', "%${sSearch}%")
                    ilike('artist', "%${sSearch}%")
                    ilike('title', "%${sSearch}%")
                    ilike('status', "%${sSearch}%")
                }
            }
        }

        def totalRecords = Artwork.count();
        def totalDisplayRecords = Artwork.createCriteria().count {
            createAlias('gallery', 'g')
            if (sSearch) {
                or {
                    ilike('g.owner', "%${sSearch}%")
                    ilike('g.galleryName', "%${sSearch}%")
                    ilike('artist', "%${sSearch}%")
                    ilike('title', "%${sSearch}%")
                    ilike('status', "%${sSearch}%")
                }
            }

        }

        def jsonList = [];

        jsonList = artworksList.collect {
            [
                    0: "<input type=\"checkbox\" value=\"${it?.id}\" name=\"artwork_id\" style=\"display: none;\">\n" + "<span class=\"custom checkbox\"></span>",
                    1: formatDate(date: it.dateCreated, format: 'dd/MMM/yy'),
                    2: "<a href='${g.createLink(controller: 'admin', action: 'userEdit', id: AppUser.findByUsername(it?.gallery?.owner)?.id)}'>${it?.gallery?.owner}</a>",
                    3: "<a href='${g.createLink(controller: it.gallery?.galleryNameUrl)}'>${it.gallery?.galleryName}</a>",
                    4: it.artist,
                    5: """${it?.title} <div style='width: 50px; height: 50px; overflow: hidden;display:inline;'>
<a href='${it?.artworkDetailImageUrl}' rel='lightbox' title='${it?.title}'>
<img class='lazy' src='${it?.thumbnailUrl}' alt='' data-original='${it?.thumbnailUrl}' style='max-width:50px;max-height:50px;'>
</a></div>""",
                    6: it?.tags?:'',
                    7: it?.status,
                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;

    }

    def flaggedArtworksJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def artworksList = Flag.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike('flaggedUser', "%${sSearch}%")
                    ilike('flaggingUser', "%${sSearch}%")
                    ilike('comment', "%${sSearch}%")
                    ilike('sectionFlagged', "%${sSearch}%")
                    ilike('status', "%${sSearch}%")
                }
            }
        }

        def totalRecords = Flag.count();
        def totalDisplayRecords = Flag.createCriteria().count {
            if (sSearch) {
                or {
                    ilike('flaggedUser', "%${sSearch}%")
                    ilike('flaggingUser', "%${sSearch}%")
                    ilike('comment', "%${sSearch}%")
                    ilike('sectionFlagged', "%${sSearch}%")
                    ilike('status', "%${sSearch}%")
                }
            }

        }

        def jsonList = [];

        jsonList = artworksList.collect {
            def domainInstance = grailsApplication.getArtefact("Domain", it?.flaggedObject)?.
                    getClazz()?.get(it?.flaggedObjectId)
            def sectionLink = ""
            if (domainInstance) {
                if (domainInstance instanceof Gallery) {
                    sectionLink = "<a href='${createLink(controller: domainInstance?.galleryNameUrl)}'>" + it.sectionFlagged + "</a>"
                } else if (domainInstance instanceof Artwork) {
                    sectionLink = "<a href='${createLink(controller: 'artwork', action: 'artworkview', id: domainInstance?.id)}'>" + it.sectionFlagged + "</a>"
                } else if (domainInstance instanceof Comment) {
                    sectionLink = "<a href='${createLink(controller: 'comment', action: 'viewComment', id: domainInstance?.id)}'>" + it.sectionFlagged + "</a>"
                }
            }
            [


                    0: "<input type=\"checkbox\" value=\"${it?.id}\" name=\"flag_id\" style=\"display: none;\">\n" + "<span class=\"custom checkbox\"></span>",
                    1: formatDate(date: it.dateCreated, format: 'dd/MMM/yy'),
                    2: it.flaggedUser,
                    3: it.flaggingUser,
                    4: it.comment,
                    5: sectionLink,
                    6: it.status,
                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;

    }

    def galleryArtworkList() {
        def gallery= Gallery.get(params.id)
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def artworksList = Artwork.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            createAlias('gallery','g')
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike("artist","%${sSearch}%")
                    ilike("title","%${sSearch}%")
                }

            }
            eq('g.id',gallery.id)
        }

        def totalRecords = Artwork.count();
        def totalDisplayRecords = Artwork.createCriteria().count {
            createAlias('gallery','g')
            if (sSearch) {
                or {
                    ilike("artist","%${sSearch}%")
                    ilike("title","%${sSearch}%")
                }

            }
            eq('g.id',gallery.id)

        }
        def jsonList = [];
        jsonList = artworksList.collect {
            [
                    0: "<input type=\"checkbox\" value=\"${it?.id}\" name=\"artwork_id\" style=\"display: none;\">\n" + "<span class=\"custom checkbox\"></span>",
                    1: formatDate(date: it.dateCreated, format: 'dd/MMM/yy'),
                    2:  """<div style='width: 50px; height: 50px; overflow: hidden;display:inline;'>
<a href='${it?.artworkDetailImageUrl}' rel='lightbox' title='${it?.title}'>
<img class='lazy' src='${it?.thumbnailUrl}' alt='' data-original='${it?.thumbnailUrl}' style='max-width:50px;max-height:50px;'>
</a></div>""",
                    3: it.title,
                    4: it.getTotalComments()?:"0",
                    5: it.flags.size()>0?"yes":"no",
                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;

    }

    def userDetails(){
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.error = 'User not found.'
            redirect(controller:'admin',action: "listUsers",params:[page:'standard'])
            return
        }
        render view: 'userDetails', model: [appUserInstance: appUserInstance, roleMap: buildUserModel(appUserInstance)]
    }

    def forgotPassword(){
        def user = AppUser.get(params.id)
        if (!user) {
            flash.error = message(code: 'spring.security.ui.forgotPassword.user.notFound')
            redirect (controller:'admin',action: 'userDetails',id: user.id)
        }

        def registrationCode = new RegistrationCode(username: user.username).save()

        String url =createLink(absolute:true,
                                controller: 'register', action: 'resetPassword',
                                params: [t: registrationCode.token])

        String urlContactUs = createLink(absolute: true,
                controller: 'contactUs',action:'index' )
        String urlPrivacyPolicy = createLink(absolute:true,
                controller: 'general',action: 'privacyPolicy')
        String urlToS = createLink(absolute:true,
                controller: 'general',action: 'termsOfService')
        String urlNotification = createLink(absolute: true,
                controller: 'appUser',action: 'notificationSettings')

        def conf = SpringSecurityUtils.securityConfig
        def body = grailsApplication.config.security.forgotPassword.emailBody
        if (body.contains('$')) {
            body = evaluate(body, [user: user, url: url,urlContactUs:urlContactUs, urlPrivacyPolicy: urlPrivacyPolicy, urlToS: urlToS, urlNotification: urlNotification])
        }
        mailService.sendMail {
            to user.email
            from grailsApplication.config.security.forgotPassword.emailFrom
            subject grailsApplication.config.security.forgotPassword.emailSubject
            html body.toString()
        }

        flash.message='Reset password link has been sent to user.'
        [emailSent: true]
        redirect (controller:'admin',action: 'userDetails',id: user.id)

    }

    def deactivateUser(){
       toggleUser(false);
    }

    def activateUser(){
        toggleUser(true);
    }

    def toggleUser(activate){
        def user = AppUser.get(params.id)
        if (!user){
            flash.error='User not found.'
            redirect controller: 'admin',action: 'listUsers',params:[page:'standard']
        }else{
            user.setEnabled(activate)
            if (user.save()){
                flash.message="User ${user.username} has been ${activate?"activated.":"deactivated."}"
                redirect controller: 'admin',action: 'userDetails',id: user.id
            }else{
                flash.error= 'An error has been encountered. Please report this to technical support.'
                redirect controller: 'admin',action: 'listUsers',params:[page:'standard']
            }
        }
    }


    def updateFlags() {
        def flagIds = params.list('flag_id')
        def flags
        if (flagIds.size() > 0)
            flags = Flag.findAllByIdInList(flagIds);
        def bulkAction = params?.bulkAction
        if (!"".equals(bulkAction) && flags) {

            if ("Open".equals(bulkAction) || "Resolved".equals(bulkAction)) {
                flags*.status = bulkAction
                if (flags*.save(flush: true)) {
                    flash.message = 'Flagged items have been updated.'
                } else {
                    flash.errors = flags*.errors
                }
            } else if ("Delete".equals(bulkAction)) {
                try {


                    flags.each {
                        def domainInstance = grailsApplication.getArtefact("Domain", it?.flaggedObject)?.
                                getClazz()?.get(it?.flaggedObjectId)
                        domainInstance?.removeFromFlags(it);
                        it.delete();
                        domainInstance?.save(flush:true);

                    }
                    flash.message = 'Flag items have been deleted.'
                } catch (Exception e) {
                    flash.errors = 'An error has been encountered while deleting flag items.'
                }
            }
        }
        redirect action: 'listUsers', params: [page: 'flagged'], fragment: 'flag'
    }

    protected void addRoles(user) {
        for (String key in params.keySet()) {
            if (key.contains('ROLE') && 'on' == params.get(key)) {
                AppUserRole.create user, Role.findByAuthority(key), true
            }
        }
    }

    protected void addAdminRoles(user, authority) {
        AppUserRole.create user, Role.findByAuthority(authority), true
    }

    protected findById() {
        def user = AppUser.get(params.id)
        if (!user) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
            redirect action: 'listAdministrators'
        }
        user
    }

    protected boolean versionCheck(String messageCode, String messageCodeDefault, instance, model) {
        if (params.version) {
            def version = params.version.toLong()
            if (instance.version > version) {
                instance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [message(code: messageCode, default: messageCodeDefault)] as Object[],
                        "Another user has updated this instance while you were editing")
                render view: 'edit', model: model
                return false
            }
        }
        true
    }


    protected Map buildUserModel(user) {

        String authorityFieldName = SpringSecurityUtils.securityConfig.authority.nameField
        String authoritiesPropertyName = SpringSecurityUtils.securityConfig.userLookup.authoritiesPropertyName

        List roles = sortedRoles()
        Set userRoleNames = user[authoritiesPropertyName].collect { it[authorityFieldName] }
        def granted = [:]
        def notGranted = [:]
        for (role in roles) {
            String authority = role[authorityFieldName]
            if (userRoleNames.contains(authority)) {
                granted[(role)] = userRoleNames.contains(authority)
            }
            else {
                notGranted[(role)] = userRoleNames.contains(authority)
            }
        }

        return (granted + notGranted)
    }

    protected List sortedRoles() {
        lookupRoleClass().list().sort { it.authority }
    }

    protected Class<?> lookupRoleClass() {
        return Role.class

    }

    protected String evaluate(s, binding) {
        new SimpleTemplateEngine().createTemplate(s).make(binding)
    }

    def listGalleries(){

    }

    def galleryListJSON(){
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        //def adminRole = Role.findByAuthority("ROLE_ADMIN")
        def galleryList = Gallery.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike('galleryName', "%${sSearch}%")
                    ilike('owner', "%${sSearch}%")
                }
            }
        }

        def totalRecords = Gallery.count();
        def totalDisplayRecords = Gallery.createCriteria().count {
            if (sSearch) {
                or {
                    ilike('galleryName', "%${sSearch}%")
                    ilike('owner', "%${sSearch}%")
                }
            }

        }

        def jsonList = []

        jsonList = galleryList.collect {
            def galleryInstance = it;
            [
                    0: "<input type=\"checkbox\" value=\"${it?.id}\" name=\"gallery\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                    1: it?.galleryName?:'',
                    2: it?.owner?:'',
                    3: it?.artworkType?:'',
                    4: it?.galleryOwner?:'',
                    5: it?.youngArtists?.size()?:0,
                    6: it?.artworks?.size()?:0,
                    7: formatDate(date: it?.dateCreated, format: 'dd/MM/yy'),
                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;

    }

    def actionGallery(){
        def pa = params;
        if(params?.selectedAction == 'Delete'){

            def galleries = Gallery.findAllByIdInList(params.list('gallery'));

            if (galleries) {
                try {
                    galleries*.delete(flush: true);
                    flash.message = message(code: 'admin.delete.success');
                } catch (Exception e) {
                    e.printStackTrace();
                    flash.error = message(code: 'admin.delete.failed');
                }
            }
        }

        redirect(action: 'listGalleries');
    }

    def commentsAll(){
        def commentsList
        if (params.id)
            commentsList = commentService.getCommentsList(Gallery.get(params?.id)?.owner, '');
        else
            commentsList= Comment.list();
        [activeLink:'commentsAll', commentsList: commentsList];
    }

    def commentsAllList(){
        def commentsInstance
        if (params.id)
            commentsInstance = commentService.getCommentsList(Gallery.get(params?.id)?.owner, '');
        else
            commentsInstance = Comment.list();
        def commentsMapInstance = [];
        commentsMapInstance = commentsInstance.collect {
            [
                    "<input type=\"checkbox\" value=\"${it?.id}\" name=\"myComments\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                    "<div style=\"width: 50px; height: 50px; overflow: hidden;display:inline;\"><img class=\"lazy\" src=\"${resource(dir:'images', file: 'progressImg-test.gif')}\" alt=\"\" data-original=\"${UserDetails.findByAppUser(AppUser.findByUsername(it?.username)).avatarLocationUrl?:resource(dir: 'images', file:'profile03.jpg')}\" width=\"50px\"></div>${it?.username}",
                    "${it?.location}: ${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId )?.id], fragment: 'commentsSection', Gallery.get(it?.locationId )?.galleryName):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId )?.id], fragment: 'commentsSection', Artwork.get(it?.locationId ).title):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId )?.id], fragment: 'commentsSection', Timeline.get(it?.locationId )?.artistName):''}",
                    "${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId ).id], fragment: it?.id, it?.content):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId ).id], fragment: it?.id, it?.content):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId ).id], fragment: it?.id, it?.content):''}<br />"+
                            (it?.status!='Approved'?"<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"updateStatusLink('${it?.id}', commentList, 'Approve')\">Approve</a>":'<span style="font-size:11px;">Approved</span>')+"&nbsp;\n" +
                            "<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"deleteLink(this, commentList)\">Delete</a>&nbsp;\n",
                    formatDate(format: "dd/MM/yy", date: it?.date),
                    it?.status
            ]
        }
        def data = [aaData:commentsMapInstance];
        render data as JSON;
    }
}
