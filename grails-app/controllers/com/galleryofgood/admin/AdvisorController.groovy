package com.galleryofgood.admin

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.commons.CommonsMultipartFile

class AdvisorController {

    def amazonImageService,randomService;

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [advisorInstanceList: Advisor.list(params), advisorInstanceTotal: Advisor.count(), advisorExpertise: Advisor.transients]
    }

    def create() {
        [advisorInstance: new Advisor(params)]
    }

    def save() {
        def advisorInstance = new Advisor(params)
        advisorInstance.properties = params
        if (params.imageFile.getSize()>0){
            advisorInstance?.url=amazonImageService.uploadImage(params.imageFile,randomService.nextInteger(999999999))
        }
        if (params.bio){
            advisorInstance.bio=params?.bio?.trim()
        }

        if (!advisorInstance.save(flush: true)) {
            render(view: "create", model: [advisorInstance: advisorInstance])
            return
        }else{
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'advisor.label', default: 'Advisor'), advisorInstance.id])
        redirect(action: "show", id: advisorInstance.id)
    }

    def show() {
        def advisorInstance = Advisor.get(params.id)
        if (!advisorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advisor.label', default: 'Advisor'), params.id])
            redirect(action: "list")
            return
        }

        [advisorInstance: advisorInstance]
    }

    def edit() {
        def advisorInstance = Advisor.get(params.id)
        if (!advisorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advisor.label', default: 'Advisor'), params.id])
            redirect(action: "list")
            return
        }

        [advisorInstance: advisorInstance]
    }

    def update() {
        def advisorInstance = Advisor.get(params.id)
        if (!advisorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advisor.label', default: 'Advisor'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (advisorInstance.version > version) {
                advisorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'advisor.label', default: 'Advisor')] as Object[],
                        "Another user has updated this Advisor while you were editing")
                render(view: "edit", model: [advisorInstance: advisorInstance])
                return
            }
        }

        advisorInstance.properties = params
        if (params.imageFile.getSize()>0){
            advisorInstance?.url=amazonImageService.uploadImage(params.imageFile,randomService.nextInteger(999999999))
        }

        if (params.bio)
        {advisorInstance.bio=params.bio.trim()}

        if (!advisorInstance.save(flush: true)) {
            render(view: "edit", model: [advisorInstance: advisorInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'advisor.label', default: 'Advisor'), advisorInstance.id])
        redirect(action: "show", id: advisorInstance.id)
    }

    def delete() {
        def advisorInstance = Advisor.get(params.id)
        if (!advisorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advisor.label', default: 'Advisor'), params.id])
            redirect(action: "list")
            return
        }

        try {
            advisorInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'advisor.label', default: 'Advisor'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'advisor.label', default: 'Advisor'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
