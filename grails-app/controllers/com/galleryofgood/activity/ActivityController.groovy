package com.galleryofgood.activity
import grails.converters.JSON
import com.galleryofgood.gallery.Gallery
import com.galleryofgood.gallery.Artwork
import com.galleryofgood.gallery.Timeline
import com.galleryofgood.common.Comment
import com.galleryofgood.common.UserFollow
import com.galleryofgood.security.AppUser
import com.galleryofgood.user.UserDetails

class ActivityController {

    def springSecurityService, commentService, notificationService

    def index() {
    redirect action:'comments'
    }

    def comments(){
        def myCommentsList = commentService.getMyCommentsList(springSecurityService.getCurrentUser()?.username, '');
        def commentsList = commentService.getCommentsList(springSecurityService.getCurrentUser()?.username, '');
        [activeLink:'comments', commentsList: commentsList, myCommentsList:myCommentsList];
    }

    def commentsList(){
        def commentsInstance= commentService.getCommentsList(springSecurityService.getCurrentUser()?.username, params.sSearch.toString());
        def commentsMapInstance = [];
                commentsMapInstance = commentsInstance.collect {
                    [
                        "<input type=\"checkbox\" value=\"${it?.id}\" name=\"myComments\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                        "<div style=\"width: 50px; height: 50px; overflow: hidden;display:inline;\"><img class=\"lazy\" src=\"${resource(dir:'images', file: 'progressImg-test.gif')}\" alt=\"\" data-original=\"${UserDetails.findByAppUser(AppUser.findByUsername(it?.username)).avatarLocationUrl?:resource(dir: 'images', file:'profile03.jpg')}\" width=\"50px\"></div>${it?.username}",
                        "${it?.location}: ${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId )?.id], fragment: 'commentsSection', Gallery.get(it?.locationId )?.galleryName):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId )?.id], fragment: 'commentsSection', Artwork.get(it?.locationId ).title):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId )?.id], fragment: 'commentsSection', Timeline.get(it?.locationId )?.artistName):''}",
                        "${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId ).id], fragment: it?.id, it?.content):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId ).id], fragment: it?.id, it?.content):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId ).id], fragment: it?.id, it?.content):''}<br />"+
                        (it?.status!='Approved'?"<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"updateStatusLink('${it?.id}', commentList, 'Approve')\">Approve</a>":'<span style="font-size:11px;">Approved</span>')+"&nbsp;\n" +
                        "<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"deleteLink(this, myCommentList)\">Delete</a>&nbsp;\n" +
                        (it?.status!='Reported'?"<a class=\"buttonForModal secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\">Report</a>":'<span style="font-size:11px;">Reported</span>'),
                        formatDate(format: "dd/MM/yy", date: it?.date),
                        it?.status
                    ]
                }
        def data = [aaData:commentsMapInstance];
        render data as JSON;
    }

    def myCommentsList(){
        def commentsInstance= commentService.getMyCommentsList(springSecurityService.getCurrentUser()?.username, params.sSearch.toString());
        def commentsMapInstance = [];
                commentsMapInstance = commentsInstance.collect{
                    [
                        "<input type=\"checkbox\" value=\"${it?.id}\" name=\"myComments\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                        "<div style=\"width: 50px; height: 50px; overflow: hidden;display:inline;\"><img class=\"lazy\" src=\"${resource(dir:'images', file: 'progressImg-test.gif')}\" alt=\"\" data-original=\"${UserDetails.findByAppUser(AppUser.findByUsername(it?.username)).avatarLocationUrl?:resource(dir: 'images', file:'profile03.jpg')}\" width=\"50px\"></div>${it?.username}",
                        "${it?.location}: ${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId )?.id], fragment: 'commentsSection', Gallery.get(it?.locationId )?.galleryName):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId )?.id], fragment: 'commentsSection', Artwork.get(it?.locationId ).title):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId )?.id], fragment: 'commentsSection', Timeline.get(it?.locationId )?.artistName):''}",
                        "${it?.inGallery()?g.link(controller: 'gallery', action: 'viewComments', params: [id: Gallery.get(it?.locationId ).id], fragment: it?.id, it?.content):it?.inArtwork()?g.link(controller: 'artwork', action: 'artworkview', params: [id:Artwork.get(it?.locationId ).id], fragment: it?.id, it?.content):it?.inTimeline()?g.link(controller: 'timeline', action: 'timelineview', params: [id: Timeline.get(it?.locationId ).id], fragment: it?.id, it?.content):''}<br />"+
                        "<a class=\"secondary_link\" href=\"javascript:\" commentid=\"${it?.id}\" onclick=\"deleteLink(this, myCommentList)\">Delete</a>&nbsp;\n",
                        formatDate(format: "dd/MM/yy", date: it?.date),
                        it?.status=="New!"?"<b>Pending</b>":it?.status
                    ]
                }
        def data = [aaData:commentsMapInstance];
        render data as JSON;
    }

    def deleteComment={
        def response = [message:"Nothing to delete."];
        def commentInstance = Comment.get(params.id );
        try{
                commentInstance.delete(flush: true);
                response = [message:"Deleted"];
        }catch(Exception e){
                println(e);
                response = [message:"An error has occurred. Try to refresh the table."];
        }
        render response as JSON;
    }

    def updateCommentStatus={
        def response = [message:"Nothing to update."];

        def commentInstance = Comment.get(params.id );
        commentInstance.status=params.newStatus=="Approve"?"Approved":params.newStatus=="Publish"?"Published":params.newStatus=="Report"?"Reported":'New!';

        if (commentInstance.save()){
            response = [message:"Updated"];
        }else{
            response = [message:"An error has occurred."];
        }

        render response as JSON;
    }

    def following(){
        def followersCount = UserFollow.countByFollowingId(Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.id?:"");
        def followingCount = UserFollow.countByFollowerId(springSecurityService.getCurrentUser()?.id );
        [activeLink:'following', userInstance:springSecurityService.getCurrentUser(), followersCount:followersCount, followingCount:followingCount]
    }

    def followersList(){
        def followersInstance = UserFollow.findAllByFollowingId(Gallery.findByOwner(springSecurityService.getCurrentUser()?.username)?.id, [sort: 'dateCreated', order: 'desc'])
        def followersMapInstance = [];
        followersMapInstance = followersInstance.collect{
            def galleryOwnerInstance = AppUser.findById(it.followerId );
            def galleryInstance = Gallery.findByOwner(galleryOwnerInstance.username);
            boolean following = UserFollow.findByFollowerIdAndFollowingId(springSecurityService.getCurrentUser()?.id, galleryInstance.id)?true:false;
            [
                    "<input type=\"checkbox\" value=\"${galleryInstance.id}\" name=\"followers\" style=\"display: none;\">\n"+"<span class=\"custom checkbox\"></span>",
                    "<div style=\"width: 50px; height: 50px; overflow: hidden;display:inline;\"><img class=\"lazy\" src=\"${resource(dir:'images', file: 'progressImg-test.gif')}\" alt=\"\" data-original=\"${UserDetails.findByAppUser(AppUser.findByUsername(galleryOwnerInstance?.username)).avatarLocationUrl?:'https://s3.amazonaws.com/galleryofgood/common/avatar/profile01.jpg'}\" width=\"50px\"></div>${galleryOwnerInstance?.username}",
                    galleryInstance?.galleryName,
                    "<a class=\"nice small white button radius follow\" id=\"${galleryInstance.id}\" href=\"javascript: followable('${following?"unfollow":"follow"}', '${galleryInstance.id}', true)\">${following?'Unfollow':'Follow'}</a>"
            ]
        }
        def data = [aaData:followersMapInstance];
        render data as JSON;
    }

/*
    def follow={
        def response = [status:"failed", message: "No gallery"]
        if (params.id){
            if (!UserFollow.findByFollowerIdAndFollowingId(springSecurityService.getCurrentUser()?.id, params.id )){
                if(new UserFollow(followerId: springSecurityService.getCurrentUser()?.id, followingId: params.id ).save(flush: true))
                    response = [status:"followed", message: "Unfollow"];
                else
                    response = [status:"failed", message: "An error has occurred"];
            }else
                response = [status:"failed", message: "You are already a follower."];
        }
        render response as JSON;
    }
*/
    def follow={
        def response = [status:"failed", message: "No gallery"]
        if (params.id){
            def galleryInstance = Gallery.get(params.id);
            if (!UserFollow.findByFollowerIdAndFollowingId(springSecurityService.getCurrentUser()?.id, params.id )){
                def message
                if (UserFollow.findByFollowerIdAndFollowingId(AppUser.findByUsername(galleryInstance.owner).id, Gallery.findByOwner(springSecurityService.getCurrentUser()?.username).id))
                    message = "${springSecurityService.getCurrentUser()?.username} of <a href=\'${createLink(uri: '/', absolute: true)+galleryInstance?.galleryNameUrl}\'>${galleryInstance?.galleryName}</a> gallery is following your gallery. Copy and paste this URL if the link does not work: <a href='${createLink(uri: '/', absolute: true) + galleryInstance?.galleryNameUrl}\'>${createLink(uri: '/', absolute: true) + galleryInstance?.galleryNameUrl}</a>"
                else
                    message = "${springSecurityService.getCurrentUser()?.username} of <a href=\'${createLink(uri: '/', absolute: true)+galleryInstance?.galleryNameUrl}\'>${galleryInstance?.galleryName}</a> gallery is following your gallery. Visit <a href=\'${createLink(uri: '/', absolute: true)+galleryInstance?.galleryNameUrl}\'>${galleryInstance?.galleryName}</a> and follow back. Copy and paste this URL if the link does not work: <a href='${createLink(uri: '/', absolute: true) + galleryInstance?.galleryNameUrl}\'>${createLink(uri: '/', absolute: true) + galleryInstance?.galleryNameUrl}</a>"
                //notificationService.addNotification(AppUser.findByUsername(galleryInstance?.owner), "Hi, <br><br> ${message}. Thank you. <br><br>Gallery Of Good", new Date(), "${params.id}" ,"Gallery", "follow");
                notificationService.addNotification(AppUser.findByUsername(galleryInstance?.owner), g.render(template: '/common/followEmail', model: [notiOwner: galleryInstance.owner,message: message]).toString(), new Date(), "${params.id}" ,"Gallery", "follow");
                if(new UserFollow(followerId: springSecurityService.getCurrentUser()?.id, followingId: params.id ).save(flush: true))
                    response = [status:"followed", message: "Unfollow"];
                else
                    response = [status:"failed", message: "An error has occurred"];
            }else
                response = [status:"failed", message: "You are already following this gallery."];
        }
        render response as JSON;
    }

    def unfollow={
        def response = [status:"failed", message: "No gallery"]
        if (params.id){
            def followInstance = UserFollow.findByFollowerIdAndFollowingId(springSecurityService.getCurrentUser()?.id, params.id )
            if (followInstance){
                try {
                    followInstance.delete(flush: true);
                    response = [status:"unfollowed", message: "Follow Gallery"];
                }catch (Exception e){
                    response = [status:"failed", message: "An error has occurred"];
                }
            }else
                response = [status:"failed", message: "You are not following this gallery. No need to unfollow."];
        }
        render response as JSON;
    }

    //Followable infinite scroll
    def myFollowablesList ={
        def followingInstance = UserFollow.findAllByFollowerId(springSecurityService.getCurrentUser()?.id );
        def galleryInstance
        if (followingInstance)
            galleryInstance = Gallery.findAll("from Gallery g where g.id in :following", ["following":followingInstance.followingId], params);
        else
            galleryInstance = null;
        render(template: "galleryListLoad", model:[galleryInstance:galleryInstance]);
    }

    def activityGuide(){
        render view:'activityGuide',model: [activeLink:'activityGuide']
    }

}
