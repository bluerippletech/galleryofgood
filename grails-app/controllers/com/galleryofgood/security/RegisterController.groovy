package com.galleryofgood.security

import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.codehaus.groovy.grails.plugins.springsecurity.NullSaltSource
import org.codehaus.groovy.grails.plugins.springsecurity.ui.RegistrationCode
import com.galleryofgood.user.UserDetails

class RegisterController extends grails.plugins.springsecurity.ui.RegisterController {

    def springSecurityService

    def index = {
        [command: new RegisterCommand()]

    }

    def captcha = {
        def random =new Random()
        def randomNumber=random.nextInt(4)
        session["captcha"]=randomNumber
        render session["captcha"]
    }


    def updateFirstLogin = {

        def username=springSecurityService.getCurrentUser()?.username
        def appUser=AppUser.findByUsername(username)
        appUser.hasLoggedIn="Yes"
        appUser.save()
        springSecurityService.reauthenticate appUser.username
        render ""

    }
    def thankYou = {
        render view: "thankYou"
    }

    def verifyRegistration = {

        def conf = SpringSecurityUtils.securityConfig
        String defaultTargetUrl = conf.successHandler.defaultTargetUrl

        String token = params.t

        def registrationCode = token ? RegistrationCode.findByToken(token) : null
        if (!registrationCode) {
            flash.error = message(code: 'spring.security.ui.register.badCode')
            redirect uri: defaultTargetUrl
            return
        }

        def user
        RegistrationCode.withTransaction { status ->
            user = AppUser.findByUsername(registrationCode.username)
            if (!user) {
                return
            }
            user.accountLocked = false
            user.save()
            def UserRole = AppUserRole
            def Role = Role
            for (roleName in conf.ui.register.defaultRoleNames) {
                UserRole.create user, Role.findByAuthority(roleName)
            }
            registrationCode.delete()
        }

        if (!user) {
            flash.error = message(code: 'spring.security.ui.register.badCode')
            redirect uri: defaultTargetUrl
            return
        }

        springSecurityService.reauthenticate user.username

//        flash.message = message(code: 'spring.security.ui.register.complete')
        render view:'activated', model:[user:user]
//        redirect uri: conf.ui.register.postRegisterUrl ?: defaultTargetUrl
    }

    def register = { RegisterCommand command ->
        def conf = SpringSecurityUtils.securityConfig
        command.setSessionValue(session["captcha"] as Integer)
        command.validate()
        if (command.hasErrors()) {
            render view: 'index', model: [command: command]
            return
        }

        String salt = saltSource instanceof NullSaltSource ? null : command.username
//        String password = springSecurityService.encodePassword(command.password, salt)
        def user = new AppUser(email: command.email, username: command.username,
                password: command.password, accountLocked: true, enabled: true,
                ageGroup:command.radio1,
                optin:command.optin, userDetails: new UserDetails())
        if (!user.validate() || !user.save() || !user.userDetails.save()) {
            // TODO

            println renderErrors(bean: user)
        }
        def UserRole = AppUserRole
        def Role = Role
        for (roleName in conf.ui.register.defaultRoleNames) {
            UserRole.create user, Role.findByAuthority(roleName)
        }

        def registrationCode = new RegistrationCode(username: user.username).save()
        String url = generateLink('verifyRegistration', [t: registrationCode.token])
        String urlHomepage= createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath"
                ,uri: "/")
        String urlContactUs = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'contactUs')
        String urlPrivacyPolicy = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'general',action: 'privacyPolicy')
        String urlToS = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'general',action: 'termsOfService')
        String urlNotification = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'appUser',action: 'notificationSettings')
        String urlParentGuide = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'general',action:'gettingStarted')
        String urlDeleteAccount = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'appUser',action:'deleteAccount',params:[username:user.username])
        def body
        if (user.ageGroup=="above13"){
            body=grailsApplication.config.emailRegistration.registerAbove13.emailBody

        }else{
            body=grailsApplication.config.emailRegistration.registerBelow13.emailBody
        }

        if (body.contains('$')) {
            body = evaluate(body, [user: user, url: url,urlHomepage:urlHomepage,urlContactUs:urlContactUs,urlParentGuide:urlParentGuide,urlDeleteAccount:urlDeleteAccount,urlNotification:urlNotification,urlPrivacyPolicy:urlPrivacyPolicy,urlToS:urlToS])
        }
        mailService.sendMail {
            to command.email
            from grailsApplication.config.emailRegistration.emailFrom
            subject grailsApplication.config.emailRegistration.emailSubject
            html body.toString()
        }

        render view: 'thankYou', model: [command: command]
    }


    def forgotPassword = {

        if (!request.post) {
            // show the form
            return
        }

        String username = params.username
        if (!username) {
            flash.error = message(code: 'spring.security.ui.forgotPassword.username.missing')
            return
        }

        def user = AppUser.findByEmail(username)
        if (!user) {
            flash.error = message(code: 'spring.security.ui.forgotPassword.user.notFound')
            return
        }

        def registrationCode = new RegistrationCode(username: user.username).save()

        String url = generateLink('resetPassword', [t: registrationCode.token])
        String urlContactUs = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'contactUs')
        String urlPrivacyPolicy = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'general',action: 'privacyPolicy')
        String urlToS = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'general',action: 'termsOfService')
        String urlNotification = createLink(base: "$request.scheme://$request.serverName:$request.serverPort$request.contextPath",
                controller: 'appUser',action: 'notificationSettings')
        
        def body = grailsApplication.config.security.forgotPassword.emailBody

        if (body.contains('$')) {
            body = evaluate(body, [user: user, url: url,urlContactUs:urlContactUs, urlPrivacyPolicy: urlPrivacyPolicy, urlToS: urlToS, urlNotification: urlNotification])
        }
        mailService.sendMail {
            to user.email
            from grailsApplication.config.security.forgotPassword.emailFrom
            subject grailsApplication.config.security.forgotPassword.emailSubject
            html body.toString()
        }

        [emailSent: true]
    }

    def resetPassword = { ResetPasswordCommand command ->

        String token = params.t

        def registrationCode = token ? RegistrationCode.findByToken(token) : null
        if (!registrationCode) {
            flash.error = message(code: 'spring.security.ui.resetPassword.badCode')
            redirect uri: SpringSecurityUtils.securityConfig.successHandler.defaultTargetUrl
            return
        }

        if (!request.post) {
            return [token: token, command: new ResetPasswordCommand()]
        }

        command.username = registrationCode.username
        command.validate()

        if (command.hasErrors()) {
            return [token: token, command: command]
        }

        String salt = saltSource instanceof NullSaltSource ? null : registrationCode.username
        RegistrationCode.withTransaction { status ->
            def user = AppUser.findByUsername(registrationCode.username)
//            user.password = springSecurityService.encodePassword(command.password, salt)
            user.password=command.password
            user.save()
            registrationCode.delete()
        }

        springSecurityService.reauthenticate registrationCode.username

        flash.message = message(code: 'spring.security.ui.resetPassword.success')

        def conf = SpringSecurityUtils.securityConfig
        String postResetUrl = conf.ui.register.postResetUrl ?: conf.successHandler.defaultTargetUrl
        redirect uri: postResetUrl
    }

    static final email2Validator = { value, command ->
        if (command.email != command.email2) {
            return 'command.email2.error.mismatch'
        }
    }
    
    static final captchaValidator = {value, command->
        if (command.captcha != command.sessionValue){
            return 'command.captcha.error.invalid'
        }
    }

    static final passwordValidatorCustom = { String password, command ->
        if (command.username && command.username.equals(password)) {
            return 'command.password.error.username'
        }
        if  (command.password.contains(' ')){
            return 'command.password.error.spaces'
        }
    }

}



class RegisterCommand {

    String username
    String email
    String email2
    String password
    String password2
    Integer captcha
    Integer sessionValue
    String terms
    String optin
    String radio1

    static constraints = {
        username blank: false, minSize: 2, maxSize: 20, validator: { value, command ->
            if (value) {
                def User = ApplicationHolder.application.getDomainClass(
                        SpringSecurityUtils.securityConfig.userLookup.userDomainClassName).clazz
                if (value.contains(' ')){
                    return 'command.username.error.spaces'
                }
                if (User.findByUsername(value)) {
                    return 'registerCommand.username.unique'
                }
            }
        }
        captcha blank:false, validator: RegisterController.captchaValidator
        sessionValue blank:true,nullable: true
        email blank: false, email: true, validator: {value,command->
            if (value){
                def user=AppUser.findByEmail(value)
                if (user){
                    return 'registerCommand.email.unique'
                }
            }
                
        }
        email2 validator: RegisterController.email2Validator
        password blank: false, minSize: 6, maxSize: 20, validator: RegisterController.passwordValidatorCustom
        password2 nullable: true, validator: RegisterController.password2Validator
        terms validator: {value,command->
            if (command.radio1=="above13" &&(value =="" ||value ==null)){
                return 'registerCommand.terms.blank.error'

            }
        },blank: true,nullable: true
        optin blank: true, nullable: true
        radio1 blank:true,nullable: true
    }
}

class ResetPasswordCommand {
    String username
    String password
    String password2

    static constraints = {
        password blank: false, minSize: 6, maxSize: 20, validator: RegisterController.passwordValidatorCustom
        password2 validator: RegisterController.password2Validator
    }
}