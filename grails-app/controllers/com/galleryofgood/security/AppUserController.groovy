package com.galleryofgood.security

import org.springframework.dao.DataIntegrityViolationException
import com.galleryofgood.user.UserDetails

class AppUserController {

    def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def create() {
        [appUserInstance: new AppUser(params)]
    }

    def save() {
        def appUserInstance = new AppUser(params)
        if (!appUserInstance.save(flush: true)) {
            render(view: "create", model: [appUserInstance: appUserInstance,activeLink:"You"])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUserInstance.id])
        redirect(action: "show", id: appUserInstance.id)
    }

    def show() {
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "list")
            return
        }

        [appUserInstance: appUserInstance]
    }

    def edit() {
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "list")
            return
        }

        [appUserInstance: appUserInstance]
    }

    def update() {
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (appUserInstance.version > version) {
                appUserInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'appUser.label', default: 'AppUser')] as Object[],
                        "Another user has updated this AppUser while you were editing")
                render(view: "edit", model: [appUserInstance: appUserInstance,activeLink:"You"])
                return
            }
        }

        appUserInstance.properties = params

//        bindData(appUserInstance,params,[exclude:'password'])

        if (!appUserInstance.save(flush: true)) {
            render(view: "edit", model: [appUserInstance: appUserInstance,activeLink:"You"])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUserInstance.id])
        redirect(action: "show", id: appUserInstance.id)
    }

    def delete() {
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "list")
            return
        }

        try {
            appUserInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    //New actions

    def deleteAccount() {
        def username=params.username
        if (AppUser.findByUsernameAndAccountLocked(username,true))   {
            [username:username]
        }else{
            redirect(url: "/")
        }
    }

    def deleted() {
        render view: 'deleteSuccess'
    }

    def confirmDelete() {
        def user = AppUser.findByUsername(params.username ?: "")
        user?.delete()
        def logoutUrl = "/appUser/deleted"
        redirect uri: "/j_spring_security_logout?spring-security-redirect=$logoutUrl"
    }

    def accountSettings = {
        render view: 'accountSettings', model: [user: springSecurityService.getCurrentUser(),activeLink:"You"]
    }

    def updateAccount = { UpdateAccountCommand command ->
        def user = AppUser.findByUsername(command.username)

        if (command.website?.equals("http://")){
            command.setWebsite("");
        }

        command.encryptedPassword = user.password;
        command.currentPassword = command.currentPassword?springSecurityService.encodePassword(command.currentPassword):''

        command.validate();
        if (command.hasErrors()) {
            println "errors found"
            render view: 'accountSettings', model: [user: command,activeLink:"You"]
        } else {
            AppUser.withTransaction { status ->
                user = AppUser.findByUsername(command.username)
                if (command.password)
                    user.password = command.password
                user.email = command.email
                user.website=command.website
                user.save()
                UserDetails.findByAppUser(user).setAvatarLocationUrl(params.avatar);
            }

            springSecurityService.reauthenticate command.username

            flash.message = "Your account settings have been updated."
            redirect(action: "accountSettings")
        }
    }

    def notificationSettings = {
        render view:'notificationSettings', model: [user: springSecurityService.getCurrentUser(),activeLink:"You"]
    }

    def saveNotificationSettings={
        def user = AppUser.findByUsername(springSecurityService.getCurrentUser()?.username ?: "")
        def userDetails = user?.userDetails
        if (!userDetails){
            userDetails = new UserDetails(params)
            user.userDetails=userDetails
        }else{
            userDetails.properties=params
        }
        userDetails.appUser=user
        userDetails.save()
        flash.message="Your notification settings have been updated."
        redirect action:'notificationSettings'
    }


    static final passwordValidatorCustom = { String password, command ->
        if (command.username && command.username.equals(password)) {
            return 'command.password.error.username'
        }
        if  (command.password?.contains(' ')){
            return 'command.password.error.spaces'
        }

    }

    static final currentPasswordValidatorCustom = { String currentPassword, command ->
        if (!currentPassword && command.password)
            return 'command.currentPassword.error.blank'
        if (currentPassword!=command.encryptedPassword && command.password)
            return 'command.currentPassword.error.match'
    }

}

class UpdateAccountCommand {
    String username
    String encryptedPassword
    String currentPassword
    String password
    String password2
    String email
    String website="http://"

    static constraints = {
        currentPassword blank: true, nullable: true, validator: AppUserController.currentPasswordValidatorCustom
        password blank: true, nullable: true, minSize: 6, maxSize: 20, validator: AppUserController.passwordValidatorCustom
        password2 blank: true, nullable: true, validator: RegisterController.password2Validator
        email blank: false, nullable: false, email: true, validator: {value,command->
            if (value!=command.email){
                def user=AppUser.findByEmail(value)
                if (user){
                    return 'registerCommand.email.unique'
                }
            }

        }
        website blank: true,nullable:true,url: true
    }
}