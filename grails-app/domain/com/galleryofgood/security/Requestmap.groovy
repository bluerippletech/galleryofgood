package com.galleryofgood.security

import com.galleryofgood.master.Base

class Requestmap{
    String id


	String url
	String configAttribute

	static mapping = {
        id generator: 'uuid'
		cache true
	}

	static constraints = {
		url blank: false, unique: true
		configAttribute blank: false
	}
}
