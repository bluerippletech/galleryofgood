package com.galleryofgood.security

import com.galleryofgood.master.Base

class Role{

    String id
    String description;
	String authority

	static mapping = {
        id generator: 'uuid'
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}
}
