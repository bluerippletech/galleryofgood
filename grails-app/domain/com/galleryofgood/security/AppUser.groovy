package com.galleryofgood.security

import com.galleryofgood.user.UserDetails
import com.galleryofgood.master.Base
import com.galleryofgood.gallery.Gallery
import com.galleryofgood.common.Comment

class AppUser{

	transient springSecurityService

    static transients = ['role', 'galleryName', 'activity', 'flagged'];

    String id

	String username
	String password
    String email
    String lastName
    String firstName
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

    Date dateCreated
    Date lastUpdated
    Date lastLoginDate
    
    
    //Additional User Details
    String ageGroup
    String optin
    String hasLoggedIn="No"
    String website
    

    static hasOne=[userDetails:UserDetails]

	static constraints = {
		username blank: false, unique: true
		password blank: false
        email unique:true,email:true
        lastName blank: true, nullable: true
        firstName blank: true, nullable: true
        ageGroup blank: true, nullable:true
        optin blank: true, nullable:true
        hasLoggedIn blank:true,nullable: true
        website blank:true,nullable:true,url: true
        lastLoginDate blank: true,nullable: true
	}

	static mapping = {
		password column: '`password`'
        id generator:'uuid'
	}

	Set<Role> getAuthorities() {
		AppUserRole.findAllByAppUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

    String getRole(){
        String temp = ""
        this.getAuthorities().each{
                temp += "${it.description}";
        }
        return temp;
    }

    Long getActivity(){
        return Comment.countByUsername(this.username) as Long;
    }

    Long getFlagged(){
        return Gallery.findByOwner(this.username)?.artworks?.flags?.size()?:0 as Long;
    }

    String getGalleryName(){
        return Gallery.findByOwner(this.username).galleryName;
    }

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
