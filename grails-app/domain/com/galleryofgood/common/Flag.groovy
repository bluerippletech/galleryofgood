package com.galleryofgood.common

import com.galleryofgood.master.Base

class Flag extends Base{


    String flaggingUser
    String comment

    String flaggedObject
    String flaggedObjectId

    String flaggedUser
    String sectionFlagged

    String status="Open"

    static constraints = {
        comment(maxSize:255)
        flaggedObjectId(blank: true,nullable: true)
        flaggedObject(blank: true,nullable: true)
        status(inList: ["Open","Resolved"])
        flaggedUser(blank: true,nullable: true)
        sectionFlagged(blank: true,nullable: true)
    }
}
