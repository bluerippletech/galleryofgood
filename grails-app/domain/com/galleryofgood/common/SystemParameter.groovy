package com.galleryofgood.common

import com.galleryofgood.master.Base

class SystemParameter extends Base{

    String name;
    String content;

    static constraints = {

        name(inList:['AdminEmail','SupportEmail','ContactUsEmail'], maxSize:255)
        content(maxSize:255);

    }
}
