package com.galleryofgood.common

import com.galleryofgood.security.AppUser
import com.galleryofgood.master.Base

class Notification extends Base {

    AppUser subscriber;
    String message;
    boolean processed = false;

    Date creationDate;
    String schedule;

    String topicId;
    String topicLocation;

    String type;

    static constraints = {
        type(maxSize: 255, blank: true, nullable: true);
        message(maxSize: 4000, blank: false);
    }
}
