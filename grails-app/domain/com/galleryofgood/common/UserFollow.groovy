package com.galleryofgood.common

import com.galleryofgood.master.Base

class UserFollow extends Base{

    String followerId;
    String followingId;
    Date subscribed = new Date();

    static constraints = {
        followerId(blank:false);
        followingId(blank:false);
    }
}
