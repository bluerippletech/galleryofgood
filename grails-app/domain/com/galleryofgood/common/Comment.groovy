package com.galleryofgood.common

import com.galleryofgood.master.Base

class Comment extends Base{

    String username;
    String content;
    Date date = new Date();
    String status = "New!";
    String location;
    String locationId;

    static transients = ['inGallery', 'inArtwork', 'inTimeline'];

    static hasMany = [flags:Flag]

    static constraints = {
        username(blank: false);
        content(blank: false, maxSize: 500);
        location(blank: false);
        locationId(blank: false);
        //status(inList: ["Approved", "Published", "Reported", "New!"]);
    }

    boolean inGallery(){
        if(location=="Gallery")
            return true;
        else
            return false;
    }

    boolean inArtwork(){
        if(location=="Artwork")
            return true;
        else
            return false;
    }

    boolean inTimeline(){
        if(location=="Artist Timeline")
            return true;
        else
            return false;
    }
}
