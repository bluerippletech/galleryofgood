package com.galleryofgood.common

import com.galleryofgood.master.Base

class Homepage extends Base {
    //main homepage

    String      sectionHeader1
    String      boxheadline1
    String      boxcopy1
    String      buttontext1
    String      buttonlink1

    String      sectionHeader2
    String      boxheadline2
    String      boxcopy2
    String      buttontext2
    String      buttonlink2

    String      sectionHeader3
    String      boxheadline3
    String      boxcopy3
    String      buttontext3
    String      buttonlink3




    //Acclaim Section
    String      acclaimHeader
    String      testimonial1
    String      headline1
    String      imageurl1
    String      testimonial2
    String      headline2
    String      imageurl2
    String      testimonial3
    String      headline3
    String      imageurl3

    //slider images
    String featureImageUrl1
    String featureImageLink1
    String featureImageUrl2
    String featureImageLink2
    String featureImageUrl3
    String featureImageLink3
    String featureImageUrl4
    String featureImageLink4
    String featureImageUrl5
    String featureImageLink5

    String featureImageThumbnail1
    String featureImageThumbnail2
    String featureImageThumbnail3
    String featureImageThumbnail4
    String featureImageThumbnail5


    String featuredArtwork1
    String featuredArtwork2
    String featuredArtwork3
    String featuredArtwork4
    String featuredArtwork5
    String featuredArtwork6
    String featuredArtwork7
    String featuredArtwork8
    String featuredArtwork9
    String featuredArtwork10

    //Retrospective
    String retrospectiveContent
    String retrospectiveImage
    String retrospectiveUrl

    String featuredArtworkByType1
    String featureArtworkType1
    String featuredArtworkByType2
    String featureArtworkType2
    String featuredArtworkByType3
    String featureArtworkType3

    String timelinelink



    static constraints = {
        acclaimHeader(blank:true,nullable: true)
        testimonial1(blank: true,nullable:true)
        headline1(blank: true,nullable: true)
        imageurl1(blank: true,nullable:true)
        testimonial2(blank: true,nullable:true)
        headline2(blank: true,nullable: true)
        imageurl2(blank: true,nullable:true)
        testimonial3(blank: true,nullable:true)
        headline3(blank: true,nullable: true)
        imageurl3(blank: true,nullable:true)

        sectionHeader1(blank:true,nullable: true)
        boxheadline1(blank:true,nullable: true)
        boxcopy1(blank:true,nullable: true)
        buttontext1(blank:true,nullable: true)
        buttonlink1(blank:true,nullable: true,url: true)

        sectionHeader2(blank:true,nullable: true)
        boxheadline2(blank:true,nullable: true)
        boxcopy2(blank:true,nullable: true)
        buttontext2(blank:true,nullable: true)
        buttonlink2(blank:true,nullable: true,url: true)

        sectionHeader3(blank:true,nullable: true)
        boxheadline3(blank:true,nullable: true)
        boxcopy3(blank:true,nullable: true)
        buttontext3(blank:true,nullable: true)
        buttonlink3(blank:true,nullable: true,url: true)

        featuredArtwork1(blank: true,nullable:true)
        featuredArtwork2(blank: true,nullable:true)
        featuredArtwork3(blank: true,nullable:true)
        featuredArtwork4(blank: true,nullable:true)
        featuredArtwork5(blank: true,nullable:true)
        featuredArtwork6(blank: true,nullable:true)
        featuredArtwork7(blank: true,nullable:true)
        featuredArtwork8(blank: true,nullable:true)
        featuredArtwork9(blank: true,nullable:true)
        featuredArtwork10(blank: true,nullable:true)

        retrospectiveContent(blank: true,nullable: true,maxSize: 2000)
        retrospectiveImage(blank: true,nullable: true)
        retrospectiveUrl(blank: true,nullable: true)


        featuredArtworkByType1(blank: true,nullable: true)
        featureArtworkType1(blank: true,nullable: true)
        featuredArtworkByType2(blank: true,nullable: true)
        featureArtworkType2(blank: true,nullable: true)
        featuredArtworkByType3(blank: true,nullable: true)
        featureArtworkType3(blank: true,nullable: true)

        featureImageUrl1(blank: true,nullable:true)
        featureImageLink1(blank: true,nullable: true)
        featureImageUrl2(blank: true,nullable:true)
        featureImageLink2(blank: true,nullable: true)
        featureImageUrl3(blank: true,nullable:true)
        featureImageLink3(blank: true,nullable: true)
        featureImageUrl4(blank: true,nullable:true)
        featureImageLink4(blank: true,nullable: true)
        featureImageUrl5(blank: true,nullable:true)
        featureImageLink5(blank: true,nullable: true)

        featureImageThumbnail1(blank: true,nullable:true)
        featureImageThumbnail2(blank: true,nullable:true)
        featureImageThumbnail3(blank: true,nullable:true)
        featureImageThumbnail4(blank: true,nullable:true)
        featureImageThumbnail5(blank: true,nullable:true)


        timelinelink(blank: true,nullable: true)


    }
}
