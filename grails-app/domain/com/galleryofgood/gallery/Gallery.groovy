package com.galleryofgood.gallery

import com.galleryofgood.common.Flag
import com.galleryofgood.master.Base
import com.galleryofgood.common.Comment
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import com.galleryofgood.common.UserFollow
import com.galleryofgood.security.AppUser

class Gallery{

    String id
    
    String owner
    String galleryName
    String description
    String artworkType
    String ownerYearOfBirth
    String galleryOwner="Myself"
    String galleryNameUrl

    String artisticJourney=""

    List youngArtists

    Date dateCreated
    Date lastUpdated

    static hasMany=[youngArtists:YoungArtist,
                    artworks:Artwork,timelines:Timeline,
                    flags:Flag
                    /*comments:com.galleryofgood.common.Comment*/]

    static mapping = {
        youngArtists cascade: 'all-delete-orphan'
        artworks cascade: 'all-delete-orphan'
        timelines cascade: 'all-delete-orphan'
        artworks sort: 'dateCreated', order: 'desc'
        version false
        id generator:'uuid'
    }

    static transients=['isForYoungArtist','followerCount'];

    boolean isForYoungArtist(){
        if (galleryOwner == "YoungArtist")
            return true;
        else
            return false;
    }
    

    static constraints = {
        ownerYearOfBirth(blank: true,nullable: true)
        galleryNameUrl(blank:false,nullable: false,unique:true,validator:galleryNameUrlValidator)
        galleryName(blank: false,nullable: false,unique:true, maxSize:255,validator: galleryNameValidator)
        description(maxSize:4000);
        galleryOwner(inList: ["Myself","YoungArtist"])
        galleryName validator: galleryNameValidator
        artworkType(inList: ["Drawing","Painting","Crafts","Mixed Media","Sculpture","Tinker Toys","Photography","Digital"])
        artisticJourney(blank: true,nullable: true,maxSize:4000)
        /*comments(lazy:false, cascade:"all,delete-orphan");*/
    }


    static final galleryNameValidator = { String galleryName, obj ->
        if (galleryName.matches('^.*[!@#$%^&].*$')) {
            return 'gallery.galleryName.specialCharacters.error'
        }
        if (galleryName.toLowerCase() in ConfigurationHolder.config.reservedNames||obj.galleryNameUrl.toLowerCase() in ConfigurationHolder.config.reservedNames){
            return 'gallery.galleryName.reserved.error'
        }
    }

    static final galleryNameUrlValidator = { val, obj ->
        if (val.matches('^.*[!@#$%^&].*$')) {
            return 'gallery.galleryName.specialCharacters.error'
        }
        if (val.toLowerCase() in ConfigurationHolder.config.reservedNames){
            return 'gallery.galleryName.reserved.error'
        }
    }


    def afterDelete(){
        Comment.withNewSession {
            try{
                Comment.findAllByLocationAndLocationId('Gallery', id)*.delete();
            }catch (Exception e){
                println(e);
            }
        }
        UserFollow.withNewSession {
            try{
                UserFollow.findAllByFollowingId(id)*.delete();
                UserFollow.findAllByFollowerId(AppUser.findByUsername(this.owner).id)*.delete();
            }catch (Exception e){
                println(e);
            }
        }
    }

    def getFollowerCount(){
        def totalFollowers = UserFollow.countByFollowingId(id)
        return totalFollowers
    }

    def getComments(){
        def comments = Comment.findAllByLocationAndLocationId("Gallery",id)
        return comments
    }

    String toString(){
        galleryName
    }
}
