package com.galleryofgood.gallery

import com.galleryofgood.artwork.ArtworkUtils
import com.galleryofgood.master.Base

class YoungArtist extends Base {

    String nickname
    String yearOfBirth

    Gallery gallery

    static belongsTo=[gallery:Gallery]

    static transients = ['ageGroup']

    static constraints = {
        nickname(unique: true, blank: false, nullable: false, maxSize:12, minSize: 1);
    }

    def getAgeGroup(){
        def ageGroup=ArtworkUtils.ageGroups.find{it.value.contains(new Date().getAt(Calendar.YEAR).toInteger() - yearOfBirth.toInteger())}
        return ageGroup.key
    }

    def afterInsert(){
        Timeline.withNewSession{
            def timeline = Timeline.findOrSaveWhere(artistName: nickname, gallery: gallery)
        }
    }


    def beforeDelete(){
        //TODO: disassociate from gallery
//        gallery.removeFromYoungArtists(this);

        //TODO: remove related artworks
        Artwork.withNewSession{
            def artworks = Artwork.findAllByArtist(nickname)
            artworks*.delete();
        }

        //TODO: remove related timeline
        Timeline.withNewSession{
            def timeline = Timeline.findByArtistName(nickname)
            timeline?.delete()
        }
    }

    def beforeUpdate(){
        if (isDirty('nickname')){
            def previousNickname= getPersistentValue('nickname')
            Artwork.withNewSession{
                def artworks = Artwork.findAllByArtistAndGallery(previousNickname,gallery)
                artworks*.artist= nickname
                artworks*.save();

                def timeline = Timeline.findByGalleryAndArtistName(gallery,previousNickname)
                if (timeline){
                    timeline.artistName=nickname
                    timeline.save();
                }
            }
            println previousNickname
            println nickname
        }else{
            println "nickname not changed"
        }





    }




}
