package com.galleryofgood.gallery

import org.grails.taggable.Taggable
import com.galleryofgood.artwork.ArtworkUtils
import com.galleryofgood.common.Flag
import com.galleryofgood.master.Base
import com.galleryofgood.common.Comment
import org.grails.taggable.TagLink
import org.joda.time.LocalDate
import com.galleryofgood.common.SystemParameter


class Artwork extends Base implements Taggable {

    def amazonImageService,galleryService

    String title
    String fileTitle


    String url
    String artworkDetailImageUrl
    String thumbnailUrl

    
    String artworkType
    String artist
    Integer artworkAge
    
    String dimensions = ""
    String materials
    String description = ""
    String messFactor

    String showInTimeline

    String status="New"


    static belongsTo=[gallery:Gallery]
    static hasMany = [flags:Flag
                       /*comments: com.galleryofgood.common.Comment*/]

    static transients = ['ageGroup','totalComments','artistAgeGroup']

    static constraints = {
        artworkType(inList: ["Drawing","Painting","Crafts","Mixed Media","Sculpture","Tinker Toys","Photography","Digital"])
        messFactor(blank: true,nullable: true, inList: ["No muss, no fuss","Spread out the newspaper","Grab a smock","Take cover!"])

        dimensions(blank:true,nullable: true)
        materials(blank: true,nullable: true)
        description(blank:true,maxSize: 4000,nullable: true)

        title(blank: false,nullable:false,maxSize:100)
        showInTimeline(blank: true,nullable:true)
        artworkAge(nullable: false,blank:false, validator:{ val,obj->
            def currentYear = (new LocalDate()).getYear()
            def artistAge = 0;
            if (obj.gallery?.galleryOwner?.equals("Myself")){
                artistAge = currentYear - Integer.parseInt(obj.gallery.ownerYearOfBirth)
            }else{
                artistAge = currentYear - Integer.parseInt(YoungArtist.findByNickname(obj.artist).yearOfBirth)
            }

            if (artistAge<val){
                return 'artwork.invalid.artwork.age'
            }

        })


        artist(blank: true,nullable: true,validator: {val,obj->
            if ("YoungArtist".equals(obj?.gallery?.galleryOwner)&&(val=="" || val==null))
                return 'artwork.artist.blank.error'
        })

        url(blank:true,nullable: true)
        artworkDetailImageUrl(blank: true,nullable:true)
        thumbnailUrl(blank:true,nullable:true)
        //artworkComments(lazy:false);
        status(blank: true,nullable:true,inList:['New','Approved','Disapproved'])
    }


    def getAgeGroup(){
        def ageGroup=ArtworkUtils.ageGroups.find{it.value.contains(artworkAge)}
        return ageGroup.key
    }

    def getArtistAgeGroup(){
        def currentYear = (new LocalDate()).getYear()
        def artistYearOfBirth
        if (gallery?.galleryOwner?.equals("Myself")){
            artistYearOfBirth=gallery?.ownerYearOfBirth
        }else{
            artistYearOfBirth=YoungArtist.findByNickname(artist)?.yearOfBirth
        }
        return galleryService.getAgeGroup(currentYear - Integer.parseInt(artistYearOfBirth?:currentYear))
    }

    def artworkService
    def afterInsert(){
        artworkService.sendNewArtworkUploadedNotification(gallery,title)
    }

    def beforeDelete(){
//        gallery.removeFromArtworks(this)
        //TODO: remove images from amazon.


        TagLink.withSession{
            amazonImageService.deleteImage(url)
            amazonImageService.deleteImage(artworkDetailImageUrl)
            amazonImageService.deleteImage(thumbnailUrl)
        }

        TagLink.withNewSession{
            def tagLinks =TagLink.findAllByTagRef(id);
            tagLinks*.delete();


            def comments= Comment.findAllByLocationAndLocationId('Artwork', id)
            comments*.delete();
        }

    }

    def getTotalComments(){
        def comments = Comment.findAllByLocationAndLocationId("Artwork",id)
        return comments?.size()

    }
}
