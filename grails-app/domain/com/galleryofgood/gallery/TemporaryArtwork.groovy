package com.galleryofgood.gallery

import com.galleryofgood.master.Base

class TemporaryArtwork extends Base {

    String artworkUrl
    Artwork artwork
    String originalFilename


    static constraints = {
        artworkUrl(blank: false,nullable: false)
        artwork(blank:true,nullable: true)

    }
}
