package com.galleryofgood.gallery

import com.galleryofgood.master.Base
import com.galleryofgood.common.Comment
import com.galleryofgood.common.Homepage

class Timeline extends Base{

    String artisticJourney
    String artistName

    static belongsTo=[gallery:Gallery]
    static hasMany = [/*timelineComments: com.galleryofgood.common.Comment*/];

    static constraints = {
        artistName(blank: false,nullable:false)
        artisticJourney(blank: true, nullable:true, maxSize: 1000)
        //timelineComments(lazy:false);
    }

    def beforeDelete(){

        //TODO: disassociate from gallery.
//        gallery.removeFromTimelines(this);
        //TODO: remove related Comments
        def comments = Comment.findAllByLocationAndLocationId('Artist Timeline', id);
        comments*.delete();
    }

    def afterDelete(){
        Homepage.withNewSession {
            def homepageInstance = Homepage.findByTimelinelink(this.id)
            if(homepageInstance){
                homepageInstance.timelinelink = null
                if(!homepageInstance.save()){
                    println homepageInstance.errors;
                }
            }
        }
    }
}
