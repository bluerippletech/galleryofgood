package com.galleryofgood.admin

class Advisor {

    String name
    String title
    String url
    String expertise
    String bio

    static transients = ['formattedExpertise']

    static constraints = {
        name(blank: false,nullable: false)
        title(blank: false,nullable: false)
        url(blank: true,nullable: true,maxSize: 4000)
        expertise(blank: true,nullable: true)
        bio(blank: true,nullable: true,maxSize: 4000)

    }

    def getFormattedExpertise(){
        def finalString = ""
        if(expertise){
        def list = expertise?.split(",")
        def total = list.size()
        def i = 1
        list.each {
            if (i==1){
                finalString+=it
            }else if (i==total){
                finalString+=" and ${it}"
            }else{
                finalString+=", ${it}"
            }
            i+=1
        }
        return finalString
        }
    }
}
