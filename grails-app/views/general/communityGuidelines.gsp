<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='community.guidelines'/></title>
    <r:require modules="foundation"/>
</head>

<body>
<!-- main content area -->
<div class="row community_guidelines">
    <div class="eight columns">
        <h2 class="heading color-black">Community Guidelines</h2>
        <p class="cg-toptxt">We want to make Gallery of Good a good clean place for kids and parents to showcase art. While we believe in the magic of purple cows, abstract art and definitely coloring outside the lines, there are some rules we all need to follow to keep things wholesome and safe.</p>
        <br /><br />
        <hr />
        <br /><br />
    </div>
    <div class="four columns community_guidelines-tip">
        <r:img alt="Note" uri="/images/note.jpg" id="create_account_note" />
        <p style='text-align:left;'>Want the basics of opening a gallery? Head over to <a href="${createLink(controller:'general',action:'gettingStarted')}">Getting Started</a> and get those burning questions answered.
            <br /><br />
            At anytime, you can read our <a href="${createLink(controller:'general',action:'privacyPolicy')}">Privacy Policy</a> & <a href="${createLink(controller:'general',action:'termsOfService')}">Terms of Service</a>.
        </p>

    </div>
</div>
<div class="row">
    <div class="eight columns guidelines">
        <h5>No Art That&apos;s Inappropriate For Little Eyes</h5>
        <p style='text-align:justify;'>
            Gallery of Good is a family-friendly space. We ask that you refrain from posting art that features violence, nudity, racist or derogatory terms or depictions. If you&apos;re unsure, ask yourself if it&apos;s appropriate for a four year old. All community members are invited to flag an artwork if they think it crosses a line. If we agree, we may remove it without notifying you.
        </p>
        <br />
        <h5>Thoughtful Comments Only</h5>
        <p style='text-align:justify;'>
            When commenting on other community members&apos; artwork or comments, please provide only positive, constructive feedback. We have a zero tolerance policy for inappropriate comments or harsh critiques (Usernames, gallery and artwork titles must also be rated G). Remember, all the art in our online museum is the work of children under 19. Comments should support the artists&apos; efforts and encourage them to develop their talent. Be kind and generous with your comments!
        </p>
        <br />
        <h5>Don&#8217;t Ask, Don&#8217;t Tell</h5>
        <p style='text-align:justify;'>
            Do not give out personal information, including passwords, to anyone - and don&#8217;t ask other artists for details about their life either! It really is all about the art on Gallery of Good. Let&#8217;s keep it that way.
        </p>
        <br />
        <h5>Original Art Only, Please!</h5>
        <p style='text-align:justify;'>
          Gallery of Good prides itself on showcasing incredible works of original art. We ask that you don&apos;t upload any material that the artist did not create or that may infringe on the intellectual property of others. Besides, who needs Renoir or Monet when you&apos;ve got your own artiste at home?
        </p>
        <br />
        <h5>Ask Permission Before You Play</h5>
        <p style='text-align:justify;'>
            If you are a teacher, a proud aunt or a supportive family friend of a young artist, please get permission from the child&apos;s parent(s) or guardian(s) before opening a gallery to showcase his or her artwork.
        </p><p style='text-align:justify;'>
        While you&apos;re at it, remember to get permission from parents or guardians of any children featured in an uploaded photograph.
        </p>
        <br />
        <h5>Flag inappropriate Comments or Artwork</h5>
        <p style='text-align:justify;'>
            We take great pains to moderate the content that goes on the site. And as a community member and gallery owner, you have the power to reject a comment on your artwork that you deem unacceptable. Sometimes, however, an unsavory comment or inappropriate piece of art might fall through the cracks. If you see something offensive or not for little eyes, please use the Flag button to get our attention. We will take action pronto!
        </p>
        <br />
        <h5>Stay On Topic</h5>
        <p style='text-align:justify;'>
            There are lots of places on the Internet to chat about your home business, little Johnny&apos;s winning soccer goal and the great deal you got on shoes last week. This is not one of them. Please keep comments strictly about the artwork. The artists appreciate your focus!
        </p>
        If you own or represent a business and would like an opportunity to advertise or discuss a partnership, please <a href="${createLink(controller:'contactUs',action:'index')}">contact us</a>.
    </div>

</div>
<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
</div>



<!-- main content area -->



</body>
