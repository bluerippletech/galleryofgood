<head xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='getting.started.title'/></title>
    <r:require modules="foundation"/>
</head>

<body>
<!-- main content area -->
<div class="row general_guide">
    <div class="eight columns">
        <h2 class="heading color-black">Getting Started</h2>
        <p class="cg-toptxt">
            Gallery of Good is a family-friendly virtual space where kids showcase their creativity through their artworks! From fingerpaintings to watercolors, crafts to sculptures, if you can see it and photograph it, we can display it on our online galleries.
            <br/><br/>
            But it doesn’t stop there! You can track your little artists’ progress year over year and categorize their creations in whatever way makes sense so you have a live record of their artistic development.
            <br/><br/>
            Once you’ve created a gallery and have uploaded your child’s art, you are free to share it with friends, family and the always supportive Gallery of Good community. Our <a
                href="${createLink(controller: 'general', action: 'twoCents')}">2 Cents Circle</a> is a group of artistic advisors who are always on the lookout for busy young artists who could use an encouraging word.
            <br/><br/>
            We also aim to inspire your young Picassos with art activities designed to stimulate their creativity even more!
            <br/><br/>
            So, c’mon in and poke around. We’re glad you’re here!
        </p>
        <br/><br/>

    </div>

    <div class="four columns community_guidelines-tip">
        <r:img alt="Note" uri="/images/note.jpg" id="create_account_note"/>

        <p>A Step-by-Step gallery guide is available for registered members.</p>

        <p>Ready to open a gallery?
            <br/>
            <a href="${createLink(controller: 'register', action: 'index')}">Sign-up for a free account.</a>
        </p>

        <p>At anytime, you can read our
            <br/>
            <a href="${createLink(controller: 'general', action: 'communityGuidelines')}"
               xmlns="http://www.w3.org/1999/html">Community Guidelines.</a>

        </p>
    </div>
</div>

<div class="row">
    <div class="eight columns guidelines">
        <ul id="link-list" class="link-list">
            <li><a href="#who_can_open_gallery">Who Can Open a Gallery?</a></li>
            <li><a href="#is_free">Is There a Fee?</a></li>
            <li><a href="#type_of_artwork">What Types of Artworks Can I Display in My Gallery?</a></li>
            <li><a href="#how_many_artwork">How Many Artworks Can I Add to a Gallery?</a></li>
            <li><a href="#artist_timeline">What is the Artist Timeline?</a></li>
            <li><a href="#safe_account">Is it Safe to Open an Account?</a></li>

        </ul>

        <br/>
        <hr/>

        <div class="pgg-anchors">
            <br/>
            <a name="who_can_open_gallery">
                <h5>Who Can Open a Gallery</h5>
            </a>

            <p style="text-align:justify;">Absolutely anyone may open a gallery to show off a little prodigy’s great works of art! This is a kids-only art zone though, so please upload artwork created by children up to 18 years old.
                <br/><br/>
                Parents or legal guardians of children under 13 may open and manage their children’s galleries. You’ll just need to <a href="${createLink(controller: 'register', action: 'index')}">sign up for a free account</a> before you set up your gallery.
                <br/><br/>
                If you want to use Gallery of Good for commercial purposes, please <a
                    href="${createLink(controller: 'contactUs', action: 'index')}">contact us</a>.
            </p>
            <br/>
            <a name="is_free">
                <h5>Is There a Fee?</h5>
            </a>

            <p style="text-align:justify;">No, it's absolutely free!</p>


            <br />

            <a name="type_of_artwork">
                <h5>What Types of Artworks Can I Display in My Gallery?</h5>
            </a>

            <p style='text-align:justify;'>You can showcase any type of original, visual art that falls into these categories:</p>
            <ul>
                <li><b>Drawings—</b>From squiggles to simple stick figures to complex composition, comics or cartoons created with pencils, pens, charcoal, etc.
                </li>
                <li><b>Paintings—</b>Watercolor, oil, acrylic or tempera, it could be painted with professional brushes or spread with chubby fingers.
                </li>
                <li><b>Crafts—</b>Sewing projects like puppets, dolls or pillows, projects with glue and paper, greeting cards, cardboard cut-outs, costumes, macaroni art, etc.
                </li>
                <li><b>Mixed Media—</b>Any artwork that uses two or more mediums combined. Think mosaics or collages.
                </li>
                <li><b>Sculpture—</b>Clay, play dough, plaster or any other moldable material for 2d relief or 3d creations.
                </li>
                <li><b>Tinker Toys—</b>
                </li>
                <li><b>Photography—</b>Photos of any style and subject.
                </li>
                <li><b>Digital—</b>Works of art created on computer, tablet or smart phone.</li>

            </ul>

            <p style='text-align:justify;'>Can't find a category for your artwork? <a
                    href="${createLink(controller: 'contactUs', action: 'index')}">Drop us a line</a> and we'll help you out.
            </p>

            <p style='text-align:justify;'>Gallery of Good reserves the right to remove any artwork that contains offensive, violent, or inappropriate subject matter for our young audience. Read our <a
                    href="${createLink(controller: 'general', action: 'communityGuidelines')}">Community Guidelines</a> and <a
                    href="${createLink(controller: 'general', action: 'termsOfService')}">Terms of Service</a> to learn more.
            </p>

            <br/>
            <a name="how_many_artwork">

                <h5>How Many Artworks Can I Add to a Gallery?</h5>
            </a>

            <p style='text-align:justify;'>As many as you wish! We do have guidelines on maximum file sizes (5MB or less). Check out our Step-by-Step Gallery Guide after you <a
                    href="${createLink(controller: 'register', action: 'index')}">sign-up</a>.</p>
            <br/>

            <a name="artist_timeline">
                <h5>What is the Artist Timeline?</h5>

            </a>

            <p style='text-align:justify;'>The Artist Timeline is a collection of artworks that best represent the artist's progress year over year. This is the place where artists can share the story of their artistic journey. To see how it works, please visit the
                featured retrospective in the <a href="${createLink(controller: 'artwork', action: 'landing')}">Artworks</a> section.
            </p>
            <br/>

            <a name="safe_account">
                <h5>Is it Safe to Open an Account?</h5>
            </a>

            <p style='text-align:justify;'>We are committed to protecting the privacy and the safety of our members, especially our young artists. We want to ensure that all members enjoy the site in a safe environment. Here's how we're doing it:</p>
            <ul>
                <li>We never ask for real names. When you sign up we ask you to create a unique, unidentifiable username.</li>
                <li>We never sell any member's information.</li>
                <li>We abide by the Children's Online Privacy Protection Act.</li>
                <li>We evaluate all artworks before they are published to ensure they meet with our <a
                        href="${createLink(controller: 'general', action: 'communityGuidelines')}">Community Guidelines</a>.
                </li>

                <li>Members may choose to display others’ comments on their work (or not) before they are published.</li>
                <li>Our <a
                        href="${createLink(controller: 'general', action: 'communityGuidelines')}">Community Guidelines</a> enforce a wholesome, kid-friendly community. Members who do not abide by it are unequivocally asked to leave.
                </li>
                <li>We offer community members the opportunity to flag comments or other content they feel is inappropriate.</li>
                <li>Our commitment to online safety and member privacy is detailed in our <a
                        href="${createLink(controller: 'general', action: 'privacyPolicy')}">Privacy Policy</a> and <a
                        href="${createLink(controller: 'general', action: 'termsOfService')}">Terms of Service</a>.</li>
            </ul>
            <br/>
        </div>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p></p>
        <br/>
    </div>

</div>
<!-- main content area -->

</body>
