<%@ page import="com.galleryofgood.admin.Advisor" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='twocentscircle.title'/></title>
    <r:require modules="foundation,lazyload"/>
    <r:script>
        $(function(){
            $('img.lazy').lazyload({effect : "fadeIn"});
        })
    </r:script>
</head>
<body>
<!-- main content area -->


<div class="row">
    <div class="twelve columns">
        <p></p>
    </div>
</div>
<div class="row mgp-mid">
    <div class="eight columns">
        <h2 class="heading color-black">The 2 cents circle</h2>
        <p>2¢c is Gallery of Good&apos;s artistic advisory committee made up of creative professionals, artists, art teachers, parents and individuals supportive of the visuals arts. They help shape the artistic skills and nurture creative thinking in each of our young artists by giving encouraging feedback through comments. We are honored to have these mulit-talented professionals support our community.</p>
    </div>
    <div class="four columns heading color-black cca-price">
        2<span>¢</span>c
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p><br /><br /></p>
    </div>
</div>
<div class="row">
    <div class="twelve columns">

        <div id="twoCents" class="artwork_list cca-artwork_list">

            <g:each in="${Advisor.findAll()}" status="i" var="advisorInstance">
                <g:set var="advisors" value="advisorInstance"/>
               <g:if test="${i%3==0}">
                   <div class="twelve columns" style="margin:0;padding:0 18px 0 0;">
               </g:if>
                <div>
                <div style="width: 300px; height: 245px; overflow: hidden;background-color:white;padding: 0 18px 5px 0">
                    <img class="lazy" src="" alt="" data-original="${advisorInstance?.url}" style="max-width:100%;max-height:100%"/>
               </div>
                <div class="artwork ma-artwork">
                        <h5>${fieldValue(bean: advisorInstance, field: "name")} : ${fieldValue(bean: advisorInstance, field: "title")}
                            <p>${advisorInstance.getFormattedExpertise()}</p></h5>
                        <br />
                        <p>${advisorInstance?.bio}</p>
                    </div>
                </div>
                <g:if test="${i%3==2}">
                    </div>
                </g:if>

            </g:each>

        </div>

        <div class="clear-both"></div>

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <br /><br /><br /><br /><br /><br />
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        <br /><br /><br />
    </div>
</div>


<div class="row">
    <div class="twelve columns">
        <br /><p></p><br />
    </div>
</div>
<!-- main content area -->

</body>
