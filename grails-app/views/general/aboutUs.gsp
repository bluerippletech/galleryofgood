<%@ page import="com.galleryofgood.common.Homepage" %>
<head xmlns="http://www.w3.org/1999/html">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='about.us.title'/></title>
    <r:require modules="foundation"/>
</head>
<body>

<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <h2 class="heading color-black" style="padding-bottom:0">About Us</h2>
        <p>At Gallery of Good, we nurture creativity in children through the visual arts.
        We could list a thousand ways to do it, but here are our favorites:</p>
        <div class="gg-timeline">
            <h2 class="heading color-gray" style="text-align:center;background-color:white;">Gallery of Good</h2>
            <ul class="timeline clearfix">
                <li class="right-column timelineUnit"><div class="timelineEmptySpace"/></li>
                <li class="left-column timelineUnit"><div class="timelineEmptySpace2"/></li>
                <li class="timelineUnit left-column">
                    <div class="timelineUnitContainer">
                        <div class="timelineLine"></div>
                        <div class="tluc-top"></div>
                        <div class="tluc-inner">
                            <r:img uri="/images/timeline1_StartThemYoung.png" alt="" />
                        </div>
                        <div class="tluc-bottom"></div>
                    </div>
                    <div class="timelineText">
                        <p>Childhood is the perfect time to develop creative thinking skills that will last a lifetime. Kids are naturally less inhibited and more willing to express their vivid imaginations. Research tells us that children who are encouraged to be creative have better self-esteem, optimal brain development and do better in math and reading. And when they become grown-ups, they’re the innovative thinkers, powerful leaders and change agents that make the world go round.
                        </p>
                    </div>
                </li>

                <li class="timelineUnit right-column">
                    <div class="timelineUnitContainer">
                        <div class="timelineLine"></div>
                        <div class="tluc-top"></div>
                        <div class="tluc-inner">
                            <r:img uri="/images/timeline2_EquipYourArtists.png" alt="" />
                        </div>
                        <div class="tluc-bottom"></div>
                    </div>
                    <div class="timelineText">
                        <p>There are endless ways of introducing creativity to your child, including music and performing arts. But at Gallery of Good, we focus on the visual arts. Why? Because all children draw. Put a crayon, pencil, piece of chalk or stick in a child’s hand and she will create something. Drawing is perhaps the most natural and accessible way for children to practice creativity. </p>
                    </div>
                </li>
                <li class="timelineUnit left-column">
                    <div class="timelineUnitContainer">
                        <div class="timelineLine"></div>
                        <div class="tluc-top"></div>
                        <div class="tluc-inner">
                            <r:img uri="/images/timeline3_KeepThemCreating.png" alt="" />
                        </div>
                        <div class="tluc-bottom"></div>
                    </div>
                    <div class="timelineText">
                    <p>The older we get, the more we’re afraid to look silly, pay attention to stodgy rules and start to color inside the lines! It’s no wonder that creativity is a skill that can go limp from disuse. At Gallery of Good, we‘re keeping those precious creative juices flowing in the early years right on through to adolescence.</p>
                    <p>Our <a href="${createLink(controller: 'artwork', action: 'landing')}">galleries</a> are the perfect place to proudly display kids’ art. Our artistic timeline offers a visual way to track their artistic journey. Our creative challenges stimulate fresh inspiration and ideas. What’s more, our <a href="${createLink(controller: 'general', action: 'twoCents')}">2 Cents Circle</a>, a merry band of artistic advisors, is always offering encouraging words and constructive feedback. </p>
                    </div>
                </li>
                <li class="timelineUnit right-column">
                    <div class="timelineUnitContainer">
                        <div class="timelineLine"></div>
                        <div class="tluc-top"></div>
                        <div class="tluc-inner">
                            <r:img uri="/images/timeline4_ImagineThePossibilities.png" alt="" />
                        </div>
                        <div class="tluc-bottom"></div>
                    </div>
                    <div class="timelineText">
                    <p>We invite you to regularly clear off the kitchen table, dump out all the art supplies in the house and let them have at it! Better yet, join in and create with your kids. When you’re done, take those pictures and upload all that art so you – and others – can admire your creations and use them as inspiration for more.</p>
                    <strong>Get messy! Invent! Imagine!</strong>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</div>
<div class="row agg">
    <div class="six columns">
        <h5>About Our Founder</h5>
        <p>Vanessa Rementilla is an art junkie. Her earliest scribbles can be traced back to age 3 while sitting in art classes her mom taught to bigger kids. Later, she pursued Visual Communication Arts and Digital Media Design in several universities. Today, she enjoys bringing together strategy, design and technology as a Digital Creative Director.</p>
        <p>Art is an everyday expression in her home. Vanessa and her boys (7 and 4) draw, paint, build cars and forts out of found objects, make music and solve problems creatively. Gallery of Good is her passion project, inspired by the creative potential she sees in all children.</p>
    </div>
    <g:render template="/common/comments" model="[modelInstance: homepageInstance]" params="[id:homepageInstance.id]"/>
</div>
<!-- main content area -->


</body>
