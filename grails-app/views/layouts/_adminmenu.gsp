<sec:ifLoggedIn>
<div class="two columns ap-left">
    <div>
        <p>CONTENT</p>
        <hr />
        <a href="${createLink(controller:'admin',action:'homepage')}">Homepage</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'artwork')}">Artworks</a>
        <hr />
        <a href="${createLink(controller:'advisor',action:'list')}">2cc</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'comments')}">About Comments</a>
        <hr />
        %{----}%
        %{--<a href="#">Experiences</a>--}%
        %{--<hr />--}%
        %{--<a href="#">Fundraising</a>--}%
        %{--<hr />--}%
        <br />
        <br />
    </div>
    <div>
        <p>USERS</p>
        <hr />
        <a href="${createLink(controller:'admin',action:'listUsers')}">All Accounts</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'listUsers', params:['page':'standard'], fragment: 'standardUser')}">Standard Account</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'listUsers', params:['page':'newArtwork'], fragment: 'newArtwork')}">New Artworks</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'listUsers', params:['page':'flagged'], fragment: 'flag')}">Flagged</a>
        <hr />
        <br />
        <br />
    </div>
    <div>
        <p>ADMIN</p>
        <hr />
        <a href="${createLink(controller:'admin',action:'listAdministrators')}">All Administrators</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'listGalleries')}">All Galleries</a>
        <hr />
        <a href="${createLink(controller:'admin',action:'commentsAll')}">All Comments</a>
        <hr />
        <a href="${createLink(action: 'createAdmin',controller:'admin')}">Add Administrator</a>
        <hr />
        <a href="${createLink(action: 'yourProfile',controller:'admin')}">Your Profile</a>
        <hr />
        <br/>
        <br/>
    </div>
    <div>
        <p>ROLES</p>
        <hr/>
        <a href="${createLink(controller:'role',action:'search')}">Search</a>
        <hr/>
        <a href="${createLink(controller:'role',action:'create')}">Create</a>
        <hr/>
        <br/>
        <br/>
    </div>
    <div>
        <p>System Parameters</p>
        <hr/>
        <a href="${createLink(controller:'systemParameter',action:'editParameters')}">System Parameters</a>
        <hr/>
        <a href="${createLink(controller:'role',action:'create')}">Create</a>
        <hr/>
    </div>
</div>
</sec:ifLoggedIn>