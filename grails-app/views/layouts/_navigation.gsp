<!-- navigation -->
<g:if test="${sec.loggedInUserInfo(field: 'hasLoggedIn') == 'No'&&"You".equals(activeLink)}">
    <r:script>
        $(function() {
            jQuery.ajax({
			type:'GET',
			url:'${createLink(controller: 'register', action: 'updateFirstLogin')}',
			success:function(data, textStatus) {
            },
            error:function(XMLHttpRequest, textStatus, errorThrown) {
            }
		});
        });
    </r:script>
    <div class="row">
        <div class="twelve columns">
            <div id="tool-tip" class="alert-box">
                <r:img uri="/images/tool_tip_arrow.jpg" class="float-left"/>
                Here's where you manage your account settings.
                <br>
                Only you can see this when you log in.
                <a class="close" href="">×</a>
            </div>
        </div>
    </div>
</g:if>
<div class="row" id="navigation">
    <div class="twelve columns">
        <ul class="nav-bar">
            <sec:ifLoggedIn>
            <li id="you"><a href="${createLink(controller:'appUser',action:"accountSettings")}" class="${"You".equals(activeLink)?"current-page":""} navigate-header">You</a></li>
                <li id="artworks"><a class="navigate-header" href="${createLink(controller:'artwork',action:'landing')}">Artworks</a></li>
            %{--<li id="artworks"><a href="${createLink(controller:'gallery')}" class="${"Artworks".equals(activeLink)?"current-page":""}">Artworks</a></li>--}%
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <li id="artworks"><a href="${createLink(controller:'artwork',action:'landing')}">Artworks</a></li>
                <li></li>
            </sec:ifNotLoggedIn>
            <li></li>
            <li></li>
            %{--<li id="experiences"><a href="#">Experiences</a></li>--}%
            %{--<li id="fundraisers"><a href="#">Fundraisers</a></li>--}%
            <li>
                <div id="search-module">
                    <g:form controller="search" action="searchItems" class="custom" method="post">
                        <input name="searchKey" class="search-box" type="text" value="" placeholder="Search"/>
                        <g:select name="searchType" from="${['Artworks']}" value="${searchType?:"Artworks"}"/>
                        <g:submitButton name="submit" class="submit-button" type="submit" value=""/>
                        <div class="clear-both"></div>
                    </g:form>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- navigation -->