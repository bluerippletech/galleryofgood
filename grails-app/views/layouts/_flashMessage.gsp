<g:if test="${flash.message}">
<div class="row" id="messagealert">
    <div class="twelve columns">
            <div class="alert-box success">
                ${flash.message}
                <a href="" class="close">&times;</a>
            </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#messagealert").fadeOut(10000);
    });
</script>

</g:if>
