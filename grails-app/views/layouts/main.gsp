<!doctype html xmlns:fb="http://ogp.me/ns/fb#">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" xmlns:fb="http://ogp.me/ns/fb#"><!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width"/>
    <newrelic:browserTimingHeader/>
    <link rel="shortcut icon" href="${resource(dir:'images', file:'favicon.ico')}">

    <title><g:layoutTitle default="Gallery of Good"/></title>
    <!-- Included CSS Files -->
    <r:require modules="core"/>

    <!--[if lt IE 9]>
		<!--<link rel="stylesheet" href="stylesheets/ie.css">-->
	<![endif]-->


    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <r:layoutResources/>

</head>

<body>
<div class="container">
    <g:render template="/layouts/header"/>
    <g:render template="/layouts/navigation" model="[activeLink:activeLink]"/>
    <g:layoutBody/>
    <g:render template="/layouts/footer"/>
</div>
<!-- container -->




<!-- Included JS Files -->
%{--<script src="javascripts/foundation.js"></script>--}%
%{--<script src="javascripts/app.js"></script>--}%
<r:layoutResources/>
<newrelic:browserTimingFooter/>
</body>
</html>