<!-- header -->
<div class="row" id="header">
    <div class="two columns col1">
        <h1><a href="${createLink(uri: '/')}"><span>Gallery of Good&trade;</span></a></h1>
    </div>

    <div class="two columns col2">
        <h3>Keep Creating!</h3>
    </div>

    <div class="two columns col3">
        &nbsp;
    </div>

    <div class="two columns col4">
        &nbsp;
    </div>

    <div class="two columns col5">
        <ul>
            <sec:ifNotLoggedIn>
                <li><a href="${createLink(controller:'login',action:'auth')}">Sign-in</a></li>
                <li><a href="${createLink(controller:'register',action:'index')}">Sign-up</a></li>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <li>Hello,</li>
                <li id="username"><a href="${createLink(controller:"appUser",action:"accountSettings")}"><sec:loggedInUserInfo field="username"/></a></li>
                <li class="last"><a href="${createLink(controller:'logout')}">Log-out</a></li>
            </sec:ifLoggedIn>
        </ul>
    </div>

    <div class="two columns col6">
        <ul>
            <li>Guides:</li>
            <li><a href="${createLink(controller:'general',action:'gettingStarted')}"><g:message code="gettingStarted.label"/></a></li>
            <li class="last"><a href="${createLink(controller:'general',action:'communityGuidelines')}"><g:message code="communityGuide.label"/></a></li>
        </ul>
    </div>
</div>
<!-- header -->