<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Create your account - Gallery of Good</title>
    <g:layoutHead/>
</head>

<body>

%{--<g:javascript src='jquery/jquery.jgrowl.js'/>--}%
%{--<g:javascript src='jquery/jquery.checkbox.js'/>--}%
%{--<g:javascript src='spring-security-ui.js'/>--}%
<g:layoutBody/>

</body>
</html>
