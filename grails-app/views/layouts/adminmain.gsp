<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width" />
    <title><g:layoutTitle default="Gallery of Good Administration"/></title>
    <link rel="shortcut icon" href="${resource(dir:'images', file:'favicon.ico')}">
    <!-- Included CSS Files -->
    <r:require modules="core,foundation"/>
    <!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->
    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <r:layoutResources/>
</head>
<body>
<!-- container -->
<div class="container">

    <sec:ifLoggedIn>
<!-- header -->
<div class="row admin-panel" id="header">
    <div class="two columns col1">
        <h1><a href="#"><span>Gallery of Good&trade;</span></a></h1>
    </div>
    <div class="two columns col2"></div>
    <div class="two columns col3"></div>
    <div class="two columns col4"></div>
    <div class="two columns col5"></div>
    <div class="two columns col6">
        <a href="#"><sec:loggedInUserInfo field="username"/></a>
        |
        <a href="${createLink(controller:'logout')}">Logout</a>
    </div>
</div>
<!-- header -->
    </sec:ifLoggedIn>
<div class="row">
    <div class="twelve columns">&nbsp;</div>
</div>
<!-- main content area -->
<div class="row">
<g:render template="/layouts/adminmenu"/>
<g:layoutBody/>

</div>
<!-- main content area -->


</div>
<!-- container -->
<r:layoutResources/>
</body>
</html>
