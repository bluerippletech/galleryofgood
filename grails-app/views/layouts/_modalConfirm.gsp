<r:require module="jqueryDirtyForms"/>
<div id="modalConfirm" class="reveal-modal modal-view" style="position:fixed">
    <p style="margin:0">
        You have unsaved changes. Are you sure you want to leave this page?
    </p>

    <div class="btn_grp" style="margin:15px 0 0 -10px;text-align: center">
        <a id="yes" class="ignoredirty continue nice small radius black button"
           style="width:110px;">Leave this page</a>
        <a id="no" class="ignoredirty cancel nice small radius red button close-reveal"
           style="width:130px;">Stay on this page</a>
        <a class="close-reveal-modal close-reveal close">×</a>
    </div>
</div>
<r:script type="text/javascript">
    $('form.custom').dirtyForms();
</r:script>