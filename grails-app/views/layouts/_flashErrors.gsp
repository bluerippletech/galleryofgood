<g:if test="${flash.errors}">
<div class="row">
    <div class="twelve columns">
    &nbsp;
            <div class="alert-box error">
                <g:each in="${flash.errors}" var="error">
                ${error}<br/>
                </g:each>
                <a href="" class="close">&times;</a>
            </div>
    </div>
</div>
</g:if>
<g:if test="${flash.error}">
    <div class="row">
        <div class="twelve columns">
            &nbsp;
            <div class="alert-box error">
                    ${flash.error}<br/>
                <a href="" class="close">&times;</a>
            </div>
        </div>
    </div>
</g:if>
