<div id="${elementId?:'modalAlert'}" class="reveal-modal modal-view">
    <p style="margin:0" id="alert-content">
    <g:if test="${flash.successMessage}">
        ${flash.successMessage}
    </g:if>
    </p>
    <a  style="margin:5px 0 0 0;" class="button small nice radius black" href="javascript:" onclick="$('.close-reveal-modal').click();">Okay</a>
    <a class="close-reveal-modal">×</a>
</div>
<div class="reveal-modal-bg modal-no-bg"></div>
<g:if test="${flash.successMessage}">
<script type="text/javascript">
    $('#${elementId?:'modalAlert'}').reveal({dismissModalClass: 'close-reveal-modal'});
</script>
</g:if>
