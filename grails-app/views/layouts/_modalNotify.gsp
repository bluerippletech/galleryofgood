<div id="${elementId?:'modalNotify'}" class="reveal-modal modal-view">
    <p style="margin:0">
        ${flash.message?:content}
    </p>
    <g:if test="${content == "Please sign-in or sign-up to flag this artwork."}">
        <a  style="margin:5px 0 0 0;" class="button small nice radius black" href="${createLink(controller: 'general', action: 'artworkFlag', params: ['id':id, 'myModal':true])}">Sign-in</a>
    </g:if>
    <g:if test="${content == "Please sign-in or sign-up to flag this gallery."}">
        <a  style="margin:5px 0 0 0;" class="button small nice radius black" href="${createLink(controller: 'general', action: 'galleryFlag', params: ['galleryName':galleryName, 'myModal':true])}">Sign-in</a>
    </g:if>
    <g:if test="${content == "Please sign-in or sign-up to follow this gallery."}">
        <g:if test="${artworkId}">
            <a  style="margin:5px 0 0 0;" class="button small nice radius black" href="${createLink(controller: 'general', action: 'galleryFollow', params: ['galleryName':galleryName, 'artworkId':artworkId])}">Sign-in</a>
        </g:if>
        <g:else>
            <a  style="margin:5px 0 0 0;" class="button small nice radius black" href="${createLink(controller: 'general', action: 'galleryFollow', params: ['galleryName':galleryName])}">Sign-in</a>
        </g:else>
    </g:if>
    <a class="close-reveal-modal">×</a>
</div>
<div class="reveal-modal-bg modal-no-bg"></div>

<script type="text/javascript">
    <g:if test="${flash.message}">
    $('#modalNotify').reveal({dismissModalClass: 'close-reveal-modal'});
    </g:if>
    <g:if test="${flash.message}">
    $(function(){window.setTimeout(
            function() {
                $(".close-reveal-modal").click();
            },
            ${timeout?:'5000'}
    );})
    </g:if>
    $('.modal-show').click(function(){
        window.setTimeout(
                function() {
                    $(".close-reveal-modal").click();
                },
                ${timeout?:'5000'}
        );
    })
</script>
