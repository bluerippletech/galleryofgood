<!-- footer -->
<div class="row" id="footer">
    <div class="two columns col1">
        <h2>Get started</h2>
        <ul>
            <sec:ifLoggedIn>
                <li>Sign-in</li>
                <li>Sign-up</li>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <li><a href="${createLink(controller:'login',action:'auth')}">Sign-in</a></li>
                <li><a href="${createLink(controller:'register',action:'index')}">Sign-up</a></li>
            </sec:ifNotLoggedIn>
        </ul>
    </div>
    <div class="two columns col2">
        <h2>Guides</h2>
        <ul>
            <li><a href="${createLink(controller:'general',action:'gettingStarted')}"><g:message code="gettingStarted.label"/></a></li>
            <li class="last"><a href="${createLink(controller:'general',action:'communityGuidelines')}"><g:message code="communityGuide.label"/></a></li>
            %{--<li><a href="${createLink(controller:'parentGuide')}">For Parents</a></li>--}%
            %{--<li><a href="#">For Teens & Tweens</a></li>--}%
            %{--<li class="last"><a href="${createLink(controller:'communityGuidelines')}">Community</a></li>--}%

        </ul>
    </div>
    <div class="two columns col3">
        <h2 id="footer_artworks">Artworks</h2>
        <ul>
            <li><a href="${createLink(controller:'register',action:'index')}">Open a Gallery</a></li>
            %{--<li><a href="${createLink(controller:'search',action:'searchItems')}">Find an Artist</a></li>--}%
            <li><a href="${createLink(controller:'search',action:'freshPaint')}">By Most Recent</a></li>
            <li><a href="${createLink(controller:'search',action:'typeAndAge')}">By Type & Age</a></li>
            <li class="last"><a href="${createLink(controller:'artwork',action:'emergingThemes')}">By Themes</a></li>
        </ul>
    </div>
    <div class="two columns col4">
        &nbsp;
    </div>
    <div class="two columns col5">
        <h2>
            <r:img uri="/images/logo_gog_footer.jpg"/>
        </h2>
        <p><g:message code="footer.copy.line1"/></p>
    </div>
    <div class="two columns col6">
        <h2>&nbsp;</h2>
        <ul>
            <li><a href="${createLink(controller:'general',action:'aboutUs')}">About Us</a></li>
            <li><a href="${createLink(controller:'general',action:'twoCents')}">2 Cents Circle</a></li>
            <li><a href="${createLink(controller:'general',action:'termsOfService')}">Terms of Service</a></li>
            <li><a href="${createLink(controller:'general',action:'privacyPolicy')}">Privacy Policy</a></li>
            <li class="last"><g:link controller="contactUs">Contact Us</g:link></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="eight columns bottom-connect">
        Connect the dots: &nbsp;
        <a href="http://www.facebook.com/galleryofgood"><r:img uri="/images/ico-footer-facebook.png" alt="" /></a>
        <a href="http://www.pinterest.com/galleryofgood"><r:img uri="/images/ico-footer-pin.png" alt="" /></a>
        <a href="http://www.twitter.com/galleryofgood"><r:img uri="/images/ico-footer-twitter.png" alt="" /></a>
    </div>
    <div class="four columns"><p id="copyright">Gallery of Good Inc. &copy; 2012</p></div>
</div>
<!-- footer -->