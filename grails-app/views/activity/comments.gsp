<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='activity.comments'/></title>
    <r:require modules="application, foundation, jqueryDatatable, lazyload"/>
    <r:script>
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<g:render template="/layouts/flashWarning"/>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'comments']"/>
<!-- secondary navigation -->
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
    <div class="row">
        <div class="twelve columns">
            <dl class="tabs">
                <dd onclick="commentList.fnReloadAjax(); commentList.fnDraw(); selectedTable = commentList"><a href="#simple1" class="active" >Received</a></dd>
                <dd onclick="myCommentList.fnReloadAjax(), myCommentList.fnDraw(); selectedTable = myCommentList"><a href="#simple2" >Sent</a></dd>
            </dl>
            <ul class="tabs-content">
                <li class="active" id="simple1Tab">
                    <g:if test="${commentsList.size()>0}">
                    <form class="custom nice" action="#" method="post" onsubmit="actionSelector(this, commentList); return false;" enctype="multipart/form-data">
                        <div class="four columns">
                        <select name="bulk-action-comments" id="bulk-action-comments" style="display:none;">
                            <option selected="">Approve</option>
                            <option>Report</option>
                            <option>Delete</option>
                        </select>
                        <div class="custom dropdown inside_tab" style="width: 136px;">
                            <a class="current" href="#">Approve</a>
                            <a class="selector" href="#"></a>
                            <ul style="width: 134px;">
                                <li style="">Approve</li>
                                <li style="">Report</li>
                                <li style="">Delete</li>
                            </ul>
                        </div>
                        <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
                        </div>
                        <div class="six columns centered commentControl">
                            Show:
                            <a href="javascript:" onclick="selectedTable.fnFilter('',5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">All</a>
                            <a href="javascript:" onclick="selectedTable.fnFilter('New',5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">New</a>
                            <a href="javascript:" onclick="selectedTable.fnFilter('Approved',5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">Approved</a>
                            <a href="javascript:" onclick="selectedTable.fnFilter('Reported',5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">Reported</a>
                        </div>
                    <table id="comments-list">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>

                            <th>Member</th>
                            <th>Section</th>
                            <th>Comment</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </form>
                    </g:if>
                    <g:else>
                        <p>Nothing yet. That's okay, the community will discover your artworks in no time!</p>

                        <img class="float-left attention-icon" src="${resource(dir: 'images', file: 'tip.jpg')}" alt="Note" />

                        <p class="attention-paragraph"><br /><br />Tip: Sometimes you just need to put it out there. Go to your gallery and share your artworks on Facebook,
                        Pinterest or Twitter. Don’t be shy!</p>

                        <div class="clear-both"></div>
                    </g:else>
                </li>
                <li id="simple2Tab">
                    <g:if test="${myCommentsList.size()>0}">
                    <form class="custom nice" action="#" method="post" onsubmit="actionSelector(this, myCommentList); return false;" enctype="multipart/form-data">
                        <div class="four columns">
                        <select name="bulk-action-myComments" id="bulk-action-myComments" style="display:none;">
                            <option>Delete</option>
                        </select>
                        <div class="custom dropdown inside_tab" style="width: 136px;">
                            <a class="current" href="#">Delete</a>
                            <a class="selector" href="#"></a>
                            <ul style="width: 134px;">
                                <li style="">Delete</li>
                            </ul>
                        </div>
                        <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
                        </div>
                        <div class="six columns centered commentControl">
                            Show:
                            <a href="javascript:" onclick="selectedTable.fnFilter('', 5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">All</a>
                            <a href="javascript:" onclick="selectedTable.fnFilter('Pending', 5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">Pending</a>
                            <a href="javascript:" onclick="selectedTable.fnFilter('Approved', 5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">Approved</a>
                            <a href="javascript:" onclick="selectedTable.fnFilter('Reported', 5); $(this).closest('div').find('a.disabled').removeClass('disabled'); $(this).addClass('disabled')" class="nice small radius white button">Reported</a>
                        </div>
                    <table id="my-comments-list">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                            <th>Member</th>
                            <th>Section</th>
                            <th>Comment</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    </form>
                    </g:if>
                    <g:else>
                        <p>We know other artists would love to hear from you. Feel free to comment on
                        artworks, galleries and artist timelines in a supportive and encouraging way. The more you give, the more you
                        may get!
                        </p>

                        %{--<img class="float-left attention-icon" src="${resource(dir: 'images', file: 'tip.jpg')}" alt="Note" />--}%

                        %{--<p class="attention-paragraph"><br /><br /></p>--}%

                        <div class="clear-both"></div>
                    </g:else>
                </li>
            </ul>
            <script type="text/javascript">
                var commentList
                var myCommentList
                var selectedTable
                $(document).ready(function(){
                    <g:if test="${commentsList.size()>0}">
                    commentList = $("#comments-list").dataTable({
                        sScrollY: '100%', //to fix scroll y
                        bProcessing: true,
                        bServerSide: false,
                        "sAjaxSource": "${createLink(controller: 'activity', action: 'commentsList')}",
                        bJQueryUI: false,
                        sPaginationType: "bootstrap",
                        aLengthMenu: [[30], [30]],
                        iDisplayLength: 30,
                        "aaSorting": [],
                        "oLanguage": {
                            "sSearch": "Search Comments:" ,
                            "sEmptyTable":'No comments yet',
                            "sZeroRecords": 'We didn\'t find anything.'
                        },
                        "aoColumns": [
                            { "bSortable": false },
                            { "bSortable": false },
                            { "bSortable": false },
                            { "bSortable": false },
                            { "bSortable": false }, //{ "sType": "date-euro" },
                            { "bSortable": false }
                        ],
                        "aoColumnDefs": [ {
                            "aTargets": [5],
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                if(sData == 'New!'){
//                                    $(nTd).css('color', 'blue')
                                    $(nTd).css('font-weight', 'bold')
                                }
                            }
                        } ],
                        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                            //$('td:eq(0)', nRow).html( '<a href="#'+aData[0]+'">'+aData[0]+'</a>' );
                        },
                        "fnDrawCallback" : function() {
                            $("img.lazy").lazyload({effect:"fadeIn"});
                            $('.buttonForModal').click(function () {
                                $("#commentFlagForm #commentId").val($(this).attr("commentid"));
                                $("#commentContent").html($(this).closest('td').find("a").first().text())
                                $("#flagcomment").val("");
                                $('#commentModal').reveal({animation:'fadeAndPop',
                                    animationspeed:300,
                                    closeOnBackgroundClick:true,
                                    dismissModalClass:'close-reveal-modal'});
                                return false;
                            });
                            if ($('#comments-list_wrapper .pagination ul li').size()>3) {
                                $('#comments-list_wrapper .dataTables_paginate')[0].style.display = "block";
                            }
                            else {
                                $('#comments-list_wrapper .dataTables_paginate')[0].style.display = "none";
                            }
                        }

                    })
                    $(".dataTables_length").hide();
                    </g:if>
                    <g:if test="${myCommentsList.size()>0}">
                    myCommentList = $("#my-comments-list").dataTable({
                        sScrollY: '100%', //to fix scroll y
                        bProcessing: true,
                        bServerSide: false,
                        "sAjaxSource": "${createLink(controller: 'activity', action: 'myCommentsList')}",
                        bJQueryUI: false,
                        sPaginationType: "bootstrap",
                        aLengthMenu: [[30], [30]],
                        iDisplayLength: 30,
                        "aaSorting": [],
                        "oLanguage": {
                            "sSearch": "Search Comments:",
                            "sEmptyTable":'No comments yet',
                            "sZeroRecords": 'We didn\'t find anything.'
                        },
                        "aoColumns": [
                            { "bSortable": false },
                            { "bSortable": false, "sWidth": "165px"  },
                            { "bSortable": false, "sWidth": "135px"  },
                            { "bSortable": false, "sWidth": "230px" },
                            { "bSortable": false, "sWidth": "100px" }, //{ "sType": "date-euro" },
                            { "bSortable": false }
                        ],
                        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                            //$('td:eq(0)', nRow).html( '<a href="#'+aData[0]+'">'+aData[0]+'</a>' );
                        },
                        "fnDrawCallback" : function() {
                            $("img.lazy").lazyload({effect:"fadeIn"});
                            $('.buttonForModalTwo').click(function () {
                                $("#commentFlagForm #commentId").val($(this).attr("commentid"));
                                $("#commentContent").html($(this).closest('td').find("a").first().text());
                                $("#flagcomment").val("");
                                $('#commentModal').reveal({animation:'fadeAndPop',
                                    animationspeed:300,
                                    closeOnBackgroundClick:true,
                                    dismissModalClass:'close-reveal-modal'});
                                return false;
                            });
                            if ($('#my-comments-list_wrapper .pagination ul li').size()>3) {
                                $('#my-comments-list_wrapper .dataTables_paginate')[0].style.display = "block";
                            }
                            else {
                                $('#my-comments-list_wrapper .dataTables_paginate')[0].style.display = "none";
                            }
                        }
                    })
                    </g:if>
                    <g:if test="${commentsList.size()>0 || myCommentsList.size()>0}">
                    selectedTable = commentList
                    $(".dataTables_length").hide()
                    $(".dataTables_info").hide()
                    $(".dataTables_filter").find("input").addClass('input-text')
                    $(".dataTables_filter").find("input").css('display', 'inline')
                    $(".dataTables_filter").find("input").css('width', '150 px')
                    </g:if>
                    $('.commentControl').find('a:first').addClass('disabled');
                })
                function flagComment() {
                    $(".close-reveal-modal").click();
                    jQuery.ajax({type:'POST',
                        data:$("#commentFlagForm").serialize(),
                        url:'${createLink(controller: 'flag', action: 'flagComment')}',
                        success:function (data, textStatus) {
                            $("#flagcomment").val("");
                            flashWarning(data);
                            selectedTable.fnReloadAjax();
                        },
                        error:function (XMLHttpRequest, textStatus, errorThrown) {
                            flashWarning(errorThrown);
                        }
                    });
                }
                /*for links below comment content */
                function updateStatusLink(commentId, table ,newStatus){
                    $.ajax({
                        url: "${createLink(controller:params.controller, action:'updateCommentStatus')}",
                        type: "POST",
                        data: {id:commentId, newStatus:newStatus},
                        dataType: "json",
                        beforeSend: function() {
                        },
                        success: function(results) {
                            if (results.message=="Updated")
                                table.fnReloadAjax();
                            else
                                alert(results.message);
                        }
                    });
                }
                function deleteLink(link, table){
                    var row = $(link).closest("tr").get(0);
                    $.ajax({
                        url: "${createLink(controller:params.controller, action:'deleteComment')}",
                        type: "POST",
                        data: {id:$(link).attr("commentid")},
                        dataType: "json",
                        beforeSend: function() {
                        },
                        success: function(results) {
                            if (results.message=="Deleted")
                                table.fnDeleteRow(table.fnGetPosition(row));
                            else
                                alert(results.message);
                        }
                    });
                }
                /*end of for links below comment content */
                function toggleAll(aCheckbox){
                    var checkboxes=$(aCheckbox).closest("li").find(".checkbox");
                    var checkboxes2=$(aCheckbox).closest("li").find("input[type='checkbox']");
                    if ('checked'==$(aCheckbox).attr("checked")){
                        checkboxes.addClass("checked");
                        checkboxes2.attr("checked","checked");
                    }
                    else{
                        checkboxes.removeClass("checked");
                        checkboxes2.removeAttr("checked");
                    }
                }
                function actionSelector(form, table){
                    if (table == commentList){
                        if ($(form).find("select#bulk-action-comments").val()!="Delete")
                            updateStatus(form, table, $("select#bulk-action-comments").val());
                        else
                            deleteItems(form, table);
                    }
                    else if(table == myCommentList){
                        if ($(form).find("select#bulk-action-myComments").val()!="Delete")
                            updateStatus(form, table, $("select#bulk-action-myComments").val());
                        else
                            deleteItems(form, table);
                    }

                }
                function updateStatus(form, table, newStatus){
                    var checkboxes = $(form).find("tbody input[type='checkbox']:checked");
                    $.each(checkboxes, function() {
                        var row = $(this).closest("tr").get(0);
                        $.ajax({
                            url: "${createLink(controller:params.controller, action:'updateCommentStatus')}",
                            type: "POST",
                            data: {id:$(this).val(), newStatus:newStatus},
                            dataType: "json",
                            beforeSend: function() {
                            },
                            success: function(results) {
                                if (results.message=="Updated")
                                    table.fnReloadAjax();
                                else
                                    alert(results.message);
                            }
                        });
                    })
                }
                function deleteItems(form, table){
                    var checkboxes = $(form).find("tbody input[type='checkbox']:checked");
                    $.each(checkboxes, function() {
                        var row = $(this).closest("tr").get(0);
                        $.ajax({
                            url: "${createLink(controller:params.controller, action:'deleteComment')}",
                            type: "POST",
                            data: {id:$(this).val()},
                            dataType: "json",
                            beforeSend: function() {
                            },
                            success: function(results) {
                                if (results.message=="Deleted")
                                    table.fnDeleteRow(table.fnGetPosition(row));
                                else
                                    alert(results.message);
                            }
                        });
                    })
                }
            </script>
        </div>
    </div>
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->
<sec:ifLoggedIn>
    <!-- Modal Flag -->
    <form class="custom nice flag-form" action="#" method="post" enctype="multipart/form-data" id="commentFlagForm">
        <g:hiddenField name="id" id="commentId" value=""/>
        <div id="commentModal" class="reveal-modal">
            <p>From: <sec:loggedInUserInfo field="username"/><br/>
                Comment: <span id="commentContent">${artworkInstance?.title}</span>

            <p>

            <p>Tell us why your are reporting this:</p>
            <textarea id="flagcomment" name="comment" class="wider"></textarea>
            <a href="javascript:" onclick="flagComment($('#commentId').val());" class="nice small radius white button" name="submit"
               value="Submit">Submit</a>
            <a class="close-reveal-modal">&#215;</a>
        </div>
    </form>
</sec:ifLoggedIn>
</body>
