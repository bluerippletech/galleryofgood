<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='activity.following'/></title>
    <r:require modules="foundation, jqueryDatatable, lazyload"/>
    <r:script>
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'following']"/>
<!-- secondary navigation -->
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<div class="row">
<div class="twelve columns">
<dl class="tabs">
    <dd onclick="followersList.fnReloadAjax(); followersList.fnDraw()"><a href="#simple1" class="active">Followers</a></dd>
    <dd><a href="#simple2">Following</a></dd>
</dl>

<ul class="tabs-content">
<li class="active" id="simple1Tab">
    <g:if test="${followersCount>0}">
    <form class="custom nice" action="#" method="post" onsubmit="actionSelector(this, followersList); return false;" enctype="multipart/form-data">
        <select name="bulk-action-followables" id="bulk-action-followables" style="display:none;">
            <option selected="">Follow</option>
            <option>Unfollow</option>
        </select>
        <div class="custom dropdown inside_tab" style="width: 136px;">
            <a class="current" href="#">Approve</a>
            <a class="selector" href="#"></a>
            <ul style="width: 134px;">
                <li style="">Follow</li>
                <li style="">Unfollow</li>
            </ul>
        </div>
        <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
        <table id="followers-list">
            <thead>
            <tr>
                <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                <th>Member</th>
                <th>Gallery</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </form>
    </g:if>
    <g:else>
        <p>No followers yet? That’s okay – the community will discover your artworks soon.</p>

        <img class="float-left attention-icon" src="${resource(dir: 'images', file: 'tip.jpg')}" alt="Note" />

        <p class="attention-paragraph"><br /><br />In the meantime, consider following your favorite galleries. There’s a good chance they’ll want to check out your art too!</p>

        <div class="clear-both"></div>
    </g:else>
</li>
<li id="simple2Tab">
    <g:if test="${followingCount>0}">
    <form class="custom nice" action="#" method="post" onsubmit="actionSelector(this, 'grid'); return false;" enctype="multipart/form-data">
        <label style="float: left; padding-top: 6px;" for="checkbox1">
            <input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/>
            Select All
        </label>&nbsp;
        <select name="bulk-action-followablesGrid" id="bulk-action-followablesGrid" style="display:none;">
            <option selected="">Unfollow</option>
        </select>
        <div class="custom dropdown inside_tab" style="width: 136px;">
            <a class="current" href="#">Approve</a>
            <a class="selector" href="#"></a>
            <ul style="width: 134px;">
                <li style="">Unfollow</li>
            </ul>
        </div>
        <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
        <div id="galleries" class="admirations_sent"></div>
        <div class="row">
            <div class="twelve columns"></div>
        </div>
        %{--<g:if test="${followingCount>36}">--}%
            <ripple:paginate count="${36}" total="${com.galleryofgood.common.UserFollow.countByFollowerId(userInstance.id)}"  sort="galleryName" order="asc" controller="${params.controller}" action="myFollowablesList" containerId="galleries"/>
        %{--</g:if>--}%
    </form>
    </g:if>
    <g:else>
        <p>Did you know that you can follow your favorite galleries? When you find a gallery you like, simple click on the “Follow Gallery” link and you’ll be adding them right in here. It’s an easy way view the galleries and stay on top of new artworks they add.</p>

        <div class="clear-both"></div>
    </g:else>
</li>
</ul>

</div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<ripple:followableScript controller="${params.controller}"/>
<g:render template="../layouts/modalAlert"/>
<r:script type="text/javascript">
    var followersList
    function toggleAll(aCheckbox){
        var checkboxes=$(aCheckbox).closest("li").find(".checkbox");
        var checkboxes2=$(aCheckbox).closest("li").find("input[type='checkbox']");
        if ('checked'==$(aCheckbox).attr("checked")){
            checkboxes.addClass("checked");
            checkboxes2.attr("checked","checked");
        }
        else{
            checkboxes.removeClass("checked");
            checkboxes2.removeAttr("checked");
        }
    }
    $(document).ready(function(){
        <g:if test="${followersCount>0}">
        followersList = $("#followers-list").dataTable({
            sScrollY: '70%',
            bProcessing: true,
            bServerSide: false,
            "sAjaxSource": "${resource(dir: "activity", file: "followersList")}",
            bJQueryUI: false,
            sPaginationType: "bootstrap",
            aLengthMenu: [[10, 50, 100, 500, -1], [10, 50, 100, 500, "All"]],
            iDisplayLength: 10,
            "aaSorting": [],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                //$('td:eq(0)', nRow).html( '<a href="#'+aData[0]+'">'+aData[0]+'</a>' );
            },
            "fnDrawCallback" : function() {
                $("img.lazy").lazyload({effect:"fadeIn"});
                if ($('#followers-list_wrapper .pagination ul li').size()>3) {
                    $('#followers-list_wrapper .dataTables_paginate')[0].style.display = "block";
                }
                else {
                    $('#followers-list_wrapper .dataTables_paginate')[0].style.display = "none";
                }
            }
        })
        $(".dataTables_length").hide()
        $(".dataTables_info").hide()
        $(".dataTables_filter").find("input").addClass('input-text')
        $(".dataTables_filter").find("input").css('display', 'inline')
        $(".dataTables_filter").find("input").css('width', '150 px')
        </g:if>
    });
    function actionSelector(form, table){
        if (table == followersList){
            if ($(form).find("select#bulk-action-followables").val()=="Follow"){
                var checkboxes = $(form).find("tbody input[type='checkbox']:checked");
                $.each(checkboxes, function() {
                    var row = $(this).closest("tr").get(0);
                    followable("follow", $(this).val(), true);

                })
            }else{
                var checkboxes = $(form).find("tbody input[type='checkbox']:checked");
                $.each(checkboxes, function() {
                    var row = $(this).closest("tr").get(0);
                    followable("unfollow", $(this).val(), true);

                })
            }
        }
        else if(table == "grid"){
            if ($(form).find("select#bulk-action-followablesGrid").val()=="Unfollow"){
                var checkboxes = $(form).find("div input[type='checkbox']:checked");
                $.each(checkboxes, function() {
                      var item = $(this).closest("div").find("a.button")
                     unfollowGrid(item);
                })
            }
        }
    }
    //AJAX Load GRID
    function loadByPagination(max, offset, sort, order, controller, action, containerId){
        $.ajax({
            url: "${createLink(uri: '/' )}"+controller+"/"+action,
            type: "POST",
            data: {max:max, offset:offset, sort:sort, order:order},
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
               $("#"+containerId).html(results);
            }
        });
    }
    function unfollowGrid(item){
        $.ajax({
            url: "${createLink(controller: params.controller, action:'unfollow')}",
            type: "POST",
            data: {id:$(item).attr('id')},
            dataType: "json",
            beforeSend: function() {
            },
            success: function(results) {
                 if (results.status=="unfollowed")
                    $(item).closest("div").fadeOut('slow').remove();
                 else{
                    $('#modalAlert').reveal({dismissModalClass: 'close-reveal-modal'}); $('#alert-content').html(results.message);
                 }

            }
        });
    }
</r:script>
<!-- main content area -->

</body>
