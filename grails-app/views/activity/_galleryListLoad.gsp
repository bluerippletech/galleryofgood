<div id="img-container-gal" style="display: none;">
<g:if test="${galleryInstance}">
    <g:each in="${galleryInstance}" status="i" var="gallery">
    <div><!-- style is for testing purposes only  -->
        <a href="${createLink(uri: '/')+gallery.galleryNameUrl}">
            <div style="width: 225px; height: 200px; overflow-y: hidden; margin: 0; padding: 0"><img class="lazy" src="${resource(dir: 'images', file: 'white.jpg')}" alt="" data-original="${gallery.artworks.size() > 0 ? gallery.artworks.asList().first().url:resource(dir: 'images', file: 'gallery_noimg_placehldr.png')}" width="225px"></div>
        </a>
        <br>
        <input id="checkbox1" type="checkbox" style="display: none;">
        <span class="custom checkbox"></span>
        <a class="as_title" href="${createLink(uri: '/' )+gallery?.galleryNameUrl}">${gallery?.galleryName}</a>
        <br>
        <a class="nice small white button radius" id="${gallery.id}" href="javascript:" onclick="unfollowGrid(this)">Unfollow</a>
    </div>
    </g:each>
</g:if>
<g:else>
    <div class="twelve columns">
        <h4>Sorry, we did not find any results</b></h4>
        <p>Try again or browse the artworks by most recent, type and age or themes.</p>
    </div>
</g:else>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#img-container-gal").fadeIn('slow');
        $("img.lazy").lazyload({effect:"fadeIn"});
    });
</script>