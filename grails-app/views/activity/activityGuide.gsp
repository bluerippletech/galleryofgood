<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='activity.guide'/></title>
    <r:require modules="foundation"/>
    <r:script>
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'activityGuide']"/>
<!-- secondary navigation -->
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">

        <ul id="link-list">
            <li><a href="#what_are_admirations">What is Following?</a></li>
            <li><a href="#how_do_comments_work">How do Comments work?</a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <a name="what_are_admirations"><h5>What is Following?</h5></a>
        <p>When you click on “Follow Gallery” you bookmark this gallery to your Activity dashboard. It’s a great way to keep track of the galleries you like.
        <br/>You can "unfollow" a gallery anytime. They will not be notified.</p>
        <p><r:img uri="/images/following_1.jpg" class="bordergray"/></p>
        <p>Navigate to You &gt; Activity &gt; Following and find all the galleries you follow and your followers.
        </p>
        <p><r:img uri="/images/following_2.jpg" class="bordergray"/></p>
        <div class="clear-left"></div>
        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <a name="how_do_comments_work"><h5>How do Comments work?</h5></a>
        <p>Members can comment on a gallery, an artwork or an artist timeline. <br />
            Navigate to You &gt; Activity &gt; Comments to moderate the comments you sent and received.</p>
        <p><r:img uri="/images/comments_1.jpg" class="bordergray"/></p>
        <p>You will be notified by email if a comment has been made. If you have decided not to receive notification by email, you will need to log into your <br/>dashboard to see any updates. You can approve, delete, or report the comment if you've found it voilates our community guidelines.
        </p>
        <p>Note that once you have approved it, that comment gets published publicly and can't be deleted in the public facing pages.
        </p>
        <p>You can delete the approved comments to manage and clean up your dashboard from time to time but this will not erase the comments you've already sent.
        </p>

        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->

</body>
