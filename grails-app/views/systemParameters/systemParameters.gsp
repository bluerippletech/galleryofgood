<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='systemparameters.edit'/></title>
    <r:require modules="foundation"/>
</head>

<body>
<div class="ten columns ap-right">
    <g:form action="saveParameters" name='editSystemParameters' class="custom nice">

        <g:render template="/layouts/flashMessage"/>
        <g:render template="/layouts/flashErrors"/>
        <p class="ap-page">System Parameters</p>

        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <input type="submit" class="small blue button round" value="Save">
            </div>
        </div>
        <g:each in="${com.galleryofgood.common.SystemParameter.constraints.name.inList}" var="parameter">
            <div class="row">
                <div class="ten columns horizontal_fields">

                    <label for="name.${parameter}">${parameter}:</label>
                    <g:textField name="name.${parameter}"
                                 value="${com.galleryofgood.common.SystemParameter.findByName(parameter)?.content}"
                                 class="input-text"/>
                    %{--<g:hasErrors bean="${appUserInstance}" field="password">--}%
                    %{--<small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"--}%
                    %{--field="password"/></small>--}%
                    %{--</g:hasErrors>--}%

                </div>
            </div>
        </g:each>

        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <input type="submit" class="small blue button round" value="Save">
            </div>
        </div>

    </g:form>
</div>

</body>
