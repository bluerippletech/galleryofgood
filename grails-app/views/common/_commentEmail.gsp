Hi, ${notiOwner}!<br/><br/>
    ${user} ${galleryName?'of '+galleryName:''} commented on your ${location}:<br/>
    “${content}”<br/><br/>
    <g:link controller="login" action="auth" absolute="true">Sign-in</g:link> to approve, delete or flag this comment.
    Copy and paste the URL below if the link does not work: <g:link controller="login" action="auth" absolute="true">${createLink(controller:"login", action:"auth", absolute:true)}</g:link>
    <br/>
    <br/>
    Keep creating!<br/>
    The Gallery of Good Team
    <br/>
    <br/>
<div style="color:#666666;font-size:11px">
    This email was sent to  ${com.galleryofgood.security.AppUser.findByUsername(notiOwner).email}. Update your <g:link controller="appUser" action="notificationSettings" absolute="true">email preferences</g:link> anytime.
    <br/><br/>
    &copy; 2012 Gallery of Good, Inc. All Rights Reserved. <br/>
    <g:link controller="general" action="privacyPolicy" absolute="true">Privacy Policy</g:link> | <g:link controller="general" action="termsOfService" absolute="true">Terms of Service</g:link> | <g:link controller="contactUs" action="index" absolute="true">Contact Us</g:link>
</div>