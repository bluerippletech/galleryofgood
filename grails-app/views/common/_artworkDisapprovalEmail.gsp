<%@ page import="com.galleryofgood.gallery.Artwork; com.galleryofgood.gallery.Gallery" %>
Hi, ${notiOwner}!<br/><br/>
We’re sorry. After reviewing <g:each in="${artworks}" var="artwork">${artwork.title}, </g:each>we decided that the subject matter is not in keeping with our <g:link controller="general" action="communityGuidelines" absolute="true">community guidelines</g:link> and can’t be published live. Please review our <g:link controller="general" action="communityGuidelines" absolute="true">community guidelines</g:link> so that you can successfully add artworks to your gallery in the future.<br/><br/>
The Gallery of Good Team
<br/>
<br/>
<div style="color:#666666;font-size:11px">
    This email was sent to  ${com.galleryofgood.security.AppUser.findByUsername(notiOwner).email}. Update your <g:link controller="appUser" action="notificationSettings" absolute="true">email preferences</g:link> anytime.
    <br/><br/>
&copy; 2012 Gallery of Good, Inc. All Rights Reserved. <br/>
    <g:link controller="general" action="privacyPolicy" absolute="true">Privacy Policy</g:link> | <g:link controller="general" action="termsOfService" absolute="true">Terms of Service</g:link> | <g:link controller="contactUs" action="index" absolute="true">Contact Us</g:link>
</div>