<%@ page import="com.galleryofgood.common.Homepage; com.galleryofgood.security.AppUser; com.galleryofgood.user.UserDetails; com.galleryofgood.gallery.Timeline; com.galleryofgood.gallery.Artwork; com.galleryofgood.gallery.Gallery; com.galleryofgood.common.CommentService; com.galleryofgood.common.Comment" %>
<g:if test="${params.action != 'artworkview' && params.action != "timelineview" && params.action != "aboutUs"}"><div class="row agg"></g:if>
<div class="six columns">
    <a name="commentsSection" href="#" id="commentsSection"></a>
    <g:set var="commentsList"
           value="${Comment.findAllByLocationAndLocationIdAndStatus(modelInstance?.class == Timeline ? "Artist Timeline" : modelInstance?.class == Artwork ? "Artwork" : modelInstance?.class == Gallery ? "Gallery" : modelInstance?.class == Homepage ? "Homepage" : "", modelInstance?.id, 'Approved')}"/>
    <h5>Comments: <span id="comment-counter">${commentsList.size()}</span></h5>

    <div id="comment-view-container">
        <g:each in="${commentsList.sort {it.id}}" var="comment">
            <a name="${comment?.id}" href="#"></a>
            <div class="row">
                <div class="tree  columns">
                    <img width="80" height="80"
                         src="${UserDetails.findByAppUser(AppUser.findByUsername(comment.username)).avatarLocationUrl ?: resource(dir: "images", file: "eb-3.jpg")}"
                         alt="">
                </div>
                <br>

                <div class="nine columns">
                    <g:if test="${com.galleryofgood.gallery.Gallery.findByOwner(comment.username)}">
                        ${comment.username} of
                        <a href="${createLink(uri: '/') + com.galleryofgood.gallery.Gallery.findByOwner(comment.username).galleryNameUrl}">${com.galleryofgood.gallery.Gallery.findByOwner(comment.username).galleryName}</a> gallery: <span class="commentContent">${comment.content}</span>
                    </g:if>
                    <g:else>
                        ${comment.username}: <span class="commentContent">${comment.content}</span>
                    </g:else>
                    <br>
                    <sec:ifLoggedIn>
                    <a class="buttonForModal" href="#" commentid="${comment?.id}">Flag</a>
                    </sec:ifLoggedIn>
                </div>
            </div>
        </g:each>
    </div>
    <sec:ifNotLoggedIn>
        <input type="button" class="btn-blue btn-blue-smaller" onclick="location.href = '${createLink(controller:'general', action:'commentRedirect', params:['formerController':params.controller, 'formerAction':params.action, 'id':params.id])}'" value="Sign-in to Comment">
    </sec:ifNotLoggedIn>
    <sec:ifLoggedIn>
        <form class="nice" id="comment-form" onsubmit="return addComment()">
            <div class="row">
                <div class="six columns mg-txtarea">
                    <textarea id="comment" placeholder="Leave some good words for this artist."></textarea>
                    <!-- <input type="submit" class="mg-input" value="" /> -->
                    <input type="submit" value="Post" class="btn-blue-smaller">

                    <p class="moderated-txt">All comments are moderated.</p>
                </div>
            </div>
        </form>
    </sec:ifLoggedIn>
</div>
<g:if test="${params.action != 'artworkview' && params.action != "timelineview"}"></div></g:if>
<script>
    $(function () {
        $('.buttonForModal').click(function () {
            $("#commentFlagForm #id").val($(this).attr("commentid"));
            $("#commentContent").html($(this).parent().find(".commentContent").html());
            $("#flagcomment").val("");
            $('#commentModal').reveal({animation:'fadeAndPop',
                                        animationspeed:300,
                                        closeOnBackgroundClick:true,
                                    dismissModalClass:'close-reveal-modal'});
            return false;
        });
    });


    function addComment() {
        var commentContent = $("#comment").val();
        $.ajax({
            url:"${createLink(uri: '/' )}" + "comment" + "/" + "addComment" + "${params.controller == 'gallery'?'Gallery':params.controller == 'artwork'?'Artwork':params.controller == 'timeline'?'Timeline':params.controller == 'general'?'Homepage':''}",
            type:"POST",
            data:{id:"${params.id?:modelInstance.id}", content:commentContent},
            dataType:"json",
            beforeSend:function () {
            },
            success:function (results) {
                $("#comment").val("");
//                $("#comment-view-container").append("<div class=\"row new\" style=\"display:none;\"><div class=\"tree  columns\"><img width=\"80\" height=\"80\" src=\"" + results.imgUrl + "\" alt=\"\"></div><br><div class=\"nine columns\">"
//                        + results.username + " of " + results.link + " gallery: " + commentContent +
//                        "<br><a href=\"#\" data-reveal-id=\"myModal\" data-animation=\"fadeAndPop\" data-animationspeed=\"300\" data-closeonbackgroundclick=\"true\" data-dismissmodalclass=\"close-reveal-modal\">Flag</a></div>");
                $("#comment-view-container").append("<div class=\"row new\" style=\"display:none;\"><div class=\"tree  columns\"><h6>Thanks for your feedback! Your comment is awaiting moderation.</h6></div></div>");
                $(".new").show().removeClass("new");

                document.getElementById('comment-view-container').scrollTop = 9999999;
//                $("#comment-counter").text(parseInt($("#comment-counter").text()) + 1);
            }
        });
        return false;
    }

    function flagComment() {
        $(".close-reveal-modal").click();
        jQuery.ajax({type:'POST',
            data:$("#commentFlagForm").serialize(),
            url:'${createLink(controller: 'flag', action: 'flagComment')}',
            success:function (data, textStatus) {
                $("#flagcomment").val("");
                flashWarning(data);
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
                flashWarning(data);
            }
        });
    }
</script>
<sec:ifLoggedIn>
    <!-- Modal Flag -->
    <form class="custom nice flag-form" action="#" method="post" enctype="multipart/form-data" id="commentFlagForm">
        <g:hiddenField name="id" value=""/>
        <div id="commentModal" class="reveal-modal">
            <p>From: <sec:loggedInUserInfo field="username"/><br/>
                Comment: <span id="commentContent">${artworkInstance?.title}</span>

            <p>

            <p>Tell us why your are reporting this:</p>
            <textarea id="flagcomment" name="comment" class="wider"></textarea>
            <a href="#" onclick="javascript:flagComment();" class="nice small radius white button" name="submit"
               value="Submit">Submit</a>
            <a class="close-reveal-modal">&#215;</a>
        </div>
    </form>
</sec:ifLoggedIn>