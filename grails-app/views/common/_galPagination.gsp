<script type="text/javascript">
    //initialize
    $(document).ready(function(){
        loadByPagination(${attrs.count}, 0, '${attrs.sort}', '${attrs.order}', '${attrs.controller}', '${attrs.action}', '${attrs.containerId}');
        manageVisible(1)
    });
    //AJAX Pagination
    function managePagination(page, lastPage){
        if(page == "next"){
            var currentObj = $("li.current");
            if(parseInt(currentObj.find("a").html()) < lastPage){
                manageNavButtons(parseInt(currentObj.find("a").html())+1, lastPage)
                var offsetVal = (parseInt(currentObj.find("a").html())) * ${attrs.count};
                loadByPagination(${attrs.count}, offsetVal, '${attrs.sort}', '${attrs.order}', '${attrs.controller}', '${attrs.action}', '${attrs.containerId}');
                $("li#p"+(parseInt(currentObj.find("a").html())+1).toString()).addClass("current");
                currentObj.removeClass("current");
            }
        }
        else if(page == "prev"){
            var currentObj = $("li.current");
            if(parseInt(currentObj.find("a").html()) <= lastPage && parseInt(currentObj.find("a").html()) > 1){
                manageNavButtons(parseInt(currentObj.find("a").html())-1, lastPage)
                var offsetVal = (parseInt(currentObj.find("a").html())-2) * ${attrs.count};
                loadByPagination(${attrs.count}, offsetVal, '${attrs.sort}', '${attrs.order}', '${attrs.controller}', '${attrs.action}', '${attrs.containerId}');
                $("li#p"+(parseInt(currentObj.find("a").html())-1).toString()).addClass("current");
                currentObj.removeClass("current");
            }
        }
        else {
            var currentObj = $("li.current");
            manageNavButtons(page, lastPage)
            var offsetVal = (page-1) * ${attrs.count};
            loadByPagination(${attrs.count}, offsetVal, '${attrs.sort}', '${attrs.order}', '${attrs.controller}', '${attrs.action}', '${attrs.containerId}');
            $("li#p"+page.toString()).addClass("current");
            currentObj.removeClass("current");
            manageVisible(parseInt(page), parseInt(lastPage));
        }
    }
    //checker to enable/disable next and previous buttons
    function manageNavButtons(page, lastPage){
       if(page==1){
           $('li#pPrev').addClass('unavailable')
           $('li#pNext').removeClass('unavailable')
       }else if(page>1 && page<lastPage){
           $('li#pPrev').removeClass('unavailable')
           $('li#pNext').removeClass('unavailable')
       }else if(page==lastPage){
           $('li#pNext').addClass('unavailable')
           $('li#pPrev').removeClass('unavailable')
       }
       manageVisible(parseInt(page), parseInt(lastPage));
    }
    //pagination visibility manager
    function manageVisible(page, end){
        var objHolder = $("ul.pagination")
        if(page == 1){
            for(var x=1; x<=5; x++){
                if(objHolder.find('[title="'+x+'"]')){
                    objHolder.find('[title="'+x+'"]').show()
                }
            }
        }else if(page > 3 && page <= end-2){
            objHolder.find('[title]:visible').hide();
            objHolder.find('[title="'+(page-2)+'"]').show();
            objHolder.find('[title="'+(page-1)+'"]').show();
            objHolder.find('[title="'+(page)+'"]').show();
            objHolder.find('[title="'+(page+1)+'"]').show();
            objHolder.find('[title="'+(page+2)+'"]').show();
        }

    }
    //AJAX Load :: paste this code to load pagination
    <%--
    /*function loadByPagination(max, offset, sort, order, controller, action, containerId){
        $.ajax({
            url: "${createLink(uri: '/' )}"+controller+"/"+action,
            type: "POST",
            data: {max:max, offset:offset, sort:sort, order:order},
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
               $("#"+containerId).html(results);
            }
        });
    }*/
    --%>
</script>