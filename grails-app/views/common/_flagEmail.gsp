<%@ page import="com.galleryofgood.gallery.Artwork; com.galleryofgood.gallery.Gallery" %>
Hi, ${notiOwner}!<br/><br/>
Your ${location=='Gallery'?'gallery: '+Gallery.read(locationId)?.galleryName:location=='Artwork'?'artwork: '+Artwork.read(locationId)?.title:''} has been flagged by another member as potentially inappropriate for younger viewers.<br/>
It’s important that we keep our community family-friendly, so please review the  ${location=='Gallery'?'gallery':location=='Artwork'?'artwork':''} to ensure it meets our <g:link controller="general" action="communityGuidelines" absolute="true">community guidelines</g:link>. If not, please remove it within 24 hours; otherwise we will remove it permanently.<br/>
Thank you for considering our younger artists. We hope you keep creating!<br/><br/>
The Gallery of Good Team
<br/>
<br/>
<div style="color:#666666;font-size:11px">
    This email was sent to  ${com.galleryofgood.security.AppUser.findByUsername(notiOwner).email}. Update your <g:link controller="appUser" action="notificationSettings" absolute="true">email preferences</g:link> anytime.
    <br/><br/>
    &copy; 2012 Gallery of Good, Inc. All Rights Reserved. <br/>
    <g:link controller="general" action="privacyPolicy" absolute="true">Privacy Policy</g:link> | <g:link controller="general" action="termsOfService" absolute="true">Terms of Service</g:link> | <g:link controller="contactUs" action="index" absolute="true">Contact Us</g:link>
</div>