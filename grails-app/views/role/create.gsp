<html>
<head>
    <meta name='layout' content='adminmain'/>
    <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <r:require modules="foundation,jquery-ui,jquery"/>
</head>
<body>
<div class="ten columns ap-right">
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
<g:form action="save" name='roleCreateForm' class="custom nice">
    <p class="ap-page"><g:message code="default.create.label" args="[entityName]"/></p>
        <div class="row">
            <div class="nine columns">
                <label for="authority">Authority</label>
                <g:textField name="authority" value="${role?.authority}" class="input-text"/>
                <g:hasErrors bean="${role}" field="authority">
                    <small class="error" style="margin:1px;">
                        <g:renderErrors bean="${role}" field="authority"/></small>
                </g:hasErrors>
                <label for="authority">Description</label>
                <g:textField name="description" value="${role?.description}" class="input-text"/>
                <g:hasErrors bean="${role}" field="authority">
                    <small class="error" style="margin:1px;">
                        <g:renderErrors bean="${role}" field="description"/></small>
                </g:hasErrors>
            </div>
        </div>

        <div style='float:left; margin-top: 10px;'>
            <input type="submit" value="Create" id="update_submit" class="small blue button round">
        </div>

</g:form>
</div>
</body>
</html>
