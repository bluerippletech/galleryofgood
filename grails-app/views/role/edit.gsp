<html>
<head>
    <meta name='layout' content='adminmain'/>
    <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <r:require modules="foundation,jquery-ui"/>
</head>

<body>
<g:form action="update" name='roleEditForm' class="custom nice">
    <g:hiddenField name="id" value="${role?.id}"/>
    <g:hiddenField name="version" value="${role?.version}"/>
    <%
        def tabData = []
        tabData << [name: 'roleinfo', icon: 'icon_role', messageCode: 'spring.security.ui.role.info']
        tabData << [name: 'users', icon: 'icon_users', messageCode: 'spring.security.ui.role.users']
    %>
    <div class="ten columns ap-right">
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
    <p class="ap-page"><g:message code="default.edit.label" args="[entityName]"/></p>
        <dl class="tabs">
            <dd><a class="active" href="#simple1">Role</a></dd>
            <dd><a href="#simple2">Users</a></dd>
        </dl>
        <ul class="tabs-content">
            <li id="simple1Tab" class="active" style="display: block;">
                <div class="row">
                    <div class="ten columns">
                        <label for="authority">Authority</label>
                        <g:textField name="authority" value="${role?.authority}" class="input-text"/>
                        <g:hasErrors bean="${role}" field="authority">
                            <small class="error" style="margin:1px;">
                                <g:renderErrors bean="${role}" field="authority"/></small>
                        </g:hasErrors>
                        <label for="authority">Description</label>
                        <g:textField name="description" value="${role?.description}" class="input-text"/>
                        <g:hasErrors bean="${role}" field="authority">
                            <small class="error" style="margin:1px;">
                                <g:renderErrors bean="${role}" field="description"/></small>
                        </g:hasErrors>
                    </div>
                </div>
            </li>
            <li id="simple2Tab" style="display: none;">
                <div class="row">
                    <div class="ten columns">
                        <g:if test='${users.empty}'>
                            <g:message code="spring.security.ui.role_no_users"/>
                        </g:if>
                        <g:each var="u" in="${users}">
                            <g:link controller='admin' action='userEdit' id='${u.id}'>${u.username.encodeAsHTML()}</g:link><br/>
                        </g:each>

                    </div>
                </div>
            </li>
        </ul>

    <div style='float:left; margin-top: 10px;'>
        <input type="submit" value="Update" id="update_submit" class="small blue button round">
        <g:if test='${role}'>
            <a id="deleteButton" class="small red button round">Delete</a>
        </g:if>
    </div>
</g:form>
<g:if test='${role}'>
    <form action='/GalleryOfGood/role/delete' method='POST' name='deleteForm'>
        <input type="hidden" name="id" value="${role?.id}"/>
    </form>

    <div id="deleteConfirmDialog" title="Are you sure?"></div>

    <r:script>
        $(document).ready(function () {
            $("#deleteButton").bind('click', function () {
                $('#deleteConfirmDialog').dialog('open');
            });

            $("#deleteConfirmDialog").dialog({
                autoOpen:false,
                resizable:false,
                height:100,
                modal:true,
                buttons:{
                    'Delete':function () {
                        document.forms.deleteForm.submit();
                    },
                    Cancel:function () {
                        $(this).dialog('close');
                    }
                }
            });
        });
    </r:script>
</g:if>
</div>
</body>
</html>
