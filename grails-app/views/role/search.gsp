<html>
<head>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='spring.security.ui.role.search'/></title>
    <r:require modules="foundation,jquery-ui,jquery"/>
</head>

<body>
<div class="ten columns ap-right">
    <g:form action='roleSearch' name='roleSearchForm' class="custom nice">
        <p class="ap-page">Search Roles</p>

        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <input type="submit" class="small blue button round" value="Search">
            </div>
        </div>
    %{--<s2ui:form width='100%' height='200' elementId='formContainer'--}%
    %{--titleCode='spring.security.ui.role.search'>--}%

        <div class="row">
            <div class="ten columns">
                <label for="role"><g:message code='role.authority.label' default='Authority'/>:</label>
                <g:textField name='authority' class='input-text' size='50' maxlength='255' autocomplete='off'
                             value='${authority}'/>
            </div>
        </div>

    %{--</s2ui:form>--}%
        <g:if test='${searched}'>
            <div class="row">
                <div class="ten columns">
                    <%
                        def queryParams = [authority: authority]
                    %>
                    <table>
                        <thead>
                        <tr>
                                <g:sortableColumn property="authority"
                                                  title="${message(code: 'role.authority.label', default: 'Authority')}"
                                                  params="${queryParams}"/>
                        </tr>
                        </thead>

                        <tbody>
                        <g:each in="${results}" status="i" var="role">
                            <tr  class="${(i % 2) == 0 ? 'odd' : 'even'}">
                                <td><g:link action="edit"
                                            id="${role.id}">${fieldValue(bean: role, field: "authority")}</g:link></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>

                </div>
            </div>


        </g:if>


        <script>
            $(document).ready(function () {
                $("#authority").focus().autocomplete({
                    minLength:2,
                    cache:false,
                    source:"${createLink(action: 'ajaxRoleSearch')}"
                });
            });
        </script>
    </g:form>
</div>

</body>
</html>
