<%@ page import="com.galleryofgood.gallery.Artwork" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="springSecurity.denied.title" /></title>
    <r:require modules="foundation"/>
</head>
<body>
<!-- main content area -->
<div class="row">
    <div class="eight columns">
        <br />
        <br />
        <h2 class="heading color-black">oops!</h2>
        <p>	<div class='errors'><g:message code="springSecurity.denied.message" /></div></p>
    </div>

    <div class="four columns error_404_box error_box">
        500
    </div>

</div>
<div class="row">
    <div class="twelve columns">
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
<!-- main content area -->
</body>
</html>