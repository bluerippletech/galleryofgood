<html>
<head>
    <meta name='layout' content='adminmain'/>
    <title><g:message code="springSecurity.login.title"/></title>
    <r:require module="foundation"/>
</head>

<body>
<form class="custom nice" name="input" action='${postUrl}' method="POST" id='loginForm' autocomplete='off'>
    <g:hiddenField name="login_page" value="admin"/>
    <!-- container -->
    <div class="container">
        <div class="row admin-login">
            <div class="al-logo"></div>
            <div class="al-form">
                    <label>Name</label>
                    <input type='text' class='input-text' name='j_username' id='username'/>
                    <g:if test="${flash.message}">
                        <small class="error">${flash.message}</small>
                    </g:if>

                    <label>Password</label>
                    <input type='password' class='input-text' name='j_password' id='password'/>
                <input type="submit" value="Sign-in"><br/>
                <span>Forget Username / Password? <a href="mailto:${com.galleryofgood.common.SystemParameter.findByName("AdminEmail").content}">Contact Admin</a></span>

            </div>
        </div>
    </div>
</form>

<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
