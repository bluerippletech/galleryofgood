<%@ page import="com.galleryofgood.gallery.Timeline; com.galleryofgood.gallery.Gallery; com.galleryofgood.common.Comment; com.galleryofgood.gallery.Artwork" %>
<head xmlns="http://www.w3.org/1999/html">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='gallery.view.title' args='${[gallery?.galleryName]}'/></title>
    <r:require modules="application,foundation,lazyload,tagcloud,colorbox"/>
    <r:script>
        $.fn.tagcloud.defaults = {
            size:{start:14, end:18, unit:'pt'},
            color:{start:'#000', end:'#000'}
        };

        $.fn.onAvailable = function(fn){
            var sel = this.selector;
            var timer;
            var self = this;
            if (this.length > 0) {
                fn.call(this);
            }
            else {
                timer = setInterval(function(){
                    if ($(sel).length > 0) {
                        fn.call($(sel));
                        clearInterval(timer);
                    }
                },50);
            }
        };
        setTimeout(function(){
        },5000);


        function startSlideshow(){
        $("#slideshow").find("a").first().click();
        }

        function windowHeight() {
            var myWidth = 0, myHeight = 0;
            if( typeof( window.innerWidth ) == 'number' ) {
                myHeight = window.innerHeight;
            } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
                myHeight = document.documentElement.clientHeight;
            } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
                myHeight = document.body.clientHeight;
            }
        window.browserHeight = myHeight;
        }

        $(document).ready(function (){
            windowHeight();
            $('#slideshow a').colorbox({rel:'colorbox', fixed: true,
             current : 'Artworks  {current} / {total}', close: '&times;', slideshow: true, slideshowAuto: false, slideshowSpeed: 7000, slideshowStart: 'Start', slideshowStop: "Stop Slideshow", maxHeight: browserHeight
        });
        })

        $(function () {
            criteria.push("All Types");
            getItems("items_container", searchType)
            $('#tagcloud a').tagcloud();
            $("img.lazy").lazyload({effect:"fadeIn"});

        });

        function flagItem(){
        $(".close-reveal-modal").click();

            jQuery.ajax({type:'POST',
                        data:$("#flagForm").serialize(),
                        url:'${createLink(controller: 'flag', action: 'flagGallery')}',
                        success:function(data, textStatus) {
                        $("#comment").val("");
                        $('#myModal').find("div#content").html(data);
                        $('#myModal').reveal();
                        window.setTimeout(
                            function() {
                                $(".close-reveal-modal").click();
                            },
                            20000
                        );
                        //flashWarning(data);
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown) {
                        flashWarning(data);
                        }
                    });



//            alert('item is flagged, submit via ajax.');
        }
        var criteria = [];
        var searchType = "All";
        var galleryname = "${gallery.galleryName}"
        function selector(selected, filterBy){
            if($(selected).closest("li").hasClass("selected")){
                $(selected).closest("li").removeClass("selected");
                if(filterBy=="type"){
                    criteria.splice($.inArray($(selected).text().toString(), criteria),1);
                    getItems("items_container", searchType);
                }
            }else{
                if($(selected).text().toString() == "All Types"){
                    $(selected).closest("ul").find(".selected").removeClass("selected");
                    $(selected).closest("li").addClass("selected");
                    if(filterBy=="type"){
                        criteria = [];
                        criteria.push($(selected).text().toString());
                        getItems("items_container", searchType)
                    }
                }else{
                    if($(selected).closest("ul").find(".selected a").text().toString() == "All Types"){
                        $(selected).closest("ul").find(".selected").removeClass("selected");
                        criteria = [];
                        criteriaAge = [];
                    }
                    $(selected).closest("li").addClass("selected");
                    if(filterBy=="type"){
                        criteria.push($(selected).text().toString());
                        getItems("items_container", searchType)
                    }
                }
            }
        }
        function getItems(containerId, filterBy){
            var queryCriteria ="";
            $(criteria).each( function(i){
                if(queryCriteria == "")
                    queryCriteria += "criteria=" + this;
                else
                    queryCriteria += "&criteria=" + this;
            })
            queryCriteria += "&artist=" + filterBy;
            $.ajax({
                url:  "${createLink(controller: params.controller, action: 'itemsList', id: gallery?.id)}",
                type: "POST",
                data: queryCriteria,
                dataType: "html",
                beforeSend: function() {
                },
                success: function(results) {
                   $("#"+containerId).html(results);
                }
            });
        }
        function selectCriteria(selected, filterBy){
            $(selected).closest("ul").find(".selected").removeClass("selected");
            $(selected).closest("li").addClass("selected");
            searchType = filterBy;
            getItems("items_container", filterBy);
        }
        <g:if test="${params.myModal == 'true'}">
            $('#myModal').reveal();
        </g:if>
    </r:script>
</head>

<body>
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=307880795966709";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = "//platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
    }
}(document, "script", "twitter-wjs");</script>
<!-- main content area -->
%{--<div class="row mgp-ribbon mgp-top">--}%
%{--<div class="eight columns">--}%
%{--<p>--}%
%{--Artworks from this gallery have been donated to <a href="#">art fundraisers</a>.--}%
%{--</p>--}%
%{--</div>--}%
%{--<div class="four columns">--}%
%{--<a href="#"><span>goodworks: 300</span></a>--}%
%{--</div>--}%
%{--</div>--}%
<g:render template="/layouts/flashWarning"/>

<sec:ifNotLoggedIn>
    <g:render template="/layouts/modalNotify"
              model="['content': 'Please sign-in or sign-up to flag this gallery.', 'galleryName': gallery.galleryName, timeout: 10000]"/>
    <g:render template="/layouts/modalNotify"
              model="['content': 'Please sign-in or sign-up to follow this gallery.', 'galleryName': gallery.galleryName, timeout: 10000, 'elementId': 'modalNotify1']"/>
</sec:ifNotLoggedIn>

<div class="row mgp-mid">
    <div class="eight columns">
        <h2 class="heading">${gallery?.galleryName}</h2>
        <ripple:processText>
            ${gallery?.getDescription()}
        </ripple:processText>
    </div>

    <div class="four columns">
        <ul>
            <li>Artworks: ${artworks.size()}</li>
            <li>Age Group: ${ageGroups}</li>
            <li>
                <a href="" onclick="javascript:startSlideshow();
                return false;">View Slideshow</a>

                <div id="slideshow">
                    <g:each in="${artworks}" var="artwork" status="i">
                        <a href="${artwork?.url}"
                           title="<a class='slideshow-title' href=${createLink(controller: 'artwork', action: 'artworkview', id: artwork?.id)}>${artwork?.title}</a> <br/><span class='slideshow-artist'>by ${artwork?.artist}<span>"></a>
                    </g:each>
                </div>
            </li>
            <li><g:link controller="gallery" action="viewComments"
                        params="[id: gallery.id]">Comments: ${Comment.countByLocationAndLocationIdAndStatus(gallery.class == Timeline ? "Artistic Timeline" : gallery.class == Artwork ? "Artwork" : gallery.class == Gallery ? "Gallery" : "", gallery.id, "Approved")}</g:link></li>
            <g:if test="${gallery?.owner != activeArtist}">
                <li><ripple:followLink id="${gallery.id}"/><ripple:followableScript
                        controller="${params.controller}"/></li>
                <sec:ifLoggedIn>
                    <li>
                        <a href="#" data-reveal-id="myModal" data-animation="fadeAndPop" data-animationspeed="300"
                           data-closeonbackgroundclick="true"
                           data-dismissmodalclass="close-reveal-modal">Flag Gallery</a>
                    </li>
                </sec:ifLoggedIn>
            </g:if>
            <sec:ifNotLoggedIn>
                <li><a href="#" data-reveal-id="modalNotify1"
                       data-animation="fadeAndPop" data-animationspeed="300"
                       data-closeonbackgroundclick="true" data-dismissmodalclass="close-reveal-modal"
                       class="modal-show">Follow</a></li>
                <li>
                    <a href="#" data-reveal-id="modalNotify"
                       data-animation="fadeAndPop" data-animationspeed="300"
                       data-closeonbackgroundclick="true" data-dismissmodalclass="close-reveal-modal"
                       class="modal-show">Flag Gallery</a>
                </li>
            </sec:ifNotLoggedIn>
            <li><p></p></li>
            <li>
                <div class="social-container">
                    <ul class="shareLinks" style="overflow:visible;">
                        <li style="width:59px;padding:0px;"><a href="https://twitter.com/share"
                                                               class="twitter-share-button"
                                                               data-url="${bitly.shorten(url: createLink(uri: '/', absolute: true) + gallery.galleryNameUrl)}"
                                                               data-hashtags="GalleryOfGood" data-count="none">Tweet</a>
                        </li>
                        <li style="width:96px;padding:0px;"><fb:like
                                href="${createLink(uri: '/', absolute: true) + gallery.galleryNameUrl}"
                                send="true" layout="button_count" width="150" height="23px"
                                show_faces="false"></fb:like></li>
                    </ul>
                    <style>
                    ul.shareLinks {
                        overflow: auto;
                        border: none;
                    }

                    ul.shareLinks li {
                        list-style: none;
                        float: left;
                        margin-right: 5px;
                    }
                    </style>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p></p>
    </div>
</div>

<div id="secondary_navigation" class="row srdd">
    <div class="twelve columns srdd-crit">
        <ul id="level-1">
            <g:if test="${!activeArtist && gallery?.galleryOwner?.equals("YoungArtist")}">
                <li class="${activeArtist ? "selected" : "selected"}">
                    <a href="javascript:" onclick="selectCriteria(this, 'All')">All Artists</a>
                </li>

            <g:each in="${gallery?.youngArtists}" var="artist">
                <g:set var="timeline" value="${com.galleryofgood.gallery.Timeline.findByArtistName(artist.nickname)}"/>
                <li class="${activeArtist?.equals(timeline?.artistName) ? "selected" : (!gallery?.galleryOwner?.equals("YoungArtist") ? 'selected' : '')}">
                    <a href="javascript:"
                       onclick="selectCriteria(this, '${timeline?.artistName}')">${timeline?.artistName}</a>
                    <g:if test="${gallery?.artworks?.findAll {it.artist == timeline?.artistName && it.status == "Approved" && it.showInTimeline == "on"}?.size() > 0}">
                        <a href="${createLink(controller: 'timeline', action: 'timelineview', params: [id: timeline?.id])}"><r:img
                                class="clockImg" uri="/images/ico-clock.png" alt=""/></a>
                    </g:if>
                </li>
            </g:each>
            </g:if>
            <g:else>
                <g:set var="timeline" value="${com.galleryofgood.gallery.Timeline.findByArtistName(gallery.owner)}"/>
                <li class="selected">
                    <a href="javascript:"
                       onclick="selectCriteria(this, '${timeline?.artistName}')">${timeline?.artistName}</a>
                    <g:if test="${gallery?.artworks?.findAll {it.artist == timeline?.artistName && it.status == "Approved" && it.showInTimeline == "on"}?.size() > 0}">
                        <a href="${createLink(controller: 'timeline', action: 'timelineview', params: [id: timeline?.id])}"><r:img
                                class="clockImg" uri="/images/ico-clock.png" alt=""/></a>
                    </g:if>
                </li>

            </g:else>
        </ul>

        <div class="clear-both"></div>


        <ul id="level-2">
            <li class="selected"><a href="javascript:" onclick="selector(this, 'type')" id="selector-All">All Types</a>
            </li>
            <g:each in="${com.galleryofgood.gallery.Artwork.constraints.artworkType.inList}" var="type">
                <g:if test="${artworks?.findAll {it.gallery == gallery && it.artworkType == type}.size() > 0}">
                    <li><a href="javascript:" onclick="selector(this, 'type')" id="selector-${type}">${type}</a></li>
                </g:if>
            </g:each>
        <%--<li><a class="orange" href="#">Goodworks</a></li>--%>
        </ul>
    </div>
</div>

<div class="row" id="items_container">

    <!--Items-->

</div>


<div class="row">
    <div class="twelve columns">
        <br/><br/><br/>
    </div>
</div>

<div class="row mgp-tags">
    <div class="eight columns mg-adp-tags">
        <h5>Emerging Themes</h5>

        <div id="tagcloud">
            <g:each in="${tags}" var="tag">
                <a class="emerging-themes"
                   href="${createLink(controller: 'search', action: 'themeTag', params: [theme: tag.key])}"
                   rel="${tag.value}">${tag.key}</a>
            %{--<a class="emerging-themes" href="${createLink(controller: gallery?.galleryNameUrl, params: [artist: activeArtist, tag: tag.key])}"--}%
            %{--rel="${tag.value}">${tag.key}</a>--}%
            </g:each>
        </div>
    </div>

    <div class="four columns mg-adp-tags">
        <h5>More from this Gallery</h5>
        <a href="${website}" target="_blank">My website</a>
        <br/>
        %{--<a href="#">My goodworks</a>--}%
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <br/>

        <p></p><br/>
    </div>
</div>
<!-- main content area -->

<!-- container -->

<!-- Modal Container slider -->
<div id="myModal1" class="reveal-modal modal-slideshow" style="display:none;">
    <div id="featured">
        <g:each in="${artworks}" var="artwork" status="i">
        %{--<img class="lazy"  src="${createLink(uri:'/images/loading.gif')}" data-original="${artwork?.url}" alt="Slide 01" data-thumb="slide01_thumb.jpg" data-caption="#htmlCaption${i}" />--}%
            <div style="width: 920; height: 611px !important; overflow: hidden;background-color:white;"
                 data-caption="#htmlCaption${i}">
                <img src="${artwork?.url}" width="920" height="611" alt="Slide 0${i}" data-thumb="slide01_thumb.jpg"
                     style="overflow:hidden;"/>
            </div>
        </g:each>
    </div>
    <g:each in="${artworks}" var="artwork" status="i">
        <span class="orbit-caption" id="htmlCaption${i}">
            <h5>${artwork?.title}</h5>

            <p>By: ${artwork?.artist}</p>

            <p>Comments: <a
                    href="${createLink(controller: 'artwork', action: 'artworkview', id: artwork?.id, fragment: 'commentsSection')}"
                    onclick="javascript:$('.close-reveal-modal').click();">${com.galleryofgood.common.Comment.countByIdAndLocationAndStatus(artwork?.id, "Artwork", "Approved")}</a>
            </p>
        </span>
    </g:each>
    <a class="close-reveal-modal">&#215;</a>
</div> <!-- end myModal1 -->
<!-- Modal Flag -->
<sec:ifLoggedIn>
    <form class="custom nice flag-form" action="#" method="post" enctype="multipart/form-data" id="flagForm">
        <g:hiddenField name="id" value="${gallery?.id}"/>
        <div id="myModal" class="reveal-modal">
            <div id="content">
                <p>From: <sec:loggedInUserInfo field="username"/><br/>
                    Gallery: ${gallery?.galleryName}

                <p>

                <p>Please let us know why you think this artwork shouldn’t be in Gallery of Good. <br/>We will review it pronto!
                </p>
                <textarea id="comment" name="comment" class="wider"></textarea>
                <a href="#" onclick="javascript:flagItem();" class="nice small radius white button" name="submit"
                   value="Submit">Submit</a>
            </div>
            <a class="close-reveal-modal">&#215;</a>
        </div>
    </form>
</sec:ifLoggedIn>
<!-- Included JS Files -->

</body>
