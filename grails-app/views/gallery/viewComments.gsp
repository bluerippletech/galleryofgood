<%@ page import="com.galleryofgood.gallery.Gallery" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='comments.view' default="View Comments"/></title>
    <r:require module="foundation"/>
    <r:script disposition="head">
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <h2 class="heading color-black"><a href="${createLink(controller: galleryInstance?.galleryNameUrl)}">${galleryInstance?.galleryName}</a></h2>
    </div>
</div>
<g:render template="/common/comments" model="[modelInstance:galleryInstance]"/>
<!-- main content area -->

</body>
