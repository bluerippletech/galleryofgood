<g:if test="${modelInstance}">
<div class="twelve columns" id="artworks-listing">

        <!--items-->

</div>
<div class="clear-both"></div>
<script type="text/javascript">
    //Pagination AJAX load
    function loadByPagination(max, offset, sort, order, controller, action, containerId){
        var queryCriteria ="";
        $(criteria).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteria=" + this;
            else
                queryCriteria += "&criteria=" + this;
        });
        queryCriteria += "&max="+ max + "&offset=" + offset + "&sort=" + sort + "&order=" + order;
        queryCriteria += "&artist=" + searchType;
        queryCriteria += "&galleryname=" + galleryname;
        $.ajax({
            url: "${createLink(uri: '/' )}"+controller+"/"+action,
            type: "POST",
            data: queryCriteria,
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
                $("#"+containerId).html(results);
            }
        });
    }
</script>
%{--<div class="twelve columns">--}%
    <ripple:paginate count="${36}" total="${modelInstance?.size()}"  sort="dateCreated" order="desc" controller="${params.controller}" action="itemsPagination" containerId="artworks-listing"/>
%{--</div>--}%
</g:if>
<g:else>
    <g:if test="${criteria}">
    <g:set var="criteriaString" value="${criteria.toString()?.equals("All Types")?"artworks":criteria?.toString()+(!(criteria?.toString().getAt(criteria.length()-1))?.equals("s")?"s":"")}"/>
    <div class="row">
        <div class="twelve columns">
            <p>This artist has not uploaded any ${criteriaString} yet.</p>
            %{--<p>Try again or browse the artworks by most recent, type and age or themes.</p>--}%
        </div>
    </div>
    </g:if>
    <g:else>
        <p>Please select an artwork type.</p>
    </g:else>
</g:else>
