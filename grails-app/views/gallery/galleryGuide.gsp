<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='gallery.guide.title'/></title>
    <r:require modules="foundation"/>
</head>

<body>
<g:render template="/layouts/flashMessage"/>
<sec:ifLoggedIn>
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'guide']"/>
</sec:ifLoggedIn>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">

        <ul id="link-list">
            <li><a href="#setting_up_gallery">Setting Up Your Gallery</a></li>
            <li><a href="#digital_photos">Taking Digital Photos Of Artworks</a></li>
            <li><a href="#adding_editing_artworks">Adding And Editing Artworks</a></li>

            <li><a href="#artist_timeline">The Artist Timeline</a></li>
            <li><a href="#sharing">Sharing Your Gallery With Family And Friends</a></li>
        </ul>
    </div>
</div>

<div class="row">

    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <a name="setting_up_gallery"><h5>Setting Up Your Gallery</h5></a>
        <p>Navigate to <strong>You &gt; Gallery &gt; Settings</strong><br />
            <ol>
            <li>Create a unique gallery name. Choose well - gallery names can't be changed.</li>
            <li>Add a description. </li>
            <li>Select a default type of artwork, example "Drawing". By selecting this, all artworks you upload will automatically fall under this category. You can change this selection after the upload process anytime. Use this feature if most of your artworks fall under one type. You&apos;ll save time editing in the future.</li>
            <li>Add Artists - Parents or legal guardians managing their children&apos;s galleries can add up to five artists in a gallery. Adding the artist&apos;s year of birth allows us to sort the artworks based on age groupings (i.e. Age 0-3). It also allows each of the artists to have their own <a href="#artist_timeline">Artist Timeline</a>. The year of birth is never shown publicly.</li>
            </ol>
        <p>
            All artworks are evaluated by our moderators before being published live.
            You will receive an email notification when your artworks have been approved or disapproved.
            Please review our <a href="${createLink(controller: 'general',action: 'communityGuidelines')}">Community Guidelines</a> and
            <a href="${createLink(controller: 'general',action: 'termsOfService')}">Terms of Service</a> for details about what may be considered inappropriate for younger audiences.
        </p>
        <p class="bordergray"><r:img uri="/images/1.jpg" alt="" /></p>
        <r:img class="float-left attention-icon" uri="/images/note.jpg" alt="Note" />
        <p class="attention-paragraph"><br />Your gallery will not be visible until you have added artworks to it.</p>
        <div class="clear-left"></div>
        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <a name="digital_photos"><h5>Taking Digital Photos Of Artworks</h5></a>
        <p>
            <ol>
                <li>Avoid using flash photography, as it tends to alter the color quality of the artwork. Instead, find a room that brings in natural light or take photos outdoors if weather permits. Make sure no glare or shadows are cast on the artwork.</li>
                <li>Upload photos that are 5MB or less.
                File sizes can usually be adjusted right in your camera settings or in any image editing software.
                If your photos exceed the maximum file allowed, your photo will not be uploaded.
                In most digital cameras, you can adjust your setting up to a medium resolution of 2592x1944 (4:3) or 2592x1936 for iPhones.
                This setting is more than adequate in creating great images for your artworks.
                We&apos;ll take care of resizing them to fit your gallery once you&apos;ve uploaded it.
                For tech savvy members, you can edit your photos to a maximum size of 837x652 and this should scale perfectly into our gallery.</li>
                <li>If the artwork is framed, remove the frames and glass fronts before photographing artworks to reduce glare. </li>
                <li>To protect the artist&apos;s privacy, cover any artist signature that includes an identifiable first and/or last name. Use the same discretion when the photos include the face of the artist.</li>
            </ol>
        </p>
        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">

        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <a name="adding_editing_artworks"><h5>Adding And Editing Artworks</h5></a>
        <p>1. Immediately after you've created your gallery, you can follow the prompts to start uploading photos of your artworks.</p>
        <p><img src="http://placehold.it/600x300" /></p>

        <p>If your photos are not handy, you can always return at another time to upload them.</p>
        <r:img class="float-left attention-icon" uri="/images/note.jpg" alt="Note" />
        <p class="attention-paragraph"><br />You will not be able to upload your artworks unless a gallery has been set-up.</p>
        <div class="clear-left"></div>
        <p>2. To add artworks: Navigate to <strong>You &gt; Gallery &gt; Artworks</strong><br />
        <p>Click on the "Upload Artworks" button. This will take you to the image uploader page.</p>
        <p class="bordergray"><r:img uri="/images/3.jpg" alt="" /></p>
        <p>Drag and drop your image files into the gray box.</p>
                <ul style="padding-left:5%;list-style-type:disc;">
                    <li>You can drop multiple files at one time.</li>
                    <li>Acceptable file types include jpg, png or gif files only. Videos, Flash or any other media are not allowed at this time.</li>
                </ul>
        <p class="bordergray"><r:img uri="/images/4.jpg" alt="" /></p>
        <p>Once your file has fully uploaded, click on the file to add details. You may:<br /></p>
            <ol style="padding-left:10%;list-style-type: lower-alpha">
                <li>Rename the title of the artwork.<span class="mandatory">*</span></li>
                <li>Choose the type of artwork (painting, drawing, etc.)<span class="mandatory">*</span></li>
                <li>Select the artist and the age when the artwork was created.<span class="mandatory">*</span></li>
                <li>Select the Mess Factor:</li>
                <ol style="list-style-type: lower-roman;">
                    <li>"No fuss, no muss" - not messy at all.</li>
                    <li>"Spread out the Newspaper" - might leave marks on the table or surface.</li>
                    <li>"Grab a smock" - may get the artist and the working surface dirty.</li>
                    <li>"Take cover!" - be prepared for fun, messy times!</li>
                </ol>
                <li>Add artwork dimensions.</li>
                <li>Add a description or an artist statement and share step-by-step instructions on how to recreate the artwork.</li>
                <li>Add tags to describe themes represented in the artwork.</li>
            </ol>
        <p>Items marked with a <span class="mandatory">*</span> are mandatory fields. This allows us to properly sort the artworks in your gallery and throughout the site.</p>
        <p>Don't forget to save the details you've just added!</p>
        <p class="bordergray"><r:img uri="/images/5.jpg" alt="" /></p>
        <p>Once you've added all the mandatory details, the "View Artworks" button will be activated. When clicked, you can check all the recent artworks uploaded in your dashboard. At this point, they are not yet visible in your public gallery however you can continue to edit each artwork by clicking on the artwork's thumbnail image.</p>
        <p class="bordergray"><r:img uri="/images/6.jpg" alt="" /></p>
        <p>In the meantime, our moderators are reviewing the artworks you've just uploaded and in little time, you'll receive a notification if they have been approved or disapproved. Please read our <a href="${createLink(controller: 'general',action: 'communityGuidelines')}">Community Guidelines</a> and
            <a href="${createLink(controller: 'general',action: 'termsOfService')}">Terms of Use</a> to ensure a successful addition of artworks in your gallery. </p>
        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">

        <a name="artist_timeline"><h5>The Artist Timeline</h5></a>
        <p>The Artist Timeline is a collection of artworks that track the artist's progress year over year. To get you started, our system automatically selects one artwork to represent each year. To view the selected artworks in your Artist Timeline, navigate to <b>You > Gallery > Artist Timeline</b>.
        </p>
        <p>To edit the selection, click on the <b>"Edit Artworks"</b> button. You may also add a text description of the artist's journey so far.</p>
        <p class="bordergray"><r:img uri="/images/7.jpg" alt="" /></p>
        <p>In <b>You > Gallery > Artworks</b>, refer to the columns <b>"Age Created"</b> and <b>"Artist Timeline"</b> and you'll see which artworks represent an age in the artist's life. To edit this setting, simply click on the thumbnail or title of the artwork then check or uncheck the box that says <b>"This artwork represents an age in the Artist Timeline"</b>. You can pick only one (1) artwork per age.</p>
        <p class="bordergray"><r:img uri="/images/8.jpg" alt="" /></p>
        <p class="bordergray"><r:img uri="/images/9.jpg" alt="" /></p>
        <p>To view the Artist Timeline in your public gallery, click on the timeline icon beside the Artist's name. Each Artist will have their own page.</p>
        <p class="bordergray"><r:img uri="/images/10.jpg" alt="" /></p>
        <p class="bordergray"><r:img uri="/images/11.jpg" alt="" /></p>
        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">

        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <a name="sharing"><h5>Sharing Your Gallery With Family And Friends</h5></a>
        <p> <ol>
        <li>Your gallery has a permanent URL which uses this convention: <u>http://www.galleryofgood.com/nameofgallery</u> (replace &#34;nameofgallery&#34; with your gallery name).</li>
        <li>To find your gallery URL, navigate to <strong>You &gt; Gallery &gt; Settings</strong>.</li>
        <p class="bordergray"><r:img uri="/images/12.jpg" alt="" /></p>
        <li>Bookmark your gallery URL.</li>
        <li>Use the social sharing tools in your Gallery page. You can share it on Facebook, Twitter or Pinterest.</li>
        <p class="bordergray"><r:img uri="/images/13.jpg" alt="" /></p>
        </ol>

        <p><a href="#" class="secondary_link">Back to top</a></p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>

</div>
<!-- main content area -->

</body>
