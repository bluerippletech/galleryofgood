<%@ page import="com.galleryofgood.gallery.Gallery" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='settings.gallery' default="Edit Gallery"/></title>
    <r:require module="foundation"/>
    <r:script disposition="head">

        //TODO: Hide when a minor
        function addArtists() {
            var youngArtists = $(".youngartists").children();
            var newArtists = youngArtists.last().clone();
            var size = (youngArtists.size());
            if(youngArtists.find('.input-text:visible').size() < 5){
                newArtists.show();
                newArtists.find("label").first().html("Nickname for Child " + (youngArtists.find('.input-text:visible').size() + 1));
                newArtists.find("input[type='hidden']").first().attr("name", "youngArtists[" + size + "].id").val("");
                newArtists.find("input[type='hidden']").last().attr("name", "youngArtists[" + size + "].idx").val("");
                newArtists.find("input[type='text']").first().attr("name", "youngArtists[" + size + "].nickname").val("");
                newArtists.find("select").first().attr("name", "youngArtists[" + size + "].yearOfBirth");
                youngArtists.last().after(newArtists);
                if (youngArtists.find('.input-text:visible').size() == 4)
                    $("#addArtists").hide();
            }
        }

        function removeArtist(removeElement){
            $('.modal-no-bg').remove();
            window.removeDiv = $(removeElement.parentNode.parentNode)
            $('#modalDelete').reveal({dismissModalClass: 'close-reveal'});
        }
        function confirmRemoveArtist(){
            removeDiv.hide()
            removeDiv.find("input").first().val('null');
            if(!$.DirtyForms.isDirty()){
                removeDiv.find("input").first().dirtyForms('setDirty');
            }
            //show the add artist button
            $("#addArtists").show();
        }

    </r:script>
</head>

<body>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'gallerySettings']"/>
<!-- secondary navigation -->
<!-- main content area -->
<g:form class="custom nice" name="input" method="post" action="update" controller="gallery">
<g:hiddenField name="id" value="${galleryInstance?.id}"/>
<g:hiddenField name="version" value="${galleryInstance?.version}"/>
<div class="row">
    <div class="twelve columns">
        <g:hasErrors bean="${galleryInstance}" field="version">
            <div class="alert-box error">
                <g:renderErrors bean="${galleryInstance}" as="list"/>
                <a href="" class="close">&times;</a>
            </div>
        </g:hasErrors>
    </div>
</div>
<div class="row">
    <div class="six columns">
        <p>Your Gallery URL:</p>

        <p><a href="${createLink(uri:'/'+galleryInstance?.getGalleryNameUrl(),absolute: true)}">${createLink(uri:'/'+galleryInstance?.getGalleryNameUrl(),absolute: true)}</a></p>
    </div>

    <div class="six columns">
        <p>Bookmark it and share with family and friends.</p>
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<div class="row">
    <div class="six columns">
        <label>Gallery Name</label>
        <input type="hidden" name="galleryName" value="${galleryInstance?.galleryName}"/>
        <h4>${galleryInstance?.galleryName}</h4>
    </div>
    <div class="six columns">
        <p>Gallery names can't be changed.</p>

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="six columns">

        <label>Gallery Description</label>
        <textarea class="wider fixed-width-440" name="description" style="height:100px">${galleryInstance?.description}</textarea>
    </div>

    <div class="six columns">
        <p>Describe what this gallery is about.</p>
    </div>
</div>


<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="six columns">
        <label>Type of Artwork <span>(Default Setting)</span></label>
        <g:select name="artworkType" from="${Gallery?.constraints?.artworkType?.inList}"
                  value="${galleryInstance?.artworkType}"/>
    </div>

    <div class="six columns">
        <p>If most artworks fall under one type, choose a default setting.</p>

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<g:if test="${galleryInstance?.galleryOwner=="Myself"}">
<!-- for myself-->
<div class="myself">
    <div class="row">
        <div class="six columns">
            <label>Year of Birth</label>
            <g:select name="ownerYearOfBirth" from="${((new Date()).getYear() + 1900)..((new Date()).year - 18 + 1900)}"
                      value="${(galleryInstance?.ownerYearOfBirth)?: ((new Date()).getYear() + 1900)}"/>
        </div>

        <div class="five columns">
            <p><strong>Your year of birth is never shown publicly.</strong> It is used to sort artworks by age and to create your
            artist timeline. It also allows visitors to appreciate your talent in relation to your age.
            </p>

            <p>Can’t find your year of birth? Read about age eligibility in our guide, <a href="${createLink(controller: 'general',action: 'gettingStarted')}">Getting Started</a>.
            </p>
        </div>

    </div>
</div>
<!-- for myself-->
</g:if>



<g:if test="${galleryInstance?.galleryOwner=="YoungArtist"}">
<!-- for young artists-->
<div class="youngartist">
    <div class="row ">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">

            <hr/>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">

        <div class="twelve columns">
            <h4>Artists</h4>
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <div class="horizontal_fields youngartists">
                <g:each in="${galleryInstance?.getYoungArtists()}" var="artist" status="i">
                    <div>
                        <g:hiddenField name="youngArtists[${i}].id" value="${artist?.id}"/>
                        <g:hiddenField name="youngArtists[${i}].idx" value="${i}"/>
                        <div class="${hasErrors(bean: galleryInstance, field: 'youngArtists['+i+'].nickname', 'form-field error')}">
                            <label>Nickname for Child ${i + 1}</label>
                            <input type="text" class="input-text" name="youngArtists[${i}].nickname"
                                   value="${artist?.nickname}"/>
                        <small class="error">${hasErrors(bean: galleryInstance, field: 'youngArtists['+i+'].nickname', renderErrors(bean: galleryInstance, field: 'youngArtists['+i+'].nickname'))}</small>
                        </div>
                        <div>
                            <label>Year of Birth</label>
                            <g:select name="youngArtists[${i}].yearOfBirth" from="${((new Date()).getYear() + 1900)..((new Date()).year - 18 + 1900)}"
                                          value="${artist?.yearOfBirth ?: ((new Date()).getYear() + 1900)}"/>
                        </div>
                        <div class="horizontal_last"><a href="#" onclick="removeArtist(this);return false;" class="secondary_link ignoredirty">Remove</a></div>
                    </div>
                </g:each>
                <g:if test="${!galleryInstance?.youngArtists}">
                    <div>
                        <div>
                            <label>Nickname for Child 1</label>
                            <input type="text" class="input-text" name="youngArtists[0].nickname"
                                   value=""/>
                        </div>

                        <div>
                            <label>Year of Birth</label>
                            <g:select name="youngArtists[0].yearOfBirth" from="${((new Date()).getYear() + 1900)..((new Date()).year - 18 + 1900)}"
                                      value="${((new Date()).getYear() + 1900)}"/>
                        </div>
                    </div>
                </g:if>
            </div>

            <div class="clear-both"></div>

            <p><a href="javascript:addArtists();" id="addArtists" style="display: ${galleryInstance.youngArtists.size()==5?'none;':'inline'}" class="secondary_link ignoredirty">Add Artists</a></p>

        </div>

        <div class="five columns">
            <p>Never put your child's real name. Instead, create a nickname.<br/>
                This will be the child's identity on the site.</p>
            <ul class="normal-list">Selecting the child's year of birth allows us to:
                <li>Sort the child's artworks by age group</li>
                <li>Show the child's <a href="#">artist timeline</a> over the years</li>

            </ul>

            <p>Your child's age is shown only in relation to when their artwork was created. <strong>The child's year of birth is never shown.</strong>
            </p>

            <p>Can’t find your child’s year of birth? Read about age eligibility in our guide, <a href="${createLink(controller: 'general', action: 'gettingStarted')}">Getting Started</a>.
            </p>
        </div>
    </div>
</div>
<!-- end of for child-->
</g:if>

<div class="row">
    <div class="twelve columns">
        <input class="nice medium radius white button" type="submit" value="Update"/>
    </div>
</div>


<div class="row">
    <div class="twelve columns">

        &nbsp;
    </div>
</div>
</div>
</g:form>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>

        <p>&nbsp;</p>
    </div>
</div>

<%--<g:render template="/layouts/comments" model="[modelInstance:galleryInstance]"/> --%>

<!-- main content area -->
<g:render template="/layouts/modalNotify"/>
<g:render template="/layouts/modalConfirm" />

<div id="modalDelete" class="reveal-modal modal-view" style="position:fixed;">
    <p style="margin:0">
        Are you sure you want to delete this artist? All artworks created by this artist will also be deleted.
    </p>
      <div class="btn_grp" style="margin:15px 0 0 -10px;text-align: center">
        <a class="ignoredirty nice small radius black button close-reveal" onclick="confirmRemoveArtist()"
           style="width:90px;">Delete</a>
        <a class="nice small radius red button close-reveal">Cancel</a>
        <a class="ignoredirty close-reveal-modal close-reveal">×</a>
        </div>
</div>

</body>
