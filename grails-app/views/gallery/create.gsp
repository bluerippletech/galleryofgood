<%@ page import="com.galleryofgood.gallery.Gallery" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='gallery.create'/></title>
    <r:require module="foundation"/>
    <r:script disposition="head">
        $(function () {
            $(".gallery").hide();
            <g:if
            test="${galleryInstance?.galleryOwner == "Myself" || galleryInstance?.galleryOwner == null || galleryInstance?.galleryOwner == ""}">
        showMyself();
    </g:if>
        <g:elseif
                test="${galleryInstance?.galleryOwner == "YoungArtist"}">
            showYoungArtists();
        </g:elseif>


        });

        function showMyself() {
            $(".gallery").show();
            $(".youngartist").hide();
            $(".myself").show();

        }
        <g:if test="${ageGroup == "above13"}">
        function showYoungArtists() {
            $(".gallery").show();
            $(".youngartist").show();
            $(".myself").hide();
        }

        function addArtists() {
            var youngArtists = $(".youngartists").children();
            var newArtists = youngArtists.last().clone();
            var size = (youngArtists.size());
            if(size < 5){
                newArtists.find("label").first().html("Nickname for Child " + (size + 1));
                newArtists.find("input").first().attr("name", "youngArtists[" + size + "].nickname").val("");
                newArtists.find("select").first().attr("name", "youngArtists[" + size + "].yearOfBirth");
                youngArtists.last().after(newArtists);
                if (size == 4)
                    $("#addArtists").hide();

            }
        }
        </g:if>
    </r:script>
</head>

<body>
%{--<g:render template="/layouts/flashMessage"/>--}%
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'artworks']"/>
<!-- secondary navigation -->
<!-- main content area -->
<form class="custom nice" name="input" action="saveNewGallery" method="post">

<div class="row">
    <div class="twelve columns" id="horizontal_radio">
        <h4>Looks like you haven't opened a gallery yet.</h4>

        <p>Do you have questions about taking photos of artworks, uploading and more? Start with the
            <a href="${createLink(controller:"gallery", action: "guide")}">Gallery Guide</a>.</p>

        <g:if test="${ageGroup == "above13"}">
            <p>Are you all set? Tell us who this gallery is for:</p>
            <label for="galleryOwner1" onclick="javascript:showMyself();">
                <input name="galleryOwner" type="radio" id="galleryOwner1"
                       style="display:none;" ${galleryInstance?.galleryOwner == "Myself" || galleryInstance?.galleryOwner == null || galleryInstance?.galleryOwner == "" ? "checked" : ""}
                       value="Myself">
                <span class="custom radio"></span> Myself
            </label>
            <label for="galleryOwner2" onclick="javascript:showYoungArtists();">
                <input name="galleryOwner" type="radio" id="galleryOwner2"
                       style="display:none;" ${galleryInstance?.galleryOwner == "YoungArtist" ? "checked" : ""}
                       value="YoungArtist">
                <span class="custom radio"></span> My young artist(s)
            </label>
        </g:if>
        <g:else>
            <p>Ready to start? Open your gallery now.</p>
        </g:else>

    </div>
</div>
<!-- Start of Dynamic Section-->
<div class="gallery">
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr/>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="six columns">
        <div class="${hasErrors(bean: galleryInstance, field: 'galleryName', 'form-field error')}">
            <label>Gallery Name</label>
            <input type="text" class="medium input-text wider" name="galleryName"
                   value="${galleryInstance?.galleryName}"/>
            <small class="error">${hasErrors(bean: galleryInstance, field: 'galleryName', renderErrors(bean: galleryInstance, field: 'galleryName'))}</small>
        </div>
    </div>

    <div class="six columns">
        <p>Choose a gallery name using letters and/or numbers.<br/>
            Pick a good one! Gallery names can’t be changed.</p>

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="six columns">

        <label>Gallery Description</label>
        <textarea class="wider fixed-width-440" name="description" style="height:100px"></textarea>
    </div>

    <div class="six columns">
        <p>Describe what this gallery is about.</p>
    </div>
</div>


<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="six columns">
        <label>Type of Artwork <span>(Default Setting)</span></label>
        <g:select name="artworkType" from="${Gallery?.constraints?.artworkType?.inList}"
            value="${galleryInstance?.artworkType}"/>
    </div>

    <div class="six columns">
        <p>If most artworks fall under one type, choose a default setting.</p>

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<!-- for myself-->
<div class="myself">
    <div class="row">
        <div class="six columns">
            <label>Year of Birth</label>
            <g:select name="ownerYearOfBirth" from="${((new Date()).getYear() - (ageGroup == "above13"?13:0) + 1900)..((new Date()).year - (ageGroup == "below13"?12:18) + 1900)}"
                      value="${(galleryInstance?.ownerYearOfBirth) ?: ((new Date()).getYear() + 1900)}"/>
        </div>

        <div class="six columns">
            <p><strong>Your year of birth is never shown publicly.</strong> It is used to sort artworks by age and to create your
            artist timeline. It also allows visitors to appreciate your talent in relation to your age.
            </p>

            <p>Can’t find your year of birth? Read about age eligibility in our guide, <a href="${createLink(controller: 'general',action: 'gettingStarted')}">Getting Started</a>.
            </p>
        </div>

    </div>
</div>
<!-- for myself-->


<g:if test="${ageGroup == "above13"}">
<!-- for young artists-->
<div class="youngartist">
    <div class="row ">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">

            <hr/>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">

        <div class="twelve columns">
            <h4>Artists</h4>
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <div class="horizontal_fields youngartists">
                <g:each in="${galleryInstance?.youngArtists}" var="artist" status="i">
                    <div>
                        <div class="${hasErrors(bean: galleryInstance, field: 'youngArtists['+i+'].nickname', 'form-field error')}">
                            <label>Nickname for Child ${i + 1}</label>
                            <input type="text" class="input-text" name="youngArtists[${i}].nickname" value="${(galleryInstance?.youngArtists?.getAt(i))?.nickname}"/>
                            <small class="error">${hasErrors(bean: galleryInstance, field: 'youngArtists['+i+'].nickname', renderErrors(bean: galleryInstance, field: 'youngArtists['+i+'].nickname'))}</small>
                        </div>

                        <div>
                            <label>Year of Birth</label>
                            <g:select name="youngArtists[${i}].yearOfBirth"
                                      from="${((new Date()).getYear() + 1900)..((new Date()).year - 18 + 1900)}"
                                      value="${(galleryInstance?.youngArtists?.getAt(i))?.yearOfBirth ?: ((new Date()).getYear() + 1900)}"/>
                        </div>
                        <div class="horizontal_last"><a href="#" class="secondary_link">Remove</a></div>
                    </div>
                </g:each>
                <g:if test="${!galleryInstance?.youngArtists}">
                    <div>
                        <div>
                            <label>Nickname for Child 1</label>
                            <input type="text" class="input-text" name="youngArtists[0].nickname" value=""/>

                        </div>

                        <div>
                            <label>Year of Birth</label>
                            <g:select name="youngArtists[0].yearOfBirth"
                                      from="${((new Date()).getYear() + 1900)..((new Date()).year - 18 + 1900)}"
                                      value="${((new Date()).getYear() + 1900)}"/>
                        </div>
                    </div>
                </g:if>
            </div>

            <div class="clear-both"></div>

            <p id="addArtists"><a href="javascript:addArtists();" class="secondary_link">Add Artists</a></p>

        </div>

        <div class="five columns">
            <p>Never put your child's real name. Instead, create a nickname.<br/>
                This will be the child's identity on the site.</p>
            <ul class="normal-list">Selecting the child's year of birth allows us to:
                <li>Sort the child's artworks by age group</li>
                <li>Show the child's <a href="#">artist timeline</a> over the years</li>

            </ul>

            <p>Your child's age is shown only in relation to when their artwork was created. <strong>The child's year of birth is never shown.</strong>
            </p>

            <p>Can’t find your child’s year of birth? Read about age eligibility in our guide, <a href="${createLink(controller: 'general', action: 'gettingStarted')}">Getting Started</a>.
            </p>
        </div>
    </div>
</div>
<!-- end of for child-->
</g:if>


<div class="row">
    <div class="twelve columns">
        <input class="nice medium radius white button" type="submit" value="Save"/>
    </div>
</div>


<div class="row">
    <div class="twelve columns">

        &nbsp;
    </div>
</div>
</div>
</form>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>

        <p>&nbsp;</p>
    </div>
</div>
<!-- main content area -->

</body>
