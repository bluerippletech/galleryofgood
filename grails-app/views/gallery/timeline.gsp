<%@ page import="com.galleryofgood.gallery.Timeline; com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='edit.timeline'/></title>
    <r:require modules="foundation"/>
    <r:script>
        function editArtworks(){
            window.location='${createLink(controller: 'artwork', action: 'index')}';
        }
    </r:script>
</head>

<body>
<g:render template="/layouts/flashMessage"/>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'timeline']"/>
<!-- secondary navigation -->
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<g:form name="timeline" class="custom nice" action="updateArtisticJourney" controller="gallery">
    <g:hiddenField name="id" value="${gallery?.id}"/>
    <g:hiddenField name="version" value="${gallery?.version}"/>

    <g:if test="${gallery}">
        <div class="row">
            <div class="twelve columns">
                <dl class="tabs">
                    <g:if test="${gallery?.galleryOwner?.equals("YoungArtist")}">
                        <g:each in="${gallery.youngArtists}" var="artist" status="i">
                            <dd><a href="#simple${i + 1}" class="${0 == i ? "active" : ""}">${artist.nickname}</a></dd>
                        </g:each>
                    </g:if>
                    <g:else>
                        <dd><a href="#simple1" class="active">Timeline</a></dd>
                    </g:else>

                </dl>

                <ul class="tabs-content">
                    <g:if test="${gallery?.isForYoungArtist()}">
                        <g:each in="${gallery?.youngArtists}" var="artist" status="i">
                            <li class="${0 == i ? "active" : ""}" id="simple${i + 1}Tab">
                                <p>Here's a preview of the artworks that appear in your <g:link controller="timeline"
                                                                                                action="timelineview"
                                                                                                params="${[id: Timeline.findByArtistName(artist.nickname)?.id]}">Artist Timeline</g:link>.</p>
                                <r:img class="float-left attention-icon" uri="/images/note.jpg" alt="Note"/>
                                <p class="attention-paragraph">How were these selected? Our system chooses the first artwork you upload at every age.<br/>
                                    For example, if you've uploaded three artworks created at the age of 6, the first one will be the default.<br/>
                                    You can change this selection in the <a
                                        href="${createLink(controller: 'artwork', action: 'index')}">Artworks</a> section at any time.
                                </p>

                                <div class="clear-both"></div>
                                <g:each in="${gallery.artworks.findAll {it.showInTimeline == 'on' && it.artist == artist.nickname}}"
                                        var="timelineart">
                                    <hr/>
                                    <img class="float-left artwork-thumbnail" src="${timelineart.url}" alt=""
                                         width="93px;"/>

                                    <p>Title: ${timelineart.title}<br/>Age: ${timelineart.artworkAge}</p>
                                </g:each>
                                <div class="clear-both"></div>
                                <input type="button"
                                       onclick="location.href = '${createLink(controller:'artwork',action:'index')}'"
                                       class="nice medium radius white button" value="Edit Artworks"/>
                                %{--<input class="nice medium radius white button" type="submit" name="submit" value="Edit Artworks" />--}%
                                <p>&nbsp;</p>
                                <label><strong>Artistic Journey So Far:</strong></label>
                                <textarea name="artisticJourney.${artist.nickname}"
                                          class="widest fixed-width-550"
                                          placeholder="How did this artistic journey begin? Share your influencers and your inspiration.">${Timeline.findByArtistName(artist.nickname).artisticJourney}</textarea>
                                <input class="nice medium radius white button" type="submit" name="submit"
                                       value="Update"/>
                            </li>
                        </g:each>
                    </g:if>
                    <g:else>
                        <li class="active" id="simple1Tab">
                            <p>Here's a preview of the artworks that appear in your <g:link controller="timeline"
                                                                                            action="timelineview"
                                                                                            params="[id: Timeline.findByArtistName(gallery?.owner ?: '')?.id]">Artist Timeline</g:link></p>
                            <r:img class="float-left attention-icon" uri="/images/note.jpg" alt="Note"/>
                            <p class="attention-paragraph">How were these selected? Our system chooses the first artwork you upload at every age.<br/>
                                For example, if you've uploaded three artworks created at the age of 6, the first one will be the default.<br/>
                                You can change this selection in the <a
                                    href="${createLink(controller: 'artwork', action: 'index')}">Artworks</a> section at any time.
                            </p>

                            <div class="clear-both"></div>
                            <g:each in="${gallery?.artworks.findAll {it.showInTimeline == 'on'}}" var="timelineart">
                                <hr/>
                                <img class="float-left artwork-thumbnail" src="${timelineart.url}" alt=""
                                     width="93px;"/>

                                <p>Title: ${timelineart.title}<br/>Age: ${timelineart.artworkAge}</p>
                            </g:each>
                            <div class="clear-both"></div>
                            <input type="button"
                                   onclick="location.href = '${createLink(controller:'artwork',action:'index')}'"
                                   class="nice medium radius white button" value="Edit Artworks">

                            <p>&nbsp;</p>
                            <label><strong>Artistic Journey So Far:</strong></label>
                            <textarea name="artisticJourney" class="widest fixed-width-550"
                                      placeholder="How did this artistic journey begin? Share your influencers and your inspiration.">${gallery?.artisticJourney}</textarea>
                            <input class="nice medium radius white button" type="submit" name="submit" value="Update"/>
                        </li>

                    </g:else>
                </ul>
            </div>
        </div>
    </g:if>
    <g:else>

        <div class="row">
            <div class="twelve columns" id="horizontal_radio">
                <h4>Looks like you haven't opened a gallery yet. <br/>Please create a gallery first before viewing your timeline.</h4>
            </div>
        </div>

    </g:else>

</g:form>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->

</body>
