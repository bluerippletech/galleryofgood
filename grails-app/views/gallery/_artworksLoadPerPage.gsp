<div class="artwork_list" id="items_container_list" style="display: none;">
<g:each in="${modelInstance}" var="${artwork}">
    <div class="display-artwork">
        <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">
            <div style="width: 299px; height: 233px; padding: 0; margin-bottom: 15px; overflow: hidden">
                <img class="lazy" alt="" data-original="${artwork.artworkDetailImageUrl}"
                     style="max-width:100%;max-height:100%">
            </div>
        </a>
        <div class="artwork ma-artwork">
            <h5><a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">${artwork.title}</a></h5>

            <p>By: ${artwork.artist}, Age ${artwork.artworkAge}</p>
        </div>
    </div>
</g:each>
</div>
<script type="text/javascript">
    $("#items_container_list").fadeIn('slow');
    $("img.lazy").lazyload({effect:"fadeIn"});
</script>