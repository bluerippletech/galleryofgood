<%@ page import="com.galleryofgood.security.AppUser" %>



<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="appUser.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${appUserInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="appUser.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${appUserInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'country', 'error')} ">
	<label for="country">
		<g:message code="appUser.country.label" default="Country" />
		
	</label>
	<g:textField name="country" value="${appUserInstance?.country}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'ageGroup', 'error')} ">
	<label for="ageGroup">
		<g:message code="appUser.ageGroup.label" default="Age Group" />
		
	</label>
	<g:textField name="ageGroup" value="${appUserInstance?.ageGroup}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'optin', 'error')} ">
	<label for="optin">
		<g:message code="appUser.optin.label" default="Optin" />
		
	</label>
	<g:textField name="optin" value="${appUserInstance?.optin}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'hasLoggedIn', 'error')} ">
	<label for="hasLoggedIn">
		<g:message code="appUser.hasLoggedIn.label" default="Has Logged In" />
		
	</label>
	<g:textField name="hasLoggedIn" value="${appUserInstance?.hasLoggedIn}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="appUser.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${appUserInstance?.accountExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="appUser.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${appUserInstance?.accountLocked}" />
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="appUser.email.label" default="Email" />
		
	</label>
	<g:textField name="email" value="${appUserInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="appUser.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${appUserInstance?.enabled}" />
</div>

<div class="fieldcontain ${hasErrors(bean: appUserInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="appUser.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${appUserInstance?.passwordExpired}" />
</div>

