<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.account.settings' default="Manage your notifications - Gallery of Good"/></title>
    <r:require module="foundation"/>
</head>

<body>
%{--<g:render template="/layouts/flashMessage"/>--}%
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation"  model="[activePage:'notificationSettings']"/>
<!-- secondary navigation -->
<!-- main content area -->
<form class="custom nice" name="input" action="saveNotificationSettings" method="post">
    <div class="row">
        <div class="six columns">
            <p>When members comment and/or follow my gallery, notify me...</p>
            <label for="admireNotification1">
                <input name="admireNotification" type="radio" id="admireNotification1" value="IMMEDIATE" style="display:none;" ${user.userDetails?.admireNotification=="IMMEDIATE"?"checked":""}>
                <span class="custom radio"></span> Right away
            </label>

            <label for="admireNotification2">
                <input name="admireNotification" type="radio" id="admireNotification2" value="DAILY" style="display:none;" ${user.userDetails?.admireNotification=="DAILY"?"checked":""}>
                <span class="custom radio"></span> Once a day (daily digest)
            </label>
            <label for="admireNotification3">
                <input name="admireNotification" type="radio" id="admireNotification3" value="WEEKLY" style="display:none;" ${user.userDetails?.admireNotification=="WEEKLY"?"checked":""}>
                <span class="custom radio"></span> Once a week (weekly digest)
            </label>
            <label for="admireNotification4">
                <input name="admireNotification" type="radio" id="admireNotification4" value="NEVER" style="display:none;" ${user.userDetails?.admireNotification=="NEVER"?"checked":""}>
                <span class="custom radio"></span> Never, I will log-in here to check
            </label>
        </div>
        <div class="six columns">
            &nbsp;
        </div>
    </div>

    <div class="row">

        <div class="twelve columns">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="six columns">
            <p>When Gallery of Good has important news and art events...</p>
            <label for="allowSiteNotifications1">
                <input name="allowSiteNotifications" type="radio" id="allowSiteNotifications1" value="Yes" style="display:none;" ${user.userDetails?.allowSiteNotifications=="Yes"?"checked":""}>
                <span class="custom radio"></span> Notify me via email
            </label>
            <label for="allowSiteNotifications2">
                <input name="allowSiteNotifications" type="radio" id="allowSiteNotifications2" value="No" style="display:none;" ${user.userDetails?.allowSiteNotifications=="No"?"checked":""}>
                <span class="custom radio"></span> I prefer not to receive news
            </label>
        </div>
        <div class="six columns">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            <p>Your privacy is important to us. You can review our <a href="${createLink(controller:'general',action:'privacyPolicy')}">Privacy Policy</a> at anytime.</p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>

    </div>
    <div class="row">
        <div class="four columns">
            <input class="nice medium radius white button" type="submit" value="Update" />
        </div>
        <div class="eight columns">
            &nbsp;
        </div>
    </div>


</form>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
</div>
<!-- main content area -->

<div id="modal" class="reveal-modal modal-view">
    <p style="margin:0">
        ${flash.message}
    </p>
    <a class="close-reveal-modal">×</a>
</div>
<div class="reveal-modal-bg modal-no-bg"></div>
<script type="text/javascript">
<g:if test="${flash.message}">
$('#modal').reveal({dismissModalClass: 'close-reveal-modal'});
</g:if>
</script>

</body>
