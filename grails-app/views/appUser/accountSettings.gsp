<%@ page import="com.galleryofgood.security.AppUser; com.galleryofgood.user.UserDetails" %>
<g:set var="avatarLocationUrl" value="${UserDetails.findByAppUser(AppUser.findByUsername(user.username)).avatarLocationUrl}"/>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.account.settings' default="Manage your account - Gallery of Good"/></title>
    <r:require module="foundation"/>
</head>

<body>
<r:script>
    <g:if test="${"".equals(avatarLocationUrl) || avatarLocationUrl==null}">
    $(function(){
        selectImage($(".without_border").children().first());
    });
    </g:if>


    function selectImage(imageLink){
        $(imageLink).closest('ul').find("li.current_linked_image").removeClass("current_linked_image");
        $(imageLink).closest("li").addClass("current_linked_image");
        $("#avatarField").val($(imageLink).find('img').attr("src"))
    }

     $(document).ready(function(){
        var avatar = '${avatarLocationUrl}';

        $('.linked_images a').click(function(e){
            e.preventDefault();
            if(avatar!=$('#avatarField').val()){
                if(!$.DirtyForms.isDirty()){
                   $('#avatarField').dirtyForms('setDirty');
                }
            }
            else{
            $('#avatarField').dirtyForms('setClean');
            }
        })

    });
</r:script>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage:'accountSettings']"/>
<!-- secondary navigation -->
<!-- main content area -->
<form class="custom nice" name="input" action="updateAccount" method="post">
    <g:hiddenField name="username" value="${user?.username}"/>
    <div class="row">
        <div class="four columns">
            <label>Username</label>
            ${user?.username}
        </div>
        <div class="eight columns">
            <p>Username can't be changed.</p>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="four columns">
            <div class="${hasErrors(bean: user, field: 'currentPassword', 'form-field error')}">
                <label>Current Password</label>
                <input type="password" class="input-text" value="" name="currentPassword"/>
                <small class="error">${hasErrors(bean: user, field: 'currentPassword', renderErrors(bean: user, field: 'currentPassword'))}</small>
            </div>
            <div class="${hasErrors(bean: user, field: 'password', 'form-field error')}">
                <label>Password</label>
                <input type="password" class="input-text" value="" name="password"/>
                <small class="error">${hasErrors(bean: user, field: 'password', renderErrors(bean: user, field: 'password'))}</small>
            </div>
            <div class="${hasErrors(bean: user, field: 'password2', 'form-field error')}">
                <label>Re-type Password</label>
                <input type="password" class="input-text" value="" name="password2"/>
                <small class="error">${hasErrors(bean: user, field: 'password2', renderErrors(bean: user, field: 'password2'))}</small>
            </div>
        </div>
        <div class="eight columns">
            <p>When updating your password:</p>
            <ul id="password_tip">
                <li>Enter at least 6 characters.</li>
                <li>Passwords are case-sensitive. Make it strong by including capital and lowercase letters, numbers, and special symbols like >!#$%.</li>
                <li>Never make your password the same as your username.</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <div class="${hasErrors(bean: user, field: 'email', 'form-field error')}">
                <label>Email</label>
                <input type="text" class="input-text" value="${user.email}" name="email"/>
                <small class="error">${hasErrors(bean: user, field: 'email', renderErrors(bean: user, field: 'email'))}</small>
            </div>
        </div>
        <div class="eight columns">
            <p>All account notfications will be sent to this email. Keep it current.</p>
        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <div class="${hasErrors(bean: user, field: 'website', 'form-field error')}">
                <label>Website</label>
                <input type="text" class="input-text" value="${user?.website?:"http://"}" name="website" placeholder="http://"/>
                <small class="error">${hasErrors(bean: user, field: 'website', renderErrors(bean: user, field: 'website'))}</small>
            </div>
        </div>
        <div class="eight columns">
            <p>Add your personal website or blog.</p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">

            &nbsp;
        </div>
    </div>


    <div class="row">
        <div class="twelve columns">
            &nbsp;

        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <label>Avatar</label>
            <g:hiddenField name="avatar" id="avatarField" value="${avatarLocationUrl}" class="ignoredirty"/>
            <ul class="linked_images without_border ignoredirty">
                <li class="${avatarLocationUrl == 'https://s3.amazonaws.com/galleryofgood/common/avatar/profile01.jpg'?'current_linked_image':''} ignoredirty"><a href="javascript:" onclick="selectImage(this)" class="ignoredirty"><img src="https://s3.amazonaws.com/galleryofgood/common/avatar/profile01.jpg" alt=""/></a></li>
                <li class="${avatarLocationUrl == 'https://s3.amazonaws.com/galleryofgood/common/avatar/profile02.jpg'?'current_linked_image':''} ignoredirty"><a href="javascript:" onclick="selectImage(this)" class="ignoredirty"><img src="https://s3.amazonaws.com/galleryofgood/common/avatar/profile02.jpg" alt=""/></a></li>
                <li class="${avatarLocationUrl == 'https://s3.amazonaws.com/galleryofgood/common/avatar/profile03.jpg'?'current_linked_image':''} ignoredirty"><a href="javascript:" onclick="selectImage(this)" class="ignoredirty"><img src="https://s3.amazonaws.com/galleryofgood/common/avatar/profile03.jpg" alt=""/></a></li>
                <li class="${avatarLocationUrl == 'https://s3.amazonaws.com/galleryofgood/common/avatar/profile04.jpg'?'current_linked_image':''} ignoredirty"><a href="javascript:" onclick="selectImage(this)" class="ignoredirty"><img src="https://s3.amazonaws.com/galleryofgood/common/avatar/profile04.jpg" alt=""/></a></li>
                <li class="${avatarLocationUrl == 'https://s3.amazonaws.com/galleryofgood/common/avatar/profile05.jpg'?'current_linked_image':''} ignoredirty"><a href="javascript:" onclick="selectImage(this)" class="ignoredirty"><img src="https://s3.amazonaws.com/galleryofgood/common/avatar/profile05.jpg" alt=""/></a></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;

        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <input class="nice medium radius white button" type="submit" value="Update"/>
        </div>

        <div class="eight columns">
            &nbsp;
        </div>

    </div>

</form>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>

        <p>&nbsp;</p>
    </div>
</div>

<div class="row">

    <div class="six columns">
        <p>At anytime, you can always review our <a href="${createLink(controller: 'general',action: 'privacyPolicy')}">Privacy Policy</a> and <a href="${createLink(controller: 'general',action: 'termsOfService')}">Terms of Service</a>.
        </p>

    </div>

    <div class="six columns">
        &nbsp;
    </div>

</div>
<!-- main content area -->

<g:render template="/layouts/modalConfirm" />
<g:render template="/layouts/modalNotify" />
</body>
