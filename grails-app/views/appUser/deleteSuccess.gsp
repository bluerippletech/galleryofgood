<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.delete.account'/></title>
    <r:require module="foundation"/>
</head>

<body>
<form class="custom nice" name="input" action="#" method="get">

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>


    <div class="row">

        <div class="twelve columns">
            <h3>Okay, the account has been deleted.</h3>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>

    </div>

    <div class="row">
        <div class="twelve columns">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>

        </div>
    </div>

</form>

</body>
