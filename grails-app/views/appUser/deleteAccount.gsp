<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.delete.account'/></title>
    <r:require module="foundation"/>
</head>

<body>
<form class="custom nice" name="input" action="confirmDelete" method="post">
                     <g:hiddenField name="username" value="${username}"/>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>


    <div class="row">

        <div class="twelve columns">
            <h3>You are about to delete ${username}'s account.</h3>
            <h5>In case you'd like to reconsider, here's a <a href="${createLink(controller:'general',action:'parentGuide')}">Parent guide</a>.</h5>
            <h5><a href="${createLink(controller:'contactUs')}">Contact us</a> if you have any concerns or questions.</h5>
            <p></p>

        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <input type="submit" value="Delete Account" class="btn-blue">
        </div>
    </div>

</form>

</body>
