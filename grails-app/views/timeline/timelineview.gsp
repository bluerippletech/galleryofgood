<%@ page import="com.galleryofgood.gallery.Timeline; com.galleryofgood.gallery.Gallery" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='artist.timeline' args='${[timelineInstance?.artistName?.capitalize()]}' default="Timeline"/></title>
    <r:require modules="foundation,lazyload"/>
    <r:script disposition="head">
        $(function(){
            $("img.lazy").lazyload({effect:"fadeIn"});
        })
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">

        <h2 class="heading color-black">
            <a href="${createLink(controller:galleryInstance?.galleryNameUrl)}">${galleryInstance?.galleryName}</a>
        </h2>
        <br /><br />
        <div class="row srdd" id="secondary_navigation">
            <div class="twelve columns srdd-crit">
                <ul id="level-1">
                    <li>
                        <p><r:img alt="" uri="/images/ico-clock.png" class="clockImg clockImg-grey"/></p>
                        <p>Artist Timeline:</p>
                    </li>
                    <g:if test="${galleryInstance?.galleryOwner?.equals("YoungArtist")}">
                        <g:each in="${galleryInstance?.youngArtists?.sort{it?.dateCreated}}" var="youngArtist">
                    <li class="${youngArtist?.nickname?.equals(artist)?"selected":""}">
                        <a href="${createLink(controller:'timeline',action:'timelineview',id:Timeline.findByArtistName(youngArtist?.nickname)?.id)}">${youngArtist?.nickname}</a>
                    </li>
                        </g:each>
                    </g:if>
                </ul>
            </div>
        </div>
        <div class="gg-timeline mg-atp-timeline">
            <!-- <h3 id="title_gallery_of_good">
					<span></span>
					About Us
				</h3> -->
            <h3 class="heading color-black">
                ${timelineInstance?.artistName}
            <g:if test="${!artworks}">
                <br/>
                <span style="font-size:12px;font-weight: 100;font-family: Arial,sans-serif ;">has not chosen any artworks yet.</span>
            </g:if>
            </h3>

            <ul class="timeline clearfix">
                <li class="right-column timelineUnit">
                    <div class="timelineEmptySpace"></div></li>
                <li class="left-column timelineUnit">
                    <div class="timelineEmptySpace2"></div></li>
                <g:each in="${artworks}" var="artwork" status="i">
                    <g:if test="${i%2==0}">
                        <li class="timelineUnit left-column">
                            <div class="timelineUnitContainer">
                                <div class="timelineLine"></div>
                                <div class="tluc-top"></div>
                                <div class="tluc-inner">
                                    <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">
                                        <img class="lazy" src="" data-original="${artwork?.artworkDetailImageUrl}" alt="${artwork.title}" data-thumb="slide01_thumb.jpg" data-caption="#htmlCaption${i}"/>
                                    </a>
                                </div>
                                <div class="tluc-bottom"></div>
                            </div>
                            <div class="timelineText">
                                <h5 class="color-black"><a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">${artwork?.title}</a></h5>
                                <p class="mg-atp-timelineText">Age: ${artwork?.artworkAge}</p>
                            </div>
                        </li>
                    </g:if>
                    <g:else>
                        <li class="timelineUnit right-column">
                            <div class="timelineUnitContainer">
                                <div class="timelineLine"></div>
                                <div class="tluc-top"></div>
                                <div class="tluc-inner">
                                    <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">
                                        <img class="lazy" src="" data-original="${artwork?.artworkDetailImageUrl}" alt="${artwork.title}" data-thumb="slide01_thumb.jpg" data-caption="#htmlCaption${i}" />
                                    </a>
                                </div>
                                <div class="tluc-bottom"></div>
                            </div>
                            <div class="timelineText">
                                <h5 class="color-black"><a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">${artwork?.title}</a></h5>
                                <p class="mg-atp-timelineText">Age: ${artwork?.artworkAge}</p>
                            </div>
                        </li>
                    </g:else>
                </g:each>
            </ul>
        </div>
    </div>
</div>
<div class="row agg">
    <div class="six columns">
        <h5>Artistic Journey so far</h5>
        ${timelineInstance?.artisticJourney}

    </div>
    <g:render template="/common/comments" model="[modelInstance:timelineInstance]"/>
</div>
<!-- main content area -->
