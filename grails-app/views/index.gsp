<%@ page import="com.galleryofgood.gallery.Artwork" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main"/>
        <r:require modules="foundation,foundation-orbit,foundation-orbit-js,lazyload"/>
        <r:script>
            $(function(){
                $('#featured').orbit({
                    animation: 'fade',
                    directionalNav:false,
                    bullets : true,
                    bulletThumbs: true,
                    bulletThumbLocation: ''
                });
                $('img.lazy').lazyload({effect       : "fadeIn"});
                  })
        </r:script>
	</head>
	<body>
    <!-- main content area -->
    <div class="row">
        <div class="twelve columns hp-orbit">
            <div id="featured">
                <g:each in="${1..5}" var="imageNumber">
                    <g:if test="${homepage?."featureImageUrl${imageNumber}"}">
                        <div style="text-align:center;width: 956px; height: 436px; overflow: hidden;background-color:white;" data-thumb="${homepage?."featureImageThumbnail${imageNumber}"}">
                            <g:if test="${homepage?."featureImageLink${imageNumber}"}">
                                <a href="${homepage?."featureImageLink${imageNumber}"}" > <img src="${homepage?."featureImageUrl${imageNumber}"}" alt="Slide 01" style="max-height:100%;max-width:100%"/></a>
                            </g:if>
                            <g:else>
                                <img src="${homepage?."featureImageUrl${imageNumber}"}" alt="Slide 01" style="max-height:100%;max-width:100%"/>
                            </g:else>
                        </div>
                    </g:if>
                </g:each>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>
    <div class="row grid_bg_home3col">
        <div class="four columns">
            <div class="home3col">
                <h3 class="heading color-black">${homepage?.sectionHeader1?:"&nbsp;"}</h3>
                <h4 class="title_bg_blue">${homepage?.boxheadline1}</h4>
                <p>${homepage?.boxcopy1}
                </p>
                <input type="button" value="${homepage?.buttontext1}" onclick="window.location.href='${homepage?.buttonlink1}'" class="btn-blue">
            </div>
        </div>
        <div class="four columns">
            <div class="home3col">
                <h3 class="heading color-black">${homepage?.sectionHeader2?:"&nbsp;"}</h3>
                <h4 class="title_bg_blue">${homepage?.boxheadline2}</h4>
                <p>${homepage?.boxcopy2}
                </p>
                <input type="button" value="${homepage?.buttontext2}" onclick="window.location.href='${homepage?.buttonlink2}'" class="btn-blue">
            </div>
        </div>
        <div class="four columns">
            <div class="home3col">
                <h3 class="heading color-black">${homepage?.sectionHeader3?:"&nbsp;"}</h3>
                <h4 class="title_bg_orange">${homepage?.boxheadline3}</h4>
                <p>${homepage?.boxcopy3}
                </p>
                <input type="button" value="${homepage?.buttontext3}" onclick="window.location.href='${homepage?.buttonlink3}'" class="btn-orange">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            <p>&nbsp;</p>
            <br />
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="ten columns">
            <h3 id="title_fresh_paint" class="heading color-black">fresh paint</h3>
        </div>
        <div class="two columns">
            <br />
            <a href="${createLink(controller:'search',action:'freshPaint')}">view new artworks</a>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            <div class="artwork_list">
                <g:each in="${Artwork.findAllByDateCreatedBetweenAndStatus(new Date()-30, new Date(),"Approved",[max:3,sort:'dateCreated',order:'desc'])}" var="artwork">
                    <div class="display-artwork">
                        <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}">
                            <div style="width: 299px; height: 233px; padding: 0; margin-bottom: 15px; overflow: hidden;background-color:white;">
                                <img class="lazy" src="" alt="" data-original="${artwork?.artworkDetailImageUrl}" style="max-width:100%;max-height:100%"/>
                                </div>
                        </a><br>
                        <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}"><span>${artwork?.title}</span></a>
                        <br>
                        By: ${artwork?.artist}, Age: ${artwork?.artworkAge}<br>
                        Gallery: <a href="${createLink(uri:'/'+artwork?.gallery?.galleryNameUrl,absolute:true)}">${artwork?.gallery?.galleryName}</a>
                    </div>
                </g:each>
            </div>
        </div>
    </div>

    <!--
    <div class="row">
        <div class="twelve columns">
            &nbsp;
            <br />
            &nbsp;
        </div>
    </div>                 q
    <div class="row">
        <div class="twelve columns">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <h3 class="heading color-black">Art Fundraisers</h3>
        </div>
    </div>

    <div class="row grid_bg_home3col_artfundraisers">
        <div class="four columns">
            <div class="home3col">
                <div class="ending_time"><strong>*</strong> ends in <span>24</span> hours</div>
                <h4 class="title_bg_orange">Last Chance</h4>
                <div class="clear-both"></div>
                <p><span>10</span> days / <span>10</span> hours / <span>10</span> minutes</p>
                <a href="#"><img src="images/title_art_fundraisers_1.jpg" alt="" /></a>
                <div class="artwork ma-artwork">
                    <h5><a href="#">Fall Art Show Fundraiser</a></h5>
                    <p class="color-black">Bayview Glen</p>
                </div>
            </div>
        </div>
        <div class="four columns">
            <div class="home3col">
                <h4 class="title_bg_orange">On Now</h4>
                <p><span>10</span> days / <span>10</span> hours / <span>10</span> minutes</p>
                <a href="#"><img src="images/title_art_fundraisers_2.jpg" alt="" /></a>
                <div class="artwork ma-artwork">
                    <h5><a href="#">Fall Art Show Fundraiser</a></h5>
                    <p class="color-black">Bayview Glen</p>
                </div>
            </div>
        </div>
        <div class="four columns">
            <div class="home3col">
                <h4 class="title_bg_orange">Coming Soon</h4>
                <p><span>10</span> days / <span>10</span> hours / <span>10</span> minutes</p>
                <a href="#"><img src="images/title_art_fundraisers_3.jpg" alt="" /></a>
                <div class="artwork ma-artwork">
                    <h5><a href="#">Fall Art Show Fundraiser</a></h5>
                    <p class="color-black">Bayview Glen</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <p>&nbsp;</p>
            <br />
        </div>
    </div>
-->
    <div class="row">
        <div class="twelve columns">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <!-- <h3 id="title_the_critics_agree"><span></span>The Critics Agree...</h3> -->
            <h3 class="heading color-black">${homepage?.acclaimHeader}</h3>
        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <div class="home3col_noborder">
                <p>&#34;&nbsp;${homepage?.testimonial1}&nbsp; &#34;</p>
                <g:if test="${homepage?.imageurl1}">
                    <img src="${homepage?.imageurl1}"/>
                </g:if>
                <g:else>
                    <p class="quote_author">${homepage?.headline1}</p>
                </g:else>

            </div>
        </div>
        <div class="four columns">
            <div class="home3col_noborder">
                <p>&#34;&nbsp;${homepage?.testimonial2}&nbsp; &#34;</p>
                <g:if test="${homepage?.imageurl2}">
                    <img src="${homepage?.imageurl2}"/>
                </g:if>
                <g:else>
                    <p class="quote_author">${homepage?.headline2}</p>
                </g:else>
            </div>
        </div>
        <div class="four columns">
            <div class="home3col_noborder">
                <p>&#34;&nbsp;${homepage?.testimonial3}&nbsp; &#34;</p>
                <g:if test="${homepage?.imageurl3}">
                    <img src="${homepage?.imageurl3}"/>
                </g:if>
                <g:else>
                    <p class="quote_author">${homepage?.headline3}</p>
                </g:else>
            </div>
        </div>
    </div>
    <!-- main content area -->

    </body>
</html>
