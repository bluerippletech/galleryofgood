<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='account.activated.title'/></title>
    <r:require module="foundation"/>
</head>

<body>

<g:if test="${user.ageGroup=="above13"}">
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <h4>Welcome! Your account is activated.</h4>
        <h6>Now on to the good stuff...</h6>

    </div>
</div>


<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr />
    </div>

</div>

<div class="row">
    <div class="twelve columns">
        <p></p>
        <ul id="link-list">
            <li><a href="${createLink(controller:'gallery',action:'index')}">Open a Gallery</a></li>
            <li><a href="${createLink(controller: 'artwork', action: 'landing')}">Browse Artworks</a></li>
            <!--li><a href="#">View Fundraisers</a></li-->

            <li><a href="${createLink(controller:'appUser',action:'accountSettings')}">Edit Account Settings</a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>

</div>
<!-- main content area -->
</g:if>
<g:else>
    <!-- main content area -->
    <div class="row">
        <div class="twelve columns">
            <h4>Welcome! Thanks for activating your child's account.</h4>

        </div>
    </div>


    <div class="row">
        <div class="twelve columns">
            <p>&nbsp;</p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <hr />
        </div>

    </div>

    <div class="row">
        <div class="twelve columns" id="link-list">
            <p>Account notifications will be sent to <strong>${user.email}</strong>. You may change this in your <a href="${createLink(controller:'appUser',action:'accountSettings')}">account settings</a>.</p>
            <p>Need some help getting started? Head over to the <a href="${createLink(controller: 'gallery', action: 'guide' )}">Gallery Guide</a>.<br />

                If you're all set, <a href="${createLink(controller:'gallery',action:'index')}">open a gallery now</a>!</p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">

            <p>&nbsp;</p>
        </div>
    </div>
    <!-- main content area -->
</g:else>

</body>
