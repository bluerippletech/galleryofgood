<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.register.title'/></title>
    <r:require module="foundation"/>
</head>

<body>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <h4>Thanks for joining Gallery of Good!
           <g:if test="${command?.radio1=="below13"}">
               Now, ask your parents to check their email..
           </g:if>
        </h4>
        <h6>
            <g:if test="${command?.radio1=="below13"}">
                We just sent an email to
            </g:if>
            <g:else>
                We have sent an email confirmation to
            </g:else>

        <g:if test="${command?.radio1=="below13"}">
            <b>${command?.email}</b> to make sure signing up for Gallery of Good is okay. Your grown-up will need to get your account started before you begin uploading art. Let them know you’ve signed up and check back soon!
        </g:if>
            <g:else>
            <b>${command?.email}</b>. Check your email to activate your account.
            </g:else>
        
        </h6>

    </div>
</div>


<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr />
    </div>

</div>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>
</div>

<div class="row">
    <div class="two columns" style="text-align:right;">
        <r:img uri="/images/note.jpg" class="align-right"/>
    </div>
    <div class="ten columns">
        <h4>Heads up!</h4>
        <p>If you don't find the email from us, check your bulk or junk folder. Still don't see anything? <br />
            Please <a href="${createLink(controller:'contactUs',action:'index')}">contact us</a>.</p>
    </div>

</div>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>
</div>
<g:if test="${command?.radio1=="above13"}">
<div class="row">
    <div class="two columns" style="text-align:right;">
        <r:img uri="/images/tip.jpg" class="align-right"/>

    </div>
    <div class="ten columns">
        <br /><h4>Add us to your email Address Book</h4>
        <p>Make sure you receive uninterrupted notifications from us. Add our email address: <br /><strong>${grailsApplication.config.grails.mail.username}</strong> to your email contact list.</p>
    </div>
</div>
</g:if>
<!-- main content area -->

</body>
