<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.register.title'/></title>
    <r:require modules="core,foundation,captcha"/>
    <r:script disposition="head">
        $(function() {
           $(".ajax-fc-container").captcha({formId: 'registerForm',
            url:'${createLink(controller: 'register', action: 'captcha')}',
            captchaDir:'${createLinkTo(dir: 'static/images', absolute: true)}'});
        <g:if test="${command.radio1 == "below13"}">
            showBelow13();
        </g:if>
        <g:elseif test="${command.radio1 == "above13"||command.radio1==""||command.radio1==null}">
            showAbove13();
        </g:elseif>
        });

        function showBelow13(){
            $(".above13").hide();
            $(".below13").show();
            $("#details").show();

        }

        function showAbove13(){
            $(".above13").show();
            $(".below13").hide();
            $("#details").show();

        }

    </r:script>

</head>

<body>

<p/>
<div class="row">
    <div class="twelve columns">
        <h2 class="heading color-black">Create your account</h2>
    </div>
</div>
<g:form action='register' name='registerForm' class="custom nice">
<div class="row">
    <div class="four columns" id="horizontal_radio">
        <p>Are you under 13 years old?</p>
        <label for="radio1" onclick="javascript:showBelow13();">
            <input name="radio1" type="radio" id="radio1" style="display:none;"
                   value="below13" ${command.radio1 == "below13" ? "checked" : ""}>
            <span class="custom radio"></span> Yes
        </label>
        <label for="radio2" onclick="javascript:showAbove13();">
            <input name="radio1" type="radio" id="radio2" style="display:none;"
                   value="above13" ${command.radio1 == "above13" ||command.radio1==""||command.radio1==null? "checked" : ""}>
            <span class="custom radio checked"></span> No
        </label>
    </div>

    <div class="eight columns">
        <r:img uri="/images/note.jpg"/>
        <p style="display:inline-table;">If you are under 13, we’ll let your parents know when you create an account.</p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr/>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
    </div>
</div>

<div id="details">

    <div class="row">

        <div class="four columns">
            <div class="${hasErrors(bean: command, field: 'username', 'form-field error')}">
                <label>Username</label>
                <input class="input-text" id="username" type="text" value="${command?.username}"
                       name="username">
                <small class="error">${hasErrors(bean: command, field: 'username', renderErrors(bean: command, field: 'username'))}</small>
            </div>

        </div>

        <div class="eight columns">
            <p>This will be your screen name. For your own safety, please avoid using your real name.<br/>Usernames can’t be changed, so choose wisely!
            </p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <div class="${hasErrors(bean: command, field: 'password', 'form-field error')}">
                <label>Password</label>
                <input id="password" class="input-text" type="password" size="40" value="${command?.password}"
                       name="password">
                <small class="error">${hasErrors(bean: command, field: 'password', renderErrors(bean: command, field: 'password'))}</small>
            </div>

            <div class="${hasErrors(bean: command, field: 'password2', 'form-field error')}">
                <label>Re-type Password</label>
                <input id="password2" class="input-text" type="password" size="40" value="${command?.password2}"
                       name="password2">
                <small class="error">${hasErrors(bean: command, field: 'password2', renderErrors(bean: command, field: 'password2'))}</small>
            </div>
        </div>

        <div class="eight columns">
            <p>Enter at least ${command.constraints.password.minSize} characters.</p>

            <p>Make your passwords hard for others to guess. Use both capital and lowercase letters, numbers and special symbols like >!#$%.</p>

            <p>Never make your password the same as your username.</p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <div class="${hasErrors(bean: command, field: 'email', 'form-field error')}">
                <label class="above13" style="display:none;">Email</label>
                <label class="below13" style="display:none;">Parent's Email</label>
                <input id="email" type="text" size="40" value="${command?.email}" name="email" class="input-text">
                <small class="error">${hasErrors(bean: command, field: 'email', renderErrors(bean: command, field: 'email'))}</small>
            </div>

            <div class="${hasErrors(bean: command, field: 'email2', 'form-field error')}">
                <label class="above13" style="display:none;">Re-type Email</label>
                <label class="below13" style="display:none;">Re-type Parent's Email</label>
                <input id="email2" type="text" size="40" value="${command?.email2}" name="email2"
                       class="input-text">
                <small class="error">${hasErrors(bean: command, field: 'email2', renderErrors(bean: command, field: 'email2'))}</small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>


    <div class="row">
        <div class="four columns">
            <!-- captcha -->
            <div class="ajax-fc-container"></div>
        </div>
        <div class="seven columns">
            <p>This visual <a
                    href="http://en.wikipedia.org/wiki/CAPTCHA">Captcha</a> helps us keep away automated programs from entering fake accounts.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="four columns">
            <g:hasErrors bean="${command}" field="captcha">
                <small class="error">${renderErrors(bean: command, field: 'captcha')}</small>
            </g:hasErrors>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row above13">
        <div class="four columns">
            <label for="checkbox1" style="font-size:13px">
                <input type="checkbox" id="checkbox1" style="display: none;" name="terms" ${command.terms?"checked":""}>
                <span class="custom checkbox"></span> I agree to the <a href="${createLink(controller:'general',action:'termsOfService')}" onclick="parent.location='${createLink(controller:'general',action:'termsOfService')}'">Terms of Service</a> and have read and understand the <a href="${createLink(controller: 'general',action: 'privacyPolicy')}" onclick="parent.location='${createLink(controller:'general',action: 'privacyPolicy')}'">Privacy Policy</a>.
            </label>
            <g:hasErrors bean="${command}" field="terms">
                <small class="error">${renderErrors(bean: command, field: 'terms')}</small>
            </g:hasErrors>
            <label for="checkbox1" style="font-size:13px">
                <input type="checkbox" id="checkbox2" style="display: none;" name="optin" ${command.optin?"checked":""}>
                <span class="custom checkbox"></span> Yes, email me when Gallery of Good has important news and art events!
            </label>
        </div>

        <div class="eight columns">
            <p>Please review our <a href="${createLink(controller: 'general', action: 'communityGuidelines')}">Community Guidelines</a> to learn more about how we work.</p>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="four columns">
            <input type="submit" value="Create Account" class="btn-blue">
        </div>

        <div class="eight columns">
            &nbsp;
        </div>
    </div>
</div>
</g:form>
<g:if test='${emailSent}'>
    <br/>
    <g:message code='spring.security.ui.register.sent'/>
</g:if>
<g:else>

</g:else>

<script>
    $(document).ready(function () {
        $('#username').focus();
    });
</script>

</body>
