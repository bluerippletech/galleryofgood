<head>
<title><g:message code='spring.security.ui.resetPassword.title'/></title>
<meta name='layout' content='main'/>
    <r:require module="foundation"/>
</head>

<body>
	<g:form action='resetPassword' name='resetPasswordForm' autocomplete='off' class="custom nice">
	<g:hiddenField name='t' value='${token}'/>

        <div class="row">
            <div class="twelve columns">&nbsp;
                <g:if test="${flash.error}">
                    <div class="alert-box error">
                        ${flash.error}
                        <a href="" class="close">&times;</a>
                    </div>
                </g:if>
            </div>
        </div>

            <div class="row">

                <div class="twelve columns">
                    <h4><g:message code='spring.security.ui.resetPassword.description'/></h4>

                    <div class="${hasErrors(bean: command, field: 'password', 'form-field error')}">
                        <label>Password</label>
                        <input id="password" class="input-text" type="password" size="40" value="${command?.password}"
                               name="password">
                        <small class="error">${hasErrors(bean: command, field: 'password', renderErrors(bean: command, field: 'password'))}</small>
                    </div>

                    <div class="${hasErrors(bean: command, field: 'password2', 'form-field error')}">
                        <label>Re-type Password</label>
                        <input id="password2" class="input-text" type="password" size="40" value="${command?.password2}"
                               name="password2">
                        <small class="error">${hasErrors(bean: command, field: 'password2', renderErrors(bean: command, field: 'password2'))}</small>
                    </div>
                    <p></p>
                </div>
            </div>
            <div class="row">
                <div class="twelve columns">
                    <input type="submit" value="Submit" class="btn-blue">
                </div>
            </div>
	</div>
	</g:form>

	</div>

<script>
$(document).ready(function() {
	$('#password').focus();
});
</script>

</body>
