<head>
<title><g:message code='spring.security.ui.forgotPassword.title'/></title>
<meta name='layout' content='main'/>
    <r:require module="foundation"/>
</head>

<body>
<!-- main content area -->
<g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='off' class="custom nice">

    <div class="row">
        <div class="twelve columns">&nbsp;

        </div>
    </div>
    <g:if test='${emailSent}'>
        <div class="row">
            <div class="twelve columns">
                <h4>Help is on the way!</h4>
                <p>Check your email. We’ll tell you what to do next!</p>
            </div>
        </div>
        <div class="row">
            <div class="twelve columns">
                &nbsp;
            </div>
        </div>
    </g:if>
    <g:else>
        <div class="row">

            <div class="twelve columns">
                <h4>Don't remember your username or password?</h4>
                <p>Enter the email address you used to sign-up for an account.</p>
                <!--label>Email</label-->
                <input type="text" class="input-text" name="username"/>
                <g:if test="${flash.error}">
                    <small class="error">${flash.error}</small>
                </g:if>
                <p></p>
            </div>
        </div>
        <div class="row">
            <div class="twelve columns">
                <input type="submit" value="Submit" class="btn-blue">
            </div>
        </div>
    </g:else>
</g:form>



<!-- main content area -->

<script>
$(document).ready(function() {
	$('#username').focus();
});
</script>

</body>
