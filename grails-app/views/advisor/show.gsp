
<%@ page import="com.galleryofgood.admin.Advisor" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="adminmain">
		<g:set var="entityName" value="${message(code: 'advisor.label', default: 'Advisor')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="ten columns ap-right">
        <g:render template="/layouts/flashMessage"/>
        <g:render template="/layouts/flashErrors"/>
        <p class="ap-page">2 Cents Circle</p>
		<div id="show-advisor" class="content scaffold-show" role="main">
			<h5><g:message code="default.show.label" args="[entityName]" /></h5>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

            <table>
                <tr>
                    <td>
                         <g:if test="${advisorInstance?.url}">
                             <div style="width: 200px; height: 145px; overflow: hidden;background-color:white;">
                                 <img src="${advisorInstance?.url}" style="max-width:100%;max-height:100%"/>
                             </div>
                        </g:if>
                    </td>
                    <td>
                        <g:if test="${advisorInstance?.name}">
                            <span id="name-label" class="property-label"><strong><g:message code="advisor.name.label" default="Name" /></strong></span>   :

                            <span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${advisorInstance}" field="name"/></span>
                        </g:if>   <br/>
                        <g:if test="${advisorInstance?.title}">
                            <span id="title-label" class="property-label"><strong><g:message code="advisor.title.label" default="Title" /></strong></span>  :
                            <span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${advisorInstance}" field="title"/></span>
                        </g:if>     <br/>
                        <g:if test="${advisorInstance?.expertise}">
                            <span id="expertise-label" class="property-label"><strong><g:message code="advisor.expertise.label" default="Expertise" /></strong></span>  :
                            <span class="property-value" aria-labelledby="expertise-label">${advisorInstance.getFormattedExpertise()}</span>
                        </g:if>    <br/>
                        <g:if test="${advisorInstance?.bio}">
                            <span id="bio-label" class="property-label"><strong><g:message code="advisor.bio.label" default="Bio" /></strong></span>  : <br/>
                            <span class="property-value" aria-labelledby="bio-label">${advisorInstance?.bio}</span>
                        </g:if>
                    </td>
                </tr>
            </table>
			<g:form>
				<fieldset class="buttons" style="border: 0">
					<g:hiddenField name="id" value="${advisorInstance?.id}" />
					<g:link class="edit small round button blue" style="padding:9px 25px 13px" action="edit" id="${advisorInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete small round button red" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
        </div>
	</body>
</html>
