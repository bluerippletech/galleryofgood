<%@ page import="com.galleryofgood.gallery.Artwork; com.galleryofgood.admin.Advisor" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="adminmain">
    <g:set var="entityName" value="${message(code: 'advisor.label', default: 'Advisor')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <r:require modules="foundation,jquery-ui,CLeditor"/>
    <r:script>
        $(function () {
            $("#redactor_content").cleditor()
        });
    </r:script>
</head>

<body>
<div class="ten columns ap-right">
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
    <p class="ap-page">2 Cents Circle</p>
    <g:form action="save" enctype="multipart/form-data" class="custom nice">
    <div class="row">
        <div class="nine columns">
            &nbsp;
        </div>
        <div class="one column">
            <g:submitButton name="create" class="save small round button blue"
                            value="${message(code: 'advisor.button.create.label', default: 'Add Advisor')}"/>

        </div>
    </div>
        <hr/>

        <div id="create-advisor" class="content scaffold-create" role="main">
            <h4><g:message code="default.creates.label" args="[entityName]" default="Add New Advisor"/></h4>



            <label for="name">
                <g:message code="advisor.name.label" default="Name"/>
            </label>
            <g:textField name="name" class="input-text" value="${advisorInstance?.name}"/>
            <g:hasErrors bean="${advisorInstance}" field="name">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${advisorInstance}"
                                                                         field="name"/></small>
            </g:hasErrors>

            <label for="title">
                <g:message code="advisor.title.label" default="Title"/>
            </label>
            <g:textField name="title" value="${advisorInstance?.title}" class="input-text"/>
            <g:hasErrors bean="${advisorInstance}" field="title">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${advisorInstance}"
                                                                         field="title"/></small>
            </g:hasErrors>

            <label for="expertise">
                <g:message code="advisor.expertise.label" default="Expertise"/>

            </label>

            <g:set var="advisorexpertise" value="${advisorInstance?.expertise?.split(',')}"/>
            <g:each in="${Artwork?.constraints?.artworkType?.inList}" var="artworkType" status="i">
                <input type="checkbox" name="expertise" value="${artworkType}"
                       <g:each in="${advisorexpertise}" var="expertField" status="e"><g:if
                               test="${expertField == artworkType}">checked</g:if></g:each>/>${artworkType}
            </g:each>

            <label for="imageFile" style="margin:10px 0 0 0;">
                <g:message code="advisor.url.label" default="Image"/>

            </label>
            <input type="file" name="imageFile"/>

            <label for="bio" style="margin:10px 0 0 0;">
                <g:message code="advisor.bio.label" default="Bio"/>
            </label>
            <textarea id="redactor_content" name="bio">
                ${advisorInstance?.bio}
            </textarea>

            <div class="row">
                <div class="nine columns">
                    &nbsp;
                </div>

                <div class="one column">
                    <g:submitButton name="create" class="save small round button blue"
                                    value="${message(code: 'advisor.button.create.label', default: 'Add Advisor')}"/>

                </div>
            </div>
        </div>
    </g:form>
</div>
</body>
</html>
