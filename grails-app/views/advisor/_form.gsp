<%@ page import="com.galleryofgood.gallery.Artwork; com.galleryofgood.admin.Advisor" %>




<div class="fieldcontain ${hasErrors(bean: advisorInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="advisor.name.label" default="Name" />
		
	</label>
	<g:textField name="name" class="input" value="${advisorInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: advisorInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="advisor.title.label" default="Title" />
	</label>
	<g:textField name="title" value="${advisorInstance?.title}"/>
</div>


<div class="fieldcontain ${hasErrors(bean: advisorInstance, field: 'expertise', 'error')}" style="margin-top:10px;">
    <label for="expertise">
        <g:message code="advisor.expertise.label" default="Expertise" />

    </label>

    <g:set var="advisorexpertise" value="${advisorInstance?.expertise?.split(',')}"/>
    <g:each in="${Artwork?.constraints?.artworkType?.inList}" var="artworkType" status="i">
    <input type="checkbox" name="expertise" value="${artworkType}"<g:each in="${advisorexpertise}" var="expertField" status="e"><g:if test="${expertField==artworkType}">checked</g:if></g:each>
    />${artworkType}
    </g:each>
    %{--<g:select name="expertise" from="${Artwork?.constraints?.artworkType?.inList}"--}%
              %{--value="${advisorInstance?.expertise}"/>--}%
</div>

<div class="fieldcontain ${hasErrors(bean: advisorInstance, field: 'url', 'error')} ">
	<label for="imageFile">
		<g:message code="advisor.url.label" default="Image" />
		
	</label>
	<input type="file" name="imageFile"/>
</div>

<div class="fieldcontain ${hasErrors(bean: advisorInstance, field: 'bio', 'error')} ">
    <label for="bio">
        <g:message code="advisor.bio.label" default="Bio" />
    </label>
    <textarea id="redactor_content" name="bio">
        ${advisorInstance?.bio}
    </textarea>
</div>