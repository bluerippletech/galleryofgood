<%@ page import="com.galleryofgood.gallery.Artwork; com.galleryofgood.admin.Advisor" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="adminmain">
		<g:set var="entityName" value="${message(code: 'advisor.label', default: 'Advisor')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
        <r:require modules="foundation,jquery-ui,CLeditor"/>
        <r:script>
            $(function(){
                $("#redactor_content").cleditor()
            });
        </r:script>
	</head>
	<body>
    <div class="ten columns ap-right">
        <g:render template="/layouts/flashMessage"/>
        <g:render template="/layouts/flashErrors"/>
        <p class="ap-page">2 Cents Circle</p>

        <g:form method="post" enctype="multipart/form-data" class="custom nice">
        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <g:actionSubmit class="save small round button blue" action="update" value="${message(code: 'advisor.button.edit.label', default: 'Edit Advisor')}" />

            </div>
        </div>

            <hr/>

            <div id="create-advisor" class="content scaffold-create" role="main">
            <h4><g:message code="default.edit.label" args="[entityName]" default="Edit Advisor"/></h4>



            <label for="name">
                <g:message code="advisor.name.label" default="Name"/>
            </label>
            <g:textField name="name" class="input-text" value="${advisorInstance?.name}"/>
            <g:hasErrors bean="${advisorInstance}" field="name">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${advisorInstance}"
                                                                         field="name"/></small>
            </g:hasErrors>

            <label for="title">
                <g:message code="advisor.title.label" default="Title"/>
            </label>
            <g:textField name="title" value="${advisorInstance?.title}" class="input-text"/>
            <g:hasErrors bean="${advisorInstance}" field="title">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${advisorInstance}"
                                                                         field="title"/></small>
            </g:hasErrors>

            <label for="expertise">
                <g:message code="advisor.expertise.label" default="Expertise"/>

            </label>

            <g:set var="advisorexpertise" value="${advisorInstance?.expertise?.split(',')}"/>
            <g:each in="${Artwork?.constraints?.artworkType?.inList}" var="artworkType" status="i">
                <input type="checkbox" name="expertise" value="${artworkType}"
                       <g:each in="${advisorexpertise}" var="expertField" status="e"><g:if
                               test="${expertField == artworkType}">checked</g:if></g:each>/>${artworkType}
            </g:each>
            <label for="imageFile" style="margin:10px 0 0 0;">
                <g:message code="advisor.url.label" default="Image"/>
            </label>
            <img src="${advisorInstance?.url}" style="max-width: 200px;"/><br/>
            <input type="file" name="imageFile"/>

            <label for="bio" style="margin:10px 0 0 0;">
                <g:message code="advisor.bio.label" default="Bio"/>
            </label>
            <textarea id="redactor_content" name="bio">
                ${advisorInstance?.bio}
            </textarea>

            <div class="row">
                <div class="nine columns">
                    &nbsp;
                </div>

				<g:hiddenField name="id" value="${advisorInstance?.id}" />
				<g:hiddenField name="version" value="${advisorInstance?.version}" />
            <div class="one column">
					<g:actionSubmit class="save small round button blue" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:actionSubmit class="delete small round button red" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            </div>
            </div>
            </div>
			</g:form>
		</div>
        </div>
	</body>
</html>
