
<%@ page import="com.galleryofgood.admin.Advisor" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="adminmain">
		<g:set var="entityName" value="${message(code: 'advisor.label', default: 'Advisor')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
<r:script>

</r:script>
	<body>
    <div class="ten columns ap-right">
        <g:render template="/layouts/flashMessage"/>
        <g:render template="/layouts/flashErrors"/>
        <p class="ap-page">2 Cents Circle</p>
        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>
            <div class="one column">
                <g:link class="create small round button blue" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>
            </div>
        </div>
		<div id="list-advisor" class="row-fluid" role="main">
			<h4><g:message code="default.list.label" args="[entityName]" /></h4>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table width="100%">
				<thead>
					<tr>
                        <g:sortableColumn property="name" title="${message(code: 'advisor.name.label', default: 'Name')}" />
                        <g:sortableColumn property="title" title="${message(code: 'advisor.title.label', default: 'Title')}" />
						<g:sortableColumn property="expertise" title="${message(code: 'advisor.expertise.label', default: 'Expertise')}" />
                        <g:sortableColumn property="bio" title="${message(code: 'advisor.bio.label', default: 'Bio')}" />
                        <g:sortableColumn property="url" title="${message(code: 'advisor.url.label', default: 'Image')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${advisorInstanceList}" status="i" var="advisorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td><g:link action="show" id="${advisorInstance.id}">${fieldValue(bean: advisorInstance, field: "name")}</g:link></td>
                        <td>${fieldValue(bean: advisorInstance, field: "title")}</td>
						<td>
                        ${advisorInstance.getFormattedExpertise()}
						</td>
                        <td>${advisorInstance.bio}</td>
						<td>
                            <div style="width: 200px; height: 145px; overflow: hidden;background-color:white;">
                            <img src="${advisorInstance?.url}" style="max-width:100%;max-height:100%"/>
                           </div>
                             </td>
                         </tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${advisorInstanceTotal}" />
			</div>
		</div>
        </div>
	</body>
</html>
