<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='search.themeTag' default="Emerging Themes - Gallery of Good"/></title>
    <r:require modules="foundation,tagcloud"/>
    <r:script>
        $.fn.tagcloud.defaults = {
            size:{start:14, end:18, unit:'pt'},
            color:{start:'#000', end:'#000'}
        };

        $(function () {
            $('#tagcloud a').tagcloud();
        });
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <h2 class="heading color-black">Emerging Themes</h2>
        <!-- <h3 class="heading color-black">Emerging Themes</h3> -->
        <hr />
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        %{--<div id="tagcloud">--}%
            %{--<g:each in="${modelInstance}" var="tag">--}%
                %{--<a href="${createLink(controller: 'search',action:'themeTag', params: [theme: tag[0]])}"--}%
                   %{--rel="${tag[1]}">${tag[0]}</a>--}%
            %{--</g:each>--}%
        %{--</div>--}%
        <ul class="tag_cloud">
            <g:each in="${modelInstance}" var="tag">
            <li class="tag_size_${tag[1]}"><a href="${createLink(controller: 'search',action:'themeTag', params: [theme: tag[0]])}"/>${tag[0]}</li>
            </g:each>
         </ul>
        <div class="clear-both"></div>
    </div>
</div>
<!-- main content area -->




</body>