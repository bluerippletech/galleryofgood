<%@ page import="com.galleryofgood.gallery.Artwork" %>
<r:require module="foundation"/>
<script>
            $(function() {
                $("ul[name='tags']").tagit({select:true, tagSource: "${g.createLink(action: 'tags')}", allowSpaces: true});
            });
</script>
<g:if test="${"YoungArtist".equals(gallery?.galleryOwner)}">
<div class="float-left">
    <label>Artist</label>
    <select style="display:none;" name="artist">
        <g:if test="${gallery?.youngArtists.size()!=1}">
            <g:if test="${!artwork?.artist}"><option value="">Select One</option></g:if>
            <g:each in="${gallery?.youngArtists}" var="youngArtist">
                <option value="${youngArtist?.nickname}" ${artwork?.artist==youngArtist?.nickname?"SELECTED":""}>${youngArtist?.nickname}</option>
            </g:each>
        </g:if>
        <g:else>
            <g:if test="${!artwork?.artist}"><option value="">Select One</option></g:if>
            <g:each in="${gallery?.youngArtists}" var="youngArtist">
                <option value="${youngArtist?.nickname}" SELECTED>${youngArtist?.nickname}</option>
            </g:each>
        </g:else>
    </select>

    <div class="custom dropdown">
        <a href="#" class="current">${artwork?.artist?:'Select One'}</a>
        <a href="#" class="selector"></a>
        <ul>
            <g:if test="${!artwork?.artist}"><li>Select One</li></g:if>
            <g:each in="${gallery?.youngArtists}" var="youngArtist">
               <li>${youngArtist?.nickname}</li>
            </g:each>
        </ul>
    </div>
    <small class="error custom-errormsg-1 custom-errormsg-3"></small>
</div>
</g:if>
<div class="float-right">
    <label>Age Artwork Was Created:</label>
    <select style="display:none;" name="artworkAge">
        <g:if test="${!artwork.artworkAge}"><option value="">Select One</option></g:if>
        <option value="0" ${artwork.artworkAge==0?"SELECTED":""}>Less than 1 year</option>
        <g:each in="${1..18}" var="i">
            <option value="${i}" ${artwork.artworkAge==i?"SELECTED":""}>${i} year</option>
        </g:each>
    </select>
    <div class="custom dropdown">
        <a href="#" class="current">${(artwork.artworkAge!=null)?(artwork.artworkAge>1?artwork.artworkAge+" years":(artwork.artworkAge==1?"1 year":"Less than 1 year")):"Select One"}</a>
        <a href="#" class="selector"></a>
        <ul>
            <g:if test="${!artwork.artworkAge}"><li>Select One</li></g:if>
            <li>Less than 1 year</li>
            <g:each in="${1..18}" var="i">
                <li>${i} year${i>1?"s":""}</li>
            </g:each>
        </ul>
    </div>
    <small class="error custom-errormsg-1 custom-errormsg-4"></small>
</div>
<div class="clear-both"></div>
<div class="float-left">
    <label>Type</label>
    <g:select name="artworkType" from="${Artwork?.constraints?.artworkType?.inList}"
              value="${artwork?.artworkType}" style="display:none"/>
    <div class="custom dropdown">
        <a href="#" class="current">${artwork?.artworkType}</a>
        <a href="#" class="selector"></a>
        <ul>
            <g:each in="${Artwork?.constraints?.artworkType?.inList}" var="artworkType">
                <li>${artworkType}</li>
            </g:each>
        </ul>
    </div>
    <small class="error custom-errormsg-1 custom-errormsg-2"></small>
</div>
<div class="float-right">
    <label>Dimensions:</label>
    <input name="dimensions" type="text" class="input-text narrower" placeholder="e.g. 10&#34; x 25&#34;" value="${artwork?.dimensions}"/>
</div>
<div class="clear-both"></div>
<div class="float-left">
    <label>Mess Factor:</label>
    <g:select name="messFactor" from="${Artwork?.constraints?.messFactor?.inList}"
              value="${artwork?.messFactor}" style="display:none"/>
    <div class="custom dropdown">
        <a href="#" class="current">${artwork?.messFactor}</a>
        <a href="#" class="selector"></a>
        <ul>
            <g:each in="${Artwork?.constraints?.messFactor?.inList}" var="messFactor">
                <li>${messFactor}</li>
            </g:each>
        </ul>
    </div>
    <small class="error custom-errormsg-1 custom-errormsg-3"></small>
</div>
<div class="clear-both"></div>
<div class="float-left">
<label>Description/Artist Statement:</label>
<textarea class="wider fixed-width-440" name="description" placeholder="Share something about this artwork or offer step-by-step instructions on how to make it and the materials used.">${artwork.description}</textarea>
<small class="error custom-errormsg-1 custom-errormsg-4"></small>
</div>
<div class="clear-both"></div>
<div class="float-left" style="width:464px;">
    <label>Enter descriptive tags separated by commas to sort artworks by theme:</label>
    <ul name="tags">
        <g:each in="${artwork?.tags}">
            <li>${it}</li>
        </g:each>
    </ul>

</div>
<div class="clear-both"></div>
%{--<div class="float-left">--}%
    %{--<label>Or select relevant tags:</label>--}%
    %{--<g:each in="${relevantTags}" var="tag">--}%
        %{--<a href="javascript:addTag('${formName}','${tag}');">${tag}</a>--}%
    %{--</g:each>--}%

%{--</div>--}%