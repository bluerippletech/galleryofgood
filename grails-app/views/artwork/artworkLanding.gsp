<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='artwork.landing'/></title>
    <r:require modules="foundation,lazyload"/>
    <r:script>
        $(function () {
            $('#featured').orbit({bullets:true,animationSpeed:1600,timer:false});
            $('img.lazy').lazyload({effect: "fadeIn"});
        });
    </r:script>
</head>

<body>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <p id="title_fresh_paint_view_new"><a
                href="${createLink(controller: 'search', action: 'freshPaint')}">view new artworks</a></p>

        <h2 class="heading color-black">fresh Paint</h2>

        <div class="clear-both"></div>
    </div>
</div>

<div class="row">
    <div class="twelve columns artworkSlider">
        <div id="featured">
            <g:each in="${sliderArtworks}" var="artworkNumbers" status="n">
                <g:if test="${n%2==0}">

                    <div style="text-align:center;width: 820px; height: 436px; overflow: hidden;background-color:white;padding:0 76px;">
                    <g:set var="lastNumber" value="${sliderArtworks?.size()}"/>
                    <g:each in="${sliderArtworks}" var="artworkNumber" status="i">

                        <g:if test="${lastNumber==i+1 && n==i}">
                            <div style="width:48%;margin:0 auto;"><div style="height:322px"><a href="${createLink(controller:'artwork',action:'artworkview',id:artworkNumber?.id)}"><img class="lazy" src="${resource(dir: 'images', file: 'white.jpg')}" data-original="${artworkNumber?.artworkDetailImageUrl}" alt="${artworkNumber?.title}" style="max-height:100%;max-width:100%"/></a>
                            </div>
                                <div style="font-family: Arial;font-size:13px;margin-top:16px;line-height: 1.2;">
                                    <a href="${createLink(controller:'artwork',action:'artworkview',id:artworkNumber?.id)}" style="font-size:18px;font-weight:bold">${artworkNumber?.title}</a>           <br/>
                                    By: ${artworkNumber?.artist}, Age: ${artworkNumber?.getAgeGroup()} <br/>
                                    Gallery: <a href="${createLink(controller:artworkNumber?.gallery?.galleryNameUrl)}">${artworkNumber?.gallery?.galleryName}</a>
                                </div>
                            </div >
                        </g:if>
                        <g:if test="${n==i && lastNumber!=i+1}">
                        <div style="width:48%;float:left;"><div style="height:322px"><a href="${createLink(controller:'artwork',action:'artworkview',id:artworkNumber?.id)}"><img class="lazy" src="${resource(dir: 'images', file: 'white.jpg')}" data-original="${artworkNumber?.artworkDetailImageUrl}" alt="${artworkNumber?.title}" style="max-height:100%;max-width:100%"/></a>
                        </div>
                        <div style="font-family: Arial;font-size:13px;margin-top:16px;line-height: 1.2;">
                            <a href="${createLink(controller:'artwork',action:'artworkview',id:artworkNumber?.id)}" style="font-size:18px;font-weight:bold">${artworkNumber?.title}</a>           <br/>
                            By: ${artworkNumber?.artist}, Age: ${artworkNumber?.getAgeGroup()} <br/>
                            Gallery: <a href="${createLink(controller:artworkNumber?.gallery?.galleryNameUrl)}">${artworkNumber?.gallery?.galleryName}</a>
                        </div>
                        </div >
                        </g:if>

                        <g:if test="${n+1==i}">
                            <div style="float:left;width:16px;height:100%;">&nbsp;</div>
                            <div style="width:48%;float:left;"><div style="height:322px"><a href="${createLink(controller:'artwork',action:'artworkview',id:artworkNumber?.id)}"><img class="lazy" src="${resource(dir: 'images', file: 'white.jpg')}" data-original="${artworkNumber?.artworkDetailImageUrl}" alt="${artworkNumber?.title}" style="max-height:100%;max-width:100%"/></a>
                            </div>
                                <div style="font-family: Arial;font-size:13px;margin-top:16px;line-height: 1.2;">
                                    <a href="${createLink(controller:'artwork',action:'artworkview',id:artworkNumber?.id)}" style="font-size:18px;font-weight:bold">${artworkNumber?.title}</a>           <br/>
                                    By: ${artworkNumber?.artist}, Age: ${artworkNumber?.getAgeGroup()} <br/>
                                    Gallery: <a href="${createLink(controller:artworkNumber?.gallery?.galleryNameUrl)}">${artworkNumber?.gallery?.galleryName}</a>
                                </div>
                            </div >
                        </g:if>
                     </g:each>

                    </div>
                </g:if>
            </g:each>
        </div>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <g:if test="${homepageInstance?.timelinelink}">
            <p id="title_retrospective_view_timeline"><a href="${createLink(controller:'timeline',action:'timelineview',id:homepageInstance?.timelinelink)}">view timeline</a></p>
        </g:if>
        <h3 id="title_retrospective"><span></span>Retrospective</h3>

        <div class="clear-both"></div>
        <h4 class="heading color-black rs-h">${homepageInstance?.retrospectiveContent}</h4>
        <a href="${homepageInstance?.retrospectiveUrl}">
            <g:if test="${homepageInstance?.retrospectiveUrl}">
                <img class="lazy" src="${resource(dir: 'images', file: 'white.jpg')}" alt="" data-original="${homepageInstance?.retrospectiveImage}">
            </g:if>
            %{--<img src="${homepageInstance?.retrospectiveImage}" alt=""/>--}%
        </a>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr/>
    </div>
</div>

<div class="row grid_bg_home3col_drawing_painting_crafts">
    <div class="four columns">
        <div class="home3col">
            <g:set var="featureArtwork1" value="${Artwork.get(homepageInstance?.featuredArtworkByType1)}"/>
            <h3 class="heading color-black feature-box">${homepageInstance?.featureArtworkType1}</h3>

            <div style="width: 299px; height: 233px; padding: 0; margin-bottom: 15px; overflow: hidden" class="display-artwork">
                <a href="${createLink(controller: 'artwork', action:'artworkview', id: featureArtwork1?.id)}"><img  style="max-height:100%;max-width:100%" src="${featureArtwork1?.artworkDetailImageUrl}" alt=""/></a>
            </div>

            <div class="artwork ma-artwork display-artwork">
                <h5><a href="${createLink(controller: 'artwork', action:'artworkview', id: featureArtwork1?.id)}">${featureArtwork1?.title}</a></h5>

                <p>By: ${featureArtwork1?.artist}, Age Group: ${featureArtwork1?.getAgeGroup()}</p>

                <p>Gallery: <a
                        href="${createLink(uri: "/" + featureArtwork1?.gallery?.galleryNameUrl)}">${featureArtwork1?.gallery?.galleryName}</a>
                </p>
            </div>
        </div>
    </div>

    <div class="four columns">
        <div class="home3col">
            <g:set var="featureArtwork1" value="${Artwork.get(homepageInstance?.featuredArtworkByType2)}"/>
            <h3 class="heading color-black feature-box">${homepageInstance?.featureArtworkType2}</h3>

            <div style="width: 299px; height: 233px; padding: 0; margin-bottom: 15px; overflow: hidden" class="display-artwork">
                <a href="${createLink(controller: 'artwork', action:'artworkview', id: featureArtwork1?.id)}"><img  style="max-height:100%;max-width:100%" src="${featureArtwork1?.artworkDetailImageUrl}" alt=""/></a>
            </div>

            <div class="artwork ma-artwork display-artwork">
                <h5><a href="${createLink(controller: 'artwork', action:'artworkview', id: featureArtwork1?.id)}">${featureArtwork1?.title}</a></h5>

                <p>By: ${featureArtwork1?.artist}, Age Group: ${featureArtwork1?.getAgeGroup()}</p>

                <p>Gallery: <a
                        href="${createLink(uri: "/" + featureArtwork1?.gallery?.galleryNameUrl)}">${featureArtwork1?.gallery?.galleryName}</a>
                </p>
            </div>
        </div>
    </div>

    <div class="four columns">
        <div class="home3col">
            <g:set var="featureArtwork1" value="${Artwork.get(homepageInstance?.featuredArtworkByType3)}"/>
            <h3 class="heading color-black feature-box">${homepageInstance?.featureArtworkType3}</h3>

            <div style="width: 299px; height: 233px; padding: 0; margin-bottom: 15px; overflow: hidden" class="display-artwork">
                <a href="${createLink(controller: 'artwork', action:'artworkview', id: featureArtwork1?.id)}"><img  style="max-height:100%;max-width:100%" src="${featureArtwork1?.artworkDetailImageUrl}" alt=""/></a>
            </div>

            <div class="artwork ma-artwork display-artwork">
                <h5><a href="${createLink(controller: 'artwork', action:'artworkview', id: featureArtwork1?.id)}">${featureArtwork1?.title}</a></h5>

                <p>By: ${featureArtwork1?.artist}, Age Group: ${featureArtwork1?.getAgeGroup()}</p>

                <p>Gallery: <a
                        href="${createLink(uri: "/" + featureArtwork1?.gallery?.galleryNameUrl)}">${featureArtwork1?.gallery?.galleryName}</a>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr/>
    </div>
</div>
<!--
<div class="row">
    <div class="twelve columns">
        <p id="title_goodworks_view_all"><a href="#">view all</a></p>
        <h3 id="title_goodworks"><span></span>Goodworks</h3>
        <div class="clear-both"></div>
    </div>
</div>

<div class="row grid_bg_home3col_goodworks">
    <div class="four columns">
        <div class="home3col">
            <a href="#"><img src="images/goodworks_01.jpg" alt="" /></a>
            <div class="artwork ma-artwork">
                <h5><a href="#">Title of Artwork</a></h5>
                <p>By: Robin 30, Age Group: 3-5</p>
                <p>Gallery: <a href="#">Art Machine</a></p>
            </div>
        </div>
    </div>
    <div class="four columns">
        <div class="home3col">
            <a href="#"><img src="images/goodworks_02.jpg" alt="" /></a>
            <div class="artwork ma-artwork">
                <h5><a href="#">Title of Artwork</a></h5>
                <p>By: Robin 30, Age Group: 3-5</p>
                <p>Gallery: <a href="#">Art Machine</a></p>
            </div>
        </div>
    </div>
    <div class="four columns">
        <div class="home3col">
            <a href="#"><img src="images/goodworks_03.jpg" alt="" /></a>
            <div class="artwork ma-artwork">
                <h5><a href="#">Title of Artwork</a></h5>
                <p>By: Robin 30, Age Group: 3-5</p>
                <p>Gallery: <a href="#">Art Machine</a></p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>
    </div>
</div>


<div class="row">
    <div class="twelve columns">
        <hr />
    </div>
</div>
-->
<div class="row">
    <div class="twelve columns">
        <p id="title_emerging_themes_view_all"><a
                href="${createLink(controller: 'artwork', action: 'emergingThemes')}">view all</a></p>

        <h2 class="heading color-black">Emerging Themes</h2>
        <!-- <h3 class="heading color-black">Emerging Themes</h3> -->
        <div class="clear-both"></div>
    </div>
</div>
%{--Start G:each here--}%
<g:each in="${emergingThemes}" var="theme">
    <div class="row">
        <div class="four columns">
            <p class="theme_tag_size_10"><a href="${createLink(controller:'search',action: 'themeTag')}?theme=${theme.key}">${theme.key}</a></p>
        </div>

        <div class="eight columns">
            <div class="theme_artwork_thumb">
                <g:each in="${theme.value}" var="artwork">

                    <div style="width: 200px; height: 165px; overflow: hidden; display:inline-block;text-align:center;vertical-align: top;margin-bottom:10px;">
                        <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id)}"><img style="max-height:100%;max-width:100%;"  class="lazy" src="" alt="" data-original="${artwork?.artworkDetailImageUrl}" alt=""/></a>
                    </div>
                </g:each>
            </div>
        </div>
    </div>
</g:each>
<div class="row">
    <div class="twelve columns">
        <p>&nbsp;</p>

        <p>&nbsp;</p>
    </div>
</div>
<!-- main content area -->

</body>
