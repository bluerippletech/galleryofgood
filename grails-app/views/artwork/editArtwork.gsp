<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='edit.artwork'/></title>
    <r:require modules="foundation,tagit,jcrop,core"/>
    <r:script>
            $(function() {
            var jcrop_api;
                $("ul[name='tags']").tagit({select:true, tagSource: "${g.createLink(action: 'tags')}", allowSpaces: true});
	            $(".manipulation").ajaxStart(function(){
	            $(".manipulation").removeAttr("onclick").removeAttr("href");
                });

                $t = $("#imageContainer"); // CHANGE it to the table's id you have

                $("#overlay").css({
                    opacity : 0.5,
        //            top     : $t.offset().top,
                    width   : $t.outerWidth(),
                    height  : $t.outerHeight(),
                    'background-color' : 'whitesmoke',
                    display : 'none'
                });

                $("#img-load").css({
                    top  : ($t.height() / 2),
                    left : ($t.width() / 2),
                    position:'relative'
                });


            });

                function showOverlay(){
                    $t = $("#imageContainer img"); // CHANGE it to the table's id you have

                    $("#overlay").css({
                        opacity : 0.5,
            //            top     : $t.offset().top,
                        width   : $t.outerWidth(),
                        height  : $t.outerHeight(),
                        position: 'absolute',
                        'z-index' : 1000,
                        float   : 'left'
                    });

                    $("#img-load").css({
                        top  : ($t.height() / 2),
                        left : ($t.width() / 2)
                    });

                    $("#overlay").show();
                }

                function hideOverlay(){
                    $("#overlay").hide();
                }


            function clearCoords(){
                $('#x').val(0);
	            $('#y').val(0);
	            $('#x2').val(0);
                $('#y2').val(0);
                $('#w').val(0);
                $('#h').val(0);
            };
            function showCoords(c){
	            $('#x').val(c.x);
	            $('#y').val(c.y);
	            $('#x2').val(c.x2);
                $('#y2').val(c.y2);
                $('#w').val(c.w);
                $('#h').val(c.h);
            };
            function cropHighlight(c){
                showCoords(c);
                $("#crop-button").find('img').addClass('highlight-crop');
            };
            function cropRelease(){
                clearCoords();
                 $("#crop-button").find('img').removeClass('highlight-crop');
            }
            function addTag(formName,aTagName){
                $("#"+formName).find("[name='tags']").tagit("createTag",aTagName);
            }

            function completeRotation(data){
                    $("#loading").hide();
		            var imageID="#jcrop_target";
		            $(imageID).parent().parent().html(data);
		            $(".manipulation").removeAttr("disabled");
		            hideOverlay();

        //TODO: better loading.
    %{--var img_src = $(imageID).attr('src');--}%
    %{--var timestamp = new Date().getTime();--}%
    %{--$(imageID).attr('src',img_src+'?'+timestamp);--}%
        }

              function rotate(right, obj){
              if(!$(obj).attr('disabled')){
                  jQuery.ajax({type:'GET',
                          url:'${createLink(controller: 'artwork', action: 'rotateImage', id: artwork.id)}?right='+right,
						beforeSend:function(data){
//						  addLoading();
                          $(".manipulation").attr("disabled","disabled");
						  showOverlay();
						},
		                success:function(data, textStatus) {
		                    completeRotation(data);
		                    $(".manipulation").removeAttr("disabled");
		                },
		                error:function(XMLHttpRequest, textStatus, errorThrown) {
//		                	$.jGrowl(XMLHttpRequest.responseText);
		                }
		            });
             }
            }

        function crop(obj){
        if(!$(obj).attr('disabled')){
            if (($("#w").val()==0&&$("#h").val()==0)||$("#w").val()==null||$("#h").val()==null){
                jcrop_api.setSelect([0,0,$(".jcrop-holder").width(),$(".jcrop-holder").height()]);
            }else{
//              $('#updateArtwork').submit();
                jQuery.ajax({type:'POST',
                    data:$("#updateArtwork").serialize(),
                    url:'${createLink(controller: 'artwork', action: 'cropImage', id: artwork.id)}',
                    beforeSend:function(data){
//						  addLoading();
                      $(".manipulation").attr("disabled","disabled");
                      showOverlay();
                    },
                    success:function(data, textStatus) {
                        completeRotation(data);
                        $(".manipulation").removeAttr("disabled");
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown) {
//		                	$.jGrowl(XMLHttpRequest.responseText);
                    }
                });
            }
        }
        }

    </r:script>
</head>

<body>
%{--<g:render template="/layouts/flashMessage"/>--}%
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'artworks']"/>
<!-- secondary navigation -->
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>

%{--<form class="custom nice" action="updateArtwork" method="post" enctype="multipart/form-data">--}%
<g:form name="updateArtwork" action="updateArtwork" class="custom nice" controller="artwork">
    <g:hiddenField name="id" value="${artwork.id}"/>
    <g:hiddenField name="version" value="${artwork.version}"/>
    <div class="row">
        <div class="twelve columns">
            <h4>Edit Artwork</h4>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="twelve columns horizontal_fields">
            <div>
                    <div id="overlay" class="progress-indicator">
    <r:img uri="/images/ajax-loader.gif" class="img-load" alt="loading" id="img-load"/>
    </div>

    <div id="imageContainer">
        <g:render template="imageSection" model="${[artwork: artwork]}"/>
    </div>
    <br>

    </div>

    <div>
        <div class="float-left">
            <label>Title</label>
            <g:textField name="title" value="${artwork.title}" class="input-text wider"/>
            <g:hasErrors bean="${artwork}" field="title">
                <small class="error"><g:renderErrors bean="${artwork}" field="title"/></small>
            </g:hasErrors>
        </div>
        <br/><br/>
        <g:if test="${artwork?.gallery?.galleryOwner?.equals("YoungArtist")}">
            <div class="float-left">
                <label>Artist</label>
                <select style="display:none;" name="artist">
                    <g:each in="${artwork?.gallery?.youngArtists}" var="youngArtist">
                        <option value="${youngArtist.nickname}" ${artwork.artist == youngArtist.nickname ? "SELECTED" : ""}>${youngArtist.nickname}</option>
                    </g:each>
                </select>
                <g:hasErrors bean="${artwork}" field="artist">
                    <small class="error"><g:renderErrors bean="${artwork}" field="artist"/></small>
                </g:hasErrors>
            </div>
        </g:if>
        <div class="float-right">
            <label>Age artwork was created</label>
            <select style="display:none;" name="artworkAge">
                <option value="0" ${artwork.artworkAge == 0 ? "SELECTED" : ""}>Less than 1 year old</option>
                <g:each in="${1..18}" var="i">
                    <option value="${i}" ${artwork.artworkAge == i ? "SELECTED" : ""}>${i} year${i>1?"s":""} old</option>
                </g:each>
            </select>
            <g:hasErrors bean="${artwork}" field="artworkAge">
                <br/>
                <small class="error" style="padding-bottom:0px;"><g:renderErrors bean="${artwork}" field="artworkAge"/></small>
            </g:hasErrors>
        </div>

        <div class="clear-both"></div>

        <br/><br/>

        <div class="float-left">
            <label>Type</label>
            <g:select name="artworkType" from="${Artwork?.constraints?.artworkType?.inList}"
                      value="${artwork?.artworkType}" style="display:none"/>
            <g:hasErrors bean="${artwork}" field="artworkType">
                <small class="error"><g:renderErrors bean="${artwork}" field="artworkType"/></small>
            </g:hasErrors>

        </div>

        <div class="float-right">
            <label>Dimensions:</label>
            <g:textField name="dimensions" value="${artwork.dimensions}" class="input-text narrower" placeholder="e.g. 10&#34; x 25&#34;"/>
        </div>

        <div class="clear-both"></div>

        <div class="float-left">
            <label for="checkbox1">
                <g:checkBox name="showInTimeline" value="${artwork.showInTimeline}"/>
                This artwork represents an age in the Artist Timeline.
            </label>
        </div>

        <div class="clear-both"></div>

        <div class="float-left">
            <label>Mess Factor:</label>
            <g:select name="messFactor" from="${Artwork?.constraints?.messFactor?.inList}"
                      value="${artwork?.messFactor}" style="display:none"/>

            <div class="clear-both"></div>

            <div class="float-left">
                <label>Description/Artist Statement:</label>
                <g:textArea name="description" value="${artwork.description}" class="wider fixed-width-440"/>
            </div>

            <div class="clear-both"></div>

            <div class="float-left" style="width:464px;">
                <label>Enter descriptive tags separated by commas to sort artworks by theme:</label>
                <ul name="tags">
                    <g:each in="${artwork?.tags}">
                        <li>${it}</li>
                    </g:each>
                </ul>

            </div>
            <div class="clear-both"></div>
            <div class="float-left">
                <input class="nice medium radius white button" type="submit" value="Update"/>
                <input class="nice medium radius white button" type="button" value="Cancel"
                       onclick="window.location.href = '${createLink( controller:"artwork", action:"index")}'"/>

            </div>
        </div>

    </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <p>&nbsp;</p>
        </div>
    </div>

    <div class="row">
        <div class="five columns">
                </div>

        <div class="seven columns">
            &nbsp;
        </div>
    </div>
</g:form>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->
<g:render template="/layouts/modalConfirm" />
<g:render template="/layouts/modalNotify" />
</body>
