<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='list.artwork'/></title>
    <r:require modules="foundation"/>
    <r:script>
        function redirectToUpload(){
            window.location='${createLink(controller:'artwork',action:'uploadArtwork')}';
        }

        function toggleAll(aCheckbox){
          var checkboxes=$(aCheckbox).closest("table").find(".checkbox");
          var checkboxes2=$(aCheckbox).closest("table").find("input[type='checkbox']");
          if ('checked'==$(aCheckbox).attr("checked")){
            checkboxes.addClass("checked");
            checkboxes2.attr("checked","checked");
            }
          else{
            checkboxes.removeClass("checked");
            checkboxes2.removeAttr("checked");

            }
        }
        function bulkaction(formObj){
        var currentDropdown = $('div.dropdown').children('a.current:last').text();
        var tickedCheckbox = $('#simple1Tab').find('span.checked').length;
        if ($('#modalDelete').css('visibility')=='visible'){
        return true;
        }
        else if (currentDropdown=='Delete'){
        $('.reveal-modal-bg').remove();
        $('#modalDelete').reveal({dismissModalClass: 'close-reveal'});
        return false;;
//        return true;
        }
        else if(tickedCheckbox==0){
         $('#modalNotify').find('p').text('Please click on the checkbox and choose a Bulk Action from the drop down menu.');
         if($('.modal-no-bg').size()==0){$('.reveal-modal-bg').css('opacity','0')}
         $('#modalNotify').reveal({dismissModalClass: 'close-reveal-modal'});
         return false;
        }
        else{
         $('#modalNotify').find('p').text('Please select a Bulk Action from the drop down menu and then click on Apply.');
         if($('.modal-no-bg').size()==0){$('.reveal-modal-bg').css('opacity','0')}
         $('#modalNotify').reveal({dismissModalClass: 'close-reveal-modal'});
         return false;
        }
        }

        $('#yes').click(function(){
            $('.reveal-modal-show').click()
        })

        $('.reveal-modal-show').click(function(){
            var currentDropdown = $('div.dropdown').children('a.current:last').text();
            if(currentDropdown!='Delete'){
                window.setTimeout(
                    function() {
                        $(".close-reveal-modal").click();
                    },
                ${timeout?:'5000'}
                );
            }
        })

        $(document).ready(function(){
            var artworkTable = $('li#simple1Tab').find('tr').size();
            if(artworkTable==1){
             $('#artworksTable').hide();
             $('#noArtworks').show();
            }
         });
        $('dl.tabs a').click(function(e){
           var tab = $(e.target);
           var hashtag = tab.attr('href');
           var artworkTable = $('li'+hashtag+'Tab').find('tr').size();
           if(artworkTable==1)
           {
             $('#artworksTable').hide();
             $('#noArtworks').show();
           }
           else{
             $('#artworksTable').show()
             $('#noArtworks').hide();
           }

        })
        <g:if test="${params?.selectedTab}">
            $(function(){
                $("#"+"${params?.selectedTab}").find("a").click()
            })
        </g:if>

        <g:if test="${params?.reveal=='forApproval'}">
            $('#${params?.reveal}').reveal({
                animation: 'fadeAndPop',
                animationspeed: 300,
                closeOnBackgroundClick: false,
                dismissModalClass: 'close-reveal-modal'
              });
        </g:if>
    </r:script>
</head>
<body>
<!-- secondary navigation -->
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'artworks']"/>
<!-- secondary navigation -->
<!-- main content area -->

<g:form name="artworkListing" controller="artwork" action="delete" onsubmit="return bulkaction(this);" class="custom nice">
    <g:hiddenField name="selectedTab" id="selectedTab" value="tab1"/>
    <div class="row">
        <div class="eight columns">
            &nbsp;
        </div>
        <div class="four columns">
            <a class="large white nice button radius uaed-add-artwork-btn" href="#" onclick="javascript:redirectToUpload();return false;">Upload Artworks</a>
        </div>
    </div>

    <div class="row">
        <div class="twelve columns">
            <h4>Edit Artworks</h4>
            <dl class="tabs">
                <g:if test="${gallery?.galleryOwner?.equals("YoungArtist")}">
                <g:each in="${gallery.youngArtists}" var="artist" status="i">
                    <dd id="tab${i+1}"><a href="#simple${i+1}" class="${i==0?"active":""}" onclick="$('#selectedTab').val('tab${i+1}')">${artist.nickname}</a></dd>
                </g:each>
                </g:if>
                <g:else>
                    <dd id="tab1"><a href="#simple1" class="active">Artworks</a></dd>
                </g:else>
            </dl>
            <div id="noArtworks" style="display:none;"><h5>Let’s see those artworks! Click <strong>Add Artworks</strong> on top to start uploading.</h3></div>
            <div id="artworksTable">
            <select style="display:none;width:120px;" name="bulkAction">
                <option SELECTED>Bulk&nbsp;Actions</option>
                <option value="delete">Delete</option>
            </select>
            <input class="nice small radius white button reveal-modal-show" type="submit" name="submit" value="Apply" />
            <ul class="tabs-content">
                <g:if test="${gallery?.galleryOwner?.equals("YoungArtist")}">
                <g:each in="${gallery.youngArtists}" var="artist" status="i">
                    <li class="${i==0?"active":""}" id="simple${i+1}Tab">
                        <table width="100%">
                            <thead>
                            <tr>
                                <th><label for="checkbox1">
                                    <input type="checkbox" id="checkbox1" style="display: none;" onchange="javascript:toggleAll(this);return false;" >
                                    <span class="custom checkbox" ></span>
                                </label></th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Statement</th>
                                <th>Age Created</th>
                                <th>Artist Timeline</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${com.galleryofgood.gallery.Artwork.findAllByArtistAndGallery(artist.nickname,gallery, [sort: 'dateCreated', order: 'desc'])}" var="artwork" status="j">
                                <tr>
                                    <td><label for="checkbox1">
                                        <input type="checkbox" id="checkbox1" style="display: none;" name="artwork_id" value="${artwork.id}">
                                        <span class="custom checkbox"></span>
                                    </label></td>
                                    <td style="padding:0;margin:0"><a href="${createLink(controller:'artwork',action:'editArtwork',id:artwork.id)}"><img src="${artwork.url}" alt="" width="56px;"/></a></td>
                                    <td><a href="${createLink(controller:'artwork',action:'editArtwork',id:artwork.id)}">${artwork.title}</a></td>
                                    <td>${artwork.artworkType}</td>
                                    <td>${artwork.description?.size()>100?"${artwork.description[0..100]}...":"${artwork.description?:""}"}</td>
                                    %{--<g:set var="tags" value="${(artwork?.tags?.join(", "))}"/>--}%
                                    <td>${artwork.artworkAge}</td>
                                    <td>
                                        <g:if test="${artwork.showInTimeline=='on'}">
                                            <r:img uri="/images/checkmark.gif" alt="" />
                                        </g:if>
                                    </td>
                                    <td>${artwork.status=='New'?'Pending':artwork.status}</td>
                                </tr>

                            </g:each>
                            </tbody>
                        </table>

                    </li>
                </g:each>
                </g:if>
                <g:else>
                    <li class="active" id="simple1Tab">
                        <table width="100%">
                            <thead>
                            <tr>
                                <th><label for="checkbox1">
                                    <input type="checkbox" id="checkbox1" style="display: none;" onchange="javascript:toggleAll(this);return false;" >
                                    <span class="custom checkbox" ></span>
                                </label></th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Statement</th>
                                <th>Age Created</th>
                                <th>Artist Timeline</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${com.galleryofgood.gallery.Artwork.findAllByGallery(gallery, [sort: 'dateCreated', order: 'desc'])}" var="artwork" status="j">
                                <tr>
                                    <td><label for="checkbox1">
                                        <input type="checkbox" id="checkbox1" style="display: none;" name="artwork_id" value="${artwork.id}">
                                        <span class="custom checkbox"></span>
                                    </label></td>
                                    <td style="padding:0;margin:0"><a href="${createLink(controller:'artwork',action:'editArtwork',id:artwork.id)}"><img src="${artwork.thumbnailUrl}" alt="" width="56px;"/></a></td>
                                    <td><a href="${createLink(controller:'artwork',action:'editArtwork',id:artwork.id)}">${artwork.title}</a></td>
                                    <td>${artwork.artworkType}</td>
                                    <td>${artwork.description?.size()>100?"${artwork.description[0..100]}...":"${artwork.description?:""}"}</td>
                                    <td>${artwork.artworkAge}</td>
                                    <td>
                                        <g:if test="${artwork.showInTimeline=='on'}">
                                            <r:img uri="/images/checkmark.gif" alt="" />
                                        </g:if>
                                    </td>
                                    <td>${artwork.status=='New'?'Pending':artwork.status}</td>
                                </tr>

                            </g:each>
                            </tbody>
                        </table>

                    </li>

                </g:else>
            </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="five columns">
            <!--input class="nice medium radius white button" type="submit" name="submit" value="Submit" /-->
        </div>
        <div class="seven columns">
            &nbsp;
        </div>
    </div>

</g:form>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->
<g:render template="/layouts/modalNotify"/>
<div id="modalDelete" class="reveal-modal modal-view" style="position: fixed">
    <p style="margin:0">
        Are you sure you want to delete selected artworks?
    </p>
    <div class="btn_grp" style="margin:15px 0 0 -10px;text-align: center">
        <a id="yes" class="nice small radius black button"
           style="width:90px;">Yes</a>
        <a id="no" class="nice small radius red button close-reveal" style="width:90px;">No</a>
        <a class="close-reveal-modal close-reveal">×</a>
    </div>
</div>

<!--Upload Modal Success-->

<div id="forApproval" class="reveal-modal modal-view" style="position:fixed;">
    <p style="margin:0 0 15px">Your upload is complete! We'll send you a note once the artworks
        go live in your gallery. In the meantime, you can continue to edit each
        artwork if haven't already done so.
    </p>
    %{--<form action="#" class="custom nice">--}%
        %{--<label for="checkbox1">--}%
            %{--<g:checkBox name="uploadReminder"/>--}%
            %{--Got it, don’t show this everytime I upload new artworks--}%
        %{--</label>--}%
    %{--</form>--}%
    <div class="btn_grp" style="margin:15px 0 0 -10px;">
        <a class="nice small radius black button" style="width:90px;" onclick="$('.close-reveal-modal').click()">Okay</a>
        <a class="close-reveal-modal close-reveal close">×</a>
    </div>
</div>

</body>
