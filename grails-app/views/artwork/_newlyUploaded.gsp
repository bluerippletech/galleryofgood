<li><div class="row">
    <div class="twelve columns">
        <hr>
    </div>
    </div>
<div class="row newImage">
    <div class="twelve columns horizontal_fields">
        <g:form name="newImage${newImageCount}" class="custom nice">
        <g:hiddenField name="id" value="${artwork?.id}"/>
        <g:hiddenField name="fileTitle" value="${artwork.fileTitle}"/>
        <g:hiddenField name="gallery.id" value="${gallery.id}"/>
        <g:hiddenField name="url" value="${artwork.url}"/>
        <div class='row'>
            <div class='twelve columns horizontal_fields'>
                <div class='thumbnail'>
                    <img src='${artwork.url}' alt='No image' id='test' width='70px' height='55px'/><br/>
                    <%--<a href='#' class='secondary_link'>Edit Image</a>--%>
                </div>

                <div class="editSection ten columns horizontal_fields">

                    <div class="float-left">
                        <label>Title</label>
                        <input name="title" class='input-text wider' style="display: none" type='text' value='${artwork.title}'>
                        <span id="artworkTitle" style="display: inline-block;">${artwork.title}</span>
                        <small class="error"></small>
                    </div>

                    <div class="float-right">
                        <a class="secondary_link edit_details_link" href="javascript:showEditDetails('newImage${newImageCount}',true);">Edit Details</a>
                        <a class="secondary_link close_save_link" href="#" onclick="javascript:deleteArtwork('newImage${newImageCount}');return false;" style="display:none;">Delete</a><br/>
                        <a class="secondary_link close_save_link" href="#" onclick="javascript:closeSave('newImage${newImageCount}');return false;" style="display:none;">Close</a> <r:img uri="/images/ajax-loader.gif" class="loader" style="display:none;"/>

                    </div>
                    <div class="subsection" style="display:none;">

                    </div>
                </div>
            </div>
        </div>
        </g:form>
    </div>
</div>
</li>

