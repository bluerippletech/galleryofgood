<g:hiddenField name="x"/>
<g:hiddenField name="y"/>
<g:hiddenField name="x2"/>
<g:hiddenField name="y2"/>
<g:hiddenField name="w"/>
<g:hiddenField name="h"/>
<div>
<img id="jcrop_target" src="${artwork.artworkDetailImageUrl}?${new java.util.Date()}" alt="" width="299px;"/><br/>
    <div style="margin-top:10px;">
        <ul class="linked_images_no_border">
            <li>
                <a class="manipulation" href="#" onclick="javascript:rotate('N', this);return false;">
                <r:img class="rotate" uri="/images/Rotate_left_hoover.png" />
            </a>
            </li>
            <li><a class="manipulation" href="#" onclick="javascript:rotate('Y', this);return false;"><r:img uri="/images/Rotate_right_hoover.png" class="rotate"/></a></li>
            <li><a class="manipulation" id="crop-button" href="#" onclick="javascript:crop(this);return false;"><r:img uri="/images/Crop_hoover.png" class="rotate" /></a></li>
        </ul>
    </div>

</div>
<script type="text/javascript">
    $(function () {
        $('#jcrop_target').Jcrop({
            onChange:showCoords,
            onSelect:cropHighlight,
            onRelease:cropRelease
        },function(){

            jcrop_api = this;});
    });

</script>
