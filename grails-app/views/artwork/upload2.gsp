<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='upload.artwork'/></title>
    <r:require modules="foundation,core,filedrop,tagit,underscoreJS,customScrollbar"/>
    <r:script>
        var currentArtworkSelected=null;
        var projectWebRoot= "${createLink(controller: 'artwork',action:'getArtworkMetadata')}";
        var getArtworkTags= "${createLink(controller: 'artwork',action:'getArtworkTags')}";
        function saveMetadata(){
            $.ajax({type:'POST',
                    //async:false,
		    		data:$("#metadata").serialize(),
			    	url: "${createLink(controller: 'artwork', action: 'saveNewArtworkMetadata')}",
    			    success:function(data) {
                        $("small").text("");
                        $("#uploadButton").removeClass("disabled");
                        $("#metaSaved").show();
                    },
                    error:function(error) {
                        $("small").text("");
                    var errorJSON= $.parseJSON(error.responseText);
                    _.each(errorJSON,function(num){
                        $("[name='"+num.field+"']").parent().find("small").text(num.message).show();
                                });
                    },
                    complete: function(data){
                        //hideLoader(formName);
                    }
                });
        }

        function deleteArtwork(){
        $.ajax({type:'GET',url:'${createLink(controller:'artwork',action:'remoteDeleteTemporary')}/'+currentArtworkSelected,
                success:function(data){
                    $("[tempid='"+currentArtworkSelected+"']").remove();
                    $("#metadatainput").find("input").val("");
                    $("#metadatainput").find("select").val("");
                    $("#metadatainput").hide();
                    $("#initialmessage").show();
                $("")
                }});
        }

        function viewArtworkListing(){
            if ($("#uploadButton").hasClass("disabled")) return;
            window.location='${createLink(controller: 'artwork', action: 'index', params:['reveal':'forApproval'])}';
        }

        $(function() {
            $("#metadatainput").hide();
            $("ul[name='tags']").tagit({select:true, tagSource: "${g.createLink(action: 'tags')}", allowSpaces: true});
            $(document).ready(function(){
                $("#dropbox").mCustomScrollbar({
                      set_width:false,
                      set_height:'100%',
                      horizontalScroll:false,
                      scrollInertia:550,
                      scrollEasing:"easeOutCirc",
                      mouseWheel:"auto",
                      autoDraggerLength:true,
                      scrollButtons:{
                        enable:false,
                        scrollType:"continuous",
                        scrollSpeed:20,
                        scrollAmount:40
                      },
                      advanced:{
                        updateOnBrowserResize:true,
                        updateOnContentResize:true,
                        autoExpandHorizontalScroll:false
                      }
                    });
            });
        });
    </r:script>
</head>
<body>
<g:render template="/layouts/secondaryNavigation" model="[activePage: 'artworks']"/>
<g:if test="${flash.successMessage}">
    <g:render template="../layouts/modalAlert" model="['elementId':'successModal']"/>
</g:if>
<form class="custom nice" action="#" method="post" enctype="multipart/form-data" id="metadata">
    <div class="row">
        <div class="twelve columns">
            <h4>Upload Artworks</h4>
        </div>
    </div>
    <div class="row">
        <div class="four columns" style="display:-webkit-inline-box;font-size:0.9em;">
            <div class="circle"><span>1</span></div>
            <b>Drag and drop your files into the gray box.</b>
                <br/>
            Jpg, png and gif files 5MB or less are acceptable.
        </div>
        <div class="four columns" style="margin-left:57px;font-size:0.9em;">
            <div class="circle"><span>2</span></div>
            <b>Select the file to add details. </b><br/>
            Fields marked <span class="mandatory">*</span> are mandatory.
        </div>
        <div class="five columns" style="margin-left:-56px;display:-webkit-inline-box;font-size:0.9em;">
            <div class="circle"><span>3</span></div>
            <b>View artworks in your dashboard.</b><br/>
            Uploaded artworks are now ready for our team to review.
        </div>
    </div>
    <div class="row">
        <hr/>
    </div>
    <div class="row">
        <div class="ten columns" style="font-size:0.9em;padding-top:12px;">
                  Please remember all artworks are moderated. Give us a little time to check them out before they can be seen live. You'll be notified soon.
        </div>
        <div class="one columns" style="text-align:right;margin-left:-10px;">
            <a id="uploadButton" class="nice large radius white button disabled" onclick="viewArtworkListing()" name="upload" style="padding:11px 25px 13px;"
               value="Done">View&nbsp;Artworks</a>
        </div>
    </div>
    <div class="row">
        <hr/>
    </div>
    <div class="row">
        <div class="six columns">
            <div id="dropbox">
                <span class="message">Drag and drop your files here. <br /></span>
            </div>
        </div>
        <input type="hidden" name="id"/>
        <input type="hidden" name="version"/>
        <input type="hidden" name="tempartworkid" id="tempartworkid"/>

        <div class="six columns">
            <div id="initialmessage" class="initialuploadmessage" style="display:none;">
                %{--Select a file to add details.--}%
            </div>
            <div id="metadatainput">
                <div id="metaSaved" style="margin-bottom: 20px;display:none;">
                   <img class="float-left" src="${resource(dir: 'images', file: 'check.png')}" alt="" style="margin:-5px 5px 0 0"/>
                   <span style="font-size:15px;">Successfully saved! Got more files? Continue to add details to them once the progress bar has completed.</span>
                </div>
            <div class="float-left">
                <label>Title <span class="mandatory">*</span></label>
                <input name="title" class='input-text' type='text' value='${tests}' style="width:456px;">
                <small class="error" style="margin-top:0px !important;padding:0px !important;"></small>
            </div>
                <br/>
            <g:if test="${"YoungArtist".equals(gallery?.galleryOwner)}">
                <div class="float-left">
                    <label>Artist <span class="mandatory">*</span></label>
                    <select style="display:none;" name="artist">
                        <g:if test="${gallery?.youngArtists.size()!=1}">
                            <g:if test="${!artwork?.artist}"><option value="">Select One</option></g:if>
                            <g:each in="${gallery?.youngArtists}" var="youngArtist">
                                <option value="${youngArtist?.nickname}" ${artwork?.artist==youngArtist?.nickname?"SELECTED":""}>${youngArtist?.nickname}</option>
                            </g:each>
                        </g:if>
                        <g:else>
                            <g:if test="${!artwork?.artist}"><option value="">Select One</option></g:if>
                            <g:each in="${gallery?.youngArtists}" var="youngArtist">
                                <option value="${youngArtist?.nickname}" SELECTED>${youngArtist?.nickname}</option>
                            </g:each>
                        </g:else>
                    </select>

                    <div class="custom dropdown">
                        <a href="#" class="current">${artwork?.artist?:'Select One'}</a>
                        <a href="#" class="selector"></a>
                        <ul>
                            <g:if test="${!artwork?.artist}"><li>Select One</li></g:if>
                            <g:each in="${gallery?.youngArtists}" var="youngArtist">
                                <li>${youngArtist?.nickname}</li>
                            </g:each>
                        </ul>
                    </div>
                    <small class="error custom-errormsg-1 custom-errormsg-3" style="margin-top:0px !important;padding:0px !important;"></small>
                </div>
            </g:if>
            <div class="${"YoungArtist".equals(gallery?.galleryOwner)?"float-right":"float-left"}">
                <label>Age Artwork was Created <span class="mandatory">*</span></label>
                <select style="display:none;" name="artworkAge">
                    <g:if test="${!artwork?.artworkAge}"><option value="">Select One</option></g:if>
                    <option value="0" ${artwork?.artworkAge==0?"SELECTED":""}>Less than 1 year old</option>
                    <g:each in="${1..18}" var="i">
                        <option value="${i}" ${artwork?.artworkAge==i?"SELECTED":""}>${i} year${i>1?"s":""} old</option>
                    </g:each>
                </select>
                <div class="custom dropdown">
                    <a href="#" class="current">${(artwork?.artworkAge!=null)?(artwork?.artworkAge>1?artwork?.artworkAge+" years":(artwork?.artworkAge==1?"1 year":"Less than 1 year")):"Select One"}</a>
                    <a href="#" class="selector"></a>
                    <ul>
                        <g:if test="${!artwork?.artworkAge}"><li>Select One</li></g:if>
                        <li>Less than 1 year old</li>
                        <g:each in="${1..18}" var="i">
                            <li>${i} year${i>1?"s":""} old</li>
                        </g:each>
                    </ul>
                </div>
                <small class="error custom-errormsg-1 custom-errormsg-4" style="margin-top:0px !important;padding:0px !important;"></small>
            </div>
            <div class="clear-both"></div>
            <div class="float-left">
                <label>Type <span class="mandatory">*</span></label>
                <g:select name="artworkType" from="${Artwork?.constraints?.artworkType?.inList}"
                          value="${artwork?.artworkType}" style="display:none"/>
                <div class="custom dropdown">
                    <a href="#" class="current">${artwork?.artworkType}</a>
                    <a href="#" class="selector"></a>
                    <ul>
                        <g:each in="${Artwork?.constraints?.artworkType?.inList}" var="artworkType">
                            <li>${artworkType}</li>
                        </g:each>
                    </ul>
                </div>
                <small class="error custom-errormsg-1 custom-errormsg-2" style="margin-top:0px !important;padding:0px !important;"></small>
            </div>
            <div class="float-right">
                <label>Dimensions</label>
                <input name="dimensions" type="text" class="input-text narrower" placeholder="e.g. 10&#34; x 25&#34;" value="${artwork?.dimensions}"/>
            </div>
            <div class="clear-both"></div>
            <div class="float-left">
                <label>Mess Factor:</label>
                <g:select name="messFactor" from="${Artwork?.constraints?.messFactor?.inList}"
                          value="${artwork?.messFactor}" style="display:none"/>
                <div class="custom dropdown">
                    <a href="#" class="current">${artwork?.messFactor}</a>
                    <a href="#" class="selector"></a>
                    <ul>
                        <g:each in="${Artwork?.constraints?.messFactor?.inList}" var="messFactor">
                            <li>${messFactor}</li>
                        </g:each>
                    </ul>
                </div>
                <small class="error custom-errormsg-1 custom-errormsg-3" style="margin-top:0px !important;padding:0px !important;"></small>
            </div>
            <div class="clear-both"></div>
            <div class="float-left">
                <label>Description/Artist Statement</label>
                <textarea  style="width:456px;" class="wider" name="description" placeholder="Share something about this artwork or offer step-by-step instructions on how to make it and the materials used.">${artwork?.description}</textarea>
                <small class="error custom-errormsg-1 custom-errormsg-4" style="margin-top:0px !important;padding:0px !important;"></small>
            </div>
            <div class="clear-both"></div>
            <div class="float-left" style="width:464px;">
                <label>Enter descriptive tags separated by commas to sort artworks by theme</label>
                <ul name="tags" style="width:445px;">
                    <g:each in="${artwork?.tags}">
                        <li>${it}</li>
                    </g:each>
                </ul>

            </div>
            <div class="clear-both"></div>
                <div class="row buttons">
                    <div class="six columns">
                        <a id="saveButton" class="nice medium radius white button" onclick="javascript:saveMetadata();return false;" name="save"
                           value="Save">Save</a>
                        <a id="deleteButton" class="nice medium radius white button" onclick="javascript:deleteArtwork();return false;" name="delete"
                           value="Delete">Delete</a>
                    </div>

                    <div class="seven columns">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>

</div>


<!-- main content area -->


<div id="forApproval" class="reveal-modal modal-view" style="position:fixed;">
    <p style="margin:0 0 15px">Your artworks have been uploaded! Our art moderators will be checking them out in the next little while. We’ll send
    you an email once they can be seen live. In the meantime, you can edit your artwork titles, add descriptions and
    more details.
    </p>
    <form action="#" class="custom nice">
        <label for="checkbox1">
            <g:checkBox name="uploadReminder"/>
            Got it, don’t show this everytime I upload new artworks
        </label>
    </form>
    <div class="btn_grp" style="margin:15px 0 0 -10px;">
        <a class="nice small radius black button" href="${createLink(controller:'artwork',action:'index')}" style="width:90px;">Okay</a>
        <a class="close-reveal-modal" href="${createLink(controller:'artwork',action:'index')}">×</a>
    </div>
</div>

<div id="modalConfirm" class="reveal-modal modal-view" style="position:fixed">
    <p style="margin:0">
        Are you sure you want to delete this artwork?
    </p>
    <div class="btn_grp" style="margin:15px 0 0 -10px;text-align: center">
        <a id="no" class="ignoredirty cancel nice small radius red button close-reveal" onclick="deleteArtwork();"
           style="width:130px;">Yes, delete it</a>
        <a id="yes" class="ignoredirty continue nice small radius black button close-reveal"
           style="width:110px;">No, keep it</a>
        <a class="close-reveal-modal close-reveal close">×</a>
    </div>
</div>


</body>
