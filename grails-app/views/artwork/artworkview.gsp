<%@ page import="com.galleryofgood.common.Comment; org.apache.commons.lang.StringEscapeUtils; com.galleryofgood.gallery.Gallery" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='artwork.view' args="${[artworkInstance?.title]}"/></title>
    <r:require modules="application,foundation,jquery,tagcloud,lazyload,colorbox"/>
    <r:script>
        $.fn.tagcloud.defaults = {
            size:{start:14, end:18, unit:'pt'},
            color:{start:'#000', end:'#000'}
        };

        function startSlideshow(){
        $("#slideshow").find("a").first().click();
        }

        function windowHeight() {
            var myWidth = 0, myHeight = 0;
            if( typeof( window.innerWidth ) == 'number' ) {
                myHeight = window.innerHeight;
            } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
                myHeight = document.documentElement.clientHeight;
            } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
                myHeight = document.body.clientHeight;
            }
        window.browserHeight = myHeight;
        }

        $(document).ready(function(){
        windowHeight();
        $('#slideshow a').colorbox({rel:'colorbox', fixed: true,
        current : 'Artworks  {current} / {total}', close: '&times;', slideshow: true, slideshowAuto: false, slideshowSpeed: 7000, slideshowStart: 'Start', slideshowStop: "Stop Slideshow", maxHeight: browserHeight
        });
        });
        $(function(){
            $('#tagcloud a').tagcloud();
            $('img.lazy').lazyload({effect:"fadeIn"});

//            $('#featured').orbit({
//                 animation: 'fade',                  // fade, horizontal-slide, vertical-slide, horizontal-push
//                 animationSpeed: 800,                // how fast animtions are
//                 timer: true, 			 // true or false to have the timer
//                 advanceSpeed: 4000, 		 // if timer is enabled, time between transitions
//                 pauseOnHover: false, 		 // if you hover pauses the slider
//                 startClockOnMouseOut: false, 	 // if clock should start on MouseOut
//                 startClockOnMouseOutAfter: 1000, 	 // how long after MouseOut should the timer start again
//                 directionalNav: true, 		 // manual advancing directional navs
//                 captions: true, 			 // do you want captions?
//                 captionAnimation: 'fade', 		 // fade, slideOpen, none
//                 captionAnimationSpeed: 800, 	 // if so how quickly should they animate in
//                 bullets: false,			 // true or false to activate the bullet navigation
//                 bulletThumbs: false,		 // thumbnails for the bullets
//                 bulletThumbLocation: '',		 // location from this file where thumbs will be
//                 afterSlideChange: function(){} 	 // empty function
//    	    });
//    	    $("#featured").css('width','940px;').css('height','450px;').css('background',"background: #000 url('/images/orbit/loading.gif') no-repeat center center; overflow: hidden; }");
//    	    $("#featured img").hide();
//    	    $("#featured div").hide();
        });

        function flagItem(){
        $(".close-reveal-modal").click();
            jQuery.ajax({type:'POST',
                        data:$("#flagForm").serialize(),
                        url:'${createLink(controller: 'flag', action: 'flagArtwork')}',
                        success:function(data, textStatus) {
                            $("#flagcomment").val("");
                            $('#myModal').find("div#content").html(data);
                            $('#myModal').reveal();
                            window.setTimeout(
                                function() {
                                    $(".close-reveal-modal").click();
                                },
                                20000
                            );
                        //flashWarning(data);
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown) {
                        flashWarning(data);
                        }
                    });
        }

        <g:if test="${params.myModal == 'true'}">
            $('#myModal').reveal();
        </g:if>
    </r:script>

</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=307880795966709";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<g:render template="/layouts/flashMessage"/>
<div class="row">
    <div class="twelve columns">
        <div id="warning" class="alert-box [success warning error]" style="display:none;">
            <span></span>
            <a href="" class="close">&times;</a>
        </div>
    </div>
</div>
<!-- main content area -->
<div class="row">
    <div class="eight columns mg-adp-left">
        <h2 class="heading color-black">
            <a href="${createLink(controller:artworkInstance?.gallery?.galleryNameUrl)}">${artworkInstance?.gallery?.galleryName}</a>
        </h2>
        <a title="${artworkInstance?.title}" href="#" class="colorbox" onclick="return startSlideshow()">
           <img class="lazy" src="" data-original="${artworkInstance?.artworkDetailImageUrl}" alt="${artworkInstance?.title}" data-thumb="slide01_thumb.jpg" data-caption="#htmlCaption${i}"/>
        </a>
        <br/><br/>

        <h3><a title="${artworkInstance.title}" href="images/fancybox-images/9_b.jpg"
               data-reveal-id="myModal1">${artworkInstance.title}</a></h3>

        <p><ripple:processText>${artworkInstance?.description}</ripple:processText></p>
    </div>

    <div class="four columns mg-adp">
        <ul class="mg-adp-navigation">
            <li class="prev">
                <a href="${createLink(controller: 'artwork', action: 'viewPrevious',params:['artwork.id':artworkInstance?.id])}" class="prev-btn"></a>
            </li>
            <li>
                <a href="" onclick="javascript:startSlideshow();return false;" class="play-btn"
                   title="View Slideshow"></a>
                <div id="slideshow">
                    <g:each in="${artworkInstance?.gallery?.artworks.findAll{it.status == 'Approved'}}" var="artwork" status="i">
                        <g:if test="${artwork?.id==artworkInstance?.id && i!=0}">
                              <script type="text/javascript">
                                  $('#slideshow a:first').before('<a rel="colorbox" href="${artwork?.url}" title="<a class=\'slideshow-title\' href=${createLink(controller: 'artwork',action: 'artworkview', id:artwork?.id)}>${artwork?.title}</a> <br/><span class=\'slideshow-artist\'>by ${artwork?.artist}<span>"></a>')
                              </script>
                        </g:if>
                        <g:else>
                        <a rel="colorbox" href="${artwork?.url}" title="<a class='slideshow-title' href=${createLink(controller: 'artwork',action: 'artworkview', id:artwork?.id)}>${artwork?.title}</a> <br/><span class='slideshow-artist'>by ${artwork?.artist}<span>"></a>
                        </g:else>
                    %{--<span class="orbit-caption" id="htmlCaption${i}">--}%
                    %{--<h5>${artwork?.title}</h5>--}%

                    %{--<p>By: ${artwork?.artist}</p>--}%

                    %{--<p>Comments: <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id,fragment:'commentsSection' )}" onclick="javascript:$('.close-reveal-modal').click();">${com.galleryofgood.common.Comment.countByIdAndLocation(artwork?.id,"Artwork")}</a></p>--}%
                    %{--</span>--}%
                    </g:each>
                </div>
                %{--<a data-reveal-id="myModal1" href="images/fancybox-images/9_b.jpg" class="play-btn"--}%
                   %{--title="Lorem ipsum dolor sit amet"></a>--}%
            </li>
            <li class="next">
                <a href="${createLink(controller: 'artwork', action: 'viewNext',params:['artwork.id':artworkInstance?.id])}" class="next-btn"></a>
            </li>
        </ul>
        <br/>

        <div class="mg-adp-right">
            <ul>
                Artist: ${artworkInstance.artist}<br/>
                Age Group: ${artworkInstance?.getArtistAgeGroup()}<br/>
                <hr/>
                Age Artwork was Created: ${artworkInstance?.artworkAge}<br/>
                Type: ${artworkInstance?.artworkType}<br/>
                <g:if test="${artworkInstance?.dimensions != null}" >Dimensions: ${artworkInstance?.dimensions}<br/></g:if>
                Mess Factor: ${artworkInstance?.messFactor}<br/>
                Copyright © All rights reserved
                <hr/>
                <a href="#commentsSection">Comments: ${Comment.countByLocationAndLocationIdAndStatus( "Artwork", artworkInstance?.id, 'Approved')}</a><br/>
                <sec:ifLoggedIn>
                <g:if test="${artworkInstance?.gallery?.owner!=activeArtist}">
                    <ripple:followLink id="${artworkInstance.gallery.id}"/><ripple:followableScript controller="Gallery"/><br/>
                    <a href="javascript:" data-reveal-id="myModal"
                       data-animation="fadeAndPop" data-animationspeed="300"
                       data-closeonbackgroundclick="true" data-dismissmodalclass="close-reveal-modal" >Flag Artwork</a>
                </g:if>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <a href="#" data-reveal-id="modalNotify1"
                           data-animation="fadeAndPop" data-animationspeed="300"
                           data-closeonbackgroundclick="true" data-dismissmodalclass="close-reveal-modal"
                           class="modal-show">Follow Gallery</a><br/>
                    <a href="#" data-reveal-id="modalNotify"
                       data-animation="fadeAndPop" data-animationspeed="300"
                       data-closeonbackgroundclick="true" data-dismissmodalclass="close-reveal-modal" class="modal-show">Flag Artwork</a>
                </sec:ifNotLoggedIn>

                %{--<ul class="mgpm-social">--}%
                <div class="social-container">
                <ul class="shareLinks" style="overflow:visible;">
                    <li style="width:46px;"><a href="http://pinterest.com/pin/create/button/?url=${URLEncoder.encode(createLink(controller:'artwork',action:'artworkview',id:artworkInstance?.id,absolute:true).toString())}&media=${URLEncoder.encode(artworkInstance?.url)}&description=${URLEncoder.encode(artworkInstance?.title)}"
                           class="pin-it-button"
                           count-layout="horizontal">
                        <img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a></li>
                    <li style="width:59px;"><a href="https://twitter.com/share" class="twitter-share-button" data-url="${bitly.shorten(url:createLink(controller: 'artwork', action: 'artworkview', absolute: true, params: [id: artworkInstance.id]).toString())}" data-hashtags="GalleryOfGood" data-count="none">Tweet
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></a>
                    </li>
                    <li style="width:111px;"><fb:like href="${createLink(controller: 'artwork', action: 'artworkview', absolute: true, params: [id: artworkInstance.id])}" send="true" layout="button_count" width="110" show_faces="false"></fb:like></li>


                </ul>
                <style>
                    ul.shareLinks {overflow:auto;}
                    ul.shareLinks li {list-style:none; float:left; margin-right:5px;}
                </style>
                </div>
                %{--</ul>--}%
                <br/><br/><br/>
            </ul>
        </div> <!-- end mg-adp-right -->
    </div>
</div>
%{--<div class="clear-both" style="height: 3em"></div>--}%
<div class="row agg">
    <g:render template="/common/comments" model="[modelInstance: artworkInstance]"/>
    <div class="six columns mg-adp-tags">
        <h5>Tags</h5>

        <ul class="tag_cloud">
            <g:each in="${tags}" var="tag">
                <li class="tag_size_${tag[1]}">
                    <a href="${createLink(controller: 'search', action: 'themeTag', params: [theme: tag[0]])}">${tag[0]}</a>
                </li>
            </g:each>
        </ul>
    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <p><br/><br/><br/><br/><br/><br/></p>
    </div>
</div>

<!-- main content area -->
<!-- Modal Container slider -->
<div id="myModal1" class="reveal-modal modal-slideshow">
    %{--<g:each in="${artworkInstance?.gallery?.artworks}" var="artwork" status="i">--}%
        %{--<span class="orbit-caption" id="htmlCaption${i}">--}%
            %{--<h5>${artwork?.title}</h5>--}%

            %{--<p>By: ${artwork?.artist}</p>--}%

            %{--<p>Comments: <a href="${createLink(controller:'artwork',action:'artworkview',id:artwork?.id,fragment:'commentsSection' )}" onclick="javascript:$('.close-reveal-modal').click();">${com.galleryofgood.common.Comment.countByIdAndLocation(artwork?.id,"Artwork")}</a></p>--}%
        %{--</span>--}%
    %{--</g:each>--}%
    <img src="${artworkInstance?.artworkDetailImageUrl}"/>
    <a class="close-reveal-modal">&#215;</a>
</div> <!-- end myModal1 -->
<!-- Modal Flag -->
<sec:ifLoggedIn>
<form class="custom nice flag-form" action="#" method="post" enctype="multipart/form-data" id="flagForm"">
    <g:hiddenField name="id" value="${artworkInstance?.id}"/>
    <div id="myModal" class="reveal-modal">
        <div id="content">
            <p>From: <sec:loggedInUserInfo field="username"/><br/>
                Artwork: ${artworkInstance?.title}

            <p>

            <p>Tell us why your are reporting this:</p>
            <textarea id="flagcomment" name="comment" class="wider"></textarea>
            <a href="#" onclick="javascript:flagItem();" class="nice small radius white button" name="submit"
               value="Submit">Submit</a>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</form>
</sec:ifLoggedIn>
<sec:ifNotLoggedIn>
    <g:render template="/layouts/modalNotify" model="['content':'Please sign-in or sign-up to flag this artwork.', id:artworkInstance.id, timeout:10000]"/>
    <g:render template="/layouts/modalNotify"
              model="['content': 'Please sign-in or sign-up to follow this gallery.', 'galleryName': artworkInstance.gallery.galleryName, 'artworkId': artworkInstance.id, timeout: 10000, 'elementId':'modalNotify1']"/>
</sec:ifNotLoggedIn>
<!-- Included JS Files -->
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
</body>