<%@ page import="com.galleryofgood.gallery.Gallery" %>
<g:set var="gallery" value="${Gallery.findByOwner(appUserInstance?.username)}"/>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.users'/></title>
    <r:require modules="foundation, jqgrid, jquery-ui, jqueryDatatable,lightbox2"/>
    <r:script>
    var artworkTable
        $(function(){
            artworkTable=$('#artwork-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                "bAutoWidth":true,
                sAjaxSource: '${createLink(controller: 'admin', action: 'galleryArtworkList', id: gallery?.id)}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumnDefs":[
                    {"sName":"id", "sClass": "indexLeft","aTargets": [ 0 ], "bSortable":false,"sWidth":"20px"},
                    { "sName": "dateCreated", "aTargets": [ 1 ], "bSortable":true,"sType":"date","sWidth":"70px"},
                    { "sName": "artworkImage", "aTargets": [ 2 ], "bSortable":false,"bSearchable":false,"sWidth":"60px"},
                    { "sName": "title", "aTargets": [ 3 ],bSortable:true},
                    { "sName": "comments", "aTargets": [ 4 ],bSortable:false,bSearchable:false,"sWidth":"40px","sClass":"alignCenter"},
                    { "sName": "flagged", "aTargets": [ 5 ],bSortable:false,bSearchable:false,"sWidth":"40px","sClass":"alignCenter"}

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                }
                ,"fnDrawCallback":function(){
                    if ($('#artwork-list_wrapper .pagination ul li').size()>3) {
                        $('#artwork-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#artwork-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                }
            } );

            $(".dataTables_length").hide()
            $(".dataTables_info").hide()
            $(".dataTables_filter").find("input").addClass('input-text')
            $(".dataTables_filter").find("input").css('display', 'inline')
            $(".dataTables_filter").find("input").css('width', '150 px')


        })
        function toggleAll(aCheckbox){
            var checkboxes=$(aCheckbox).closest("li").find(".checkbox");
            var checkboxes2=$(aCheckbox).closest("li").find("input[type='checkbox']");
            if ('checked'==$(aCheckbox).attr("checked")){
                checkboxes.addClass("checked");
                checkboxes2.attr("checked","checked");
            }
            else{
                checkboxes.removeClass("checked");
                checkboxes2.removeAttr("checked");
            }
        }
    </r:script>
</head>

<body>
<style media="all" type="text/css">
.alignCenter { text-align: center; }
</style>
<div class="ten columns ap-right">
    <p class="ap-page">Users Details</p>
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
    <hr/>

    <div class="row-fluid">
        <div class="four columns">
            Username: ${appUserInstance?.username}<br/>
            Email: ${appUserInstance?.email}<br/>
            < 13: ${"below13".equals(appUserInstance?.ageGroup) ? "yes" : "no"} <br/>
            Artists: ${Gallery?.findByOwner(appUserInstance?.username)?.youngArtists?.size()}<br/>
            Enabled: ${appUserInstance?.enabled ? "Yes" : "No"}

        </div>

        <div class="four columns">
            Account: Standard<br/>
            Registration Date: ${appUserInstance?.dateCreated}<br/>
            Notifications: ${appUserInstance?.userDetails?.allowSiteNotifications}<br/>
            Last Activity: ${appUserInstance?.lastLoginDate} <br/>
        </div>

        <div class="four columns" style="text-align:right;">
            <a class="btn nice small radius white button"
               href="${createLink(controller: 'admin', action: 'forgotPassword', id: appUserInstance?.id)}">Send Reset Password</a><br/> <br/>
            <a class="btn nice small radius white button"
               href="mailto:${appUserInstance?.email}">Email User</a><br/><br/>
            <g:if test="${appUserInstance?.enabled}">
                <a class="btn nice small radius white button"
                   href="${createLink(controller: 'admin', action: 'deactivateUser', id: appUserInstance?.id)}">Deactivate Account</a><br/><br/>
            </g:if>
            <g:else>
                <a class="btn nice small radius white button"
                   href="${createLink(controller: 'admin', action: 'activateUser', id: appUserInstance?.id)}">Activate Account</a><br/><br/>
            </g:else>
        </div>
    </div>
    <hr/>
    <br/>

    <div class="row-fluid">
        <div class="four columns">
            Gallery:<g:if test="${gallery}">
            <a href="${createLink(controller: gallery?.galleryNameUrl)}">${gallery?.toString()}</a>
        </g:if>
            <g:else>
                none
            </g:else>
        </div>
        <g:if test="${gallery}">
            <div class="four columns offset-by-four" style="text-align:right;">
                <a class="btn nice small radius white button">Deactivate Gallery</a>
            </div>
        </g:if>
    </div>
    <g:if test="${gallery}">
        <div class="row-fluid">
            <div class="two columns">Total Artworks:${gallery?.artworks?.size()}</div>

            <div class="two columns">Followers: ${gallery?.getFollowerCount()}</div>

            <div class="two columns">Comments: ${gallery?.getComments()?.size()}</div>

            <div class="two columns">Flags: ${gallery?.flags?.size()}</div>
            <div class="four columns"></div>
        </div>
        <br/>

        <g:form class="custom nice">
            <g:if test="${gallery?.galleryOwner?.equals("YoungArtist")}">
                <div class="row-fluid">
                    <div class="twelve columns">
                        <br/>
                    <g:select from="${gallery?.youngArtists}" name="youngArtist" optionKey="nickname" optionValue="nickname"
                    noSelection="['':'-Please select a young artist-']"
                        onchange="populateSearch(this.options[selectedIndex].text);"
                    />
                        <r:script>
                        function populateSearch(artistName){
                            artworkTable.fnFilter(artistName);
                        }
                        </r:script>
                    </div>
                </div>
            </g:if>
        <div class="row-fluid">
            <div width="100%">

                <table id="artwork-list" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                        <th>Date</th>
                        <th>Artwork</th>
                        <th>Title</th>
                        <th>Comments</th>
                        <th>Flagged</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
        </g:form>

    </g:if>

</div>
</body>
