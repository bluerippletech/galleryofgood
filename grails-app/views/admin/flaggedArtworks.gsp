<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.flagged.artwork'/></title>
    <r:require modules="foundation"/>
</head>

<body>
<div class="ten columns ap-right">
    <p class="ap-page">Flagged Artworks</p>
    <table>
        <thead>
        <tr>
            <th><label for="checkbox1">
                <input type="checkbox" id="checkbox1" style="display: none;">
                <span class="custom checkbox"></span>
            </label></th>
            <th>Image</th>
            <th>Title</th>
            <th>Type</th>
            <th>Mess Factor</th>
            <th>Size</th>
            <th>Statement</th>
            <th>Themes</th>
            <th>Age Created</th>
            <th>Artist Timeline</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${flaggedArtworksList}" var="artwork">
            <tr>
                <td><label for="checkbox1">
                    <input type="checkbox" id="checkbox1" style="display: none;" name="artwork_id" value="${artwork.id}">
                    <span class="custom checkbox"></span>
                </label></td>
                <td style="padding:0;margin:0"><img src="${artwork.url}" alt="" width="56px;"/></td>
                <td><a href="${createLink(controller:'artwork',action:'editArtwork',id:artwork.id)}">${artwork.title}</a></td>
                <td>${artwork.artworkType}</td>
                <td>${artwork.messFactor}</td>
                <td>${artwork.dimensions}</td>
                <td>${artwork.description?.size()>100?"${artwork.description[0..100]}...":"${artwork.description?:""}"}</td>
                <g:set var="tags" value="${(artwork?.tags?.join(", "))}"/>
                <td>${tags?.size()>100?"${tags[0..100]}...":"${tags}"}</td>
                <td>${artwork.artworkAge}</td>
                <td>
                    <g:if test="${artwork.showInTimeline=='on'}">
                        <r:img uri="/images/checkmark.gif" alt="" />
                    </g:if>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>

</div>

</body>
