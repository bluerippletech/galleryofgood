<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.homepage'/></title>
    <r:require modules="foundation,jgrowl"/>
    <r:script>
        function deleteSliderImage(imageNumber){
            if (confirm('Are you sure you want to delete this image?')){
                 jQuery.ajax({type:'GET',
						url:'${createLink(controller: 'homepage', action: 'deleteSliderImage')}?imageNumber='+imageNumber,
		                success:function(data, textStatus) {
		                    $.jGrowl(data);
		                    $("#sliderImage"+imageNumber+" .aprs-img").fadeOut(300, function() { $(this).remove(); });
		                },
		                error:function(XMLHttpRequest, textStatus, errorThrown) {
		                	$.jGrowl(XMLHttpRequest.responseText,{header:'Errors',life:10000,theme:'growl-error'});
		                }
		            });
            }
            else{return false;}
        }

        function deleteThumbnailImage(imageNumber){
            if (confirm('Are you sure you want to delete this image?')){
                 jQuery.ajax({type:'GET',
						url:'${createLink(controller: 'homepage', action: 'deleteThumbnailImage')}?imageNumber='+imageNumber,
		                success:function(data, textStatus) {
		                    $.jGrowl(data);
		                    $("#sliderImage"+imageNumber+" .aprs-img2").fadeOut(300, function() { $(this).remove(); });
		                },
		                error:function(XMLHttpRequest, textStatus, errorThrown) {
		                	$.jGrowl(XMLHttpRequest.responseText,{header:'Errors',life:10000,theme:'growl-error'});
		                }
		            });
            }
            else{return false;}
        }

         function deleteAcclaimImage(imageNumber){
            if (confirm('Are you sure you want to delete this image?')){
                 jQuery.ajax({type:'GET',
						url:'${createLink(controller: 'homepage', action: 'deleteAcclaimImage')}?imageNumber='+imageNumber,
		                success:function(data, textStatus) {
		                    $.jGrowl(data);
		                    $("#acclaimImage"+imageNumber).fadeOut(300, function() { $(this).remove(); });
		                },
		                error:function(XMLHttpRequest, textStatus, errorThrown) {
		                	$.jGrowl(XMLHttpRequest.responseText,{header:'Errors',life:10000,theme:'growl-error'});
		                }
		            });
            }
            else{return false;}
        }

        <g:if test="${params?.selectedTab}">
            $(function(){
                $("#${params?.selectedTab}").find("a").click()
            })
        </g:if>

    </r:script>
</head>

<body>
<g:form name="homepage" controller="admin" action="updateHomepage" method="POST" enctype="multipart/form-data">
<g:hiddenField name="id" value="${homepageInstance?.id}"/>
<g:hiddenField name="selectedTab" id="selectedTab" value="simple1"/>
<div class="ten columns ap-right">
<g:render template="/layouts/flashMessage"/>
<g:render template="/layouts/flashErrors"/>
<p class="ap-page">Homepage</p>

<div class="row">
    <div class="nine columns">
        &nbsp;
        <a href="${createLink(controller:'homepage',action:'index')}">View homepage</a>
    </div>
    <div class="one column">
        <input type="submit" class="small round button blue" value="Save">
    </div>
</div>

<dl class="tabs">
    <dd id="simple1"><a class="active" href="#simple1" onclick="$('#selectedTab').val('simple1')">Slider</a></dd>
    <dd id="simple2"><a href="#simple2" onclick="$('#selectedTab').val('simple2')">Feature Boxes</a></dd>
    <dd id="simple3"><a href="#simple3" onclick="$('#selectedTab').val('simple3')">Fundraisers</a></dd>
    <dd id="simple4"><a href="#simple4" onclick="$('#selectedTab').val('simple4')">Acclaim</a></dd>
</dl>

<ul class="tabs-content">
<li id="simple1Tab" class="active" style="display: block;">
    <g:render template="/admin/slideImageUpload" model="${['imageNumber': 1, 'homepageInstance': homepageInstance]}"/>
    <g:render template="/admin/slideImageUpload" model="${['imageNumber': 2, 'homepageInstance': homepageInstance]}"/>
    <g:render template="/admin/slideImageUpload" model="${['imageNumber': 3, 'homepageInstance': homepageInstance]}"/>
    <g:render template="/admin/slideImageUpload" model="${['imageNumber': 4, 'homepageInstance': homepageInstance]}"/>
    <g:render template="/admin/slideImageUpload" model="${['imageNumber': 5, 'homepageInstance': homepageInstance]}"/>
</li>
<li id="simple2Tab" style="display: none;">
    <div class="apr-container row">
        <div class="aprc-inner">
            <p>Section Header 1:</p>
            <g:textField name="sectionHeader1" value="${homepageInstance?.sectionHeader1}"/>
            <g:hasErrors bean="${homepageInstance}" field="sectionHeader1">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="sectionHeader1"/></small>
            </g:hasErrors>
            <br/><br/>

            <p>Box 1:</p>
            <label>Headline:</label>
            <g:textField name="boxheadline1" value="${homepageInstance?.boxheadline1}"/>
            <g:hasErrors bean="${homepageInstance}" field="boxheadline1">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="boxheadline1"/></small>
            </g:hasErrors>
            <br/><br/>
            <label>Body Copy:</label>
            <g:textArea name="boxcopy1">${homepageInstance?.boxcopy1}</g:textArea>
            <g:hasErrors bean="${homepageInstance}" field="boxcopy1">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="sectionHeader1"/></small>
            </g:hasErrors>
            <br/>
            <label>CTA Button Text:</label>
            <g:textField name="buttontext1" value="${homepageInstance?.buttontext1}"/>
            <g:hasErrors bean="${homepageInstance}" field="buttontext1">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="buttontext1"/></small>
            </g:hasErrors>
            <br/><br/>
            <label>CTA Button Link:</label>
            <g:textField name="buttonlink1" value="${homepageInstance?.buttonlink1}"/>
            <g:hasErrors bean="${homepageInstance}" field="buttonlink1">
                <small class="error" style="margin:1px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="buttonlink1"/></small>
            </g:hasErrors>
        </div>

        <div class="aprc-inner">
            <div class="aprc-inner">
                <p>Section Header 2:</p>
                <g:textField name="sectionHeader2" value="${homepageInstance?.sectionHeader2}"/>
                <g:hasErrors bean="${homepageInstance}" field="sectionHeader2">
                    <small class="error" style="margin:2px;"><g:renderErrors bean="${homepageInstance}"
                                                                             field="sectionHeader2"/></small>
                </g:hasErrors>
                <br/><br/>

                <p>Box 2:</p>
                <label>Headline:</label>
                <g:textField name="boxheadline2" value="${homepageInstance?.boxheadline2}"/>
                <g:hasErrors bean="${homepageInstance}" field="boxheadline2">
                    <small class="error" style="margin:2px;"><g:renderErrors bean="${homepageInstance}"
                                                                             field="boxheadline2"/></small>
                </g:hasErrors>
                <br/><br/>
                <label>Body Copy:</label>
                <g:textArea name="boxcopy2">${homepageInstance?.boxcopy2}</g:textArea>
                <g:hasErrors bean="${homepageInstance}" field="boxcopy2boxcopy2">
                    <small class="error" style="margin:2px;"><g:renderErrors bean="${homepageInstance}"
                                                                             field="sectionHeader2"/></small>
                </g:hasErrors>
                <br/>
                <label>CTA Button Text:</label>
                <g:textField name="buttontext2" value="${homepageInstance?.buttontext2}"/>
                <g:hasErrors bean="${homepageInstance}" field="buttontext2">
                    <small class="error" style="margin:2px;"><g:renderErrors bean="${homepageInstance}"
                                                                             field="buttontext2"/></small>
                </g:hasErrors>
                <br/><br/>
                <label>CTA Button Link:</label>
                <g:textField name="buttonlink2" value="${homepageInstance?.buttonlink2}"/>
                <g:hasErrors bean="${homepageInstance}" field="buttonlink2">
                    <small class="error" style="margin:2px;"><g:renderErrors bean="${homepageInstance}"
                                                                             field="buttonlink2"/></small>
                </g:hasErrors>
            </div>
        </div>
    </div>

    <div class="apr-container row">
        <div class="aprc-inner">
            <p>Section Header 3:</p>
            <g:textField name="sectionHeader3" value="${homepageInstance?.sectionHeader3}"/>
            <g:hasErrors bean="${homepageInstance}" field="sectionHeader3">
                <small class="error" style="margin:3px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="sectionHeader3"/></small>
            </g:hasErrors>
            <br/><br/>

            <p>Box 3:</p>
            <label>Headline:</label>
            <g:textField name="boxheadline3" value="${homepageInstance?.boxheadline3}"/>
            <g:hasErrors bean="${homepageInstance}" field="boxheadline3">
                <small class="error" style="margin:3px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="boxheadline3"/></small>
            </g:hasErrors>
            <br/><br/>
            <label>Body Copy:</label>
            <g:textArea name="boxcopy3">${homepageInstance?.boxcopy3}</g:textArea>
            <g:hasErrors bean="${homepageInstance}" field="boxcopy3boxcopy3">
                <small class="error" style="margin:3px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="sectionHeader3"/></small>
            </g:hasErrors>
            <br/>
            <label>CTA Button Text:</label>
            <g:textField name="buttontext3" value="${homepageInstance?.buttontext3}"/>
            <g:hasErrors bean="${homepageInstance}" field="buttontext3">
                <small class="error" style="margin:3px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="buttontext3"/></small>
            </g:hasErrors>
            <br/><br/>
            <label>CTA Button Link:</label>
            <g:textField name="buttonlink3" value="${homepageInstance?.buttonlink3}"/>
            <g:hasErrors bean="${homepageInstance}" field="buttonlink3">
                <small class="error" style="margin:3px;"><g:renderErrors bean="${homepageInstance}"
                                                                         field="buttonlink3"/></small>
            </g:hasErrors>

        </div>
    </div>
</li>
<li id="simple3Tab" style="display: none;">
    This is simple tab 3's content. It's, you know...okay.
</li>
<li id="simple4Tab" style="display: none;">
    <div class="apr-container row">
        <div class="aprc-inner">
            <p>Section Header</p>
            <g:textField name="acclaimHeader" value="${homepageInstance?.acclaimHeader}" placeholder="the critics agree..."/>
            <g:render template="acclaimSection" model="${[homepageInstance:homepageInstance,number:1]}"/>
            <g:render template="acclaimSection" model="${[homepageInstance:homepageInstance,number:2]}"/>
            <g:render template="acclaimSection" model="${[homepageInstance:homepageInstance,number:3]}"/>
        </div>

    </div>
</li>
</ul>

<div class="row">
    <div class="nine columns">
        &nbsp;
    </div>

    <div class="one column">
        <input type="submit" class="small round button blue" value="Save">
    </div>
</div>
</div>
</g:form>
</body>
