<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.new.artwork'/></title>
    <r:require modules="foundation,jqueryDatatable,lightbox2"/>
    <r:script>
        function toggleAll(aCheckbox){
            var checkboxes=$(aCheckbox).closest("table").find(".checkbox");
            var checkboxes2=$(aCheckbox).closest("table").find("input[type='checkbox']");
            if ('checked'==$(aCheckbox).attr("checked")){
                checkboxes.addClass("checked");
                checkboxes2.attr("checked","checked");
            }
            else{
                checkboxes.removeClass("checked");
                checkboxes2.removeAttr("checked");

            }
        }

        $(function(){
            var oTable=$('#new-artwork-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                "bAutoWidth":true,
                sAjaxSource: '${createLink(controller: 'admin', action: 'newArtworksJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumnDefs":[
                    {"sName":"id", "sClass": "indexLeft","aTargets": [ 0 ], "bSortable":false},
                    { "sName": "dateCreated", "aTargets": [ 1 ], "bSortable":true,"sType":"date"},
                    { "sName": "g.owner", "aTargets": [ 2 ],bSortable:true},
                    { "sName": "g.galleryName", "aTargets": [ 3 ],bSortable:true },
                    { "sName": "artist", "aTargets": [ 4 ] },
                    { "sName": "title", "aTargets": [ 5 ]},
                    { "sName": "status","sClass": "indexRight", "aTargets": [ 6 ]}

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#new-artwork-list_wrapper .pagination ul li').size()>3) {
                        $('#new-artwork-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#new-artwork-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } );

            var flaggedArtworkTable=$('#flagged-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                "bAutoWidth":true,
                sAjaxSource: '${createLink(controller: 'admin', action: 'flaggedArtworksJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumnDefs":[
                    {"sName":"id", "sClass": "indexLeft","aTargets": [ 0 ], "bSortable":false},
                    { "sName": "dateCreated", "aTargets": [ 1 ], "bSortable":true,"sType":"date"},
                    { "sName": "flaggedUser", "aTargets": [ 2 ],bSortable:true},
                    { "sName": "flaggingUser", "aTargets": [ 3 ],bSortable:true },
                    { "sName": "comment", "aTargets": [ 4 ] },
                    { "sName": "sectionFlagged", "aTargets": [ 5 ]},
                    { "sName": "status","sClass": "indexRight", "aTargets": [ 6 ]}

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#flagged-list_wrapper .pagination ul li').size()>3) {
                        $('#flagged-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#flagged-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } );

            $(".dataTables_length").hide()
            $(".dataTables_info").hide()
            $(".dataTables_filter").find("input").addClass('input-text')
            $(".dataTables_filter").find("input").css('display', 'inline')
            $(".dataTables_filter").find("input").css('width', '150 px')

            $(".bulkAction").css("width","133px");
            $(".bulkAction").find("ul").css("width","131px");




        })

    </r:script>
</head>

<body>
<g:render template="/layouts/flashMessage"/>
<g:render template="/layouts/flashErrors"/>

<div class="ten columns ap-right">
    <p class="ap-page">Users</p>

    <dl class="tabs">
        <dd class="${"all".equals(activeTab)?"active":""}"><a href="#all">All</a></dd>
        <dd class="${"standard".equals(activeTab)?"active":""}"><a href="#standard">Standard</a></dd>
        <dd class="${"newArtwork".equals(activeTab)?"active":""}"><a href="#newArtwork">New Artwork</a></dd>
        <dd class="${"flagged".equals(activeTab)?"active":""}"><a href="#flagged">Flagged</a></dd>
    </dl>

    <ul class="tabs-content">
        <li class="${"all".equals(activeTab)?"active":""}" id="allTab">All Users</li>
        <li class="${"standard".equals(activeTab)?"active":""}" id="standardTab">Standard</li>
        <li class="${"newArtwork".equals(activeTab)?"active":""}" id="newArtworkTab">
            <g:form name="newArtworks" controller="admin" action="changeArtworkStatus"
                    onsubmit="return confirm('Are you sure you want to change selected artworks?')" class="custom nice">

                <div class="row">
                    New this month: ${newArtworksThisMonth}<br/>
                    Total all-time: ${totalArtworks}
                </div>

                <select width="200px" style="display:none;width:133px;" name="bulkAction">
                    <option SELECTED>Bulk&nbsp;Actions</option>
                    <option value="Approved">Approve</option>
                    <option value="Disapproved">Disapprove</option>
                </select>

                <div class="custom dropdown bulkAction" style="width: 133px;" id="bulkAction">
                    <a href="#" class="current">Bulk&nbsp;Actions</a>
                    <a href="#" class="selector"></a>
                    <ul style="width: 131px; ">
                        <li class="selected" style="">Bulk&nbsp;Actions</li>
                        <li style="">Approve</li>
                        <li style="">Disapprove</li>
                    </ul>
                </div>
                <input class="nice small radius white button" type="submit" name="submit" value="Apply"/>

                <div class="row-fluid">
                    <table id="new-artwork-list" width="100%">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/>
                            </th>
                            <th>Date</th>
                            <th>Username</th>
                            <th>Gallery</th>
                            <th>Artist</th>
                            <th>Artwork Title and Image</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </g:form>
        </li>
        <li class="${"flagged".equals(activeTab)?"active":""}" id="flaggedTab">
            <g:form name="flaggedArtworks" controller="admin" action="updateFlags"
                    onsubmit="return confirm('Are you sure you want to change selected flags?')" class="custom nice">
                <div class="row">
                    New this month: <br/>
                    Total all-time:
                </div>
                <select width="200px" style="display:none;width:133px;" name="bulkAction">
                    <option SELECTED>Bulk&nbsp;Actions</option>
                    <option value="Open">Open</option>
                    <option value="Resolved">Resolved</option>
                    <option value="Delete">Delete</option>
                </select>

                <div class="custom dropdown bulkAction" style="width: 133px;" id="bulkAction">
                    <a href="#" class="current">Bulk&nbsp;Actions</a>
                    <a href="#" class="selector"></a>
                    <ul style="width: 131px; ">
                        <li class="selected" style="">Bulk&nbsp;Actions</li>
                        <li style="">Open</li>
                        <li style="">Resolved</li>
                        <li style="">Delete</li>
                    </ul>
                </div>
                <input class="nice small radius white button" type="submit" name="submit" value="Apply"/>

                <div class="row-fluid">
                    <table id="flagged-list" width="100%">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/>
                            </th>
                            <th>Date</th>
                            <th>User Flagged</th>
                            <th>Sent By</th>
                            <th>Message</th>
                            <th>Section Flagged</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </g:form>
        </li>
    </ul>

</div>

</body>
