<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.admin'/></title>
    <r:require modules="foundation,jqgrid,jquery-ui,jqueryDatatable"/>
    <r:script>
        var selectedTable
        $(function(){
                selectedTable = $('#gallery-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                sAjaxSource: '${createLink(controller: 'admin', action: 'galleryListJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false }, //{ "sType": "date-euro" },
                    { "bSortable": false }
                ],
                "aoColumnDefs":[

                    { "sName": "", "aTargets": [ 0 ] },

                    { "sName": "galleryName", "aTargets": [ 1 ] },

                    { "sName": "owner", "aTargets": [ 2 ] },

                    { "sName": "", "aTargets": [ 3 ] },

                    { "sName": "", "aTargets": [ 4 ] },

                    { "sName": "", "aTargets": [ 5 ] },

                    { "sName": "", "aTargets": [ 6 ] },

                    { "sName": "", "aTargets": [ 7 ] }

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#gallery-list_wrapper .pagination ul li').size()>3) {
                        $('#gallery-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#gallery-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } );
            $(".dataTables_length").hide()
            $(".dataTables_info").hide()
            $(".dataTables_filter").find("input").addClass('input-text')
            $(".dataTables_filter").find("input").css('display', 'inline')
            $(".dataTables_filter").find("input").css('width', '150 px')

        })
        function toggleAll(aCheckbox){
            var checkboxes=$(aCheckbox).closest("form").find(".checkbox");
            var checkboxes2=$(aCheckbox).closest("form").find("input[type='checkbox']");
            if ('checked'==$(aCheckbox).attr("checked")){
                checkboxes.addClass("checked");
                checkboxes2.attr("checked","checked");
            }
            else{
                checkboxes.removeClass("checked");
                checkboxes2.removeAttr("checked");
            }
        }

            $(function(){
                selectedTable.fnFilter('Administrator', 1);
            })


    </r:script>
</head>

<body>
<div class="ten columns ap-right">
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
    <p class="ap-page">Gallery</p>
    <g:form class="custom nice" controller="admin" action="actionGallery" method="post" enctype="multipart/form-data">
        <div class="four columns">
            <select name="selectedAction" id="bulk-action-comments" style="display:none;">
                <option selected="Delete" value="Delete">Delete</option>
            </select>
            <div class="custom dropdown inside_tab" style="width: 136px;">
                <a class="current" href="#">Delete</a>
                <a class="selector" href="#"></a>
                <ul style="width: 134px;">
                    <li style="">Delete</li>
                </ul>
            </div>
            <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
        </div>
        %{--<div class="four columns offset-by-four">--}%
            %{--<g:link class="small button white nice radius" controller="admin" action="createAdmin">Add</g:link>--}%
        %{--</div>--}%
        <div class="clear-both"></div>
        <div>
            <table id="gallery-list" width="100%">
                <thead>
                <tr>
                    <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                    <th>Gallery Name</th>
                    <th>Owner</th>
                    <th>Artwork Type</th>
                    <th>Owner Type</th>
                    <th>Young Artists</th>
                    <th>Artwork</th>
                    <th>Date Created</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </g:form>
</div>
</body>
