<div class="apr-container" id="sliderImage${imageNumber}">
    <p>Feature ${imageNumber}</p>
    <label>Image:</label>
    <input type="file" name="sliderImage${imageNumber}"/>
    <g:if test="${homepageInstance?."featureImageUrl${imageNumber}"}">
        <div class="aprs-img">
            <img src="${homepageInstance?."featureImageUrl${imageNumber}"}" alt=""/>
            <a href="#" onclick="javascript:deleteSliderImage(${imageNumber});
            return false;"></a>
        </div>
    </g:if>
    <label>Image Link:</label>
    <input type="text" value="${homepageInstance?."featureImageLink${imageNumber}"}"
           name="featureImageLink${imageNumber}" placeholder="http://"/>
    <label>Thumbnail Image:</label>
    <input type="file" name="thumbnailImage${imageNumber}"/>
    <g:if test="${homepageInstance?."featureImageThumbnail${imageNumber}"}">
        <div class="aprs-img2">
            <img src="${homepageInstance?."featureImageThumbnail${imageNumber}"}" alt=""/>
            <a href="#" onclick="javascript:deleteThumbnailImage(${imageNumber});
            return false;"></a>
        </div>
    </g:if>

</div>