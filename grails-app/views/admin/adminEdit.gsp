<%@ page import="com.galleryofgood.security.Role" %>
<g:if test="${appUserInstance?.id}">
    <g:set var="status" value="Edit"/>
    <g:set var="action" value="updateAdmin"/>
</g:if>
<g:else>
    <g:set var="status" value="Add"/>
    <g:set var="action" value="save"/>
</g:else>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='parent.guide'/></title>
    <r:require modules="foundation"/>

</head>

<body>
<g:form action="${action}" name='userCreateForm' class="custom nice">
    <g:hiddenField name="id" value="${appUserInstance?.id}"/>
    <g:hiddenField name="version" value="${appUserInstance?.version}"/>
    <div class="ten columns ap-right">
        <g:render template="/layouts/flashMessage"/>
        <g:render template="/layouts/flashErrors"/>
        %{--${status}--}%
        <p class="ap-page">Permissions</p>
        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <input type="submit" class="small blue button round" value="Update">
            </div>
        </div>
        <dl class="tabs">
            <dd><a class="active" href="#simple1">${appUserInstance.username != loggedInUser?'Administrator Profile':'Your Profile'}</a></dd>
            %{--<dd><a href="#simple2">Roles</a></dd>--}%
        </dl>
        <ul class="tabs-content">
            <li id="simple1Tab" class="active" style="display: block;">
                <div class="row">
                    <div class="ten columns">
                        <label for="username">${message(code: 'user.username.label', default: 'Username')}:</label>
                        <g:if test="${params.action!='save'}">
                            <g:if test="${appUserInstance.username != loggedInUser}">
                                <span style="float:right;"><g:message code="user.username.change.label" default="Sorry, usernames can't be changed."/></span>
                            </g:if>
                            <g:else>
                                <span style="float:right;"><g:message code="user.username.change.label" default="Sorry, you can't change your username."/></span>
                            </g:else>
                            <g:hiddenField name="username" value="${appUserInstance?.username}" class="input-text"/>
                            <p>${appUserInstance?.username}</p>
                        </g:if>
                        <g:else>
                            <g:textField name="username" value="${appUserInstance?.username}" class="input-text" />
                        </g:else>
                        <g:hasErrors bean="${appUserInstance}" field="username">
                            <small class="error" style="margin:1px;">
                                <g:renderErrors bean="${appUserInstance}" field="username"/></small>
                        </g:hasErrors>
                        <label>${message(code: 'user.email.label', default: 'Email')}:</label>
                        <g:textField name="email" class="input-text" value="${appUserInstance?.email}"/>
                        <g:hasErrors bean="${appUserInstance}" field="email">
                            <small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"
                                                                                     field="email"/></small>
                        </g:hasErrors>
                        <label>${message(code: 'user.password.label', default: 'Password')}:</label>
                        <g:passwordField name="password" class="input-text"/>
                        <g:hasErrors bean="${appUserInstance}" field="password">
                            <small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"
                                                                                     field="password"/></small>
                        </g:hasErrors>
                        <label for="firstName">${message(code: 'user.firstname.label', default: 'First Name')}:</label>
                        <g:textField name="firstName" value="${appUserInstance?.firstName}" class="input-text"/>
                        <g:hasErrors bean="${appUserInstance}" field="firstName">
                            <small class="error" style="margin:1px;">
                                <g:renderErrors bean="${appUserInstance}" field="firstName"/></small>
                        </g:hasErrors>
                        <label for="lastName">${message(code: 'user.lastname.label', default: 'Last Name')}:</label>
                        <g:textField name="lastName" value="${appUserInstance?.lastName}" class="input-text"/>
                        <g:hasErrors bean="${appUserInstance}" field="lastName">
                            <small class="error" style="margin:1px;">
                                <g:renderErrors bean="${appUserInstance}" field="lastName"/></small>
                        </g:hasErrors>
                        <g:if test="${appUserInstance.username != loggedInUser}">
                        <label for="account">${message(code: 'user.account.label', default: 'Account')}:</label>
                        %{--<g:textField name="lastName" value="${appUserInstance?.account}" class="input-text"/>--}%
                        <g:select name="authority" from="${Role.list()}" optionKey="authority" optionValue="description" value="${authority?:appUserInstance?.getAuthorities()?.authority}"/>
                        %{--<g:hasErrors bean="${role}" field="lastName">--}%
                        %{--<small class="error" style="margin:1px;">--}%
                        %{--<g:renderErrors bean="${appUserInstance}" field="lastName"/></small>--}%
                        %{--</g:hasErrors>--}%
                        <label><g:checkBox name="enabled"
                                           value="${appUserInstance?.enabled}"/>&nbsp;${message(code: 'user.enabled.label', default: 'Enabled')}</label>
                        <g:hasErrors bean="${appUserInstance}" field="enabled">
                            <small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"
                                                                                     field="enabled"/></small>
                        </g:hasErrors>
                        <label><g:checkBox name="accountExpired"
                                           value="${appUserInstance?.accountExpired}"/>&nbsp;${message(code: 'user.accountExpired.label', default: 'Account Expired')}</label>
                        <g:hasErrors bean="${appUserInstance}" field="accountExpired">
                            <small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"
                                                                                     field="accountExpired"/></small>
                        </g:hasErrors>
                        <label><g:checkBox name="enabled"
                                           value="${appUserInstance?.accountLocked}"/>&nbsp;${message(code: 'user.accountLocked.label', default: 'Account Locked')}</label>
                        <g:hasErrors bean="${appUserInstance}" field="accountLocked">
                            <small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"
                                                                                     field="accountLocked"/></small>
                        </g:hasErrors>
                        <label><g:checkBox name="enabled"
                                           value="${appUserInstance?.passwordExpired}"/>&nbsp;${message(code: 'user.passwordExpired.label', default: 'Password Expired')}</label>
                        <g:hasErrors bean="${appUserInstance}" field="passwordExpired">
                            <small class="error" style="margin:1px;"><g:renderErrors bean="${appUserInstance}"
                                                                                     field="passwordExpired"/></small>
                        </g:hasErrors>
                        </g:if>
                    </div>
                </div>
            </li>

            %{--<li id="simple2Tab" style="display: none;">--}%
                %{--<div class="row">--}%
                    %{--<div class="ten columns">--}%
                        %{--<g:each var="entry" in="${roleMap}">--}%
                            %{--<div>--}%
                                %{--<g:checkBox name="${entry.key.authority}" value="${entry.value}"/>--}%
                                %{--<g:link controller='role' action='edit' id='${entry?.key.id}'>${entry?.key.authority.encodeAsHTML()}</g:link>--}%
                            %{--</div>--}%
                        %{--</g:each>--}%

                    %{--</div>--}%
                %{--</div>--}%
            %{--</li>--}%

        </ul>

        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <input type="submit" class="small blue button round" value="Update">
            </div>
        </div>
    </div>


%{--<table>--}%
%{--<tbody>--}%

%{--<s2ui:textFieldRow name='username' labelCode='user.username.label' bean="${user}"--}%
%{--labelCodeDefault='Username' value="${user?.username}"/>--}%

%{--<s2ui:passwordFieldRow name='password' labelCode='user.password.label' bean="${user}"--}%
%{--labelCodeDefault='Password' value="${user?.password}"/>--}%

%{--<s2ui:checkboxRow name='enabled' labelCode='user.enabled.label' bean="${user}"--}%
%{--labelCodeDefault='Enabled' value="${user?.enabled}"/>--}%

%{--<s2ui:checkboxRow name='accountExpired' labelCode='user.accountExpired.label' bean="${user}"--}%
%{--labelCodeDefault='Account Expired' value="${user?.accountExpired}"/>--}%

%{--<s2ui:checkboxRow name='accountLocked' labelCode='user.accountLocked.label' bean="${user}"--}%
%{--labelCodeDefault='Account Locked' value="${user?.accountLocked}"/>--}%

%{--<s2ui:checkboxRow name='passwordExpired' labelCode='user.passwordExpired.label' bean="${user}"--}%
%{--labelCodeDefault='Password Expired' value="${user?.passwordExpired}"/>--}%
%{--</tbody>--}%
%{--</table>--}%

%{--<g:each var="auth" in="${authorityList}">--}%
%{--<div>--}%
%{--<g:checkBox name="${auth.authority}" />--}%
%{--<g:link controller='role' action='edit' id='${auth.id}'>${auth.authority.encodeAsHTML()}</g:link>--}%
%{--</div>--}%
%{--</g:each>--}%


%{--<div style='float:left; margin-top: 10px; '>--}%
%{--<s2ui:submitButton elementId='create' form='userCreateForm' messageCode='default.button.create.label'/>--}%
%{--</div>--}%

</g:form>

</body>
