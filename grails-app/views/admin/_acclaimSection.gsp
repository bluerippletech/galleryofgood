<br>
<br>

<p>Box ${number}:</p>
<label>Testimonial:</label>
<textarea
        name="testimonial${number}"
        class="aprci-custom_textarea">${homepageInstance?."testimonial${number}"}</textarea>
<label>Headline:</label>
<g:textField name="headline${number}" placeholder="Name of parent, school or group"
             value="${homepageInstance?."headline${number}"}"/>

<p class="aprci-custom-p">--------------------------OR ----------------------------</p>

<label>Image:</label>
<input type="file" name="acclaimImage${number}"/>
<br/><br/>
<g:if test="${homepageInstance?."imageurl${number}"}">
    <div class="aprci-img" id="acclaimImage${number}">
    <img src="${homepageInstance?."imageurl${number}"}" alt=""/>
        <a href="#" onclick="javascript:deleteAcclaimImage(${number});return false;"></a>
    </div>
</g:if>
<br/>
<br/>