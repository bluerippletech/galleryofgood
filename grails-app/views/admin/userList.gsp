<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.users'/></title>
    <r:require modules="foundation, jqgrid, jquery-ui, jqueryDatatable, export,lightbox2"/>
    <r:script>
        $(function(){
            $('#user-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                sAjaxSource: '${createLink(controller: 'admin', action: 'regularListJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false }, //{ "sType": "date-euro" },
                    { "bSortable": false }
                ],
                "aoColumnDefs":[
                    { "sName": "", "aTargets": [ 0 ] },
                    { "sName": "", "aTargets": [ 1 ] },
                    { "sName": "username", "aTargets": [ 3 ] },
                    { "sName": "email", "aTargets": [ 4 ] },
                    { "sName": "", "aTargets": [ 5 ] },
                    { "sName": "", "aTargets": [ 6 ] },
                    { "sName": "", "aTargets": [ 7 ] },
                    { "sName": "", "aTargets": [ 8 ] }
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#user-list_wrapper .pagination ul li').size()>3) {
                        $('#user-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#user-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } );

            $('#user-list-standard').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                sAjaxSource: '${createLink(controller: 'admin', action: 'regularListJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false }, //{ "sType": "date-euro" },
                    { "bSortable": false }
                ],
                "aoColumnDefs":[
                    { "sName": "", "aTargets": [ 0 ] },
                    { "sName": "", "aTargets": [ 1 ] },
                    { "sName": "username", "aTargets": [ 3 ] },
                    { "sName": "email", "aTargets": [ 4 ] },
                    { "sName": "", "aTargets": [ 5 ] },
                    { "sName": "", "aTargets": [ 6 ] },
                    { "sName": "", "aTargets": [ 7 ] },
                    { "sName": "", "aTargets": [ 8 ] }
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#user-list-standard_wrapper .pagination ul li').size()>3) {
                        $('#user-list-standard_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#user-list-standard_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } ).fnFilter('Standard', 1);

             var oTable=$('#new-artwork-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                "bAutoWidth":true,
                "aaSorting": [[ 1, "desc" ]],
                sAjaxSource: '${createLink(controller: 'admin', action: 'newArtworksJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumnDefs":[
                    {"sName":"id", "sClass": "indexLeft","aTargets": [ 0 ], "bSortable":false},
                    { "sName": "dateCreated", "aTargets": [ 1 ], "bSortable":true,"sType":"date"},
                    { "sName": "g.owner", "aTargets": [ 2 ],bSortable:true},
                    { "sName": "g.galleryName", "aTargets": [ 3 ],bSortable:true },
                    { "sName": "artist", "aTargets": [ 4 ] },
                    { "sName": "title", "aTargets": [ 5 ]},
                    { "sName": "tag", "aTargets": [ 6 ], "bSortable":false},
                    { "sName": "status","sClass": "indexRight", "aTargets": [ 7 ]}

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#new-artwork-list_wrapper .pagination ul li').size()>3) {
                        $('#new-artwork-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#new-artwork-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } );

            var flaggedArtworkTable=$('#flagged-list').dataTable( {
                bProcessing: true,
                bServerSide: true,
                iDisplayLength: 30,
                "bAutoWidth":true,
                sAjaxSource: '${createLink(controller: 'admin', action: 'flaggedArtworksJSON')}',
                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumnDefs":[
                    {"sName":"id", "sClass": "indexLeft","aTargets": [ 0 ], "bSortable":false},
                    { "sName": "dateCreated", "aTargets": [ 1 ], "bSortable":true,"sType":"date"},
                    { "sName": "flaggedUser", "aTargets": [ 2 ],bSortable:true},
                    { "sName": "flaggingUser", "aTargets": [ 3 ],bSortable:true },
                    { "sName": "comment", "aTargets": [ 4 ] },
                    { "sName": "sectionFlagged", "aTargets": [ 5 ]},
                    { "sName": "status","sClass": "indexRight", "aTargets": [ 6 ]}

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                },
                 "fnDrawCallback" : function() {
                    if ($('#flagged-list_wrapper .pagination ul li').size()>3) {
                        $('#flagged-list_wrapper .dataTables_paginate')[0].style.display = "block";
                    }
                    else {
                       $('#flagged-list_wrapper .dataTables_paginate')[0].style.display = "none";
                    }
                 }
            } );
            $(".dataTables_length").hide()
            $(".dataTables_info").hide()
            $(".dataTables_filter").find("input").addClass('input-text')
            $(".dataTables_filter").find("input").css('display', 'inline')
            $(".dataTables_filter").find("input").css('width', '150 px')
                $(".bulkAction").css("width","133px");
            $(".bulkAction").find("ul").css("width","131px");
            $('.lb-close img').attr('src','${r.resource(dir: 'images',file: 'close.png')}');
        })
        function toggleAll(aCheckbox){
            var checkboxes=$(aCheckbox).closest("li").find(".checkbox");
            var checkboxes2=$(aCheckbox).closest("li").find("input[type='checkbox']");
            if ('checked'==$(aCheckbox).attr("checked")){
                checkboxes.addClass("checked");
                checkboxes2.attr("checked","checked");
            }
            else{
                checkboxes.removeClass("checked");
                checkboxes2.removeAttr("checked");
            }
        }
        <g:if test="${params?.pageFragment=='standardUserTab'}">
            $(function(){
                $("#standardUser").find("a").click()
            })
        </g:if>
        %{--<g:elseif test="${'newArtwork'.equals(page)}">--}%
            %{--$(function(){--}%
                %{--$("#newArtwork").find("a").click();--}%
            %{--});--}%
        %{--</g:elseif>--}%
        %{--<g:elseif test="${'flagged'.equals(page)}">--}%
            %{--$(function(){--}%
                %{--$("#flagged").find("a").click();--}%
            %{--});--}%
        %{--</g:elseif>--}%
        %{--<g:else>--}%
                %{--$(function(){$('#sub-detail-all').show();});--}%
        %{--</g:else>--}%
    </r:script>
</head>

<body>
<div class="ten columns ap-right">
    <p class="ap-page">Users</p>
    <dl class="tabs">
        <dd><a class="active" href="#simple1" >All</a></dd>
        <dd id="standardUser"><a href="#standardUser" id="standard" >Standard</a></dd>
        <dd id="newArtwork" class="${"newArtwork".equals(activeTab)?"active":""}"><a href="#newArtwork">New Artwork</a></dd>
        <dd id="flagged" class="${"flagged".equals(activeTab)?"active":""}"><a href="#flag">Flagged</a></dd>
        %{--<dd><a href="#simple3">Feature by Type</a></dd>--}%
        %{--<dd><a href="#simple4">Themes</a></dd>--}%
    </dl>
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
    <ul class="tabs-content">
        <li id="simple1Tab" class="active" style="display: block;">
            <g:form class="custom nice" controller="admin" action="actionUsers" method="post" enctype="multipart/form-data">
                <g:hiddenField name="renderPageFragment" value='simple1Tab'/>
                <div id="sub-detail-all">
                    <div class="four columns">
                        <h5>New this month</h5>
                        <p>All: ${counter.allNew}</p>
                        <p>Standard: ${counter.standardNew}</p>
                    </div>
                    <div class="four columns">
                        <h5>Total all-time</h5>
                        <p>All: ${counter.allAllTime}</p>
                        <p>Standard: ${counter.standardAllTime}</p>
                    </div>
                    <div class="four columns end">
                        <a class="small button white nice radius" href="${createLink(controller: 'admin', action: 'exportToCSV', params:[format:'csv', extension:'csv'])}">Export All Users .CSV »</a>
                    </div>
                </div>
                <div class="clear-both"></div>
                <div>
                <div class="four columns">
                    <select name="selectedAction" id="bulk-action-comments" style="display:none;">
                        <option selected="Delete">Delete</option>
                    </select>
                    <div class="custom dropdown inside_tab" style="width: 136px;">
                        <a class="current" href="#">Delete</a>
                        <a class="selector" href="#"></a>
                        <ul style="width: 134px;">
                            <li style="">Delete</li>
                        </ul>
                    </div>
                    <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
                </div>
                <div class="clear-both"></div>
                <div>
                <table id="user-list" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                        <th>Account</th>
                        <th>Date</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>>13</th>
                        <th>Gallery</th>
                        <th>Activity</th>
                        <th>Flagged</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                </div>
            </g:form>
	    </li>
        <li id="standardUserTab">
            <div id="sub-detail-standard">
                <div class="four columns">
                    <p>New this month: ${counter.standardNew}</p>
                    <p>Total all-time: ${counter.standardAllTime}</p>
                </div>
                <div class="four columns">

                </div>
                <div class="four columns end">
                    <a class="small button white nice radius" href="${createLink(controller: 'admin', action: 'exportToCSV', params:[format:'csv', extension:'csv', userRole:'standard'])}">Export All Users .CSV »</a>
                </div>
            </div>

            <div class="clear-both"></div>
            <g:form class="custom nice" controller="admin" action="actionUsers" method="post" enctype="multipart/form-data">
                <g:hiddenField name="renderPageFragment" value='standardUserTab'/>
                <div>
                <div class="four columns">
                    <select name="selectedAction" id="bulk-action-users" style="display:none;">
                        <option selected="Delete">Delete</option>
                    </select>
                    <div class="custom dropdown inside_tab" style="width: 136px;">
                        <a class="current" href="#">Delete</a>
                        <a class="selector" href="#"></a>
                        <ul style="width: 134px;">
                            <li style="">Delete</li>
                        </ul>
                    </div>
                    <input class="nice small radius white button" type="submit" name="submit" value="Apply" />
                </div>
                <div class="clear-both"></div>
                <div>
                    <table id="user-list-standard" width="100%">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                            <th>Account</th>
                            <th>Date</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>>13</th>
                            <th>Gallery</th>
                            <th>Activity</th>
                            <th>Flagged</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </div>
            </g:form>
        </li>
        <li id="newArtworkTab">
            <g:form name="newArtworks" controller="admin" action="changeArtworkStatus"
                    onsubmit="return confirm('Are you sure you want to change selected artworks?')" class="custom nice">

                <div class="row">
                    New this month: ${newArtworksThisMonth}<br/>
                    Total all-time: ${totalArtworks}
                </div>

                <select width="200px" style="display:none;width:133px;" name="bulkAction">
                    <option SELECTED>Bulk&nbsp;Actions</option>
                    <option value="Approved">Approve</option>
                    <option value="Disapproved">Disapprove</option>
                    <option value="Delete">Delete</option>
                </select>

                <div class="custom dropdown bulkAction" style="width: 133px;" id="bulkAction">
                    <a href="#" class="current">Bulk&nbsp;Actions</a>
                    <a href="#" class="selector"></a>
                    <ul style="width: 131px; ">
                        <li class="selected" style="">Bulk&nbsp;Actions</li>
                        <li style="">Approve</li>
                        <li style="">Disapprove</li>
                        <li style="">Delete</li>
                    </ul>
                </div>
                <input class="nice small radius white button" type="submit" name="submit" value="Apply"/>

                <div class="row-fluid">
                    <table id="new-artwork-list" width="100%">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/></th>
                            <th>Date</th>
                            <th>Username</th>
                            <th>Gallery</th>
                            <th>Artist</th>
                            <th>Artwork Title and Image</th>
                            <th>Tags</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </g:form>
        </li>
        <li class="${"flagged".equals(activeTab)?"active":""}" id="flagTab">
            <g:form name="flaggedArtworks" controller="admin" action="updateFlags"
                    onsubmit="return confirm('Are you sure you want to change selected flags?')" class="custom nice">
                <div class="row">
                    New this month: ${flaggedThisMonth}<br/>
                    Total all-time: ${totalFlags}
                </div>
                <select width="200px" style="display:none;width:133px;" name="bulkAction">
                    <option SELECTED>Bulk&nbsp;Actions</option>
                    <option value="Open">Open</option>
                    <option value="Resolved">Resolved</option>
                    <option value="Delete">Delete</option>
                </select>

                <div class="custom dropdown bulkAction" style="width: 133px;" id="bulkAction">
                    <a href="#" class="current">Bulk&nbsp;Actions</a>
                    <a href="#" class="selector"></a>
                    <ul style="width: 131px; ">
                        <li class="selected" style="">Bulk&nbsp;Actions</li>
                        <li style="">Open</li>
                        <li style="">Resolved</li>
                        <li style="">Delete</li>
                    </ul>
                </div>
                <input class="nice small radius white button" type="submit" name="submit" value="Apply"/>

                <div class="row-fluid">
                    <table id="flagged-list" width="100%">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="checkAll" onchange="toggleAll(this)"/>
                            </th>
                            <th>Date</th>
                            <th>User Flagged</th>
                            <th>Sent By</th>
                            <th>Message</th>
                            <th>Section Flagged</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </g:form>
        </li>

    </ul>
</div>
</body>
