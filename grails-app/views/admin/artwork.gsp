<%@ page import="com.galleryofgood.gallery.Timeline; com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='adminmain'/>
    <title><g:message code='admin.artwork.settings'/></title>
    <r:require modules="foundation,jquery-ui,jgrowl,CLeditor"/>
    <r:script>
        $(function(){
        $("#redactor_content").cleditor();
        //$("#redactor_content").redactor({path:'${createLink(uri: '/js/redactor')}'});
        $("#featureArtworkType1").next().width("400px");
        $("#featureArtworkType1").next().find("ul").width("398px");
        $("#featureArtworkType2").next().width("400px");
        $("#featureArtworkType2").next().find("ul").width("398px");
        $("#featureArtworkType3").next().width("400px");
        $("#featureArtworkType3").next().find("ul").width("398px");
        $("#timelinelink").next().find("ul").width("398px");
        enableAutocomplete("#featuredArtworkByType1Description");
        enableAutocomplete("#featuredArtworkByType2Description");
        enableAutocomplete("#featuredArtworkByType3Description");
        });

        function enableAutocomplete(aTextboxId){
        $(aTextboxId).autocomplete({
            source: '${createLink(controller: 'artwork', action: 'artworkAutocomplete')}',
            minLength:0,
            select:function(event,ui){
            populateIdAndImage(ui,aTextboxId);

            },
            change:function(event,ui){changed(ui,aTextboxId);}
        }).data( "autocomplete" )._renderItem = function( ul, item ) {
            var inner_html = '<a>' +
    '                           <div class="list_item_container">' +
    '                               <div class="image" style="width:60px;height:57px;overflow:hidden;vertical-align:top;display:inline-block;"><img width="56px;" src="' + item.image + '"></div>' +
    '                               <div class="label"  style="display:inline-block;">' + item.label + '</div>'
            return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append(inner_html)
                    .appendTo( ul );
        };

        }

        function populateIdAndImage(ui,aTextboxId){
            $(aTextboxId).parent().find("img").attr("src",ui.item.image);
            $(aTextboxId).parent().find("input[type='hidden']").val(ui.item.id);
        }
        function changed(ui,aTextboxId){
            if(ui.item.id){
                populateIdAndImage(ui,aTextboxId);
            }
        }

        function clearFeaturedArtwork(aTextboxId){
        var noImageAvailableUrl='${createLinkTo(file: 'images/NoImageAvailable.png')}';
            $(aTextboxId).parent().find("img").attr("src",noImageAvailableUrl);
            $(aTextboxId).parent().find("input[type='hidden']").val("");
            $(aTextboxId).val("");
        }

        function deleteRetrospectiveImage(){
        if (confirm('Are you sure you want to delete this image?')){
                 jQuery.ajax({type:'GET',
						url:'${createLink(controller: 'homepage', action: 'deleteRetrospectiveImage')}',
		                success:function(data, textStatus) {
		                    $.jGrowl(data);
		                    $("#retrospective_image").fadeOut(300, function() { $(this).remove(); });
		                },
		                error:function(XMLHttpRequest, textStatus, errorThrown) {
		                	$.jGrowl(XMLHttpRequest.responseText,{header:'Errors',life:10000,theme:'growl-error'});
		                }
		            });
            }
            else{return false;}

        }
        <g:if test="${params?.selectedTab}">
            $(function(){
                $("#"+"${params?.selectedTab}").find("a").click()
            })
        </g:if>
    </r:script>
</head>

<body>

<div class="ten columns ap-right">
    <g:render template="/layouts/flashMessage"/>
    <g:render template="/layouts/flashErrors"/>
    <g:form class="custom nice" method="post" action="updateArtworkSettings" name="input" enctype="multipart/form-data">
        <g:hiddenField name="id" value="${homepageInstance?.id}"/>
        <g:hiddenField name="selectedTab" id="selectedTab" value="${params?.selectedTab?:'simple1'}"/>
        <p class="ap-page">Artworks</p>

        <div class="row">
            <div class="nine columns">
                &nbsp;
            </div>

            <div class="one column">
                <input type="submit" class="small round button blue" value="Save">
            </div>
        </div>

        <dl class="tabs">
            <dd id="simple1"><a class="active" href="#simple1"  onclick="$('#selectedTab').val('simple1')">Slider</a></dd>
            <dd id="simple2"><a href="#simple2" onclick="$('#selectedTab').val('simple2')">Retrospecive</a></dd>
            <dd id="simple3"><a href="#simple3" onclick="$('#selectedTab').val('simple3')">Feature by Type</a></dd>
            <dd id="simple4"><a href="#simple4" onclick="$('#selectedTab').val('simple4')">Themes</a></dd>
        </dl>

        <ul class="tabs-content">
            <li id="simple1Tab" class="active" style="display: block;">
                <div class="apr-container">

                    <p>Fresh Paint Slider:</p>
                    <br/>
                    <g:each in="${1..10}" var="artworkNumber">
                        <g:render template="featuredArtwork"
                                  model="${['artworkNumber': artworkNumber, 'artwork': Artwork.get(homepageInstance?."featuredArtwork${artworkNumber}")]}"/>
                    </g:each>
                    <br/>
                </div>
            </li>
            <li id="simple2Tab" style="display: none;">
                <div class="apr-container row">
                    <div class="aprc-inner">
                        <p>Retrospective</p>
                        <label>Headline:</label>
                        <textarea id="redactor_content" name="retrospectiveContent">
                            ${homepageInstance?.retrospectiveContent}
                        </textarea>
                        <label>Image:</label>
                        <input type="file" name="retrospectiveFile"/>
                        <br/>
                        <br/>
                        <g:if test="${homepageInstance?.retrospectiveImage}">
                            <div id="retrospective_image" class="aprci-img aprci-artwork-img">
                                <img alt="${createLinkTo(file: 'images/NoImageAvailable.png')}"
                                     src="${homepageInstance?.retrospectiveImage}">
                                <a href="#" onclick="javascript:deleteRetrospectiveImage();
                                return false;"></a>
                            </div>
                        </g:if>
                        <br/>
                        <br/>
                        <label>Image Link:</label>
                        <input name="retrospectiveUrl" type="text" value="${homepageInstance?.retrospectiveUrl}"
                               placeholder="http://"/>
                        <br/>
                        <br/>
                        <label>Show Timeline link:</label>
                        <g:select from="${Timeline.findAll()}" name="timelinelink" optionKey="id"
                                  optionValue="artistName"
                        value="${homepageInstance?.timelinelink}"/>
                    </div>
                </div>

            </li>
            <li id="simple3Tab" style="display: none;">
                <div class="apr-container row">
                    <div class="aprc-inner">

                        <div class="row">
                            <div class="ten columns" style="display:inline-block;">
                                <g:hiddenField name="featuredArtworkByType1"
                                               value="${homepageInstance?.featuredArtworkByType1}"/>
                                <label>Artwork Type 1:</label>
                                <g:select style="display:none;width:400px;"
                                          name="featureArtworkType1"
                                          from="${com.galleryofgood.gallery.Artwork.constraints.artworkType.inList}"
                                          value="${homepageInstance?.featureArtworkType1}"
                                          noSelection="['': '-Please select artwork type.-']"/>
                                <label for="featuredArtworkByType1Description"
                                       style="margin-bottom:8px;">Featured Artwork for Type 1:</label>
                                <input type="text" name="featuredArtworkByType1Description"
                                       id="featuredArtworkByType1Description" size="30"
                                       class="input-text"
                                       value="${Artwork.get(homepageInstance?.featuredArtworkByType1)?.title}"/>

                                <div class="aprs-img">
                                    <img id="featured_artwork_by_type_1_image"
                                         src="${Artwork.get(homepageInstance?.featuredArtworkByType1)?.thumbnailUrl}"
                                         alt="${createLinkTo(file: 'images/NoImageAvailable.png')}" width="56px;"
                                         height="56px;" style="width:56px;height:56px;"/>
                                    <a href="#"
                                       onclick="javascript:clearFeaturedArtwork('#featuredArtworkByType1Description');
                                       return false;"></a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="ten columns" style="display:inline-block;">
                                <g:hiddenField name="featuredArtworkByType2"
                                               value="${homepageInstance?.featuredArtworkByType2}"/>
                                <label>Artwork Type 2:</label>
                                <g:select style="display:none;width:400px;"
                                          name="featureArtworkType2"
                                          from="${com.galleryofgood.gallery.Artwork.constraints.artworkType.inList}"
                                          value="${homepageInstance?.featureArtworkType2}"
                                          noSelection="['': '-Please select artwork type.-']"/>
                                <label for="featuredArtworkByType2Description"
                                       style="margin-bottom:8px;">Featured Artwork for Type 2:</label>
                                <input type="text" name="featuredArtworkByType2Description"
                                       id="featuredArtworkByType2Description" size="30"
                                       class="input-text"
                                       value="${Artwork.get(homepageInstance?.featuredArtworkByType2)?.title}"/>

                                <div class="aprs-img">
                                    <img id="featured_artwork_by_type_2_image"
                                         src="${Artwork.get(homepageInstance?.featuredArtworkByType2)?.thumbnailUrl}"
                                         alt="${createLinkTo(file: 'images/NoImageAvailable.png')}" width="56px;"
                                         height="56px;" style="width:56px;height:56px;"/>
                                    <a href="#"
                                       onclick="javascript:clearFeaturedArtwork('#featuredArtworkByType2Description');
                                       return false;"></a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="ten columns" style="display:inline-block;">
                                <g:hiddenField name="featuredArtworkByType3"
                                               value="${homepageInstance?.featuredArtworkByType3}"/>
                                <label>Artwork Type 3:</label>
                                <g:select style="display:none;width:400px;"
                                          name="featureArtworkType3"
                                          from="${com.galleryofgood.gallery.Artwork.constraints.artworkType.inList}"
                                          value="${homepageInstance?.featureArtworkType3}"
                                          noSelection="['': '-Please select artwork type.-']"/>
                                <label for="featuredArtworkByType3Description"
                                       style="margin-bottom:8px;">Featured Artwork for Type 3:</label>
                                <input type="text" name="featuredArtworkByType3Description"
                                       id="featuredArtworkByType3Description" size="30"
                                       class="input-text"
                                       value="${Artwork.get(homepageInstance?.featuredArtworkByType3)?.title}"/>

                                <div class="aprs-img">
                                    <img id="featured_artwork_by_type_3_image"
                                         src="${Artwork.get(homepageInstance?.featuredArtworkByType3)?.thumbnailUrl}"
                                         alt="${createLinkTo(file: 'images/NoImageAvailable.png')}" width="56px;"
                                         height="56px;" style="width:56px;height:56px;"/>
                                    <a href="#"
                                       onclick="javascript:clearFeaturedArtwork('#featuredArtworkByType3Description');
                                       return false;"></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <li id="simple4Tab" style="display: none;">
                This is simple tab 3's content. It's, you know...okay.
            </li>
        </ul>
    </g:form>
</div>

</body>
