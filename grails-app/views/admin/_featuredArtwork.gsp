<%@ page import="com.galleryofgood.gallery.Artwork" %>
<div class="row">
    <div class="ten columns" style="display:inline-block;">
        <g:hiddenField name="featuredArtwork${artworkNumber}" value="${artwork?.id}"/>
        <label for="featured_artwork_${artworkNumber}"
               style="margin-bottom:8px;">Featured Artwork ${artworkNumber}:</label>
        <input type="text" name="featured_artwork_${artworkNumber}" id="featured_artwork_${artworkNumber}" size="30"
               class="input-text" value="${artwork?.title}"/>

        <div class="aprs-img">
            <img id="featured_artwork_1_image" src="${artwork?.thumbnailUrl}" alt="Please select artwork." width="56px;" height="56px;" style="width:56px;height:56px;"/>
            <a href="#" onclick="javascript:clearFeaturedArtwork('#featured_artwork_${artworkNumber}');return false;"></a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        enableAutocomplete("#featured_artwork_${artworkNumber}");
    });
</script>
