<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='search.themeTag' default="Emerging Themes - Gallery of Good"/></title>
    <r:require modules="foundation, lazyload"/>
    <r:script>
    //Pagination AJAX load
    function loadByPagination(max, offset, sort, order, controller, action, containerId){
        $.ajax({
            url: "${createLink(uri: '/' )}"+controller+"/"+action,
            type: "POST",
            data: {max:max, offset:offset, sort:sort, order:order, tag:"${params?.theme}"},
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
               $("#"+containerId).html(results);
            }
        });
    }
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <!-- <h3 id="title_emerging_themes"><span></span>Emerging Themes</h3> -->
        <h3 class="heading color-black">artworks with the theme: ${params?.theme}</h3>

    </div>
</div>

<div class="row">
    <div class="twelve columns">
        <hr class="astr-hr" />
        <a href="${createLink(controller: 'artwork', action: 'emergingThemes')}">Find more themes</a>
        <br /><br />
    </div>
</div>

<!-- sort links -->
<!-- secondary navigation -->
<div class="row">
    <div class="twelve columns" id="artworks-listing">
        <!-- Artworks Items -->
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        <br /><br />

    </div>
</div>
%{--<div class="row">--}%
    %{--<div class="twelve columns">--}%
        <ripple:paginate count="${36}" total="${artworksInstance.size()}"  sort="dateCreated" order="asc" controller="${params.controller}" action="themeTagPerPage" containerId="artworks-listing"/>
    %{--</div>--}%
%{--</div>--}%
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->

</body>