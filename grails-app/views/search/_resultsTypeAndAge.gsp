<g:if test="${artworksInstance}">
<div class="row">
    <div class="twelve columns" id="artworks-listing">
        <!-- Artworks Items -->
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        <br /><br />

    </div>
</div>
<script type="text/javascript">
    //Pagination AJAX load
    function loadByPagination(max, offset, sort, order, controller, action, containerId){
        var queryCriteria ="";
        $(criteria).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteria=" + this;
            else
                queryCriteria += "&criteria=" + this;
        });
        $(criteriaAge).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteriaAge=" + this;
            else
                queryCriteria += "&criteriaAge=" + this;
        });
        queryCriteria += "&max="+ max + "&offset=" + offset + "&sort=" + sort + "&order=" + order;
        $.ajax({
            url: "${createLink(uri: '/' )}"+controller+"/"+action,
            type: "POST",
            data: queryCriteria,
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
                $("#artworks-listing").html(results);
            }
        });
    }
</script>
%{--<div class="row">--}%
    %{--<div class="twelve columns">--}%
        <ripple:paginate count="${36}" total="${artworksInstance?.size()}"  sort="title" order="asc" controller="${params.controller}" action="itemsPerPage" containerId="artworks-listing"/>
    %{--</div>--}%
%{--</div>--}%
</g:if>
<g:else>
    <div class="row">
        <div class="twelve columns">
            <h4>Sorry, we did not find any results</h4>
            <p>Try again or browse the artworks by most recent, type and age or themes.</p>
        </div>
    </div>
</g:else>
