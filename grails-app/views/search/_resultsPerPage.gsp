<g:if test="${artworksInstance}">
    <div id="img-container-gal" style="display: none;" class="artwork_list">
        <g:each in="${artworksInstance}" var="artwork">
            <div class="display-artwork">
                <div style="width: 299px; height: 233px; padding: 0; margin-bottom: 15px; overflow: hidden"><a href="${createLink(controller: "artwork", action:"artworkview", params: [id:artwork.id] )}"><img class="lazy" src="" alt="" data-original="${artwork.thumbnailUrl}" style="max-height:100%;max-width:100%"></a></div>
                <div class="artwork ma-artwork">
                    <h5><a href="${createLink(controller: "artwork", action:"artworkview", params: [id:artwork.id] )}">${artwork.title}</a></h5>
                    <p>By: ${artwork.artist}, Age ${artwork.artworkAge}</p>
                    <p>Gallery: <a href="${createLink(uri: '/' )+artwork.gallery.galleryNameUrl}">${artwork.gallery.galleryName}</a></p>
                </div>
            </div>
        </g:each>
    </div>
</g:if>
<g:else>
    <div class="twelve columns">
        <h4>Sorry, we did not find any results</b></h4>
        <p>Try again or browse the artworks by most recent, type and age or themes.</p>
    </div>
</g:else>
<div class="clear-both"></div>
<script type="text/javascript">
        $("#img-container-gal").fadeIn('slow');
        $("img.lazy").lazyload({effect:"fadeIn"});
</script>