<%@ page import="com.galleryofgood.gallery.Gallery; com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='search.searchItems' default="Search Results - Gallery of Good"/></title>
    <r:require modules="foundation, lazyload, jqueryCookies"/>
    <r:script>
    var criteria = [];
    var criteriaAge = [];
    var filterByVar
    var searchKeyVar
    function selector(selected, filterBy){
        if($(selected).closest("li").hasClass("selected")){
            $(selected).closest("li").removeClass("selected");
            if(filterBy=="type"){
                criteria.splice($.inArray($(selected).text().toString(), criteria),1);
                getItems("items_container", filterBy);
            }else if(filterBy=="age"){
                criteriaAge.splice($.inArray($(selected).text().toString(), criteriaAge),1);
                getItems("items_container", filterBy);
            }
        }else{
            if($(selected).text().toString() == "All"){
                $(selected).closest("ul").find(".selected").removeClass("selected");
                $(selected).closest("li").addClass("selected");
                if(filterBy=="type"){
                    criteria = [];
                    criteria.push($(selected).text().toString());
                    getItems("items_container", filterBy)
                }else if(filterBy=="age"){
                    criteriaAge = [];
                    criteriaAge.push($(selected).text().toString());
                    getItems("items_container", filterBy)
                }
            }else{
                if($(selected).closest("ul").find(".selected a").text().toString() == "All"){
                    $(selected).closest("ul").find(".selected").removeClass("selected");
                    criteria = [];
                    criteriaAge = [];
                }
                $(selected).closest("li").addClass("selected");
                if(filterBy=="type"){
                    criteria.push($(selected).text().toString());
                    getItems("items_container", filterBy)
                }else if(filterBy=="age"){
                    criteriaAge.push($(selected).text().toString());
                    getItems("items_container", filterBy);
                }

            }
        }
    }
    function selectCriteria(selected){
        if($(selected).text().toString() == "All"){
            $(selected).closest("ul").find(".selected").removeClass("selected");
            $(selected).closest("li").addClass("selected");
            $("#age-groups").fadeOut();
            $("#artwork-types").fadeOut();
            getItems("items_container", "all")
        }else if($(selected).text().toString() == "Artworks"){
            $(selected).closest("ul").find(".selected").removeClass("selected");
            $(selected).closest("li").addClass("selected");
            $("#age-groups").hide();
            $("#artwork-types").fadeIn();
            criteria.push("All");
            criteriaAge = [];
            getItems("items_container", "type");
        }else if($(selected).text().toString() == "Artists"){
            $(selected).closest("ul").find(".selected").removeClass("selected");
            $(selected).closest("li").addClass("selected");
            $("#artwork-types").hide();
            $("#age-groups").fadeIn();
            criteriaAge.push("All");
            criteria = [];
            getItems("items_container", "age");
        }
    }
    function getItems(containerId, filterBy){
        filterByVar = filterBy;
        searchKeyVar = "${params.searchKey}";
        var queryCriteria ="";
        $(criteria).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteria=" + this;
            else
                queryCriteria += "&criteria=" + this;
        });
        $(criteriaAge).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteriaAge=" + this;
            else
                queryCriteria += "&criteriaAge=" + this;
        });
        queryCriteria += (filterBy=="type")?"&searchType=Artworks":(filterBy=="age")?"&searchType=Artists":"&searchType=All"
        queryCriteria += "&searchKey=${params.searchKey}"
        $.ajax({
            url:  "${createLink(controller: params.controller, action:'searchItemsGet')}",
            type: "POST",
            data: queryCriteria,
            dataType: "html",
            beforeSend: function() {
               var typesTemp = ""
               if(criteria.length <= 1){
                  if(criteria.length == 1)
                    typesTemp = (criteria=='All')?'All types':criteria;
               }else{
                   $(criteria).each( function(i){
                        if(i==(criteria.length-1))
                            typesTemp += ' and '
                        else if(i!=0)
                            typesTemp += ', '

                        typesTemp += (this=='All')?'all types':this;
                   })
               }
               $("#artworkType-container").text(typesTemp);
               $(".toggler").show();
            },
            success: function(results) {
               $("#"+containerId).html(results);
               if(criteria.length < 1)
                    $(".toggler").hide();
            }
        });
    }
    $(document).ready(function(){
        if("${params.searchType}" == "Artworks"){
            $("#artwork-types").show();
            criteria.push("All");
            getItems("items_container", "type");
        }else if("${params.searchType}" == "Artists"){
            $("#age-groups").show();
            criteriaAge.push("All");
            getItems("items_container", "age");
        }else if("${params.searchType}" == "All"){
            getItems("items_container", "all");
        }
        $("img.lazy").lazyload();
        $(".toggler").hide();
    });
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <p></p>
    </div>
</div>
<g:if test="${params.searchKey}">
<div class="row">
    <div class="twelve columns srdd-top-fix" id="details-search">
        <h5>We found <span id="count-container" style="font-weight: bold;"></span> artworks with <span style="font-weight: bold;">${params.searchKey}</span><span class="toggler"> in <span id="artworkType-container" style="font-weight: bold;"></span></span>.</h5>
        %{--<h4>There are <span id="count-container"></span> ${params.searchKey} in <span id="searchType-container">${params.searchType}</span>.</h4>--}%
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        <p></p>
    </div>
</div>
<div id="secondary_navigation" class="row srdd">
    <div class="twelve columns">
        <ul id="level-1">
            %{--<li class="${params.searchType == 'All'?'selected':''}"><a href="javascript:" onclick="selectCriteria(this)">All</a></li>--}%
            <li class="${params.searchType == 'Artworks'?'selected':''}"><a href="javascript:" onclick="selectCriteria(this)">Artworks</a></li>
            %{--<li class="${params.searchType == 'Artists'?'selected':''}"><a href="javascript:" onclick="selectCriteria(this)">Artists</a></li>--}%
        </ul>
        <div class="clear-both"></div>
        <div id="artwork-types" style="display: none">
            <ul id="level-2">
                <li class="selected"><a href="javascript:" onclick="selector(this, 'type')" id="selector-All">All</a></li>
                <g:each in="${com.galleryofgood.gallery.Artwork.constraints.artworkType.inList}" var="type">
                    <li><a href="javascript:" onclick="selector(this, 'type')" id="selector-${type}">${type}</a></li>
                </g:each>
            </ul>
        </div>
        <div class="clear-both"></div>
        <div id="age-groups" style="display: none">
            <ul id="level-2">
                <li>Age Group:</li>
                <li class="selected"><a href="javascript:" onclick="selector(this, 'age')" id="selector-All">All</a></li>
                <g:each in="${com.galleryofgood.artwork.ArtworkUtils.ageGroups}" var="group">
                    <li><a href="javascript:" onclick="selector(this, 'age')" id="selector-${group.key=='14-18'?'Above 13':group.key}">${group.key=='14-18'?'Above 13':group.key}</a></li>
                </g:each>
            </ul>
        </div>
        <div id="items_container">
            <!-- Items -->
        </div>
    </div>
</div>
<div class="clear-both"></div>
<div id="artworks_container">
    <div class="row">
        <div class="twelve columns">

        </div>
    </div>
</div>
</g:if>
<div class="row tip-box" id="tip-search" <g:if test="${params.searchKey}">style="display: none" </g:if>>
    <div class="two columns">
        <img width="88" height="97" alt="Tip" src="${resource(dir:"images", file:"tip.jpg")}">
    </div>
    <div class="ten">
        <br /><br />
        <b>Looking for a gallery?</b> If you know the gallery name, enter
    this URL: <b>http://www.galleryofgood.com/nameofgallery</b>. <br />(Replace <b>"nameofgallery"</b> with the actual gallery name.)<br /><br />
        <g:if test="${params.searchKey}"><a href="#" class="close" id="inline_close" onclick="$.cookie('searchTip', 'No', { path: '/', expires: 7 });"><b>Got it, don't show this message again.</b></a> </g:if>
    </div>
</div>
<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->

</body>