<%@ page import="com.galleryofgood.gallery.Timeline; com.galleryofgood.security.AppUser; com.galleryofgood.user.UserDetails; com.galleryofgood.gallery.Gallery" %>
<ul id="items-listing-paged" style="display: none">
    <g:each in="${modelInstance}" var="model">
        <g:if test="${model.getClass() == com.galleryofgood.gallery.Artwork}">
            <g:set var="artwork" value="${model}"/>
            <li class="display-artwork">
                <div style="width: 225px; height: 175px; overflow: hidden"><a href="${createLink(controller: "artwork", action:"artworkview", params: [id:artwork.id] )}"><img class="lazy" src="${resource(dir:"images", file: "white.jpg")}" alt="" data-original="${artwork?.thumbnailUrl}" style="max-height:100%;max-width:100%"></a></div>
                <h5><a href="${createLink(controller: "artwork", action:"artworkview", params: [id:artwork.id] )}">${artwork?.title}</a></h5>
                <p>By: ${artwork?.artist}, Age ${artwork?.artworkAge}</p>
                <p>Gallery: <a href="${createLink(uri: '/' )+artwork?.gallery?.galleryNameUrl}">${artwork?.gallery?.galleryName}</a></p>
            </li>
        </g:if>
        <g:elseif test="${model.getClass() == com.galleryofgood.security.AppUser}">
            <g:set var="appUser" value="${model}" />
            <li class="display-artwork">
                <div style="width: 225px; height: 245px; overflow: hidden"><a href="${createLink(controller: "timeline", action:"timelineview", params: [id:Timeline.findByArtistName(appUser?.username)?.id] )}"><img class="lazy" src="${resource(dir:"images", file: "white.jpg")}" alt="" data-original="${UserDetails.findByAppUser(appUser).avatarLocationUrl?:resource(dir: 'images', file:'profile03.jpg')}" style="max-height:100%;max-width:100%"></a></div>
                <h5><a href="${createLink(controller: "timeline", action:"timelineview", params: [id:Timeline.findByArtistName(appUser?.username)?.id] )}">${appUser?.username}</a></h5>
                <p>Age Group: ${appUser?.ageGroup}</p>
                <p>Gallery: <a href="${createLink(uri: '/' )+Gallery.findByOwner(appUser?.username)?.galleryNameUrl}">${Gallery.findByOwner(appUser?.username)?.galleryName}</a></p>
            </li>
        </g:elseif>
        <g:elseif test="${model.getClass() == com.galleryofgood.gallery.YoungArtist}">
            <g:set var="youngArtist" value="${model}" />
            <li class="display-artwork">
                <div style="width: 225px; height: 245px; overflow: hidden"><a href="${createLink(controller: "timeline", action:"timelineview", params: [id:Timeline.findByArtistName(youngArtist?.nickname)?.id] )}"><img class="lazy" src="${resource(dir:"images", file: "white.jpg")}" alt="" data-original="${UserDetails.findByAppUser(AppUser.findByUsername(youngArtist.gallery.owner)).avatarLocationUrl?:resource(dir: 'images', file:'profile03.jpg')}" style="max-height:100%;max-width:100%"></a></div>
                <h5><a href="${createLink(controller: "timeline", action:"timelineview", params: [id:Timeline.findByArtistName(youngArtist?.nickname)?.id] )}">${youngArtist?.nickname}</a></h5>
                <p>Age Group: ${youngArtist?.getAgeGroup()}</p>
                <p>Gallery: <a href="${createLink(uri: '/' )+youngArtist?.gallery?.galleryNameUrl}">${youngArtist?.gallery?.galleryName}</a></p>
            </li>
        </g:elseif>
    </g:each>
</ul>
<script type="text/javascript">
    $("#count-container").text("${totalCount?:'0'}");
    $("#searchType-container").text("${searchType}");
    $("#items-listing-paged").fadeIn('slow');
    $("img.lazy").lazyload({effect:"fadeIn"});
</script>
