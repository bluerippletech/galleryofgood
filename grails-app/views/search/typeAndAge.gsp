<%@ page import="com.galleryofgood.gallery.Artwork" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='search.typeAndAge' default="Type And Age - Gallery of Good"/></title>
    <r:require modules="foundation, lazyload"/>
    <r:script>
    var criteria = [];
    var criteriaAge = [];
    function selector(selected, filterBy){
        if($(selected).closest("li").hasClass("selected")){
            $(selected).closest("li").removeClass("selected");
            if(filterBy=="type"){
                criteria.splice($.inArray($(selected).text().toString(), criteria),1);
                getItems("artworks_container", filterBy);
            }else if(filterBy=="age"){
                criteriaAge.splice($.inArray($(selected).text().toString(), criteriaAge),1);
                getItems("artworks_container", filterBy);
            }
        }else{
//            $(selected).closest("li").addClass("selected");
//            if(filterBy=="type"){
//                criteria.push($(selected).text().toString());
//                getItems("artworks_container", filterBy)
//            }else if(filterBy=="age"){
//                criteriaAge.push($(selected).text().toString());
//                getItems("artworks_container", filterBy);
//            }

            if($(selected).text().toString() == "All"){
                $(selected).closest("ul").find(".selected").removeClass("selected");
                $(selected).closest("li").addClass("selected");
                if(filterBy=="type"){
                    criteria = [];
                    criteria.push($(selected).text().toString());
                    getItems("artworks_container", filterBy)
                }else if(filterBy=="age"){
                    criteriaAge = [];
                    criteriaAge.push($(selected).text().toString());
                    getItems("artworks_container", filterBy)
                }
            }else{
                if($(selected).closest("ul").find(".selected a").text().toString() == "All"){
                    $(selected).closest("ul").find(".selected").removeClass("selected");
                    if(filterBy=="type")
                        criteria = [];
                    if(filterBy=="age")
                        criteriaAge = [];
                }
                $(selected).closest("li").addClass("selected");
                if(filterBy=="type"){
                    criteria.push($(selected).text().toString());
                    getItems("artworks_container", filterBy)
                }else if(filterBy=="age"){
                    criteriaAge.push($(selected).text().toString());
                    getItems("artworks_container", filterBy);
                }

            }
        }
    }
    function getItems(containerId, filterBy){
        var queryCriteria ="";
        $(criteria).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteria=" + this;
            else
                queryCriteria += "&criteria=" + this;
        });
        $(criteriaAge).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteriaAge=" + this;
            else
                queryCriteria += "&criteriaAge=" + this;
        });
        $.ajax({
            url:  "${createLink(controller: params.controller, action:'itemsList')}",
            type: "POST",
            data: queryCriteria,
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
               $("#"+containerId).html(results);
            }
        });
    }
    $(function(){
        selector($('#selector-type-All'), 'type');
        selector($('#selector-age-All'), 'age');
    })
    </r:script>
</head>
<body>
<g:render template="/layouts/flashMessage"/>
<!-- main content area -->
<div class="row">
    <div class="twelve columns">
        <h3 id="title_type_and_age"><span></span>Type & Age</h3>
    </div>
</div>

<!-- sort links -->
<div class="row sort_links srdd" id="secondary_navigation">
    <div class="twelve columns srdd-crit">
        <%--
        <ul id="level-1">
            <li class="selected"><a href="#">&nbsp;&nbsp;All</a></li>
            <li><a href="#">Artworks</a></li>
            <li><a href="#">Artists</a></li>
        </ul>
        <div class="clear-both"></div>
        --%>
        <ul id="level-2">
            <li><a href="javascript:" onclick="selector(this, 'type')" id="selector-type-All">All</a></li>
            <li>|</li>
            <g:each in="${com.galleryofgood.gallery.Artwork.constraints.artworkType.inList}" var="type">
                <li><a href="javascript:" onclick="selector(this, 'type')" id="selector-${type}">${type}</a></li>
            </g:each>
        </ul>
        <div class="clear-both"></div>
        <ul id="level-2">
            <li><a href="javascript:" onclick="selector(this, 'age')" id="selector-age-All">All</a></li>
            <li>|</li>
            <g:each in="${com.galleryofgood.artwork.ArtworkUtils.ageGroups}" var="group">
                <li><a href="javascript:" onclick="selector(this, 'age')" id="selector-${group.key}">${group.key}</a></li>
            </g:each>
        </ul>
    </div>
</div>
<!-- secondary navigation -->
<div id="artworks_container">

    <!--Artworks -->

</div>

<div class="row">
    <div class="twelve columns">
        &nbsp;
    </div>
</div>
<!-- main content area -->

</body>