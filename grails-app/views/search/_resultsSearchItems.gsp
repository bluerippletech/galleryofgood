<%@ page import="com.galleryofgood.gallery.Gallery; com.galleryofgood.gallery.Artwork" %>
<g:if test="${modelInstance}">
<div class="listing" id="items-container-list" style="display: none;">
    <!-- Items -->
</div>
<script type="text/javascript">
    $("#items-container-list").fadeIn('slow');
    $("img.lazy").lazyload();
    //Pagination AJAX load
    function loadByPagination(max, offset, sort, order, controller, action, containerId){
        var queryCriteria ="";
        $(criteria).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteria=" + this;
            else
                queryCriteria += "&criteria=" + this;
        });
        $(criteriaAge).each( function(i){
            if(queryCriteria == "")
                queryCriteria += "criteriaAge=" + this;
            else
                queryCriteria += "&criteriaAge=" + this;
        });
        queryCriteria += "&max="+ max + "&offset=" + offset + "&sort=" + sort + "&order=" + order;
        queryCriteria += (filterByVar=="type")?"&searchType=Artworks":(filterByVar=="age")?"&searchType=Artists":"&searchType=All"
        queryCriteria += "&searchKey=" + searchKeyVar;
        $.ajax({
            url: "${createLink(uri: '/' )}"+controller+"/"+action,
            type: "POST",
            data: queryCriteria,
            dataType: "html",
            beforeSend: function() {
            },
            success: function(results) {
                $("#"+containerId).html(results);
            }
        });
    }
</script>
%{--<div class="twelve columns">--}%
    <ripple:paginate count="${36}" total="${modelInstance?.size()}"  sort="title" order="asc" controller="${params.controller}" action="searchItemsPerPage" containerId="items-container-list"/>
%{--</div>--}%
</g:if>
<g:else>
    %{--<div class="twelve columns">--}%
        %{--<h4>Sorry, we did not find any results for <b>"${params.searchKey}"</b></h4>--}%
        %{--<p>Try again or browse the artworks by most recent, type and age or themes.</p>--}%
    %{--</div>--}%
    <script type="text/javascript">
        $("#count-container").text("0");
        <g:if test="${params.criteria=='All'}">
        $("#secondary_navigation").hide();
        if($.cookie("searchTip")!="No")
            $(".tip-box").fadeIn();
        </g:if>
    </script>
</g:else>