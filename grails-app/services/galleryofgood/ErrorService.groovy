package galleryofgood

import com.galleryofgood.common.SystemParameter
import grails.util.GrailsUtil
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class ErrorService {

    def mailService
    def springSecurityService

    def serviceMethod() {

    }

    def sendMail(anException,loginUser,uri,params){
            def message = "Login user is:${loginUser}                                         \n" +
                          "Exception Message: ${anException.message?.encodeAsHTML()}          \n" +
                          "Caused by: ${anException.cause?.message?.encodeAsHTML()}           \n" +
                          "StackTrace:${anException.stackTraceLines.each {it.encodeAsHTML()}} \n" +
                          "Url: ${uri}                                                        \n" +
                          "Parameters: ${params}                                              \n"

            def admin = ConfigurationHolder.config.support.email;
            println "Sending email to ${admin}..."
            try{
                    mailService.sendMail {
                        to admin
                        from grailsApplication.config.security.forgotPassword.emailFrom
                        subject "An error has occurred."
                        body message
                    }
                    println "Email sent..."
            }catch (Exception e){
                e.printStackTrace()
            }
    }
}
