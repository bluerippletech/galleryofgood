package com.galleryofgood.common

import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import java.awt.Image
import java.awt.Toolkit
import java.awt.image.BufferedImage
import javax.swing.ImageIcon
import java.awt.Graphics
import org.imgscalr.Scalr
import javax.imageio.ImageIO
import com.galleryofgood.amazon.AwsPropertiesCredentials
import pl.burningice.plugins.image.test.FileUploadUtils
import uk.co.desirableobjects.ajaxuploader.exception.FileUploadException
import com.galleryofgood.gallery.Artwork

@Mixin(FileUploadUtils)
class AmazonImageService {

    static transactional=false

    def grailsApplication, ajaxUploaderService, randomService


    private static AmazonS3 s3
    private static AwsPropertiesCredentials awsCreds

    private static String bucketName
    private static String amazonUrl

    Map mimeExtensionMap = [
            "png": "image/png",
            "jpg": "image/jpeg",
            "jpeg": "image/jpeg",
            "gif": "image/gif",
            "tiff": "image/tiff",
            "pdf": "application/pdf",
            "mpeg": "video/mpeg",
            "mp4": "video/mp4",
            "mov": "video/quicktime",
            "wmv": "video/x-ms-wmv",
            "html": "text/html",
            "xml": "text/xml",
            "mp3": "audio/mpeg",
            "flv": "application/octet-stream"
    ]


    def init() {
        if (!s3) {
            def secretKey = grailsApplication.config.aws.accessKey
            def accessKey = grailsApplication.config.aws.secretKey
            amazonUrl = "https://" + grailsApplication.config.aws.domain + "/"
            bucketName = grailsApplication.config.aws.bucketName
            awsCreds = new AwsPropertiesCredentials(secretKey, accessKey)
            s3 = new AmazonS3Client(awsCreds);
        }
    }

    def uploadNewFile(newFileName, aFile) {
        init()
        def aFileName = newFileName
//        File uploaded = createTemporaryFile()
        try {

            def extension = aFileName.split("[.]")[1]
            def keyAppend = "temp/" + aFileName
            def tempKey = keyAppend
            def imageTempUrl = amazonUrl + bucketName + "/" + tempKey
            def objectMetadata = new ObjectMetadata()
            objectMetadata.setContentType(mimeExtensionMap.get(extension))

            //set expiration to 1 day for temporary files.
            use([groovy.time.TimeCategory]) {
                def expirationDate = new Date()
                expirationDate = expirationDate.plus(10.minutes)
                println expirationDate
                objectMetadata.setExpirationTimeRuleId("expire1day")
                objectMetadata.setExpirationTime(expirationDate)
            }
//            ajaxUploaderService.upload(anInputStream, uploaded)

            if (!s3.doesBucketExist(bucketName)) {
                s3.createBucket(bucketName);
            }

            def putObjectRequest = new PutObjectRequest(bucketName,
                    tempKey, aFile)
            putObjectRequest.setMetadata(objectMetadata)
            s3.putObject(putObjectRequest)
            s3.setObjectAcl(bucketName, tempKey, CannedAccessControlList.PublicRead)
            return imageTempUrl
        } catch (Exception e){
                e.printStackTrace();
        }finally {
//            uploaded.delete()
        }
    }


    def uploadTemporaryFile(aFileName, anInputStream) {
        init()
        File uploaded = createTemporaryFile()
        try {

            def extension = aFileName.split("[.]")[1]
            def keyAppend = "temp/" + aFileName
            def tempKey = keyAppend
            def imageTempUrl = amazonUrl + bucketName + "/" + tempKey
            def objectMetadata = new ObjectMetadata()
            objectMetadata.setContentType(mimeExtensionMap.get(extension))

            //set expiration to 1 day for temporary files.
            use([groovy.time.TimeCategory]) {
                def expirationDate = new Date()
                expirationDate = expirationDate.plus(10.minutes)
                println expirationDate
                objectMetadata.setExpirationTimeRuleId("expire1day")
                objectMetadata.setExpirationTime(expirationDate)
            }
            ajaxUploaderService.upload(anInputStream, uploaded)

            if (!s3.doesBucketExist(bucketName)) {
                s3.createBucket(bucketName);
            }

            def putObjectRequest = new PutObjectRequest(bucketName,
                    tempKey, uploaded)
            putObjectRequest.setMetadata(objectMetadata)
            s3.putObject(putObjectRequest)
            s3.setObjectAcl(bucketName, tempKey, CannedAccessControlList.PublicRead)
            return imageTempUrl
        } finally {
            uploaded.delete()
        }
    }

    //Creates original image, thumbnail, and detail sized image urls
    def createImagesFromArtwork(String aUrl, String aUsername, Artwork anArtwork) {
        init()
        def key = aUrl.replace(amazonUrl + bucketName + "/", "")
        def keyExtension = key.tokenize(".").last()
        def s3Object = s3.getObject(bucketName, key)

        def temporaryFile = createTemporaryFile()
        try {
            transferStreamToFile(s3Object.objectContent, temporaryFile)
            createImagesFromArtwork(temporaryFile, aUsername, anArtwork, keyExtension)

        } finally {
            temporaryFile.delete()
        }
    }

    def createImagesFromTemporaryUrl(String aUrl, String aUsername, Artwork anArtwork) {
        init()
        def key = aUrl.replace(amazonUrl + bucketName + "/", "")
        def keyExtension = key.tokenize(".").last()
        def s3Object = s3.getObject(bucketName, key)

        def temporaryFile = createTemporaryFile()
        try {
            transferStreamToFile(s3Object.objectContent, temporaryFile)
            def (url,artworkDetailImageUrl,thumbnailUrl)= createImagesFromArtwork(temporaryFile, aUsername, anArtwork, keyExtension)
            anArtwork.url=url
            anArtwork.artworkDetailImageUrl=artworkDetailImageUrl
            anArtwork.thumbnailUrl=thumbnailUrl
            return anArtwork
        } finally {
            temporaryFile.delete()
        }
    }


    def createImagesFromArtwork(File aFile, String aUsername, Artwork anArtwork, String originalExtension) {

        def resizedOriginalImage = resizeForOriginal(aFile, originalExtension)
        def artworkDetailImage = resizeForDetailPage(aFile, originalExtension)
        def thumbnailImage = resizeForThumbnail(aFile, originalExtension)
        try {


            def originalKey = "userImages/" + aUsername + "/" + (randomService.nextInteger(999999999)) + "." + originalExtension
            def detailKey = "userImages/" + aUsername + "/" + (randomService.nextInteger(999999999)) + "." + originalExtension
            def thumbnailKey = "userImages/" + aUsername + "/" + (randomService.nextInteger(999999999)) + "." + originalExtension

            def objectMetadata = new ObjectMetadata()
            objectMetadata.setContentType(mimeExtensionMap.get(originalExtension))
            objectMetadata.setCacheControl("max-age=2592000,must-revalidate")

            def putObjectRequest = new PutObjectRequest(bucketName,
                    originalKey, resizedOriginalImage)
            putObjectRequest.setMetadata(objectMetadata)
            s3.putObject(putObjectRequest)
            s3.setObjectAcl(bucketName, originalKey, CannedAccessControlList.PublicRead)

            putObjectRequest = new PutObjectRequest(bucketName,
                    detailKey, artworkDetailImage)
            putObjectRequest.setMetadata(objectMetadata)
            s3.putObject(putObjectRequest)
            s3.setObjectAcl(bucketName, detailKey, CannedAccessControlList.PublicRead)

            putObjectRequest = new PutObjectRequest(bucketName,
                    thumbnailKey, thumbnailImage)
            putObjectRequest.setMetadata(objectMetadata)
            s3.putObject(putObjectRequest)
            s3.setObjectAcl(bucketName, thumbnailKey, CannedAccessControlList.PublicRead)

            def fullUrl = amazonUrl + bucketName + "/"
            originalKey = fullUrl + originalKey
            detailKey = fullUrl + detailKey
            thumbnailKey = fullUrl + thumbnailKey


            [originalKey, detailKey, thumbnailKey]
        } finally {
            resizedOriginalImage.delete()
            artworkDetailImage.delete()
            thumbnailImage.delete()

        }

    }

    def uploadImage(multiPartFile,imageName){
        uploadImage(multiPartFile,imageName,true)
    }

    def uploadImage(multiPartFile, imageName,bAllowResize) {
        if (multiPartFile?.size == 0)
            return null
        init()
        def fileName = (multiPartFile?.fileItem?.fileName)
        def extension = fileName?.tokenize(".")?.last()

        def key = "common/slider/" + imageName + "." + extension
        if (!s3.doesBucketExist(bucketName)) {
            s3.createBucket(bucketName);
        }

        def temporaryFile = createTemporaryFile()
        try {
            transferStreamToFile(multiPartFile.getInputStream(),temporaryFile)
            def resizedImage
            if (bAllowResize)
                resizedImage=resizeForOriginal(temporaryFile,extension)
            else
                resizedImage=temporaryFile
            try{
                def objectMetadata = new ObjectMetadata()
                objectMetadata.setContentType(mimeExtensionMap.getAt(extension))


                def putObjectRequest = new PutObjectRequest(bucketName,key,resizedImage)
                putObjectRequest.setMetadata(objectMetadata)
                s3.putObject(putObjectRequest)
                s3.setObjectAcl(bucketName, key, CannedAccessControlList.PublicRead)
                def url = amazonUrl + bucketName + "/" + key
                return url
            }finally{
                resizedImage.delete()
            }


        } finally {
            temporaryFile.delete()
        }




    }


    def deleteImage(anImageUrl) {
        if ("".equals(anImageUrl) || anImageUrl == null) {
            return false;

        } else {
            init()
            def key = anImageUrl.replaceFirst("https://s3.amazonaws.com/" + bucketName + "/", "")
            try {
                s3.deleteObject(bucketName, key)
            } catch (Exception e) {
                return false;
            }

            return true;
        }
    }


    def resizeImageWidth(File aFile, anImageWidth, originalExtension) {
        BufferedImage bufImg = getBufferedImageFromFile(aFile);
        BufferedImage resizedOriginal = Scalr.resize(bufImg, anImageWidth);

        def temporaryFile = createTemporaryFile()
        //TODO: determine correct file type first.
        ImageIO.write(resizedOriginal, originalExtension, temporaryFile);
        return temporaryFile
    }

    def resizeForOriginal(File aFile, String originalExtension) {
        resizeImageWidth(aFile, 920, originalExtension)
    }

    def resizeForDetailPage(File aFile, String originalExtension) {
        resizeImageWidth(aFile, 617, originalExtension)

    }

    def resizeForThumbnail(File aFile, String originalExtension) {
        resizeImageWidth(aFile, 299, originalExtension)

    }

    def getImageFromArtwork(artwork) {
        init()
        def key = artwork.url?.replaceFirst(amazonUrl + bucketName + "/", "")
        def fileName = key.split("/").last()
        def temporaryFile = createTemporaryFile()

        def file = new FileOutputStream(temporaryFile)
        def out = new BufferedOutputStream(file)
        def s3Object = (s3.getObject(bucketName, key))
        out << s3Object.objectContent
        out.close()
        def img = ImageIO.read(temporaryFile);
        return [img, temporaryFile]

    }




    def getBufferedImageFromFile(File anImageFile) {
        Image img = Toolkit.getDefaultToolkit().createImage(anImageFile.getAbsolutePath())
        img = new ImageIcon(img).getImage();

        BufferedImage bufferedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
        //Create the graphics object from the BufferedImage
        Graphics g = bufferedImage.createGraphics();

        //Draw the image on the graphics of the BufferedImage
        g.drawImage(img, 0, 0, null);

        //Dispose the Graphics
        g.dispose();

        //return the BufferedImage
        return bufferedImage;


    }


    def deleteImage(String aUrl) {
        init();
        if (!"".equals(aUrl)) {
            def key = aUrl?.replaceFirst(amazonUrl + bucketName + "/", "")
            try{
            s3.deleteObject(bucketName, key)
            } catch (Exception e){
                return false;
            }
        }
        return true;

    }


    private File createTemporaryFile() {
        File uploaded
//        if (Config.imageUpload?.containsKey('temporaryFile')) {
//            uploaded = new File("${Config.imageUpload.temporaryFile}")
//        } else {
        uploaded = File.createTempFile('grails', 'ajaxupload')
//        }
        return uploaded
    }

    void transferStreamToFile(InputStream inputStream, File file) {

        try {
            file << inputStream
        } catch (Exception e) {
            throw new FileUploadException(e)
        }

    }


}
