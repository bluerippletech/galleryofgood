package com.galleryofgood.common
import com.galleryofgood.gallery.Gallery
import com.galleryofgood.security.AppUser
import com.galleryofgood.gallery.Timeline
import com.galleryofgood.gallery.Artwork

class CommentService {

    def serviceMethod() {

    }

    def getCommentsList(String username, String search) {
        def galleryInstance = Gallery.findByOwner(username);
        if (!galleryInstance)
            return [];
        def commentsInstance =  getCommentsByDomain(galleryInstance);
        galleryInstance?.artworks?.each {
            if(it)
                commentsInstance?.addAll(getCommentsByDomain(it))
        }
        galleryInstance?.timelines?.each {
            if(it)
                commentsInstance?.addAll(getCommentsByDomain(it));
        };
        return commentsInstance;
    }

    def getMyCommentsList(String username, String search){
        def commentsInstance =  Comment.findAllByUsername(username);
        return commentsInstance;
    }

    private def getCommentsByDomain(def modelInstance){
        def commentsInstance = Comment.findAllByLocationAndLocationId(modelInstance.class==Timeline?"Artist Timeline":modelInstance.class==Artwork?"Artwork":modelInstance.class==Gallery?"Gallery":"", modelInstance.id);
        return commentsInstance;
    }

    def getAdminComments(user){
        return Comment.findAllByLocationAndLocationId('Homepage', Homepage.list().first().id);
    }
}
