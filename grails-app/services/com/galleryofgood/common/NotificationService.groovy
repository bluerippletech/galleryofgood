package com.galleryofgood.common

import com.galleryofgood.security.AppUser
import com.galleryofgood.user.UserDetails
import grails.plugin.mail.MailService

class NotificationService {

    def MailService mailService

    def serviceMethod() {

    }

    def addNotification(AppUser subscriber, String message, Date creationDate, String topicId, String topicLocation, String type){
        try{
            if (UserDetails.findByAppUser(subscriber).allowSiteNotifications == "Yes"){
                def notificationInstance = new Notification( subscriber: subscriber, message: message, creationDate: creationDate, schedule: UserDetails.findByAppUser(subscriber).admireNotification, topicId: topicId, topicLocation: topicLocation, type: type );
                if(!notificationInstance.save(flush: true)){
                    notificationInstance.errors.each{
                        println it
                    }
                    throw new RuntimeException()
                }
            }else
                throw new RuntimeException()
        }catch(Exception e){
            e.printStackTrace()
        }

    }

    def getNotifications(String schedule){
        try{
            def notificationInstance = Notification.findAllByScheduleAndProcessed(schedule, false);
            return notificationInstance;
        }catch(Exception e){
            e.printStackTrace()
        }
    }

    def sendEmail(Notification notificationInstance){
        String subjectValue = notificationInstance.type=="comment"?"Someone commented on your art!":notificationInstance.type=="follow"?"You have followers!":notificationInstance.type=="artworkApproval"?"Your artworks are now live!":notificationInstance.type=="flag"?"Your ${notificationInstance.topicLocation} has been flagged":notificationInstance.type=="flagFrom"?"Your flag has been received":"Notification"
        try{
            mailService.sendMail {
                to notificationInstance.subscriber.email;
                from grailsApplication.config.security.forgotPassword.emailFrom
                subject subjectValue
                html notificationInstance.message
            }
            notificationInstance.processed=true;
            if(!notificationInstance.save(flush: true)){
                notificationInstance.errors.each{
                    System.err.println it
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
