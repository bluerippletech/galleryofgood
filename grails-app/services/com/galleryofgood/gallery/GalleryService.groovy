package com.galleryofgood.gallery

import org.joda.time.LocalDate

class GalleryService {
    static transactional = true


    def getArtworksAgeGroups(artworks) {
        def ageGroups = []
        def ages = artworks*.artworkAge
        def zerothree = ages.findAll {0 <= it && it <= 3}
        def fiveseven = ages.findAll {4 <= it && it <= 7}
        def eightten = ages.findAll {8 <= it && it <= 10}
        def eleventhirtnee = ages.findAll {11 <= it && it <= 13}
        def fourteeneighteen = ages.findAll {14 <= it && it <= 18}

        if (zerothree) {
            ageGroups.add("0-3")

        }
        if (fiveseven) {
            ageGroups.add("4-7")

        }
        if (eightten) {
            ageGroups.add("8-10")

        }
        if (eleventhirtnee) {
            ageGroups.add("11-13")

        }
        if (fourteeneighteen) {
            ageGroups.add("14-18")

        }

        return ageGroups.join(",");


    }

    def getAgeGroups(aGallery) {
        Gallery gallery = aGallery
        def artworks = gallery.getArtworks()
        getArtworksAgeGroups(artworks)

//        0-3, 5-7, 8-10, 11-13, 14-18
    }


    def getArtistsAgeGroupsInGallery(aGallery){
        Gallery gallery = aGallery
        def currentYear = (new LocalDate()).getYear()
        if ("Myself".equals(gallery.getGalleryOwner())){
            return getAgeGroup(currentYear - Integer.parseInt(gallery?.ownerYearOfBirth));
        }else{
            Set ageGroups=[]
            def youngArtistsAges=gallery.youngArtists*.yearOfBirth;
            youngArtistsAges = youngArtistsAges.sort()
            youngArtistsAges.each{
                ageGroups.add(getAgeGroup(currentYear - Integer.parseInt(it)))
            }
            return ageGroups.join(",");
        }
    }

    def getAgeGroup(anAge){
        if (0<=anAge && anAge<=3){
            return "0-3"
        }else if (4<=anAge && anAge<=7){
            return "4-7"
        }else if (8<=anAge && anAge<=10){
            return "8-10"
        }else if (11<=anAge && anAge<=13){
            return "11-13"
        }else if (14<=anAge && anAge<=18){
            return "14-18"
        }
    }


}
