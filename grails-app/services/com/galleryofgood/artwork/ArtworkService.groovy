package com.galleryofgood.artwork

import com.galleryofgood.gallery.Artwork
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.auth.PropertiesCredentials
import com.amazonaws.services.s3.AmazonS3Client
import javax.imageio.ImageIO
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.ObjectMetadata
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImageOp
import java.awt.geom.Point2D
import com.galleryofgood.security.AppUserRole
import org.grails.taggable.TagLink
import com.galleryofgood.amazon.AwsPropertiesCredentials
import org.imgscalr.Scalr
import org.grails.taggable.Tag
import com.galleryofgood.common.SystemParameter

class ArtworkService {

    def amazonImageService,grailsApplication,mailService

    private static String bucketName
    private static String amazonUrl
    private static AmazonS3 s3
    private static AwsPropertiesCredentials awsCreds

    private static screenScaleWidth=299
   def init(){
       if (!s3) {
           def secretKey=grailsApplication.config.aws.accessKey
           def accessKey=grailsApplication.config.aws.secretKey
           amazonUrl="https://"+grailsApplication.config.aws.domain+"/"
           bucketName=grailsApplication.config.aws.bucketName
           awsCreds = new AwsPropertiesCredentials(secretKey,accessKey)
           s3 = new AmazonS3Client(awsCreds);
       }
   }

    def deleteArtworkFile(Artwork anArtwork){
        if(anArtwork){
            def gallery = anArtwork?.getGallery();

            //TODO: log successful delete.
            if (!anArtwork?.getId())
                return true;
            gallery?.removeFromArtworks(anArtwork);
            anArtwork?.delete();
            if (!gallery?.hasErrors()&&gallery?.save()){
                return true
            }else{
                return false;
            }
        }
    }

    def getCloudForTags(tags){
        def taglist
        if(tags instanceof List){
            if(tags?.size() > 0){
                taglist = TagLink.createCriteria().list() {

                    tag{
                        if(tags){
                            'in'("name",tags)
                            'eq'("approved",true)
                        }
                    }

                    projections{
                        groupProperty('tag')
                        count('tag')
                    }
                }
            }else{
                taglist = []
            }
        }else{
            taglist = TagLink.createCriteria().list() {

                tag{
                    if(tags){
                        'in'("name",tags)
                        'eq'("approved",true)
                    }
                }

                projections{
                    groupProperty('tag')
                    count('tag')
                }
            }
        }
        return taglist

    }

    def getEmergingThemesArtworks(numberOfThemes){
        def taglist = TagLink.createCriteria().list() {
            tag{
                'eq'("approved",true)
            }
//            createAlias "tag","tag"
            projections{
                count 'tag.id','tagCount'
                groupProperty 'tag'
            }
            order('tagCount','desc')
        }

        taglist=taglist.subList(0,taglist.size()>=3?3:taglist.size())
        def emergingThemes=[:]
        List artworks
        taglist.each{
            String aTagName=it[1]
            artworks = Artwork.findAllByTag(aTagName).sort{Math.random()}
            artworks = artworks.findAll{it.status=="Approved"}
            def aSublist
            if (artworks){
                aSublist=artworks?.subList(0,(artworks.size()>=3?3:artworks.size()))
            }
            emergingThemes.put(aTagName,aSublist)
        }
        return emergingThemes

    }




    def getRelevantTags(aString){
        def tagsToFind=aString?.tokenize(" ")
        if (!tagsToFind)
            return ""
        def taglist = Tag.createCriteria().list() {
                'in'("name",tagsToFind)
//            ilike("name",tagsToFind)
                'eq'("approved",true)
        }
        def tags = taglist?taglist*.name:""
        return tags
    }

    def getThemesTop(num){
        def taglist
        taglist = TagLink.createCriteria().list() {
            createAlias("tag","t")
            'eq'("t.approved",true)
            projections{
                groupProperty('tag')
                count('tag')
                property("t.dateCreated")
            }
        }
        taglist = taglist?.sort{it[2]}.reverse();
        def newList
        if(taglist)
            newList = taglist[0..(taglist?.size()>num?num:(taglist?.size()-1))];
        else
            newList = [];
        return newList
//        return (newList?.sort{Math.random()})
    }


    def replaceImageInArtwork(Artwork artwork,temporaryFile,username){
        init()
        def originalExtension = artwork?.url?.tokenize(".")?.last()

        amazonImageService.deleteImage(artwork?.url)
        amazonImageService.deleteImage(artwork?.artworkDetailImageUrl)
        amazonImageService.deleteImage(artwork?.thumbnailUrl)

        def (originalUrl,detailUrl,thumbnailUrl)=amazonImageService.createImagesFromArtwork(temporaryFile,username,artwork,originalExtension)

        artwork.url=originalUrl
        artwork.artworkDetailImageUrl=detailUrl
        artwork.thumbnailUrl=thumbnailUrl

        artwork.save()

    }

    def resizeImage(artwork, x,y,x2,y2,w,h,username){
        def originalExtension=artwork?.url?.tokenize(".").last()
        def (img,temporaryFile)= amazonImageService.getImageFromArtwork(artwork)
        try{
            def croppedImage
            def scalePercentage = (img.getWidth()/screenScaleWidth)
            def newX=(((x as Double) as Integer)*scalePercentage) as Integer
            def newY=(((y as Double) as Integer)*scalePercentage) as Integer
            def newW=(((w as Double) as Integer)*scalePercentage) as Integer
            def newH=(((h as Double) as Integer)*scalePercentage) as Integer


            croppedImage=Scalr.crop(img,newX as int,newY as int,newW as int,newH as int,null)
//        burningImageService.doWith(temporaryFile,'temp')
//                .execute {
//            it.crop(newX,newY,newW,newH)
//        }
            ImageIO.write(croppedImage,originalExtension,temporaryFile)

            //TODO: scale coordinates to original image coordinates.
            replaceImageInArtwork(artwork,temporaryFile,username);
        }finally{
            temporaryFile.delete()
        }

    }

    def rotateImage(artwork,boolean right,username){
        def (inputImage,temporaryFile)=amazonImageService.getImageFromArtwork(artwork)
        try{
            def rotatedImage

            def extension = artwork?.url?.tokenize(".").last()

            if(right){
                rotatedImage=rotateImageRight(inputImage,90)
            }else{
                rotatedImage=rotateImageLeft(inputImage,90)
            }
            ImageIO.write(rotatedImage,extension,temporaryFile)

            replaceImageInArtwork(artwork,temporaryFile,username);
        }finally{
            temporaryFile.delete()
        }

    }

    def rotateImageRight(BufferedImage img,int degrees){
        BufferedImage rotatedImage = Scalr.rotate(img,Scalr.Rotation.CW_90,null)
        return rotatedImage
    }
    def rotateImageLeft(BufferedImage img,int degrees){
        BufferedImage rotatedImage = Scalr.rotate(img,Scalr.Rotation.CW_270,null)
        return rotatedImage
    }

    def getArtworksTagCount(anArtworkList){
        def tags=[:]
        anArtworkList.findAll{it.status == "Approved"}.each {
            it.tags.each{
                def tag=tags.containsKey(it)
                if (tag)
                    tags.put(it,(tags.get(it)+1))
                else
                    tags.put(it,1)
            }
        }
        return tags
    }

    def findArtworks(gallery,artist,artworkType,tag){
        def artworks=gallery?.artworks
        if (artist)
            artworks = artworks.findAll{it.artist==artist}
        if (artworkType)
            artworks = artworks.findAll{it.artworkType==artworkType}
        if (tag)
            artworks = artworks.findAll{it.tags.contains(tag)}

        return artworks

    }

    def findApprovedArtworks(gallery,artist,artworkType,tag){
        def artworks = findArtworks(gallery,artist,artworkType,tag)
        artworks = artworks.findAll{it.status=="Approved"}
        return artworks;
    }

    def sendNewArtworkUploadedNotification(gallery,title){
        try{
            runAsync{
                mailService.sendMail {
                    to SystemParameter.findByName("AdminEmail").content
                    from grailsApplication.config.security.forgotPassword.emailFrom
                    subject "A new artwork has been uploaded for Gallery - ${gallery.galleryName}."
                    html "${title} has been successfully uploaded and waiting for your approval."
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
