package com.galleryofgood.security

import com.galleryofgood.gallery.Gallery
import com.galleryofgood.common.Notification
import com.galleryofgood.timeline.TimelineController
import com.galleryofgood.common.Comment

class AppUserService {

    def serviceMethod() {

    }

    def deleteAppUser(AppUser appUser){
        try{
            //Comment.findByUsername(appUser.username)
            Notification.findAllBySubscriber(appUser)*.delete(flush: true);
            Gallery.findAllByOwner(appUser.username)*.delete(flush: true);
            AppUserRole.removeAll(appUser);
            appUser.delete(flush: true);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
