package com.galleryofgood.notification

import org.codehaus.groovy.grails.commons.ConfigurationHolder


class WeeklyJob {

    def notificationService, mailService;
    def cronExpression = ConfigurationHolder.config.cron.expression.weekly;
   //def timeout = 5000l // execute job once in 5 seconds

    def execute() {
        def immediateNoti = notificationService.getNotifications("WEEKLY")
        immediateNoti.each{
            try{
                notificationService.sendEmail(it);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
