package com.galleryofgood.notification

import com.galleryofgood.security.AppUser
import org.codehaus.groovy.grails.commons.ConfigurationHolder


class ImmediateJob {

    def notificationService, mailService;
    def cronExpression = ConfigurationHolder.config.cron.expression.immediate
    //def timeout = 120000l // execute job once in 120 seconds

    def execute() {
        def immediateNoti = notificationService.getNotifications("IMMEDIATE")
        immediateNoti.each{
            try{
                notificationService.sendEmail(it);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
