package com.galleryofgood.notification

import org.codehaus.groovy.grails.commons.ConfigurationHolder


class DailyJob {

    def notificationService, mailService;
    def cronExpression = ConfigurationHolder.config.cron.expression.daily
    //def timeout = 5000l // execute job once in 5 seconds

    def execute() {
        def immediateNoti = notificationService.getNotifications("DAILY")
        immediateNoti.each{
            try{
                notificationService.sendEmail(it);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
