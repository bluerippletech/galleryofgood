
modules = {

    overrides {
        'jquery-theme'{
            resource id:'theme',url:'/css/smoothness/jquery-ui-1.8.18.custom.css'
        }

        'ajax-uploader'{
            resource url:'js/fileuploader.js' , disposition:'defer'
            resource url:'css/uploader.css'
        }

    }
    application {
        resource url:'js/application.js'
    }

    fileuploader {
        resource url:'js/fileuploader.js'
        resource url:'css/uploader.css'
    }

    core {
        resource url:'css/main.css'
        resource url:'css/galleryofgood.css'
        resource url:'css/app.css'
        resource url:'js/app.js'
    }


    jqueryDatatable{
        dependsOn 'jquery'
        resource url: 'js/jquery.dataTables.min.js'
        resource url: 'js/plugins/jquery.dataTables.plugins.js'
        resource url: 'css/data_table.css'
    }

    jqgrid {
        dependsOn 'jquery'
        resource url:'css/ui.jqgrid.css'
        resource url:'js/grid.locale-en.js'
//        resource url:'js/plugins/'
        resource url:'js/jquery.jqGrid.src.js'
    }

    images {
        resource url:'images/ajax-loader.gif',disposition:'inline'
        resource url:'images/logo_gog_footer.jpg', attrs:[alt:'Gallery of Good&trade;'], disposition:'inline'
        resource url:'images/logo_gog.jpg', attrs:[alt:'Gallery of Good&trade;'], disposition:'inline'
        resource url:'images/note.jpg', attrs:[alt:'Note'],disposition: 'inline'
        resource url:'images/tip.jpg', attrs:[alt:'Tip'],disposition: 'inline'

        resource url:'images/lightbox-blank.gif',disposition:'inline'
        resource url:'images/lightbox-btn-close.gif',disposition:'inline'
        resource url:'images/lightbox-btn-next.gif',disposition:'inline'
        resource url:'images/lightbox-btn-prev.gif',disposition:'inline'
        resource url:'images/lightbox-ico-loading.gif',disposition:'inline'

        resource url:'images/NoImageAvailable.png',disposition:'inline'

        resource url:'images/captcha/item-flower.png',disposition: 'inline'
        resource url:'images/captcha/item-none.png',disposition: 'inline'
        resource url:'images/captcha/item-hand.png',disposition: 'inline'
        resource url:'images/captcha/item-key.png',disposition: 'inline'
        resource url:'images/captcha/item-heart.png',disposition: 'inline'
        resource url:'images/captcha/item-sun.png',disposition: 'inline'
        resource url:'images/captcha/item-star.png',disposition: 'inline'
        resource url:'images/captcha/item-apple.png',disposition: 'inline'
        resource url:'images/captcha/item-pencil.png',disposition: 'inline'
        resource url:'images/captcha/item-scissors.png',disposition: 'inline'
        resource url:'images/captcha/wdb.png',disposition: 'inline'
        resource url:'images/captcha/border-left.png',disposition: 'inline'
        resource url:'images/captcha/border-right.png',disposition: 'inline'
        resource url:'images/captcha/captcha_bg_nobox.png',disposition:'inline'

        resource url:'images/guide_screen01.jpg',disposition:'inline'
        //temporary artworks


        resource url:'images/btn-blue.jpg',disposition:'inline'
        resource url:'images/btn-orange.jpg',disposition:'inline'
        resource url:'images/btn-blue-smaller.png',disposition:'inline'
        resource url:'images/slideNav-left.png',disposition:'inline'
        resource url:'images/slideNav-right.png',disposition:'inline'
        resource url:'images/slideNav-play-sprite.png',disposition:'inline'
        resource url:'images/ribbon-bg.png',disposition:'inline'

        resource url:'images/ico-footer-facebook.png',disposition:'inline'
        resource url:'images/ico-footer-pin.png',disposition:'inline'
        resource url:'images/ico-footer-twitter.png',disposition:'inline'
        resource url:'images/ico-close.png',disposition:'inline'

        resource url:'images/btn-blue-smaller.png',disposition:'inline'
        resource url:'images/ribbon-bg.png',disposition:'inline'
        resource url:'images/checkmark.gif',disposition:'inline'

        resource url:'images/Crop_hoover.png',disposition:'inline'
        resource url:'images/Crop_normal.png',disposition:'inline'
        resource url:'images/Rotate_left_hoover.png',disposition:'inline'
        resource url:'images/Rotate_left_normal.png',disposition:'inline'
        resource url:'images/Rotate_right_normal.png',disposition:'inline'
        resource url:'images/Rotate_right_hoover.png',disposition:'inline'


        resource url:'images/Homepage_Slide1.png',disposition:'inline'
        resource url:'images/Homepage_Slide2.png',disposition:'inline'
        resource url:'images/Homepage_Slide3.png',disposition:'inline'
        resource url:'images/Homepage_Slide4.png',disposition:'inline'
        resource url:'images/Homepage_Slide5.png',disposition:'inline'

        resource url:'images/ico-clock.png',disposition:'inline'

    }

    lazyload {
        dependsOn 'jquery'
        resource url:'js/jquery.lazyload.js',disposition:'defer'
    }

    tagcloud {
        dependsOn 'jquery'
        resource url:'js/jquery.tagcloud.js',disposition:'defer'
    }

    captcha {
        dependsOn 'jquery,jquery-ui'
        resource url:'css/captcha.css', disposition: 'head'
        resource url:'js/jquery.captcha.js', disposition: 'head'
    }

    foundationcss {
//        dependsOn 'foundation-js'
        resource url:'css/foundation.css'
        resource url:'css/app.css'
        resource url:'css/ie.css'

        resource url:'js/foundation.js'
        resource url:'js/modernizr.foundation.js'
        resource url:'js/app.js'
    }

    tagit {
        dependsOn 'jquery-ui'
        resource url:'css/jquery.tagit.css'
        resource url:'js/tag-it.js'
    }

    jcrop {
        resource url:'css/jquery.Jcrop.css'
        resource url:'js/jquery.Jcrop.js'
        resource url:'js/jquery.color.js'
        resource url:'images/Jcrop.gif',disposition:'inline'
    }

    jgrowl {
        dependsOn 'jquery'
        resource url:'js/jgrowl/jquery.jgrowl_minimized.js'
        resource url:'js/jgrowl/jquery.jgrowl.css'
    }


    lightbox {
        dependsOn 'jquery'
        resource url:'css/jquery.lightbox-0.5.css'
        resource url:'js/lightbox/jquery.lightbox-0.5.js'

    }

    colorbox {
        dependsOn 'jquery'
        resource url:'css/colorbox.css'
        resource url:'js/colorbox/jquery.colorbox.js'
    }

    customScrollbar {
        dependsOn 'jquery,jquery-ui'
        resource url:'js/scrollbar/jquery.mCustomScrollbar.js'
        resource url:'js/scrollbar/jquery.mousewheel.min.js'
        resource url:'css/scrollbar/jquery.mCustomScrollbar.css'
        resource url:'css/scrollbar/mCSB_buttons.png'
    }

    jqueryDirtyForms{
        dependsOn 'jquery'
        resource url:'js/jquery.dirtyforms.js'
    }

    lightbox2 {
        dependsOn 'jquery'
        resource url:'js/lightbox2/lightbox.js'
        resource url:'css/lightbox.css'
        resource url:'images/prev.png',disposition:'inline'
        resource url:'images/next.png',disposition:'inline'
        resource url:'images/close.png',disposition:'inline'
        resource url:'images/loading.gif',disposition:'inline'
        resource url:'images/bg-checker.png',disposition:'inline'
        resource url:'images/bullet.gif',disposition:'inline'

    }

    CLeditor{
        dependsOn 'jquery'
        resource url:'js/cleditor/jquery.cleditor.js'
        resource url:'js/cleditor/jquery.cleditor.css'
        resource url:'js/cleditor/images/buttons.gif'
        resource url:'js/cleditor/images/toolbar.gif'
    }

    scrollspy {
        dependsOn 'jquery'
        resource url:'js/jquery-scrollspy.js'
    }


    filedrop {
        dependsOn 'jquery'
        resource url:'js/plugins/jquery.filedrop.js'
        resource url:'js/plugins/script.js'

        resource url:'css/styles.css'
    }

    jqueryCookies{
        dependsOn 'jquery'
        resource url: 'js/jquery.cookie.js'
    }

    underscoreJS{
        dependsOn 'jquery'
        resource url: 'js/underscore-min.js'
    }
}

