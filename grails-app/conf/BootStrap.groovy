import org.apache.commons.codec.digest.DigestUtils
import com.galleryofgood.security.Role
import com.galleryofgood.security.AppUser
import com.galleryofgood.security.AppUserRole
import com.galleryofgood.user.UserDetails
import com.galleryofgood.common.Homepage
import com.galleryofgood.security.Requestmap

class BootStrap {


    def init = { servletContext ->

        //Bootstrap requestmaps
        Requestmap.findOrSaveWhere(url: '', configAttribute: '')
        Requestmap.findOrSaveWhere(url: '/activity/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/activity/index', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/activity/comments', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/activity/activityGuide', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/comment/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/comment/viewComment', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/flag/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/systemParameter/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/admin/**', configAttribute: 'ROLE_ADMIN,IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/ajaxUpload/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/artwork/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/artwork/artworkview/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/artwork/emergingThemes', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/artwork/landing', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/artwork/viewNext', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/artwork/viewPrevious', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/gallery/guide/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/guide', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/gallery/itemsList/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/itemsPagination/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/show/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/showGallery/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/timeline/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/gallery/timeline', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/gallery/viewComments/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY')
        Requestmap.findOrSaveWhere(url: '/appUser/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/requestmap/**', configAttribute: 'ROLE_ADMIN,IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/role/**', configAttribute: 'ROLE_ADMIN,IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/general/galleryFlag/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/general/galleryFollow/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/general/commentRedirect/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/general/artworkFlag/**', configAttribute: 'IS_AUTHENTICATED_FULLY')
        Requestmap.findOrSaveWhere(url: '/advisor/**', configAttribute: 'ROLE_ADMIN,IS_AUTHENTICATED_FULLY')




        if (com.galleryofgood.common.SystemParameter.count() == 0) {
            new com.galleryofgood.common.SystemParameter(name: "AdminEmail", content: "lester@bluerippletech.com").save(flush: true);
        }




        if (Homepage.findAll().size() == 0) {
            def homepage = new Homepage();
            homepage.save();
        }

        if (Role.findByAuthority("ROLE_ADMIN") == null) {
            def adminRole = new Role(authority: 'ROLE_ADMIN', description: 'Administrator')
            adminRole.save(flush: true)
            def userRole = new Role(authority: 'ROLE_USER', description: 'Standard')
            userRole.save(flush: true)
//        def pass = DigestUtils.shaHex("admin")
            def user = new AppUser(username: 'admin', password: "admin", enabled: true, email: 'admin@galleryofgood.com',
                    accountExpired: false, accountLocked: false, passwordExpired: false, userDetails: new UserDetails())
            user.save(flush: true)
            def appuserrole = new AppUserRole(role: adminRole, appUser: user)
            appuserrole.save(flush: true)
        }

    }
    def destroy = {
    }
}
