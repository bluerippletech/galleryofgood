// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }


grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [html: ['text/html', 'application/xhtml+xml'],
        xml: ['text/xml', 'application/xml'],
        text: 'text/plain',
        js: 'text/javascript',
        rss: 'application/rss+xml',
        atom: 'application/atom+xml',
        css: 'text/css',
        csv: 'text/csv',
        all: '*/*',
        json: ['application/json', 'text/json'],
        form: 'application/x-www-form-urlencoded',
        multipartForm: 'multipart/form-data'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart = false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// enable query caching by default
grails.hibernate.cache.queries = true

// set per-environment serverURL stem for creating absolute links
environments {
    development {
        grails.logging.jul.usebridge = true

        //cron expressions for notifications;
        cron.expression.immediate = "0 0/30 * 1/1 * ? *"
        cron.expression.daily = "0 0 18 1/1 * ? *"
        cron.expression.weekly = "0 0 12 ? * FRI *"
    }
    test {
        grails.logging.jul.usebridge = true
        //grails.serverURL = "http://testserver.bluerippletech.local:8080/GalleryOfGood"

        //cron expressions for notifications;
        cron.expression.immediate = "0 0/30 * 1/1 * ? *"
        cron.expression.daily = "0 0 18 1/1 * ? *"
        cron.expression.weekly = "0 0 12 ? * FRI *"

    }
    production {
        grails.logging.jul.usebridge = false
        grails.serverURL = "http://10.8.0.1:8080/GalleryOfGood"
        // it works

        //cron expressions for notifications;
        cron.expression.immediate = "0 0/30 * 1/1 * ? *"
        cron.expression.daily = "0 0 18 1/1 * ? *"
        cron.expression.weekly = "0 0 12 ? * FRI *"

//        grails.serverURL = "http://ec2-107-20-68-114.compute-1.amazonaws.com:8080/GalleryOfGood"
    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'

//    error stdout: "StackTrace"
//    all
//    root {
//        debug 'stdout'
//    }
//	debug  'groovy.nineci.hibernate',
//	       'org.springframework.security'

//    debug  'org.springframework.security'
}

imageUpload {
    temporaryFile = '/tmp/uploaded.file' // Path to where files will be uploaded
}
// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'com.galleryofgood.security.AppUser'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'com.galleryofgood.security.AppUserRole'
grails.plugins.springsecurity.authority.className = 'com.galleryofgood.security.Role'
grails.plugins.springsecurity.requestMap.className = 'com.galleryofgood.security.Requestmap'
grails.plugins.springsecurity.securityConfigType = 'Requestmap'
grails.plugins.springsecurity.active = true
grails.plugins.springsecurity.successHandler.defaultAdminUrl='/admin/homepage'
//grails.plugins.springsecurity.logout.handlerNames = ['logoutSuccessHandler']

grails.plugins.springsecurity.rememberMe.cookieName='grails_remember_me'
grails.plugins.springsecurity.rememberMe.alwaysRemember=true
grails.plugins.springsecurity.rememberMe.tokenValiditySeconds=1800
grails.plugins.springsecurity.rememberMe.key='galleryofgood'
grails.plugins.springsecurity.rememberMe.useSecureCookie=false


grails {
    plugin {
        audittrail {
            createdBy.field = "createdBy"
            createdBy.type = "java.lang.String" //fully qualified class name if not a java.lang.(String,Long,etc..)

            createdDate {
                field = "createdDate" //
                type = "java.util.Date" //the class name type
            }
            //Will try a joda time on this one
            editedDate.field = "editedDate"//date edited

            editedBy.field = "editedBy" //id who updated/edited
            editedBy.type = "java.lang.String" //fully qualified class name if not a java.lang.(String,Long,etc..)
            editedBy.constraints = "nullable:true"
//            editedBy.mapping = "column: 'whoUpdated'"

            companyId.field = "companyId" //used for multi-tenant apps
        }
    }
}


grails.galleryofgood.showR2=true


grails {
    mail {
        host = "smtp.gmail.com"
        port = 465
        username = "artietheartbot@galleryofgood.com"
        password = "gallery2012"
        props = ["mail.smtp.auth": "true",
                "mail.smtp.socketFactory.port": "465",
                "mail.smtp.socketFactory.class": "javax.net.ssl.SSLSocketFactory",
                "mail.smtp.socketFactory.fallback": "false"]
    }
}


emailRegistration {

    emailSubject = 'Activate your account'
    registerAbove13 {
        emailBody = '''\

Here you go, $user.username!<br/><br/>
Activate your account by clicking <a href="$url">here</a>.
Copy and paste this URL if the link does not work: <a href="$url">$url</a>
<br/>
<br/>
We’re so excited that you’ve joined our community. We can’t wait to hang your incredible
artworks in our virtual galleries. You can showcase just about anything you create, including
paintings, drawings, sculptures, photgraphy – even digital art. If you can take a photo
of it, we can make a home for it.
<br/><br/>
Please make sure all of the artworks you upload are appropriate for even our youngest
viewers. And remember, we’re a friendly space where only positive, constructive feedback
is allowed. Be sure to tell all your artistic friends to join you at Gallery of Good,
where great art and good vibes live.
<br/><br/>
Keep creating!<br/>
The Gallery of Good Team<br/>
<br/><div style="color:#666666;font-size:11px">
This email was sent to $user.email. Update your <a href="$urlNotification">email preferences</a> anytime.
<br/><br/>
© 2012 Gallery of Good, Inc. All Rights Reserved. <br/>
<a href="$urlPrivacyPolicy">Privacy Policy</a>   |   <a href="$urlToS">Terms of Service</a>   |   <a href="$urlContactUs">Contact Us</a>
</div>
'''
        emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'
        emailSubject = 'Activate your account'
        defaultRoleNames = ['ROLE_USER']
        postRegisterUrl = null // use defaultTargetUrl if not set
    }

    registerBelow13 {
        emailBody = '''\

Dear Parent,<br/>
<br/>
Your child has signed-up for an account at <a href="$urlHomepage">Gallery of Good</a>.
<br/><br/>
Weʼre a family-friendly space that champions creativity in children. Your budding artists
can safely showcase all of their fabulous creations including paintings, doodles, crafts,
and even LEGO® structures. If you can take a photo of it, we can make a home for it!
<br/><br/>
To activate your child’s account, please click <a href="$url">here</a>. Copy and Paste this URL if the link does not work: <a href="$url">$url</a>
<br/><br/>
To learn more about us, please read <a href="$urlParentGuide">Getting Started</a>.
<br/><br/>
If you have any questions, <a href="$urlContactUs">contact us anytime</a>. We’re so excited
that your family is joining our community. We can’t wait to hang their incredible artworks
in our virtual galleries!
<br/><br/>
Keep creating!<br/>
The Gallery of Good Team
<br/><br/>
P.S. If your email address was used without your consent, you may delete your child’s account by clicking <a href="$urlDeleteAccount">here</a>. Copy and paste this URL if the link does not work: <a href="$urlDeleteAccount">$urlDeleteAccount</a>
<br/><br/><div style="color:#666666;font-size:11px">
This email was sent to $user.email. Update your <a href="$urlNotification">email preferences</a> anytime.
<br/><br/>
© 2012 Gallery of Good, Inc. All Rights Reserved. <br/>
<a href="$urlPrivacyPolicy">Privacy Policy</a>   |   <a href="$urlToS">Terms of Service</a>   |   <a href="$urlContactUs">Contact Us</a>
</div>
'''
        emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'
        emailSubject = 'Activate your account'
        defaultRoleNames = ['ROLE_USER']
        postRegisterUrl = null // use defaultTargetUrl if not set
    }
    emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'

}
security {
    forgotPassword {
        emailBody = '''\
Here it is, $user.username!
<br/><br/>
Click <a href="$url">here</a> to reset your password. Copy and paste the url below if the link does not work: <a href="$url">$url</a>
<br/><br/>
Be sure to tuck it away somewhere safe!
<br/><br/>
Keep creating!<br/>
The Gallery of Good Team
<br/><br/>
P.S. If you did not request this, please <a href="$urlContactUs">contact us</a>.
<br/><br/><div style="color:#666666;font-size:11px">
This email was sent to $user.email. Update your <a href="$urlNotification">email preferences</a> anytime.
<br/><br/>
© 2012 Gallery of Good, Inc. All Rights Reserved. <br/>
<a href="$urlPrivacyPolicy">Privacy Policy</a>   |   <a href="$urlToS">Terms of Service</a>   |   <a href="$urlContactUs">Contact Us</a>
</div>
'''
        emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'
        emailSubject = 'Reset your password'
        postResetUrl = null // use defaultTargetUrl if not set
    }
    ui {
        register {
            emailBody = '''\
Hi $user.username!<br/>
<br/>
You (or someone pretending to be you) created an account with this email address.<br/>
<br/>
If you made the request, please click <a href="$url">here</a> to finish the registration.
'''
            emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'
            emailSubject = 'Activate your account'
            defaultRoleNames = ['ROLE_USER']
            postRegisterUrl = null // use defaultTargetUrl if not set
        }

        forgotPasswosrd {
            emailBody = '''\
Here it is $user.username,<br/>
<br/>
Click on this link to reset your password: <a href="$url">Reset Password</a><br/>
<br/>
Keep it safe now,<br/>
<br/>
The Gallery of Good Team<br/>
<br/>
P.S. If this email was sent to you in error, please<a href="$urlContactUs">contact us</a> to reset your password.
'''
            emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'
            emailSubject = 'Password Reset'
            postResetUrl = null // use defaultTargetUrl if not set
        }

        forgotPassword {
            emailBody = '''\
Hi $user.username,<br/>
<br/>
You (or someone pretending to be you) requested that your password be reset.<br/>
<br/>
If you didn't make this request then ignore the email; no changes have been made.<br/>
<br/>
If you did make the request, then click <a href="$url">here</a> to resetTT your password.
'''
            emailFrom = 'Gallery of Good<artietheartbot@galleryofgood.com>'
            emailSubject = 'Password Reset'
            postResetUrl = null // use defaultTargetUrl if not set
        }
    }
}

aws {
    domain = "s3.amazonaws.com"
    accessKey = "AKIAJUNGYYJE2OG23MQA"
    secretKey = "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"
    bucketName = "galleryofgood"
}

//Production AWS keys
//aws {
//    domain = "s3.amazonaws.com"
//    accessKey = "AKIAJMFH5QIGI3JSJQ4Q"
//    secretKey = "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"
//    bucketName = "gog"
//}

bitly.login = "brit2012"
bitly.apiKey = "R_d899f572815338e470560f2bd8604563"
bitly.domain = "bit.ly"

support.email = "support.gog@bluerippletech.com"

ckeditor {
    config = "/js/myckconfig.js"
    skipAllowedItemsCheck = false
    defaultFileBrowser = "ofm"
    upload {
        basedir = "/uploads/"
        overwrite = false
        link {
            browser = true
            upload = false
            allowed = []
            denied = ['html', 'htm', 'php', 'php2', 'php3', 'php4', 'php5',
                    'phtml', 'pwml', 'inc', 'asp', 'aspx', 'ascx', 'jsp',
                    'cfm', 'cfc', 'pl', 'bat', 'exe', 'com', 'dll', 'vbs', 'js', 'reg',
                    'cgi', 'htaccess', 'asis', 'sh', 'shtml', 'shtm', 'phtm']
        }
        image {
            browser = true
            upload = false
            allowed = ['jpg', 'gif', 'jpeg', 'png']
            denied = []
        }
        flash {
            browser = false
            upload = false
            allowed = ['swf']
            denied = []
        }
    }
}

reservedNames = ["contest", "contests", "sponsors", "sponsor", "partner", "partners", "winners", "advertise","advertisers", "advertising","idea","ideas", "gallery","galleryofgood","galleries",
                 "artgallery","artgalleries","art","lab","challenges","activities","books","book","school","schools","studios","inventors","inventions","investors",
                 "artist","artists","founder","corporate","company","fundraiser","fundraisers","goodacts","goodreads","goodthoughts","goodplaces","goodspaces","goodfood","goodinspiration","goodstorage","masters","masterclass","masterclassminis","spark","sparklab"]
//facebook.applicationSecret='97238f1bd2b061064fbc4a6686db4147'
//facebook.applicationId='307880795966709'


grails.plugins.springsecurity.useSecurityEventListener = true
grails.plugins.springsecurity.onInteractiveAuthenticationSuccessEvent = { e, appCtx ->
    com.galleryofgood.security.AppUser.withTransaction {
        def user = com.galleryofgood.security.AppUser.findById(appCtx.springSecurityService.principal.id)
        if(!user.isAttached())
            user.attach()
        user.lastLoginDate = new Date()
        user.save(flush: true, failOnError: true)
    }


}


