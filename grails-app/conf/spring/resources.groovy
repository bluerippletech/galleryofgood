import com.galleryofgood.security.RedirectLogoutSuccessHandler
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import com.galleryofgood.security.AdminPostAuthenticationChecks
import com.galleryofgood.security.AdminDaoAuthenticationProvider

// Place your Spring DSL code here
beans = {
    userDetailsService(com.galleryofgood.security.MyUserDetailsService)
    logoutSuccessHandler(RedirectLogoutSuccessHandler)
    postAuthenticationChecks(AdminPostAuthenticationChecks)


    authenticationProcessingFilter(com.galleryofgood.security.AdminAuthenticationFilter){
        def conf = SpringSecurityUtils.securityConfig
        authenticationManager = ref('authenticationManager')
        sessionAuthenticationStrategy = ref('sessionAuthenticationStrategy')
        authenticationSuccessHandler = ref('authenticationSuccessHandler')
        authenticationFailureHandler = ref('authenticationFailureHandler')
        rememberMeServices = ref('rememberMeServices')
        authenticationDetailsSource = ref('authenticationDetailsSource')
        filterProcessesUrl = conf.apf.filterProcessesUrl
        usernameParameter = conf.apf.usernameParameter
        passwordParameter = conf.apf.passwordParameter
        continueChainBeforeSuccessfulAuthentication = conf.apf.continueChainBeforeSuccessfulAuthentication
        allowSessionCreation = conf.apf.allowSessionCreation
        postOnly = conf.apf.postOnly

    }


    daoAuthenticationProvider(AdminDaoAuthenticationProvider) {
        def conf = SpringSecurityUtils.securityConfig

        userDetailsService = ref('userDetailsService')
        passwordEncoder = ref('passwordEncoder')
        userCache = ref('userCache')
        saltSource = ref('saltSource')
        preAuthenticationChecks = ref('preAuthenticationChecks')
        postAuthenticationChecks = ref('postAuthenticationChecks') // optional, omit if not needed
        hideUserNotFoundExceptions = conf.dao.hideUserNotFoundExceptions
    }


//    daoAuthenticationProvider( DaoAuthenticationProvider){
//        postAuthenticationChecks=new AdminPostAuthenticationChecks()
////        AdminPostAuthenticationChecks
//
//        userDetailsService= ref('userDetailsService');
//
////        springSecurityService = ref('springSecurityService');
//
//    }




}
