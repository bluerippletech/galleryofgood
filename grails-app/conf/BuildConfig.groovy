grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve

    def gebVersion = "0.7.0"
    def seleniumVersion = "2.23.1"

    repositories {
//        inherits true // Whether to inherit repository definitions from plugins
//        grailsPlugins()
//        grailsHome()
//        grailsCentral()
//        mavenCentral()
//
//        mavenCentral()
//        mavenLocal()
//        mavenRepo "http://snapshots.repository.codehaus.org"
//        mavenRepo "http://repository.codehaus.org"
//        mavenRepo "http://download.java.net/maven/2/"
//        mavenRepo "http://repository.jboss.com/maven2/"

        mavenRepo "http://10.0.1.16:8081/nexus/content/repositories/grailsPlugins/"
        mavenRepo "http://10.0.1.16:8081/nexus/content/repositories/central/"
        mavenRepo "http://10.0.1.16:8081/nexus/content/repositories/bluerippletech/"

        // For Geb snapshot
        mavenRepo "https://nexus.codehaus.org/content/repositories/snapshots"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        runtime 'mysql:mysql-connector-java:5.1.18'
        runtime 'com.amazonaws:aws-java-sdk:1.3.3'
        runtime "org.imgscalr:imgscalr-lib:4.2"

        //for testing
        test("org.seleniumhq.selenium:selenium-htmlunit-driver:$seleniumVersion") {
            exclude "xml-apis"
        }
        test("org.seleniumhq.selenium:selenium-support:$seleniumVersion")
        test("org.seleniumhq.selenium:selenium-chrome-driver:$seleniumVersion")
        test("org.seleniumhq.selenium:selenium-firefox-driver:$seleniumVersion")
        test("org.seleniumhq.selenium:selenium-ie-driver:$seleniumVersion")

        // You usually only need one of these, but this project uses both
        test "org.codehaus.geb:geb-spock:$gebVersion"
        test "org.codehaus.geb:geb-junit4:$gebVersion"
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.7.2"
        runtime ":resources:1.1.5"

        runtime ":spring-security-core:1.2.7"
        runtime ":spring-security-ui:0.2"
        runtime ":audit-trail:2.0.0"
        runtime ":quartz:0.4.2"

        runtime ":jquery-ui:1.8.15"
        runtime ":jqgrid:3.8.0.1"

        runtime ":random:0.1"

        runtime ":famfamfam:1.0.1"

        runtime ":svn:1.0.2"
        runtime ":validation-field:0.2"

        runtime ":foundation:2.1.4.3"

        runtime ":ajax-uploader:1.1"

        runtime ":mail:1.0"
        runtime ":burning-image:0.5.0"
        runtime ":taggable:1.0.2.BUILD-SNAPSHOT"

        runtime ":tagcloud:0.3"
        runtime ":bitly-shortener:0.1"

        runtime ":executor:0.3"

        runtime ":joda-time:1.4"
        compile ":export:1.3"

//        runtime ":facebook-sdk:0.2.0"
//        runtime ":facebook-graph:0.14"

        runtime ":cache-headers:1.1.5"
        runtime ":cached-resources:1.0"
        runtime ":zipped-resources:1.0"
        compile ":ckeditor:3.6.2.2"

        compile ":codenarc:0.17"

        compile ":newrelic:0.1"


        build ":tomcat:$grailsVersion"
//        test ":geb:0.7.0"
//        test ":spock:0.6"
    }
}
