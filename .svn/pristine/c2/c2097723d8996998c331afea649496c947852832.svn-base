package com.galleryofgood.admin

import com.galleryofgood.common.Homepage
import grails.converters.JSON
import com.galleryofgood.security.AppUser
import com.galleryofgood.security.Role
import com.galleryofgood.security.AppUserRole

import com.galleryofgood.user.UserDetails

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import com.galleryofgood.gallery.Artwork

class AdminController {
    def userCache,amazonImageService,springSecurityService,notificationService;


    def index() {
        render view: 'artwork'
    }

    def homepage() {
        def homepageInstance = Homepage.findAll().first();
        render view: 'homepage', model: [homepageInstance: homepageInstance]
    }

    def artwork() {
        def homepageInstance=Homepage.findAll().first();
        render view: 'artwork', model:[homepageInstance:homepageInstance]
    }

    def twoCC() {
        def homepageInstance=Homepage.findAll().first();
        render view: 'twocc', model:[homepageInstance:homepageInstance]
    }

    def updateArtworkSettings(){
        def homepageInstance = Homepage.get(params.id)
        if (!homepageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'homepage.label', default: 'Homepage'), params.id])
            redirect(action: "index")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (homepageInstance.version > version) {
                homepageInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'homepage.label', default: 'Homepage')] as Object[],
                        "Another user has updated this Homepage while you were editing")
                render(view: "/admin/artwork", model: [homepageInstance: homepageInstance])
                return
            }
        }

        homepageInstance.properties = params
        if (params.retrospectiveFile){
            homepageInstance?.retrospectiveImage=amazonImageService.uploadImage(params.retrospectiveFile,"retrospective")
        }
        homepageInstance?.retrospectiveUrl=params.retrospectiveUrl

        if (!homepageInstance.save(flush: true)) {
            render(view: "/admin/artwork", model: [homepageInstance: homepageInstance])
        }

        flash.message = "Artwork settings updated."
        redirect(controller: 'admin', action: "artwork")

    }

    def updateHomepage() {
        def homepageInstance = Homepage.get(params.id)
        if (!homepageInstance) {
            //TODO: should we create a homepage record?
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'homepage.label', default: 'Homepage'), params.id])
            redirect(action: "index")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (homepageInstance.version > version) {
                homepageInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'homepage.label', default: 'Homepage')] as Object[],
                        "Another user has updated this Homepage while you were editing")
                render(view: "/admin/homepage", model: [homepageInstance: homepageInstance])
                return
            }
        }

        homepageInstance.properties = params
        //TODO: handle file uploads to amazon s3. Use bucketname: galleryofgood, with key starting with 'common'
        if (params.sliderImage1?.getSize()>0){
            homepageInstance?.featureImageUrl1=amazonImageService.uploadImage(params.sliderImage1,"slider1")
        }
        if (params.sliderImage2?.getSize()>0){
            homepageInstance?.featureImageUrl2=amazonImageService.uploadImage(params.sliderImage2,"slider2")
        }
        if (params.sliderImage3?.getSize()>0){
            homepageInstance?.featureImageUrl3=amazonImageService.uploadImage(params.sliderImage3,"slider3")
        }
        if (params.sliderImage4?.getSize()>0){
            homepageInstance?.featureImageUrl4=amazonImageService.uploadImage(params.sliderImage4,"slider4")
        }
        if (params.sliderImage5?.getSize()>0){
            homepageInstance?.featureImageUrl5=amazonImageService.uploadImage(params.sliderImage5,"slider5")
        }


        if (params.acclaimImage1?.getSize()>0){
            homepageInstance?.setImageurl1(amazonImageService.uploadImage(params.acclaimImage1,"acclaim1"))
        }

        if (params.acclaimImage2?.getSize()>0){
            homepageInstance?.imageurl2=amazonImageService.uploadImage(params.acclaimImage2,"acclaim2")

        }
        if (params.acclaimImage3?.getSize()>0){
            homepageInstance?.imageurl3=amazonImageService.uploadImage(params.acclaimImage3,"acclaim3")
        }
        
        if (params.thumbnailImage1?.getSize()>0){
            homepageInstance?.featureImageThumbnail1=amazonImageService.uploadImage(params?.thumbnailImage1,"thumbnail1",false)
        }
        if (params.thumbnailImage2?.getSize()>0){
            homepageInstance?.featureImageThumbnail2=amazonImageService.uploadImage(params?.thumbnailImage2,"thumbnail2",false)
        }
        if (params.thumbnailImage3?.getSize()>0){
            homepageInstance?.featureImageThumbnail3=amazonImageService.uploadImage(params?.thumbnailImage3,"thumbnail3",false)
        }
        if (params.thumbnailImage4?.getSize()>0){
            homepageInstance?.featureImageThumbnail4=amazonImageService.uploadImage(params?.thumbnailImage4,"thumbnail4",false)
        }
        if (params.thumbnailImage5?.getSize()>0){
            homepageInstance?.featureImageThumbnail5=amazonImageService.uploadImage(params?.thumbnailImage5,"thumbnail5",false)
        }
        
        
        def noErrors = homepageInstance?.validate()
        if (!homepageInstance?.hasErrors()&&homepageInstance.save(flush: true)) {
            flash.message = message(code: 'default.updated.message', args: [message(code: 'homepage.label', default: 'Homepage'), homepageInstance.id])
            redirect(controller: 'admin', action: "homepage")
        }else{
            render(view: "/admin/homepage", model: [homepageInstance: homepageInstance])
        }
    }


    def listAdministrators = {
        render(view: 'adminList')
    }

    def listUsers = {
        render(view: 'userList')
    }

    def administratorListJSON =  {
        render userListJSON(true)
    }


    def regularListJSON = {
        render userListJSON(false)
    }

    def userListJSON(isAdmin) {

        def sortIndex = params.sidx ?: 'name'
        def sortOrder = params.sord ?: 'asc'
        def maxRows = Integer.valueOf(params.rows)
        def currentPage = Integer.valueOf(params.page) ?: 1
        def rowOffset = currentPage == 1 ? 0 : (currentPage - 1) * maxRows

        def adminRole = Role.findByAuthority("ROLE_ADMIN")
        def userlist = AppUserRole.createCriteria().list(max: maxRows, offset: rowOffset) {
            appUser {
                if (params.username)
                    ilike('username', "%${params.username}%")
                if (params.email)
                    ilike('email', "%${params.email}%")                                                                                                                     cdcd
                if (params.enabled)
                    eq('enabled', "Yes".equals(params.enabled))
                if (params.accountExpired)
                    eq('accountExpired', "Yes".equals(params.accountExpired))
                if (params.accountLocked)
                    eq('accountLocked', "Yes".equals(params.accountLocked))
            }
            role {
                if (isAdmin) {
                    eq('authority', 'ROLE_ADMIN')
                } else {
                    ne('authority', 'ROLE_ADMIN')
                }
            }
            projections{
                property('appUser')
                groupProperty('appUser')
            }
        }

        def totalRows = userlist.totalCount
        def numberOfPages = Math.ceil(totalRows / maxRows)

        def results = userlist?.collect {
            [
                    cell: [it?.username, it?.email, it?.enabled ? "Yes" : "No", it?.accountExpired ? "Yes" : "No", it?.accountLocked ? "Yes" : "No"],
                    id: it?.id
            ]
        }

        def jsonData = [rows: results, page: currentPage, records: totalRows, total: numberOfPages]
        render jsonData as JSON

    }

    def createAdmin = {
        AppUser user = new AppUser()
        render view: 'adminEdit', model: ['appUserInstance': user]
    }

    def save() {
        def appUserInstance = new AppUser(userDetails: new UserDetails())
        appUserInstance?.properties = params
        if (!appUserInstance.save(flush: true)) {
            render(view: "adminEdit", model: [appUserInstance: appUserInstance, activeLink: "You"])
            return
        }

        addAdminRoles(appUserInstance)
        flash.message = message(code: 'default.created.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUserInstance.id])
        redirect(action: "userEdit", id: appUserInstance.id)
    }

    def userEdit = {
        def appUserInstance = AppUser.get(params.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "listAdministrators")
            return
        }
        render view: 'adminEdit', model: [appUserInstance: appUserInstance, roleMap: buildUserModel(appUserInstance)]
    }

    def yourProfile = {
        def appUserInstance = AppUser.get(springSecurityService.getPrincipal()?.id)
        if (!appUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
            redirect(action: "listAdministrators")
            return
        }
        render view: 'adminEdit', model: [appUserInstance: appUserInstance, roleMap: buildUserModel(appUserInstance)]
    }

    def updateAdmin = {
        String passwordFieldName = SpringSecurityUtils.securityConfig.userLookup.passwordPropertyName

        def user = findById()
        if (!user) return
        if (!versionCheck('user.label', 'User', user, [user: user])) {
            return
        }

        def oldPassword = user.password
        bindData(user, params, [exclude: 'password'])
//        user.properties = params
        if (params.password && !params.password.equals(oldPassword) && !"".equals(params.password)) {
            user.password = params.password
        }

        if (!user.save(flush: true)) {
            render view: 'edit', model: buildUserModel(user)
            return
        }

        AppUserRole.removeAll user
//        addAdminRoles(user)
        addRoles(user)
        userCache.removeUserFromCache user['username']
        flash.message = "${message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])}"
        redirect action: 'userEdit', id: user.id
    }


    def newArtworks = {
        def newArtworksList = Artwork.findAllByDateCreatedGreaterThanEquals((new Date()).minus(7),[sort:"dateCreated",order:"desc"])
        render view: 'newArtwork',model:[newArtworksList:newArtworksList]
    }

    def flaggedArtworks = {
        def flaggedArtworksList = Artwork.createCriteria().list(){
            isNotEmpty('flags')
            order('dateCreated','desc')
        }
        render view: 'flaggedArtworks',model:[flaggedArtworksList:flaggedArtworksList]
    }

    def changeArtworkStatus ={
        def artworkIds=params.list('artwork_id')
        def artworks
        if (artworkIds.size()>0)
            artworks= Artwork.findAllByIdInList(artworkIds);

        if (!"".equals(params?.bulkAction)&& artworks){
        artworks*.status=params?.bulkAction

         if (artworks*.gallery*.save(flush: true)){
            if (params?.bulkAction=='Approved'){
                def grouped = artworks.groupBy({it.gallery})
                grouped.each {
                    def gallery = it.key;
                    notificationService.addNotification(AppUser.findByUsername(gallery.owner), g.render(template: '/common/artworkApprovalEmail', model: [notiOwner:gallery.owner, gallery:gallery, artworks:it.value]).toString(), new Date(), "0" ,"Admin", "artworkApproval");
                }
            }else if (params?.bulkAction=='Disapproved'){
               def grouped = artworks.findAll{it.status=='Disapproved'}.groupBy({it.gallery})
               grouped.each{
                    def gallery = it.key;
                    notificationService.addNotification(AppUser.findByUsername(gallery.owner), g.render(template: '/common/artworkDisapprovalEmail', model: [notiOwner:gallery.owner, gallery:gallery, artworks:it.value]).toString(), new Date(), "0" ,"Admin", "artworkDisapproval");
               }
            }
         }else{
             artworks.each{println(it.errors)}
         }

        }
//        println artworks
        redirect action: 'newArtworks'
    }



    protected void addRoles(user) {
        for (String key in params.keySet()) {
            if (key.contains('ROLE') && 'on' == params.get(key)) {
                AppUserRole.create user, Role.findByAuthority(key), true
            }
        }
    }

    protected void addAdminRoles(user) {
        AppUserRole.create user, Role.findByAuthority('ROLE_ADMIN'), true
    }

    protected findById() {
        def user = AppUser.get(params.id)
        if (!user) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
            redirect action: 'listAdministrators'
        }
        user
    }

    protected boolean versionCheck(String messageCode, String messageCodeDefault, instance, model) {
        if (params.version) {
            def version = params.version.toLong()
            if (instance.version > version) {
                instance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                        [message(code: messageCode, default: messageCodeDefault)] as Object[],
                        "Another user has updated this instance while you were editing")
                render view: 'edit', model: model
                return false
            }
        }
        true
    }


    protected Map buildUserModel(user) {

        String authorityFieldName = SpringSecurityUtils.securityConfig.authority.nameField
        String authoritiesPropertyName = SpringSecurityUtils.securityConfig.userLookup.authoritiesPropertyName

        List roles = sortedRoles()
        Set userRoleNames = user[authoritiesPropertyName].collect { it[authorityFieldName] }
        def granted = [:]
        def notGranted = [:]
        for (role in roles) {
            String authority = role[authorityFieldName]
            if (userRoleNames.contains(authority)) {
                granted[(role)] = userRoleNames.contains(authority)
            }
            else {
                notGranted[(role)] = userRoleNames.contains(authority)
            }
        }

        return (granted + notGranted)
    }

    protected List sortedRoles() {
        lookupRoleClass().list().sort { it.authority }
    }

    protected Class<?> lookupRoleClass() {
        return Role.class

    }
}
