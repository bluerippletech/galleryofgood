<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name='layout' content='main'/>
    <title><g:message code='parent.guide'/></title>
    <r:require modules="foundation"/>
</head>

<body>

<!-- main content area -->




<div class="row">
    <div class="twelve columns">

        <h3 id="title_genera_guide">
            <span></span>
            General Guide
        </h3>
    </div>

</div>
<div class="row">
    <div class="eight columns">
        <p class="general_guide-p">Gallery of Good, Inc. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex  commodo consequat. Duis aute irure dolor in reprehenderit in <a href="#">voluptate velit</a>.</p>
        <br /><br />
        <ul id="link-list" class="link-list">
            <li><a href="#who_can_open_gallery">Who Can Open a Gallery?</a></li>

            <li><a href="#is_free">Is There a Fee?</a></li>
            <li><a href="#how_many_artists">How Many Artists Can I Add in a Gallery?</a></li>
            <li><a href="#type_of_artwork">What Types of Artworks Can I Display in My Gallery?</a></li>
            <li><a href="#how_many_artwork">How Many Artworks Can I Add to a Gallery?</a></li>
            <li><a href="#artist_timeline">What is the Artist Timeline?</a></li>
            <li><a href="#safe_account">Is it Safe to Open an Account?</a></li>

        </ul>

        <br />
        <hr />

        <div class="pgg-anchors">
            <br />
            <a name="who_can_open_gallery">
                <h5>Who Can Open a Gallery</h5>
            </a>

            <p>Anyone can open a gallery, <b>however the artworks displayed must be created by children ages 0 -18</b>. Parents or legal guardians of children under 13 can open and manage their children's galleries. You will need to <a href="${createLink(controller:'register',action:'index')}">sign up for a free account</a> before you can set up your gallery.</p>
            <br />




            <a name="is_free">
                <h5>Is There a Fee?</h5>
            </a>

            <p>No, it's absolutely free!</p>


            <br />



            <a name="how_many_artists">
                <h5>How Many Artists Can I Add in a Gallery?</h5>
            </a>
            <p>You can add up to five artists per gallery.</p>

            <br />



            <a name="type_of_artwork">
                <h5>What Types of Artworks Can I Display in My Gallery?</h5>
            </a>
            <p>You can showcase any type of original, visual art that falls into these categories:</p>
            <ul>
                <li><b>Drawings—</b>From squiggles to simple stick figures to complex composition, comics or cartoons created with pencils, pens, charcoal, etc.</li>
                <li><b>Paintings—</b>Watercolour, oil, acrylic or tempera, it could be painted with professional brushes or spread with chubby fingers.</li>

                <li><b>Crafts—</b>Sewing projects like puppets, dolls or pillows; projects with glue and paper; greeting cards; cardboard cut-outs; costumes; macaroni art, etc.</li>
                <li><b>Mixed Media—</b>Any artwork that uses two or more mediums combined; think mosaics or collages.</li>
                <li><b>Sculpture—</b>Clay, play dough, plaster or any other moldable material for 2d relief or 3d creations.</li>
                <li><b>Photography—</b>Photos of any style and subject, as long as they follow our community guidelines.</li>
                <li><b>Digital—</b>Showcase those digital works of art created on computer, tablet or smart phone.</li>

            </ul>
            <p>Can't find a category for your artwork? Give us a shout and we'll help you out.</p>
            <p>Gallery of Good reserves the right to remove any artwork that contains offensive, voilent, or inappropriate subject matter for our young audience. Read our <a href="${createLink(controller:'general',action:'communityGuidelines')}">Community Guidelines</a> and <a href="${createLink(controller:'general',action:'termsOfService')}">Terms of Service</a> to learn more.</p>

            <br />


            <a name="how_many_artwork">

                <h5>How Many Artworks Can I Add to a Gallery?</h5>
            </a>
            <p>As many as you wish! We do have guidelines on maximum file sizes (3MB or less). Check out our Step-by-Step Gallery Guide after you <a href="${createLink(controller:'register',action:'index')}">sign-up</a>.</p>


            <br />

            <a name="artist_timeline">
                <h5>What is the Artist Timeline?</h5>

            </a>
            <p>The Artist Timeline is visual representation of your artist's progress year after year. One artwork is selected to represent a year in the timeline. Artists can complement their timeline with a text narrative on their artistic journey so far. Visit the <a href="#">Restrospective</a> feature in Artworks section to see a sample.</p>
            <br />
            <a name="safe_account">
                <h5>Is it Safe to Open an Account?</h5>
            </a>

            <p>We are committed to protecting the privacy and the safety of our members, especially our young artists. We want to ensure that all members enjoy the site in a safe environment. Here's how we're doing it: </p>
            <ul>
                <li>We never ask for real names. When you sign-up we ask you to create a unique, unidentifiable username.</li>
                <li>We never sell any member's information.</li>
                <li>We abide by the Children's Online Privacy Protection Act.</li>
                <li>We moderate all artworks before they are published.</li>

                <li>Members self-moderate comments before they are published.</li>
                <li>Our <a href="${createLink(controller:'communityGuidelines')}">Community Guidelines</a> enforce a wholesome, kid-friendly community. Members who do not abide by it are booted out.</li>
                <li>We offer community members the opportunity to flag comments or other content they feel is inappropriate. </li>
                <li>Our commitment to online safety and member privacy is detailed in our <a href="${createLink(controller:'general',action:'privacyPolicy')}">Privacy Policy</a> and <a href="${createLink(controller:'general',action:'termsOfService')}">Terms of Service</a>.</li>

            </ul>
            <br />
        </div>
    </div>











    <div class="row pgg-side">
        <div class="columns">
            <r:img width="74" height="61" alt="Note" uri="/images/note.jpg"/>
        </div>

        <div class="three columns">

            <p>A Step-by-Step gallery guide is available for registered members.</p>
            <p>Ready to open a gallery?
                <br />
                <a href="${createLink(controller:'register',action:'index')}">Sign-up for a free account.</a>
            </p>

            <p>At anytime, you can read our
                <br />
                <a href="${createLink(controller:'communityGuidelines')}">Community Guidelines.</a>

            </p>



        </div>
    </div>

</div>
<div class="row">
    <div class="twelve columns">
        <p></p>
        <br />
    </div>

</div>
<!-- main content area -->

</body>
