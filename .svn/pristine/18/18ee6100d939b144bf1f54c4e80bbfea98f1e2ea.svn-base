package com.galleryofgood.common

import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib

class FlagService {

    def mailService

    def notifyOfFlaggedGallery(aGallery,aFlag){
        def title="Gallery ${aGallery.galleryName} has been flagged. Please review details."
        def taglib = new ApplicationTagLib()
        def url= taglib.createLink(controller:aGallery.galleryNameUrl,absolute: true)
        sendFlagEmail(title,url,aFlag)
    }

    def notifyOfFlaggedArtwork(anArtwork,aFlag){
        def title="Artwork ${anArtwork.title} of Gallery ${anArtwork?.gallery?.galleryName} has been flagged. Please review details."
        def taglib = new ApplicationTagLib()
        def url= taglib.createLink(controller:'artwork',action: 'artworkview',id: anArtwork?.id,absolute: true)
        sendFlagEmail(title,url,aFlag)

    }

    def notifyOfFlaggedComment(aComment,aFlag){
        def title="Comment has been flagged as inappropriate. Please review details."
        def taglib = new ApplicationTagLib()
        def url= taglib.createLink(controller:'comment',action: 'viewComment',id: aComment?.id,absolute: true)
        sendFlagEmail(title,url,aFlag)

    }


    def sendFlagEmail(aTitle,aUrl,aFlag){
        try{
            mailService.sendMail {
                to SystemParameter.findByName("AdminEmail").content
                from "support@galleryofgood.com"
                subject "${aTitle}"
                html """Please review contents of ${aUrl} as it has been flagged by ${aFlag?.flaggingUser} as inappropriate.
                        <br/>
                        Message of the flag includes:
                        <br/><br/>\"${aFlag.comment}\""""
            }
        }catch (Exception e){
            println(e);
        }
    }

}
