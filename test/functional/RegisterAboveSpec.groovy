import pages.RegisterPage
import geb.spock.GebReportingSpec
/**
 * Created with IntelliJ IDEA.
 * User: onats
 * Date: 6/11/12
 * Time: 10:26 AM
 * To change this template use File | Settings | File Templates.
 */
class RegisterAboveSpec extends GebReportingSpec  {
    String getBaseUrl() { "http://localhost:8080/" }

    File getReportDir() { new File("target/reports/geb") }

    def "invalid inputs in registration"() {
        given: "I am at the registration form"
        to RegisterPage

        when: "I am entering invalid form values"
        at RegisterPage
        registerForm.username="onats"
        registerForm.password="1234"
        registerForm.password2="5678"
        registerForm.email="onats.ong@gmail.com"
        registerForm.email2="onats.ong@gmail.cim"


        //Drag captcha

        def captchaText= captchaIconToDrag.text()
        def captchaImage=$("#ajax-fc-task").find("img",src:contains(captchaText))
        def captchaDropLocation = $("#ajax-fc-circle")
        println captchaIcon(captchaText).@src
        interact {
            dragAndDropBy( captchaImage,300,300)
        }


        //no captcha
        createAccountButton.click()


        then: "I am back to register form with validation errors."
        at RegisterPage
        username.next().tag()=="small"
    }

}
