import geb.spock.GebReportingSpec
import pages.LoginPage
import pages.HomePage
import pages.ArtworkListPage
import pages.EditArtworkPage
import org.openqa.selenium.Keys
//import org.openqa.selenium.support.*
import spock.lang.Stepwise
/**
 * Created with IntelliJ IDEA.
 * User: lester
 * Date: 6/11/12
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
@Stepwise
class EditArtworkSpec extends GebReportingSpec {

    String getBaseUrl() { "http://localhost:8080/" }

    File getReportDir() { new File("target/reports/geb") }

    def "login artwork"() {
        given: "I am at the login page"
        to LoginPage

        when: "I am entering valid username and password"
        loginForm.j_username = "marclestertang"
        loginForm.j_password = "armark"
        loginButton.click(HomePage)

        then: "I am being redirected to the homepage"
        at HomePage
    }

    def "artwork list"(){
        given: "I am at Artwork List"
        to ArtworkListPage

        when: "I am clicking the first artwork"
        at ArtworkListPage
        link.click(EditArtworkPage)

        then: "I am redirected to edit artwork page"
        at EditArtworkPage
    }

    def "artwork edit"(){
        given: "I am at edit artwork page"
        at EditArtworkPage

        when: "I am entering artwork details"
        editForm.title()<< Keys.BACK_SPACE
        editForm.title = "Image"
        ddSelector.click()
        ddSelector.closest('div').find('li', 1) ;
        editForm.dimensions = "12x12"
        //editForm.messFactor = "Spread out the newspaper"
        editForm.description = "plants vs zombie"


        submitButton.click(EditArtworkPage)

        then: "I am redirected to edit artwork page"
        at EditArtworkPage
    }

}
