package pages

import geb.Page

/**
 * Created with IntelliJ IDEA.
 * User: lester
 * Date: 6/7/12
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */
class LoginPage extends Page{

    static url = "login/auth"

    static at = { title == "Login" }

    static content = {
        loginForm { $("form") }
        loginButton { $("input", value: "Sign-in") }
    }
}
