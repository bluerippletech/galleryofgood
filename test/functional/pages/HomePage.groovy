package pages

import geb.Page

/**
 * Created with IntelliJ IDEA.
 * User: lester
 * Date: 6/8/12
 * Time: 4:00 PM
 * To change this template use File | Settings | File Templates.
 */
class HomePage extends Page{
    static url = "/"

    static at = { title == "Gallery of Good" }

    static content = {
        logout{ $("a", text:"Log-out") }
    }
}
