package pages

import geb.Page

/**
 * Created with IntelliJ IDEA.
 * User: lester
 * Date: 6/11/12
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
class EditArtworkPage extends Page {

    static url = "artwork/editArtwork"

    static at = { title == "Edit Artwork - Gallery of Good"}

    static content = {
        editForm { $("form") }

        //dropdown element (Age)
        ddSelector { $("a.selector", 0) };



        submitButton { $("input", value: "Update") }
    }

}
