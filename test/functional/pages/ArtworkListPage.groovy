package pages

import geb.Page

/**
 * Created with IntelliJ IDEA.
 * User: lester
 * Date: 6/11/12
 * Time: 4:32 PM
 * To change this template use File | Settings | File Templates.
 */
class ArtworkListPage extends Page {

    static url = "artwork/index"

    static at = { title == "Upload Artwork - Gallery of Good"}

    static content = {
        link{ $("li#simple1Tab").find("a", 0) }
    }

}
