package pages

import geb.Page

/**
 * Created with IntelliJ IDEA.
 * User: onats
 * Date: 6/11/12
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
class RegisterPage extends Page{

    static url = "register/index"

    static at = { title == "Create your account - Gallery of Good"}

    static content = {
        registerForm {$("form")}
        username {$("form").find("input",name:"username")}

        captchaIconToDrag{$("div",class:'ajax-fc-container').find("span")}

        captchaIcon { iconName -> $("#ajax-fc-task").find("img",src:contains(iconName))}

        createAccountButton{$("input",class:"btn-blue")}




    }


}
