import geb.spock.GebReportingSpec
import pages.LoginPage
import pages.HomePage
import spock.lang.Stepwise
/**
 * Created with IntelliJ IDEA.
 * User: lester
 * Date: 6/7/12
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
@Stepwise
class LoginSpec extends GebReportingSpec {

    String getBaseUrl() { "http://localhost:8080/" }

    File getReportDir() { new File("target/reports/geb") }

    def "invalid login"() {
        given: "I am at the login page"
        to LoginPage

        when: "I am entering invalid password"
        loginForm.j_username = "admin"
        loginForm.j_password = "ioguffwf"
        loginButton.click(LoginPage)

        then: "I am being redirected to the login page, the password I entered is wrong"
        at LoginPage
    }

    def "login"() {
        given: "I am at the login page"
        to LoginPage

        when: "I am entering valid username and password"
        loginForm.j_username = "marclestertang"
        loginForm.j_password = "armark"
        loginButton.click(HomePage)

        then: "I am being redirected to the homepage"
        at HomePage
    }

    def "logout"() {
        given: "I am at the Home page"
        to HomePage

        when: "I am logging out"
        logout.click()

        then: "I am being redirected to the homepage"
        at HomePage
    }
}
